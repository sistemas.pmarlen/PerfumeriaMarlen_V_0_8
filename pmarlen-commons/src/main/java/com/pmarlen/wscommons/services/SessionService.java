package  com.pmarlen.wscommons.services;

import com.pmarlen.businesslogic.exception.AuthenticationException;
import com.pmarlen.security.OnlineSessionInfo;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author alfred
 */

@WebService
public interface SessionService {
    @WebMethod(operationName="iAmAlive")
    OnlineSessionInfo iAmAlive(
			@WebParam(name="onlineSessionInfo")OnlineSessionInfo onlineSessionInfo
			);

}

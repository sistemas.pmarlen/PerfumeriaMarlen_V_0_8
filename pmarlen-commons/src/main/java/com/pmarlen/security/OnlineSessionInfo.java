/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.security;

import java.io.Serializable;

/**
 *
 * @author alfredo
 */
public class OnlineSessionInfo implements Serializable{
	private String	usuarioId;
	private int		numCaja;
	private int		sucursalId;
	private long	lastTimeAlive;
	private boolean needSynchronize;

	public OnlineSessionInfo(String usuarioId, int numCaja, int sucursalId, long lastTimeAlive, boolean needSynchronize) {
		this.usuarioId = usuarioId;
		this.numCaja = numCaja;
		this.sucursalId = sucursalId;
		this.lastTimeAlive = lastTimeAlive;
		this.needSynchronize = needSynchronize;
	}
	
	/**
	 * @return the usuarioId
	 */
	public String getUsuarioId() {
		return usuarioId;
	}

	/**
	 * @param usuarioId the usuarioId to set
	 */
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	/**
	 * @return the numCaja
	 */
	public int getNumCaja() {
		return numCaja;
	}

	/**
	 * @param numCaja the numCaja to set
	 */
	public void setNumCaja(int numCaja) {
		this.numCaja = numCaja;
	}

	/**
	 * @return the sucursalId
	 */
	public int getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursalId the sucursalId to set
	 */
	public void setSucursalId(int sucursalId) {
		this.sucursalId = sucursalId;
	}

	/**
	 * @return the lastTimeAlive
	 */
	public long getLastTimeAlive() {
		return lastTimeAlive;
	}

	/**
	 * @param lastTimeAlive the lastTimeAlive to set
	 */
	public void setLastTimeAlive(long lastTimeAlive) {
		this.lastTimeAlive = lastTimeAlive;
	}

	/**
	 * @return the needSynchronize
	 */
	public boolean isNeedSynchronize() {
		return needSynchronize;
	}

	/**
	 * @param needSynchronize the needSynchronize to set
	 */
	public void setNeedSynchronize(boolean needSynchronize) {
		this.needSynchronize = needSynchronize;
	}
	
}

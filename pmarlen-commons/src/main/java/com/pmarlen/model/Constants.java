package com.pmarlen.model;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Constants
 */
public class Constants {

    public static final String PERFIL_ROOT = "root";
    public static final String PERFIL_ITHD_USER = "ithd_user";
    public static final String PERFIL_FINAL_USER = "final_user";
    public static final String PERFIL_ADMINISTRATOR = "administrator";
    public static final String PERFIL_ANALYST = "analyst";
    public static final String PERFIL_REGISTER = "register";
    public static final String PERFIL_GUEST = "guest";
    public static final int CREATE_ACTION = 1;
    public static final int UPDATE_ACTION = 2;
    public static final int PEDIDO_NOFISCAL = 0;
    public static final int PEDIDO_FISCAL = 1;
    
    public static final int ESTADO_CAPTURADO			= 1;
    public static final int ESTADO_SINCRONIZADO			= 2;
    public static final int ESTADO_VERIFICADO			= 4;  
    public static final int ESTADO_SURTIDO				= 8;
    public static final int ESTADO_FACTURADO			= 16;    
    public static final int ESTADO_ENVIADO				= 32;
    public static final int ESTADO_ENTREGADO			= 64;
    public static final int ESTADO_DEVUELTO				= 128;
	public static final int ESTADO_VENDIDO_SUCURSAL		= 256;
	public static final int ESTADO_DEVUELTO_SUCURSAL	= 512;
    public static final int ESTADO_CANCELADO			= 65536;
    
    public static final int CREACION = 10;
    public static final int ENTRADA_ALMACEN = 20;
    public static final int SALIDA_ALMACEN = 30;
    public static final int TIPO_MOV_MODIFICACION_COSTO_O_PRECIO = 50;
	public static final int TIPO_MOV_MODIFICACION_PRODUCTO_TODO  = 51;
	
	public static final int ALMACEN_PRINCIPAL   = 1;
    public static final int ALMACEN_OPORTUNIDAD = 2;
	
	public static final int FORMA_DE_PAGO_EFECTIVO = 1;
	public static final int FORMA_DE_PAGO_TC = 2;

    private static final String VERSION_FILE_RESOURCE = "/com/tracktopell/util/version/file/Version.properties";

    private static Logger logger = LoggerFactory.getLogger(Constants.class);
	
    public static String getServerVersion() {
        Properties pro = new Properties();
        String version = null;
        try {
            InputStream resourceAsStream = Constants.class.getResourceAsStream(VERSION_FILE_RESOURCE);
            if(resourceAsStream == null){
                throw new IOException("The resource:"+VERSION_FILE_RESOURCE+" doesn't exist!");
            }
            pro.load(resourceAsStream);
            logger.debug("->version=" + pro);
//            version = pro.getProperty("version.major") + "."
//                    + pro.getProperty("version.minor") + "@"
//                    + pro.getProperty("version.revision");
            version = pro.getProperty("version.major") + "."
                    + pro.getProperty("version.minor") + "."
                    + pro.getProperty("version.timestamp");

        } catch (IOException ex) {
            logger.error("Can't load Version properties:", ex);
            version = "-.-.-";
        }
        return version;
    }
	
	public static String getMD5Encrypted(String e) {

        MessageDigest mdEnc = null; // Encryption algorithm
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
        mdEnc.update(e.getBytes(), 0, e.length());
        return (new BigInteger(1, mdEnc.digest())).toString(16);
    }	
	
}

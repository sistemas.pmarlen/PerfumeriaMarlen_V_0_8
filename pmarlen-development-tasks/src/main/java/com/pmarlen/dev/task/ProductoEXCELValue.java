package com.pmarlen.dev.task;

public class ProductoEXCELValue {
	
    Integer proveedorId; 
	
    Integer id; 
    String  codigoBarras;
    String  descripcion;
    String  presentacion;
    Integer cont;
    String  unidadMedida;
    String  unidadesXCaja;
    Double  costo;
	Double  costoVenta;
    Double  precioVenta;
	Double  precioVentaOportunidad;
    Double  precioVentaSucursal;
	
	int     cantidadAlmacen;
	int     cantMinAlmacen;
	int     cantidadAlmacenOp;
	
	int     sucCanAlmacen;
	int     sucCanMinAlmacen;
	int     sucCanAlmacenOp;
	
    MarcaEXCELValue marca;
	ProveedorEXCELValue pev;
	String abrev;
	boolean enOferta2x1;
}

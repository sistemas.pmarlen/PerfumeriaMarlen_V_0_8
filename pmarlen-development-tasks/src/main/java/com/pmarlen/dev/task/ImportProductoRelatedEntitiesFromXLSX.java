/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.dev.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ImportProductoRelatedEntitiesFromXLSX {

	private LinkedHashMap<String, ProveedorEXCELValue> proveedorHT;
	private LinkedHashMap<String, ClienteEXCELValue> clienteHT;
	private LinkedHashMap<String, IndustriaEXCELValue> industriaHT;
	private LinkedHashMap<String, LineaEXCELValue> lineaHT;
	private LinkedHashMap<String, MarcaEXCELValue> marcaHT;
	private LinkedHashMap<Integer, ProductoEXCELValue> productoHT;
	private DecimalFormat dfClave;
	private DecimalFormat dfImporte;
	private DecimalFormat dfCodigoBarras;

	public ImportProductoRelatedEntitiesFromXLSX() {
		proveedorHT = new LinkedHashMap<String, ProveedorEXCELValue>();
		clienteHT = new LinkedHashMap<String, ClienteEXCELValue>();
		industriaHT = new LinkedHashMap<String, IndustriaEXCELValue>();
		lineaHT = new LinkedHashMap<String, LineaEXCELValue>();
		marcaHT = new LinkedHashMap<String, MarcaEXCELValue>();
		productoHT = new LinkedHashMap<Integer, ProductoEXCELValue>();

		dfClave = new DecimalFormat("0000");
		dfCodigoBarras = new DecimalFormat("#########0000");
		dfImporte = new DecimalFormat("#########0.000000");

	}

	public static void main(String[] args) {

//        args = new String[] {"/home/praxis/tracktopell/ultimos_respaldos/Archivos_importacion/Base_de_productos_ultima_definitiva.xlsx",
//                             "3",
//                             "524",                             
//                             "./out_ImportProductoRelatedEntitiesFromXLSX_"+(System.currentTimeMillis())+".sql"};

		if (args.length != 2) {
			System.err.println("Usage: file.xlsx outputFile");
			System.exit(1);
		}

		String fileName = args[0];
		String outputFile = args[1];

		ImportProductoRelatedEntitiesFromXLSX ipreX = new ImportProductoRelatedEntitiesFromXLSX();
		try {
			ipreX.importFromDataXLSX(fileName, outputFile);
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace(System.err);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace(System.err);
		}

	}

	private void importFromDataXLSX(String fileName, String outputFile)
			throws UnsupportedEncodingException,
			FileNotFoundException {

		PrintWriter pwOut = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF8"));

		//XSSFWorkbook embeddedWorkbook = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(fileName);
			Iterator<XSSFSheet> iteratorXSSFSheet = workbook.iterator();
			while (iteratorXSSFSheet.hasNext()) {
				XSSFSheet xssfSheet = iteratorXSSFSheet.next();
				System.err.println("==>>> SheetName: \\_" + xssfSheet.getSheetName() + "_/  rows: [" + xssfSheet.getFirstRowNum() + ", " + xssfSheet.getLastRowNum() + "]");
			
				Iterator<Row> rowIterator = xssfSheet.rowIterator();

				for (int rowNum = xssfSheet.getFirstRowNum() + 1; rowNum <= xssfSheet.getLastRowNum(); rowNum++) {
					Row row = xssfSheet.getRow(rowNum);
					try {
						if (row == null) {
							continue;
						}
						if (xssfSheet.getSheetName().equalsIgnoreCase("PROVEEDORES")) {
							processRowProvedores(row);
						} else if (xssfSheet.getSheetName().equalsIgnoreCase("CLIENTES")) {
							processRowClientes(row);
						} else if (xssfSheet.getSheetName().equalsIgnoreCase("PRODUCTOS-INVENTARIOS")) {
							processRowProductos(row);
						}
					} catch(Exception ex){
						//ex.printStackTrace(System.err);
						System.err.println("-->> "+ex.getMessage()+": rowNum="+rowNum);
					}

				}

			}
			System.err.println("==============>>> Gathering Poblacion for Proveedor =============");;
			PoblacionSelector poblacionProveedorSelector = null;
			Direccionable[] d = new Direccionable[proveedorHT.size()];
			Collection<ProveedorEXCELValue> prodValues = proveedorHT.values();
			int i=0;
			int numProveedoresPobNULL = 0;
			for(ProveedorEXCELValue pev: prodValues){
				d[i++]=pev;
				if(pev.poblacionId<=0){
					numProveedoresPobNULL++;
					System.err.println("=>>Proveedor:"+pev.razonSocial+" poblacionID="+pev.poblacionId);
				}
			}
			
			if(numProveedoresPobNULL>0){
				poblacionProveedorSelector = new PoblacionSelector("selector de Poblacion para Proveedores", d);

				poblacionProveedorSelector.setVisible(true);

				int t = 0;
				try {
					while (poblacionProveedorSelector.isWorking()) {
						Thread.sleep(100);
					}
				} catch (InterruptedException ie) {
				}

				if (poblacionProveedorSelector.isValidSelection()) {
					System.err.println("-->>>poblacionSelector Proveedores");
					for(Direccionable di: d){
						System.err.println("\t-->>>["+di.id+"] "+di.razonSocial+", poblacionId="+di.poblacionId);
					}
				} else {
					System.err.println("-->>>poblacionSelector invalid");
				}
			}
			
			System.err.println("==============>>> Gathering Poblacion for cliente =============");;
			PoblacionSelector poblacionClienteSelector = null;
			
			d = new Direccionable[clienteHT.size()];
			Collection<ClienteEXCELValue> cteValues = clienteHT.values();
			i=0;
			int numClientesPobNULL = 0;
			for(ClienteEXCELValue cev: cteValues){
				d[i++]=cev;
				if(cev.poblacionId<=0){
					numClientesPobNULL++;
					System.err.println("=>Cliente:"+cev.razonSocial+" poblacionID="+cev.poblacionId);
				} 
			}
			
			if(numClientesPobNULL>0){
				poblacionClienteSelector = new PoblacionSelector("selector de Poblacion para Clientes", d);
				poblacionClienteSelector.setVisible(true);

				try {
					while (poblacionClienteSelector.isWorking()) {
						//System.err.println("-->>>...waiting for poblacionSelector [" + (++t) + "]");
						Thread.sleep(100);
					}
				} catch (InterruptedException ie) {
				}

				if (poblacionClienteSelector.isValidSelection()) {
					System.err.println("-->>>poblacionSelector Clientes");
					for(Direccionable di: d){
						System.err.println("\t-->>>["+di.id+"] "+di.razonSocial+", poblacionId="+di.poblacionId);
					}
				} else {
					System.err.println("-->>>poblacionSelector invalid");
				}
			}
			pwOut.println("-- ========================== SCRIPT DE IMPORTACION =============================");
			pwOut.println("DELETE FROM MOVIMIENTO_HISTORICO_PRODUCTO;");
			pwOut.println("DELETE FROM PEDIDO_VENTA_DETALLE; ");
			pwOut.println("DELETE FROM PEDIDO_VENTA_ESTADO; ");
			pwOut.println("DELETE FROM PEDIDO_VENTA;");
			pwOut.println("DELETE FROM CLIENTE_CONTACTO;");
			pwOut.println("DELETE FROM PROVEEDOR_CONTACTO;");
			pwOut.println("DELETE FROM CONTACTO;");
			pwOut.println("DELETE FROM CLIENTE;");
			pwOut.println("DELETE FROM PROVEEDOR_PRODUCTO;");
			pwOut.println("DELETE FROM PRODUCTO_MULTIMEDIO;");
			pwOut.println("DELETE FROM PROVEEDOR;");
			pwOut.println("DELETE FROM MULTIMEDIO;");
			pwOut.println("DELETE FROM ALMACEN_PRODUCTO;");
			pwOut.println("DELETE FROM PRODUCTO;");
			pwOut.println("DELETE FROM MARCA;");
			pwOut.println("DELETE FROM LINEA;");
			pwOut.println("DELETE FROM INDUSTRIA;");
			pwOut.println("");
			pwOut.println("-- =================================================================================");
			pwOut.println("");
			
			//pwOut.println("-- ==================== PROVEEDOR: " + proveedorHT.size() + " ITEMS");
			// pwOut.println("\n");
			//int idProveedor = 1;
			//for (ProveedorEXCELValue pev : proveedorHT.values()) {
			
			//	pwOut.println("INSERT INTO PROVEEDOR(ID,RAZON_SOCIAL,RFC,TELEFONOS,TELEFONOS_MOVILES,FAXES,CALLE,NUM_EXTERIOR,NUM_INTERIOR,POBLACION_ID,EMAIL,URL) VALUES("
			//			+ pev.id + ",'" + pev.razonSocial + "','" + pev.rfc + "','" + pev.telefonos + "','" + pev.faxes + "','" + pev.faxes + "','" + pev.calle + "','" + pev.numExterior + "','" + pev.numInterior + "'," + pev.poblacionId + ",'" + pev.email + "','" + pev.url + "');");
			
			//}
			pwOut.println("-- ==================== CLIENTE: " + clienteHT.size() + " ITEMS");
			// pwOut.println("\n");
			//int idCliente = 1;
			for (ClienteEXCELValue cev : clienteHT.values()) {
				//pev.id = (idProveedor++);
				pwOut.println("INSERT INTO CLIENTE(ID,RAZON_SOCIAL,RFC,TELEFONOS,TELEFONOS_MOVILES,FAXES,CALLE,NUM_EXTERIOR,NUM_INTERIOR,POBLACION_ID,EMAIL,URL) VALUES("
						+ cev.id + ",'" + cev.razonSocial + "','" + cev.rfc + "','" + cev.telefonos + "','" + cev.faxes + "','" + cev.faxes + "','" + cev.calle + "','" + cev.numExterior + "','" + cev.numInterior + "'," + cev.poblacionId + ",'" + cev.email + "','" + cev.url + "');");
				// pwOut.println("\n");
			}
			// pwOut.println("\n");

			pwOut.println("-- ==================== INDUSTRIA: " + industriaHT.size() + " ITEMS");
			// pwOut.println("\n");
			int idIndustria = 1;
			for (IndustriaEXCELValue eev : industriaHT.values()) {
				eev.id = (idIndustria++);
				pwOut.println("INSERT INTO INDUSTRIA(ID,NOMBRE) VALUES (" + eev.id + ",'" + eev.nombre + "');");
				// pwOut.println("\n");
			}
			// pwOut.println("\n");
			pwOut.println("-- ==================== LINEA: " + lineaHT.size() + " ITEMS");
			// pwOut.println("\n");
			int idLinea = 1;
			for (LineaEXCELValue lev : lineaHT.values()) {
				lev.id = (idLinea++);
				pwOut.println("INSERT INTO LINEA(ID,NOMBRE) VALUES (" + lev.id + ", '" + lev.nombre + "');");
				// pwOut.println("\n");
			}
			// pwOut.println("\n");
			pwOut.println("-- ==================== MARCA: " + marcaHT.size() + " ITEMS");
			// pwOut.println("\n");
			int idMarca = 1;
			for (MarcaEXCELValue mev : marcaHT.values()) {
				mev.id = (idMarca++);
				pwOut.println("INSERT INTO MARCA(ID,LINEA_ID,INDUSTRIA_ID,NOMBRE) VALUES (" + mev.id + "," + mev.linea.id + "," + mev.industria.id + ", '" + mev.nombre + "');");
				// pwOut.println("\n");
			}
			// pwOut.println("\n");
			pwOut.println("-- ==================== PRODUCTO: " + productoHT.size() + " ITEMS");
			// pwOut.println("\n");
			int np = 0;
			for (ProductoEXCELValue prodev : productoHT.values()) {
				pwOut.println("-- ===========PRODUCTO ["+prodev.id+"]");
				pwOut.print("INSERT INTO PRODUCTO(ID,MARCA_ID,NOMBRE,PRESENTACION,CONTENIDO,UNIDADES_POR_CAJA,COSTO,COSTO_VENTA,PRECIO_BASE,UNIDAD_MEDIDA,CODIGO_BARRAS) VALUES");
				pwOut.println("(" + prodev.id + "," + prodev.marca.id + ",'" + prodev.descripcion + "','" + prodev.presentacion + "','" + prodev.cont + "'," + prodev.unidadesXCaja + "," + dfImporte.format(prodev.costo) + "," + dfImporte.format(prodev.costoVenta) + "," + dfImporte.format(prodev.precioVenta) + ",'" + prodev.unidadMedida + "','" + prodev.codigoBarras + "');\n");

				pwOut.print("INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO (ALMACEN_ID,PRODUCTO_ID,FECHA,TIPO_MOVIMIENTO_ID,CANTIDAD,COSTO,PRECIO,USUARIO_ID) ");
				pwOut.println("VALUES(1," + prodev.id + ",now(),30," + prodev.cantidadAlmacen + "," + dfImporte.format(prodev.costoVenta) + "," + dfImporte.format(prodev.precioVenta) + ",'root');");
				pwOut.println("INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) VALUES (1," + prodev.id + "," + prodev.cantidadAlmacen   + ",0," + dfImporte.format(prodev.precioVenta) + ");\n");
				pwOut.println("INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) VALUES (2," + prodev.id + "," + prodev.cantidadAlmacenOp + ",0," + dfImporte.format(prodev.precioVentaOportunidad) + ");\n");
				if (prodev.sucCanAlmacen > 0) {
					pwOut.println("INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) VALUES (3," + prodev.id + "," + prodev.sucCanAlmacen   + ",0," + dfImporte.format(prodev.precioVentaSucursal) + ");\n");
					pwOut.println("INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) VALUES (4," + prodev.id + "," + prodev.sucCanAlmacenOp + ",0," + dfImporte.format(prodev.precioVentaOportunidad) + ");\n");
				}
				//pwOut.println("INSERT INTO PROVEEDOR_PRODUCTO(PROVEEDOR_ID,PRODUCTO_ID,PRECIO_COMPRA) VALUES(" + prodev.proveedorId + "," + prodev.id + ", " + dfImporte.format(prodev.costoVenta) + ");\n");
			}
			pwOut.println("-- ===========");
			pwOut.println("-- ============================= END EXPORT");
			pwOut.close();
//			List<PoblacionesVO> poblacionesDeCP = getPoblacionesDeCP("55070");
//			System.err.println("==>>> poblacionesDeCP = "+poblacionesDeCP);
//			

		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}

	}

	private void processRowProductos(Row row) throws ParseException {
		int cells = row.getPhysicalNumberOfCells();
		//----------------------------------------------------------------------
		ProveedorEXCELValue pev = new ProveedorEXCELValue();

		//----------------------------------------------------------------------
		
		pev = proveedorHT.get(row.getCell(0).toString());
		if(pev==null){
			System.err.println("PROVEDOR ? "+row.getCell(0)+" ROW:"+row.getRowNum());
		}
		//----------------------------------------------------------------------
		IndustriaEXCELValue eev = new IndustriaEXCELValue();

		eev.id = null;
		eev.nombre = row.getCell(1).toString();

		if (industriaHT.get(eev.nombre) == null) {
			industriaHT.put(eev.nombre, eev);
		}
		//----------------------------------------------------------------------
		LineaEXCELValue lev = new LineaEXCELValue();

		lev.id = null;
		lev.nombre = row.getCell(2).toString();

		if (lineaHT.get(lev.nombre) == null) {
			lineaHT.put(lev.nombre, lev);
		}

		//----------------------------------------------------------------------
		MarcaEXCELValue mev = new MarcaEXCELValue();

		mev.id = null;
		mev.nombre = row.getCell(3).toString();
		mev.linea = lineaHT.get(lev.nombre);
		mev.industria = industriaHT.get(eev.nombre);

		if (marcaHT.get(mev.nombre) == null) {
			marcaHT.put(mev.nombre, mev);
		}

		//----------------------------------------------------------------------

		ProductoEXCELValue prodev = new ProductoEXCELValue();

		prodev.proveedorId = pev.id;
		prodev.id = new BigDecimal(row.getCell(4).toString()).intValue();//dfClave.parse(row.getCell(5).toString()).intValue();

		prodev.codigoBarras = row.getCell(5).toString();

		prodev.descripcion = row.getCell(6).toString().replace("'", "''");
		prodev.presentacion = row.getCell(7).toString();
		try {
			prodev.cont = new BigDecimal(row.getCell(8).toString()).intValue();
			prodev.unidadMedida = row.getCell(9).toString();
		} catch (NumberFormatException nfe) {
			prodev.cont = 1;
			prodev.unidadMedida = row.getCell(9).toString();
		}

		prodev.unidadesXCaja = row.getCell(10).toString();
		
		String[] doubleValues = new String[12];
		int cn = 0;
		for(cn=0;cn<doubleValues.length;cn++){
			if(cn == 4){
				doubleValues[cn]="1.0";
				continue;
			}
			try {
				doubleValues[cn]=row.getCell(11+cn).toString();
				if(doubleValues[cn].trim().length()==0){
					doubleValues[cn]="1.0";
				}
			} catch(NumberFormatException nfe){
				doubleValues[cn]="1.0";
			} catch(Exception e){
				//System.err.println("X -->> "+e.getMessage()+" ROW:"+row.getRowNum()+" COLUMN:"+(cn+11)+" ==REPLACE WITH 1.0");
				doubleValues[cn]="1.0";
			}
		}
		try{
			cn=0;
			prodev.costo = Double.parseDouble(doubleValues[cn++]);  //11
			prodev.costoVenta = prodev.costo; 
			prodev.precioVenta = Double.parseDouble(doubleValues[cn++]);//12
			prodev.precioVentaSucursal = Double.parseDouble(doubleValues[cn++]);//13			
			prodev.precioVentaOportunidad = Double.parseDouble(doubleValues[cn++]);	//14			
			cn++;
			prodev.cantidadAlmacen   = new Double(doubleValues[cn++]).intValue();//16
			prodev.cantMinAlmacen    = new Double(doubleValues[cn++]).intValue();//17
			prodev.cantidadAlmacenOp = new Double(doubleValues[cn++]).intValue();//18
			
			prodev.sucCanAlmacen     = new Double(doubleValues[cn++]).intValue();//19
			prodev.sucCanMinAlmacen  = new Double(doubleValues[cn++]).intValue();//20
			prodev.sucCanAlmacenOp   = new Double(doubleValues[cn++]).intValue();//21
			
		}catch(Exception e){
			System.err.println("X -->> "+e.getMessage()+" ROW:"+row.getRowNum()+" COLUMN:"+(cn+11));
		}
		prodev.marca = marcaHT.get(mev.nombre);

		productoHT.put(prodev.id, prodev);
	}

	private void processRowProvedores(Row row) throws ParseException {
		int cells = row.getPhysicalNumberOfCells();
		//----------------------------------------------------------------------
		ProveedorEXCELValue proveedor = new ProveedorEXCELValue();

		proveedor.id = proveedorHT.size() + 1;
		proveedor.razonSocial = row.getCell(0).toString();
		proveedor.rfc = row.getCell(1).toString();
		proveedor.telefonos = row.getCell(2).toString();
		proveedor.telefonosMoviles = row.getCell(3).toString();
		proveedor.faxes = row.getCell(4).toString();
		proveedor.calle = row.getCell(5).toString();
		proveedor.numInterior = row.getCell(6).toString();
		proveedor.numExterior = row.getCell(7).toString();
		proveedor.cp = String.valueOf(new BigDecimal(row.getCell(8).toString()).intValue());
		proveedor.email = row.getCell(9).toString();
		proveedor.url = row.getCell(10).toString();
		proveedor.plazoDePago = new BigDecimal(row.getCell(11).toString()).intValue();
		try{
			proveedor.poblacionId = new BigDecimal(row.getCell(12).toString()).intValue();
		}catch(Exception ex){
			System.err.println("PROVEEDORES POBLACIONID -->> "+row.getCell(12)+" -> "+ex.getMessage()+", ROW="+row.getRowNum());
		}
		//proveedor.poblacionId = -1;

		proveedorHT.put(proveedor.razonSocial, proveedor);
	}

	private void processRowClientes(Row row) throws ParseException {
		int cells = row.getPhysicalNumberOfCells();
		//----------------------------------------------------------------------
		ClienteEXCELValue cliente = new ClienteEXCELValue();


		cliente.id = clienteHT.size() + 1;
		cliente.razonSocial = row.getCell(0).toString();
		cliente.rfc = row.getCell(1).toString();
		cliente.telefonos = row.getCell(2).toString();
		cliente.telefonosMoviles = row.getCell(3).toString();
		cliente.faxes = row.getCell(4).toString();
		cliente.calle = row.getCell(5).toString();
		cliente.numInterior = row.getCell(6).toString();
		cliente.numExterior = row.getCell(7).toString();
		cliente.cp = String.valueOf(new BigDecimal(row.getCell(8).toString()).intValue());
		cliente.email = row.getCell(9).toString();
		cliente.url = row.getCell(10).toString();
		cliente.plazoDePago = new BigDecimal(row.getCell(11).toString()).intValue();
		try{
			cliente.poblacionId = new BigDecimal(row.getCell(12).toString()).intValue();
		}catch(Exception ex){
			System.err.println("CLIENTE POBLACIONID -->> "+row.getCell(12)+" -> "+ex.getMessage()+", ROW="+row.getRowNum());
		}
		//cliente.poblacionId = -1;

		clienteHT.put(cliente.razonSocial, cliente);
	}


	public static List<PoblacionesVO> getPoblacionesDeCP(String cp) {
		List<PoblacionesVO> pvList = new ArrayList<PoblacionesVO>();

		Connection connQuery = getConnection();
		try {
			PreparedStatement ps = connQuery.prepareStatement(
					"SELECT ID,CONCAT(TIPO_ASENTAMIENTO,' ',NOMBRE,', ',MUNICIPIO_O_DELEGACION,', ',ENTIDAD_FEDERATIVA,', C.P. ',CODIGO_POSTAL) "
					+ "NC FROM POBLACION WHERE CODIGO_POSTAL = ?");
			ps.setString(1, cp);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				PoblacionesVO pvo = new PoblacionesVO(rs.getInt(1), rs.getString(2));
				pvList.add(pvo);
			}
			rs.close();
			if(pvList.size()==0){
				System.err.println("--->>>getPoblacionesDeCP("+cp+") not found any Poblaciones !!");
			}
		} catch (SQLException se) {
			se.printStackTrace(System.err);
			throw new IllegalStateException("Cant execute query:");
		}

		return pvList;
	}
	private static Connection conn;

	private static Connection getConnection() {
		if (conn == null) {
			Properties propConn = null;
			String propfile = "../pmarlen-backend/src/test/resources/jdbc-test.properties";
			try {
				propConn = new Properties();

				propConn.load(new FileInputStream(propfile));
			} catch (IOException ioe) {
				ioe.printStackTrace(System.err);
				throw new IllegalStateException("Cant load properties:" + propfile);
			}

			String driverClass = propConn.getProperty("jdbc.driverClassName");
			try {

				Class.forName(driverClass);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace(System.err);
				throw new IllegalStateException("Cant load driver class:" + driverClass);
			}

			String url = propConn.getProperty("jdbc.url");
			String username = propConn.getProperty("jdbc.username");
			String password = propConn.getProperty("jdbc.password");
			try {
				conn = DriverManager.getConnection(url, username, password);
			} catch (SQLException se) {
				se.printStackTrace(System.err);
				throw new IllegalStateException("Cant open Connection:" + propConn);
			}

		}
		return conn;
	}

	private static void closeConnection() {
		if (conn != null) {
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.dev.task;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author alfredo
 */
public class PoblacionSelector extends javax.swing.JFrame {

	private JPanel jPanel1;
	private JPanel jPanel2;
	private JLabel[] direccionableNombres;
	private JComboBox[] poblacion;
	private javax.swing.JButton cancelarBtn;
	private javax.swing.JButton guardarBtn;
	private boolean validSelection;
	Direccionable[] direccionable;

	/**
	 * Creates new form PoblacionSelector
	 */
	public PoblacionSelector(String title, Direccionable[] direccionable) {
		super(title);
		this.working = true;
		this.direccionable = direccionable;
		initComponents();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		guardarBtn = new javax.swing.JButton();
		cancelarBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent we) {
				PoblacionSelector.this.working = false;
				//dispose();
			}

		});
		

		guardarBtn.setText("Guardar");
		guardarBtn.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {
				guardarBtnActionPerformed(evt);
			}
		});
		jPanel1.add(guardarBtn);

		cancelarBtn.setText("Cancelar");
		cancelarBtn.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelarBtnActionPerformed(evt);
			}
		});
		jPanel1.add(cancelarBtn);

		getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

		jPanel2.setLayout(new java.awt.GridLayout(direccionable.length, 1));

		direccionableNombres = new JLabel[direccionable.length];
		poblacion = new JComboBox[direccionable.length];
		int ni = 0;
		for (Direccionable d : direccionable) {
			JLabel jLabelDi = new JLabel(d.razonSocial+" :", JLabel.RIGHT);
			JComboBox jComboBoxDi = new JComboBox(createPoblacionCBM(d.cp));
			jLabelDi.setPreferredSize(new Dimension(200, 25));
			direccionableNombres[ni] = jLabelDi;
			poblacion[ni] = jComboBoxDi;

			JPanel jPanelRow = new JPanel(new FlowLayout(FlowLayout.LEFT, 2, 2));
			jPanelRow.add(jLabelDi);
			jPanelRow.add(jComboBoxDi);
			
			jPanel2.add(jPanelRow);
			
			ni++;
		}

		getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

		pack();

	}

	private ComboBoxModel createPoblacionCBM(String cp) {
		ComboBoxModel cbm = null;

		List<PoblacionesVO> poblacionesVOList = new ArrayList<PoblacionesVO>();
		poblacionesVOList.add(new PoblacionesVO(-1, "--SELECCIONE--"));
		poblacionesVOList.addAll(ImportProductoRelatedEntitiesFromXLSX.getPoblacionesDeCP(cp));

		PoblacionesVO[] poblacionesVOArr = new PoblacionesVO[poblacionesVOList.size()];

		poblacionesVOList.toArray(poblacionesVOArr);
		cbm = new DefaultComboBoxModel(poblacionesVOArr);

		return cbm;
	}

	private void guardarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarBtnActionPerformed
		if (validateSelection()) {
			this.working = false;
			this.dispose();			
		}
	}

	private void cancelarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarBtnActionPerformed
		this.working = false;
		this.dispose();		
	}

	public boolean validateSelection() {
		validSelection = true;
		int index = 0;
		for (JComboBox cbP : poblacion) {
			if (cbP.getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(this, "Debe selecionar una población", "Error al guardar", JOptionPane.ERROR_MESSAGE);
				cbP.requestFocus();
				validSelection = false;
				return validSelection;
			} else {
				direccionable[index].poblacionId = ((PoblacionesVO)cbP.getSelectedItem()).id;
			}
			index++;
		}

		return validSelection;
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		PoblacionSelector poblacionSelector = null;
		Direccionable[] d = new Direccionable[]{
			new Direccionable(1, "D1", "55070", -1),
			new Direccionable(2, "D2", "55068", -2),
			new Direccionable(3, "D3", "55065", -2),
			new Direccionable(4, "D4", "55075", -2),};

		poblacionSelector = new PoblacionSelector("Poblacion selector", d);

		poblacionSelector.setVisible(true);

		int t = 0;
		try {
			while (poblacionSelector.isWorking()) {
				//System.out.println("-->>>...waiting for poblacionSelector [" + (++t) + "]");
				Thread.sleep(100);
			}
		} catch (InterruptedException ie) {
		}

		if (poblacionSelector.isValidSelection()) {
			System.out.println("-->>>poblacionSelector.direccionable:");
			for(Direccionable di: d){
				System.out.println("\t-->>>["+di.id+"] "+di.razonSocial+", poblacionId="+di.poblacionId);
			}
		} else {
			System.out.println("-->>>poblacionSelector invalid");
		}
		
	}

	public boolean isValidSelection() {
		return validSelection;
	}
	private boolean working;

	public boolean isWorking() {
		return working;
	}
}

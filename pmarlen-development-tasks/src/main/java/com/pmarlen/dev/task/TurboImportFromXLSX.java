/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.dev.task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.util.Date;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TurboImportFromXLSX {

	private LinkedHashMap<String, ProveedorEXCELValue> proveedorHT;
	private LinkedHashMap<String, ClienteEXCELValue> clienteHT;
	private LinkedHashMap<String, IndustriaEXCELValue> industriaHT;
	private LinkedHashMap<String, LineaEXCELValue> lineaHT;
	private LinkedHashMap<String, MarcaEXCELValue> marcaHT;
	private LinkedHashMap<Integer, ProductoEXCELValue> productoHT;
	private DecimalFormat dfClave;
	private DecimalFormat dfImporte;
	private DecimalFormat dfCodigoBarras;

	public TurboImportFromXLSX() {
		proveedorHT = new LinkedHashMap<String, ProveedorEXCELValue>();
		clienteHT = new LinkedHashMap<String, ClienteEXCELValue>();
		industriaHT = new LinkedHashMap<String, IndustriaEXCELValue>();
		lineaHT = new LinkedHashMap<String, LineaEXCELValue>();
		marcaHT = new LinkedHashMap<String, MarcaEXCELValue>();
		productoHT = new LinkedHashMap<Integer, ProductoEXCELValue>();

		dfClave = new DecimalFormat("0000");
		dfCodigoBarras = new DecimalFormat("#########0000");
		dfImporte = new DecimalFormat("#########0.000000");

	}

	public static void main(String[] args) {

//        args = new String[] {"/home/alfredo/PerfumeriaMarlen_desktop/devtools/BaseProductos2014.xlsx",
//                             "org.apache.derby.jdbc.EmbeddedDriver",
//                             "jdbc:derby:/home/alfredo/xpressosystems/PerfumeriaMarlen_V_0_8/pmarlen-desktop-client/PMarlen_DB",
//                             "PMARLEN_PROD",
//							 "PMARLEN_PROD_PASSWORD"
//							};
		if (args.length != 5) {
			System.err.println("Usage: file.xlsx classNameDriver JDBC_URL user password");
			System.exit(1);
		}

		String fileName = args[0];
		String driver = args[1];
		String url = args[2];
		String usr = args[3];
		String pwd = args[4];

		System.err.println("==>>> Ver. 2014 ENE: fileName=" + fileName + ", url=" + url);

		TurboImportFromXLSX ipreX = new TurboImportFromXLSX();
		try {
			ipreX.importFromDataXLSX(fileName, driver, url, usr, pwd);
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace(System.err);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace(System.err);
		}

	}

	private void importFromDataXLSX(String fileName, String driver, String url, String user, String psswd)
			throws UnsupportedEncodingException,
			FileNotFoundException {
		System.err.println("==>>> Clasic IMPORT");
		//XSSFWorkbook embeddedWorkbook = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(fileName);
			Iterator<XSSFSheet> iteratorXSSFSheet = workbook.iterator();
			while (iteratorXSSFSheet.hasNext()) {
				XSSFSheet xssfSheet = iteratorXSSFSheet.next();
				
				System.err.println("==>>> SheetName: \\_" + xssfSheet.getSheetName() + "_/  rows: [" + xssfSheet.getFirstRowNum() + ", " + xssfSheet.getLastRowNum() + "]");
				if(!xssfSheet.getSheetName().equalsIgnoreCase("PRODUCTOS-INVENTARIOS")) {
					continue;
				}
				Iterator<Row> rowIterator = xssfSheet.rowIterator();

				for (int rowNum = xssfSheet.getFirstRowNum() + 2; rowNum <= xssfSheet.getLastRowNum(); rowNum++) {
					Row row = xssfSheet.getRow(rowNum);					
					try {
						if (row == null) {
							continue;
						} else if (row.getCell(getExcellRow("F"))!=null && row.getCell(getExcellRow("F")).getStringCellValue().trim().length()>2 && row.getCell(getExcellRow("F")).getStringCellValue().matches("[#][0-9]{3}[0-9]+")) {
							processRowProductos(row);
						}
					} catch (Exception ex) {
						ex.printStackTrace(System.err);
						//System.err.println("-->> "+ex.toString()+": rowNum="+rowNum);
						break;
					}

				}

			}

			Connection connQuery = getConnection(driver, url, user, psswd);

			Object resultInsert = null;
			int t = productoHT.size();
			int r = 0;
			int p = (r * 100) / t;
			System.out.println("========>> REVIEW");
			System.out.println();
			for (ProductoEXCELValue productoEV : productoHT.values()) {
				//System.out.println("\t "+productoEV.pev+"|"+productoEV.marca.nombre+"|"+productoEV.marca.linea.nombre+"|"+productoEV.marca.industria.nombre+"|"+productoEV.id);
			}
			//System.exit(0);
			System.out.println("=======>> DB");
			System.out.println();
			for (ProductoEXCELValue productoEV : productoHT.values()) {
				/*
				 Object provedorId = searchFor(connQuery,"SELECT ID FROM PROVEEDOR WHERE RAZON_SOCIAL LIKE ?", productoEV.pev.razonSocial);
				 if(provedorId != null){
				 productoEV.pev.id = new Integer(provedorId.toString());
				 } else {
				 Object proveedorNextId = searchFor(connQuery,"SELECT MAX(ID)+1 FROM PROVEEDOR");
				
				 resultInsert  = insertFor(connQuery, 
				 "INSERT INTO PROVEEDOR"
				 + "(ID,RAZON_SOCIAL,RFC,TELEFONOS,TELEFONOS_MOVILES,FAXES,"
				 + " CALLE,NUM_EXTERIOR,NUM_INTERIOR,POBLACION_ID,EMAIL,"
				 + "URL,FECHA_CREACION) "+
				 "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)", 
				 new Object[]{
				 proveedorNextId,
				 productoEV.pev.razonSocial,productoEV.pev.rfc,productoEV.pev.telefonos,productoEV.pev.telefonosMoviles,productoEV.pev.faxes,
				 productoEV.pev.calle,productoEV.pev.numExterior,productoEV.pev.numInterior,productoEV.pev.poblacionId,productoEV.pev.email,
				 productoEV.pev.url,new Date()
				 });
				 productoEV.pev.id = new Integer(resultInsert.toString());
				 }
				 */

				Object lineaId = searchFor(connQuery, "SELECT ID FROM LINEA WHERE NOMBRE LIKE ?", productoEV.marca.linea.nombre);
				if (lineaId != null) {
					productoEV.marca.linea.id = new Integer(lineaId.toString());
				} else {
					Object lineaNextId = searchFor(connQuery, "SELECT MAX(ID)+1 FROM LINEA");
					if(lineaNextId==null){
						lineaNextId=new Integer(1);
					}
					resultInsert = insertFor(connQuery,
							"INSERT INTO LINEA(ID,NOMBRE) VALUES(?,?)",
							new Object[]{lineaNextId, productoEV.marca.linea.nombre});
					productoEV.marca.linea.id = new Integer(resultInsert.toString());
				}

				Object industriaId = searchFor(connQuery, "SELECT ID FROM INDUSTRIA WHERE NOMBRE LIKE ?", productoEV.marca.industria.nombre);
				if (industriaId != null) {
					productoEV.marca.industria.id = new Integer(industriaId.toString());
				} else {
					Object industriaNextId = searchFor(connQuery, "SELECT MAX(ID)+1 FROM INDUSTRIA");
					if(industriaNextId==null){
						industriaNextId=new Integer(1);
					}
					resultInsert = insertFor(connQuery,
							"INSERT INTO INDUSTRIA(ID,NOMBRE) VALUES(?,?)",
							new Object[]{industriaNextId, productoEV.marca.industria.nombre});
					productoEV.marca.industria.id = new Integer(resultInsert.toString());
				}

				Object marcaId = searchFor(connQuery, "SELECT ID FROM MARCA WHERE NOMBRE LIKE ?", productoEV.marca.nombre);
				if (marcaId != null) {
					productoEV.marca.id = new Integer(marcaId.toString());
				} else {
					Object marcaNextId = searchFor(connQuery, "SELECT MAX(ID)+1 FROM MARCA");
					if(marcaNextId==null){
						marcaNextId=new Integer(1);
					}
					resultInsert = insertFor(connQuery,
							"INSERT INTO MARCA(ID,LINEA_ID,INDUSTRIA_ID,NOMBRE) VALUES(?,?,?,?)",
							new Object[]{marcaNextId, productoEV.marca.linea.id, productoEV.marca.industria.id, productoEV.marca.nombre});
					productoEV.marca.id = new Integer(resultInsert.toString());
				}

				Object productoId = searchFor(connQuery, "SELECT ID FROM PRODUCTO WHERE CODIGO_BARRAS = ?", productoEV.codigoBarras);

				if (productoId != null) {
					System.out.println("=======>> UPDATE BY CODIGO_BARRAS, PRODUCTO_ID=" + productoId + ", CODIGO_BARRAS=" + productoEV.codigoBarras + " >>");
					resultInsert = insertFor(connQuery,
							"UPDATE PRODUCTO SET "
							+ "MARCA_ID=?,NOMBRE=?,PRESENTACION=?,CONTENIDO=?,UNIDADES_POR_CAJA=?,"
							+ "COSTO=?,COSTO_VENTA=?,PRECIO_BASE=?,UNIDAD_MEDIDA=? WHERE CODIGO_BARRAS=?",
							new Object[]{productoEV.marca.id, productoEV.descripcion, productoEV.presentacion, productoEV.cont,
								new BigDecimal(productoEV.unidadesXCaja).intValue(), productoEV.costo, productoEV.costoVenta, productoEV.precioVenta,
								productoEV.unidadMedida, productoEV.codigoBarras});

					System.out.println("\t=======>> PRODUCTO.ID=" + resultInsert + " UPDATED, BUT OK?");

					productoEV.id = new Integer(productoId.toString());
					/*
					 resultInsert  = insertFor(connQuery, 
					 "INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO (ALMACEN_ID,PRODUCTO_ID,FECHA,TIPO_MOVIMIENTO_ID,CANTIDAD,COSTO,PRECIO,USUARIO_ID) "+
					 "VALUES(3,?,?,51,?,?,?,'root')", 
					 new Object[]{productoEV.id,new Date(),productoEV.cantidadAlmacen,productoEV.costo,productoEV.precioVenta});
					 if(resultInsert == null){
					 throw new IllegalStateException("Error insert.");
					 }

					 resultInsert  = insertFor(connQuery, 
					 "INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO (ALMACEN_ID,PRODUCTO_ID,FECHA,TIPO_MOVIMIENTO_ID,CANTIDAD,COSTO,PRECIO,USUARIO_ID) "+
					 "VALUES(4,?,?,51,?,?,?,'root')", 
					 new Object[]{productoEV.id,new Date(),productoEV.cantidadAlmacen,productoEV.costo,productoEV.precioVenta});
					 if(resultInsert == null){
					 throw new IllegalStateException("Error insert.");
					 }
					 */
					resultInsert = insertFor(connQuery,
							"UPDATE ALMACEN_PRODUCTO SET PRECIO_VENTA=?,CANTIDAD_ACTUAL=? WHERE PRODUCTO_ID=? AND ALMACEN_ID=3 ",
							new Object[]{productoEV.precioVentaSucursal, productoEV.sucCanAlmacen, productoEV.id});
					if (resultInsert == null) {
						throw new IllegalStateException("Error insert.");
					}
					if (productoEV.enOferta2x1) {
						resultInsert = searchFor(connQuery,
								"SELECT ID FROM OFERTA_2X1 WHERE PRODUCTO_ID=? AND ALMACEN_ID=5 ",
								new Object[]{productoEV.id});
						if (resultInsert == null) {
							resultInsert = insertFor(connQuery,
									"INSERT INTO OFERTA_2X1(PRODUCTO_ID,ALMACEN_ID,USUARIO_ID) VALUES(?,5,'root')",
									new Object[]{productoEV.id});
						}
					} else {
						resultInsert = insertFor(connQuery,
								"DELETE FROM OFERTA_2X1 WHERE PRODUCTO_ID=? AND ALMACEN_ID=5 ",
								new Object[]{productoEV.id});
					}
					/* para oportunidad */
					 resultInsert  = insertFor(connQuery, 
					 "UPDATE ALMACEN_PRODUCTO SET PRECIO_VENTA=?,CANTIDAD_ACTUAL=? WHERE PRODUCTO_ID=? AND ALMACEN_ID=4 ", 
					 new Object[]{productoEV.precioVentaOportunidad,productoEV.sucCanAlmacenOp,productoEV.id});
					 if(resultInsert == null){
						throw new IllegalStateException("Error insert.");
					 }
					 
				} else {

					Object newProductoId = searchFor(connQuery, "SELECT MAX(ID)+1 AS NEXT_PRODUCTO_ID FROM PRODUCTO", null);

					productoId = newProductoId;
					if(productoId == null){
						productoId = new Integer(1);
					}

					System.out.println("=======>> INSERT Producto.id=" + productoEV.id + ", productoId new ID=" + productoId + " ? >>");
					resultInsert = insertFor(connQuery,
							"INSERT INTO PRODUCTO("
							+ "ID,MARCA_ID,NOMBRE,PRESENTACION,CONTENIDO,UNIDADES_POR_CAJA,"
							+ "COSTO,COSTO_VENTA,PRECIO_BASE,UNIDAD_MEDIDA,CODIGO_BARRAS) "
							+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)",
							new Object[]{(Integer) productoId, productoEV.marca.id, productoEV.descripcion, productoEV.presentacion, productoEV.cont,
								new BigDecimal(productoEV.unidadesXCaja).intValue(), productoEV.costo, productoEV.costoVenta, productoEV.precioVenta,
								productoEV.unidadMedida, productoEV.codigoBarras});

					System.out.println("\t=======>> PRODUCTO.ID=" + resultInsert + " INSERTED, BUT OK?");

					productoEV.id = new Integer(resultInsert.toString());
					/*
					 resultInsert  = insertFor(connQuery, 
					 "INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO (ALMACEN_ID,PRODUCTO_ID,FECHA,TIPO_MOVIMIENTO_ID,CANTIDAD,COSTO,PRECIO,USUARIO_ID) "+
					 "VALUES(3,?,?,21,?,?,?,'root')", 
					 new Object[]{productoEV.id,new Date(),productoEV.cantidadAlmacen,productoEV.costo,productoEV.precioVenta});
					 if(resultInsert == null){
					 throw new IllegalStateException("Error insert.");
					 }

					 resultInsert  = insertFor(connQuery, 
					 "INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO (ALMACEN_ID,PRODUCTO_ID,FECHA,TIPO_MOVIMIENTO_ID,CANTIDAD,COSTO,PRECIO,USUARIO_ID) "+
					 "VALUES(4,?,?,20,?,?,?,'root')", 
					 new Object[]{productoEV.id,new Date(),productoEV.cantidadAlmacen,productoEV.costo,productoEV.precioVenta});
					 if(resultInsert == null){
					 throw new IllegalStateException("Error insert.");
					 }
					 */

					resultInsert = insertFor(connQuery,
							"INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) "
							+ "VALUES(3,?,?,?,?)",
							new Object[]{productoEV.id, productoEV.sucCanAlmacen, productoEV.cantMinAlmacen, productoEV.precioVentaSucursal});
					if (resultInsert == null) {
						throw new IllegalStateException("Error insert.");
					}
					if (productoEV.enOferta2x1) {
						resultInsert = insertFor(connQuery,
								"INSERT INTO OFERTA_2X1(PRODUCTO_ID,ALMACEN_ID,USUARIO_ID) VALUES(?,5,'root')",
								new Object[]{productoEV.id});
						
					} else {
						resultInsert = insertFor(connQuery,
								"DELETE FROM OFERTA_2X1 WHERE PRODUCTO_ID=? AND ALMACEN_ID=5 ",
								new Object[]{productoEV.id});
					}
					
					/* oportunidad */
					 resultInsert  = insertFor(connQuery, 
					 "INSERT INTO ALMACEN_PRODUCTO(ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,CANTIDAD_MINIMA,PRECIO_VENTA) "+
					 "VALUES(4,?,?,?,?)", 
					 new Object[]{productoEV.id,productoEV.sucCanAlmacenOp,productoEV.cantMinAlmacen,productoEV.precioVentaOportunidad});					
					 if(resultInsert == null){
						throw new IllegalStateException("Error insert.");
					 }
					 
				}
				r++;
				p = (r * 100) / t;
				System.out.println("OK, INSERTADO TODO DE ESTE, ==> % terminado:\t" + p + "\t\t");

			}

			System.out.println("==> OK.");

			closeConnection();

		} catch (Exception ex) {
			System.out.println("--------------------------------------->> ERROR:");

			ex.printStackTrace(System.err);
		}

	}

	private int getExcellRow(String col) {
		int index = -1;
		//System.out.print("-------->> getExcellRow("+col+")");
		if (col.length() == 2 && col.startsWith("A")) {
			index = ('Z' - 'A') + 1 + (col.charAt(1) - 'A');
		} else {
			index = col.charAt(0) - 'A';
		}
		//System.out.println("="+index);

		return index;
	}

	private void processRowProductos(Row row) throws ParseException {
		int cells = row.getPhysicalNumberOfCells();
		System.out.println("->> processRowProductos(" + row.getRowNum() + "): lastCelNum=" + row.getLastCellNum());
		if (row.getCell(getExcellRow("A")) == null || row.getLastCellNum() == -1) {
			System.out.println("\t\t->> IGNORING THIS ROW !");
			return;
		}
		//----------------------------------------------------------------------
		ProveedorEXCELValue pev = null;

		pev = proveedorHT.get(row.getCell(getExcellRow("A")).toString());
		if (pev == null) {
			//System.err.println("PROVEDOR ? "+row.getCell(0)+" ROW:"+row.getRowNum());
			pev = new ProveedorEXCELValue();

			pev.id = null;
			pev.razonSocial = row.getCell(getExcellRow("A")).toString();
			pev.cp = "55068";
			pev.poblacionId = 60816;
			pev.rfc = "KAJE010101XXX";
			pev.calle = "CALLE XX";
			pev.numInterior = "IN";
			pev.numExterior = "EX";
			pev.telefonos = "55-1234-5678,55-1234-5678";
			pev.telefonosMoviles = "55-1234-5678,55-1234-5678";
			pev.faxes = "55-1234-5678,55-1234-5678";
			pev.email = "nomail@mail.com";
			pev.plazoDePago = 30;
			pev.url = "http://pagina.com";

			proveedorHT.put(pev.razonSocial, pev);
		}
		//----------------------------------------------------------------------
		IndustriaEXCELValue eev = null;

		eev = industriaHT.get(row.getCell(getExcellRow("B")).toString());
		if (eev == null) {
			eev = new IndustriaEXCELValue();

			eev.id = null;
			eev.nombre = row.getCell(getExcellRow("B")).toString();

			industriaHT.put(eev.nombre, eev);
		}
		//----------------------------------------------------------------------
		LineaEXCELValue lev = null;

		lev = lineaHT.get(row.getCell(getExcellRow("C")).toString());

		if (lev == null) {
			lev = new LineaEXCELValue();

			lev.id = null;
			lev.nombre = row.getCell(getExcellRow("C")).toString();

			lineaHT.put(lev.nombre, lev);
		}
		//----------------------------------------------------------------------
		MarcaEXCELValue mev = null;

		mev = marcaHT.get(row.getCell(getExcellRow("D")).toString());

		if (mev == null) {
			mev = new MarcaEXCELValue();
			mev.id = null;

			mev.nombre = row.getCell(getExcellRow("D")).toString();
			mev.linea = lineaHT.get(lev.nombre);
			mev.industria = industriaHT.get(eev.nombre);

			marcaHT.put(mev.nombre, mev);
		}

		//----------------------------------------------------------------------
		ProductoEXCELValue productoEV = new ProductoEXCELValue();

		//prodev.proveedorId = pev.id;
		productoEV.pev = pev;
		productoEV.id = new BigDecimal(row.getCell(getExcellRow("E")).toString()).intValue();

		productoEV.codigoBarras = row.getCell(getExcellRow("F")).toString().replace("#", "");
		productoEV.abrev = row.getCell(getExcellRow("G")).toString().replace("'", "''");

		productoEV.descripcion = row.getCell(getExcellRow("H")).toString().replace("'", "''");
		productoEV.presentacion = row.getCell(getExcellRow("I")).toString();
		try {
			productoEV.cont = new BigDecimal(row.getCell(getExcellRow("J")).toString()).intValue();
			productoEV.unidadMedida = row.getCell(getExcellRow("K")).toString();
		} catch (NumberFormatException nfe) {
			productoEV.cont = 1;
			productoEV.unidadMedida = row.getCell(getExcellRow("K")).toString();
		}

		productoEV.unidadesXCaja = row.getCell(getExcellRow("L")).toString();

		int colE = 0;

		try {
			colE = getExcellRow("W");
			productoEV.precioVenta = Double.parseDouble(row.getCell(getExcellRow("W")).toString());
			colE = getExcellRow("X");
			productoEV.precioVentaOportunidad = Double.parseDouble(row.getCell(getExcellRow("X")).toString());
			colE = getExcellRow("Z");
			productoEV.cantidadAlmacen = new BigDecimal(row.getCell(getExcellRow("Z")).toString()).intValue();
			productoEV.cantMinAlmacen = 1;
			colE = getExcellRow("AA");
			productoEV.cantidadAlmacenOp = new BigDecimal(row.getCell(getExcellRow("AA")).toString()).intValue();
			colE = getExcellRow("AC");
			if(row.getCell(colE)!=null && row.getCell(colE).toString().trim().length()>1){
				productoEV.enOferta2x1 = row.getCell(colE).toString().equalsIgnoreCase("SI");
			}

			productoEV.sucCanAlmacen = productoEV.cantidadAlmacen;
			productoEV.sucCanMinAlmacen = productoEV.cantMinAlmacen;
			productoEV.sucCanAlmacenOp = productoEV.cantidadAlmacenOp;

			productoEV.precioVentaSucursal = productoEV.precioVenta;

		} catch (Exception e) {
			productoEV.precioVenta = 100.0;
			System.err.println("X -->> Error " + e.getLocalizedMessage() + " @ ROW:" + row.getRowNum() + ", col:" + colE);
		}

		productoEV.costo = productoEV.precioVenta / 1.15;
		productoEV.costoVenta = productoEV.precioVenta / 1.13;

		productoEV.marca = marcaHT.get(mev.nombre);

		productoHT.put(productoEV.id, productoEV);
	}

	private static Connection conn;

	private static Connection getConnection(String driver, String url, String user, String psswd) {
		if (conn == null) {

			try {
				Class.forName(driver);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace(System.err);
				throw new IllegalStateException("Cant load driver class:" + driver);
			}

			try {
				conn = DriverManager.getConnection(url, user, psswd);

			} catch (SQLException se) {
				//se.printStackTrace(System.err);
				throw new IllegalStateException("Cant open Connection:" + se);
			}

		}
		return conn;
	}

	private static void closeConnection() {
		if (conn != null) {
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace(System.err);
			}
		}
	}

	private Object searchFor(Connection connQuery, String query) throws IllegalStateException {
		return searchFor(connQuery, query, null);
	}

	private Object searchFor(Connection connQuery, String query, Object param) throws IllegalStateException {
		Object result = null;
		try {
			PreparedStatement ps = connQuery.prepareStatement(query);

			if (param != null) {
				if (param.getClass().equals(Integer.class)) {
					ps.setInt(1, ((Integer) param).intValue());
				} else if (param.getClass().equals(String.class)) {
					ps.setString(1, param.toString());
				} else {
					ps.setObject(1, param);
				}

			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = rs.getObject(1);
			}
			rs.close();

			return result;
		} catch (SQLException se) {
			se.printStackTrace(System.err);
			throw new IllegalStateException("Can't execute query:");
		}
	}

	private Object insertFor(Connection connQuery, String query, Object[] params) throws IllegalStateException {
		Object result = null;
		try {
			//System.err.println("\t\t-->>insertFor("+query+","+Arrays.asList(params) +"):");
			PreparedStatement ps = connQuery.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

			int i = 1;

			for (Object param : params) {

				if (param.getClass().equals(Integer.class)) {
					ps.setInt(i, ((Integer) param).intValue());
				} else if (param.getClass().equals(String.class)) {
					ps.setString(i, param.toString());
				} else {
					ps.setObject(i, param);
				}

				i++;
			}

			int ra = ps.executeUpdate();

			if (ra > 0) {

				ResultSet rs = ps.getGeneratedKeys();
				if (rs != null) {
					while (rs.next()) {
						result = rs.getObject(1);
					}
					rs.close();
				} else {
					result = -1;
				}
			}

			return result;
		} catch (SQLException se) {
			se.printStackTrace(System.err);
			throw new IllegalStateException("Can't execute update:" + se);
		}
	}
}

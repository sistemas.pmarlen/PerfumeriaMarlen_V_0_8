DELETE FROM USUARIO_PERFIL;
DELETE FROM USUARIO;

INSERT INTO USUARIO VALUES ('root'			,1,'root'			,'977244cbc826a0f25d07a07f194835b1'	,'dleon@perfumeriamarlen.com.mx',NULL,1);

INSERT INTO USUARIO VALUES ('uleon'			,1,'JAIR LEON'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'uleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('ecastaneda'	,1,'ELIZABETH CASTAÑEDA'	,'ef6299c9e7fdae6d775819ce1e2620b8'	,'ecastaneda@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('dleon'			,1,'DANIEL LEON'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('hmiranda'		,1,'HILDA MIRANDA'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('jnieto'		,1,'JANETH NIETO'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('aleon'			,1,'ANA LEON'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('aestrada'		,1,'ALFREDO ESTRADA'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dleon@perfumeriamarlen.com.mx',NULL,2);
INSERT INTO USUARIO VALUES ('dolvera'		,1,'DAVID OLVERA'			,'ef6299c9e7fdae6d775819ce1e2620b8'	,'dolvera@perfumeriamarlen.com.mx',NULL,2);

INSERT INTO USUARIO_PERFIL VALUES ('root'		,'root');
INSERT INTO USUARIO_PERFIL VALUES ('root'		,'pmarlenuser');

INSERT INTO USUARIO_PERFIL VALUES ('uleon'		,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('uleon'		,'admin');
INSERT INTO USUARIO_PERFIL VALUES ('uleon'		,'finances');

INSERT INTO USUARIO_PERFIL VALUES ('ecastaneda','pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('ecastaneda','admin');
INSERT INTO USUARIO_PERFIL VALUES ('ecastaneda','finances');

INSERT INTO USUARIO_PERFIL VALUES ('dleon'		,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('dleon'		,'stock');

INSERT INTO USUARIO_PERFIL VALUES ('hmiranda'	,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('hmiranda'	,'stock');

INSERT INTO USUARIO_PERFIL VALUES ('jnieto'		,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('jnieto'		,'stock');

INSERT INTO USUARIO_PERFIL VALUES ('aleon'		,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('aleon'		,'stock');

INSERT INTO USUARIO_PERFIL VALUES ('aestrada'	,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('aestrada'	,'stock');

INSERT INTO USUARIO_PERFIL VALUES ('dolvera'	,'pmarlenuser');
INSERT INTO USUARIO_PERFIL VALUES ('dolvera'	,'stock');

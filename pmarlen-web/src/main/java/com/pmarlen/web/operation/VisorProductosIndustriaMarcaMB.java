/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.web.operation;

import com.pmarlen.model.beans.Industria;
import com.pmarlen.model.beans.Marca;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.controller.IndustriaJpaController;
import com.pmarlen.model.controller.MarcaJpaController;
import com.pmarlen.model.controller.ProductoJpaController;
import com.pmarlen.web.common.view.messages.Messages;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author VEAXX9M
 */
public class VisorProductosIndustriaMarcaMB {

    @Autowired
    private ProductoJpaController productoJpaController;
    @Autowired
    private MarcaJpaController marcaJpaController;
    @Autowired
    private IndustriaJpaController industriaJpaController;
    @Autowired
    private PedidoNuevoMB pedidoNuevoMB;

    private Integer IndustriaSelectedId;
    private Integer marcaSelectedId;
    private Integer productoSelectedId;

    private List<Marca> marcaFilteredList;
    private List<Producto> productoFilteredList;

    private final Logger logger = LoggerFactory.getLogger(VisorProductosIndustriaMarcaMB.class);

    public VisorProductosIndustriaMarcaMB() {
        resetViewPage();
    }

    private void resetViewPage() {	
        resetSlectionsBeans();
    }

    private void resetSlectionsBeans() {        
        IndustriaSelectedId       = null;
        marcaSelectedId         = null;
        productoSelectedId      = null;
        productoFilteredList    = new ArrayList<Producto>();
        marcaFilteredList       = new ArrayList<Marca>();
    }
    
    public void industriaChanged(ValueChangeEvent event) {
        Integer industriaChanged = (Integer) event.getNewValue();
        logger.debug("-->> industriaChanged: industriaChanged="+industriaChanged);
        
        marcaSelectedId         = null;
        productoSelectedId      = null;
        productoFilteredList    = new ArrayList<Producto>();
        if(industriaChanged != null){
            marcaFilteredList = marcaJpaController.findMarcaEntitiesByIndustria(new Industria(industriaChanged));
        } else {
            marcaFilteredList = new ArrayList<Marca>();
        }
    }
    
    public void marcaChanged(ValueChangeEvent event) {
        Integer marcaNewValue = (Integer) event.getNewValue();
        logger.debug("-->> marcaChanged: marcaNewValue="+marcaNewValue);
        if(marcaNewValue != null){
            productoFilteredList = productoJpaController.findProductoEntitiesByMarca(new Marca(marcaNewValue));
        } else {
            productoFilteredList = new ArrayList<Producto>();
        }
        productoSelectedId      = null;
    }

    public List<SelectItem> getIndustriaList() {

        List<SelectItem> itemsList = new ArrayList<SelectItem>();
        List<Industria> list = industriaJpaController.findIndustriaEntities();
        logger.debug("-->> getIndustriaList: ");

        itemsList.add(new SelectItem(null, Messages.getLocalizedString("COMMON_SELECTONEITEM")));

        for (Industria Industria : list) {
            itemsList.add(new SelectItem(Industria.getId(), Industria.getNombre()));
        }
        return itemsList;
    }

    public List<SelectItem> getMarcaList() {
        List<SelectItem> itemsList = new ArrayList<SelectItem>();
        
        logger.debug("-->> getMarcaList: IndustriaSelectedId=" + IndustriaSelectedId);
        itemsList.add(new SelectItem(null, Messages.getLocalizedString("COMMON_SELECTONEITEM")));

        for (Marca marca : marcaFilteredList) {
            itemsList.add(new SelectItem(marca.getId(), marca.getNombre()));
        }
        return itemsList;
    }

    public List<Producto> getProductoList() {
        logger.debug("-->> getProductoList: marcaSelectedId=" + marcaSelectedId+"m productoFilteredList="+productoFilteredList);
        return productoFilteredList;
    }

    //--------------------------------------------------------------------------
    public void setProductoJpaController(ProductoJpaController productoJpaController) {
        this.productoJpaController = productoJpaController;
    }

    public void setMarcaJpaController(MarcaJpaController marcaJpaController) {
        this.marcaJpaController = marcaJpaController;
    }

    public void setIndustriaJpaController(IndustriaJpaController IndustriaJpaController) {
        this.industriaJpaController = IndustriaJpaController;
    }

    public void setPedidoNuevoMB(PedidoNuevoMB pedidoNuevoMB) {
        this.pedidoNuevoMB = pedidoNuevoMB;
    }
    //--------------------------------------------------------------------------
    public Integer getIndustriaSelectedId() {
        logger.debug("-->> getIndustriaSelectedId: IndustriaSelectedId=" + IndustriaSelectedId);
        return IndustriaSelectedId;
    }

    public void setIndustriaSelectedId(Integer IndustriaSelectedId) {
        logger.debug("-->> setIndustriaSelectedId: this.IndustriaSelectedId( "+this.IndustriaSelectedId+") = " + IndustriaSelectedId);
        this.IndustriaSelectedId = IndustriaSelectedId;
    }

    public Integer getMarcaSelectedId() {
        logger.debug("-->> getMarcaSelectedId: marcaSelectedId=" + marcaSelectedId);
        return marcaSelectedId;
    }

    public void setMarcaSelectedId(Integer marcaSelectedId) {
        logger.debug("-->> setMarcaSelectedId: this.marcaSelectedId("+this.marcaSelectedId+") = " + marcaSelectedId);
        this.marcaSelectedId = marcaSelectedId;
    }

    public Integer getProductoSelectedId() {
        return productoSelectedId;
    }

    public void setProductoSelectedId(Integer productoSelectedId) {
        this.productoSelectedId = productoSelectedId;
    }
    //--------------------------------------------------------------------------
}

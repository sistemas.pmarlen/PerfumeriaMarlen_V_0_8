/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.web.operation;

import com.pmarlen.businesslogic.LogicaFinaciera;
import com.pmarlen.businesslogic.PedidoVentaBusinessLogic;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.PedidoVentaJpaController;
import com.pmarlen.model.controller.ProductoJpaController;
import com.pmarlen.model.dto.MovimientoEntreAlmacenes;
import com.pmarlen.model.dto.ProductoEnTrancito;
import com.pmarlen.web.security.managedbean.SessionUserMB;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author VEAXX9M
 */
public class OperacionesEnAlmacenesMB {

	@Autowired
	private ProductoJpaController productoJpaController;
	@Autowired
	private PedidoVentaBusinessLogic pedidoVentaBusinessLogic;
	@Autowired
	private SessionUserMB sessionUserMB;
	private Integer sucursalId;
	private Logger logger;
	private int scrollerPage;

	private List<MovimientoEntreAlmacenes> movimientoEntreAlmacenesList;
	
	public OperacionesEnAlmacenesMB() {
		logger = LoggerFactory.getLogger(PedidosPreVisorMB.class);
		sucursalId = 1;
	}

	public List<SelectItem> getSucursales() {
		List<Sucursal> list = productoJpaController.findAllSucursales();
		List<SelectItem> result = new ArrayList<SelectItem>();
		for (Sucursal s : list) {
			SelectItem sucursalSI = new SelectItem(s.getId(), s.getNombre());
			result.add(sucursalSI);
		}

		return result;
	}

	public List<SelectItem> getSucursalesHijas() {
		List<Sucursal> list = productoJpaController.findAllSucursales();
		List<SelectItem> result = new ArrayList<SelectItem>();
		for (Sucursal s : list) {
			if (s.getSucursal() == null) {
				continue;
			}
			SelectItem sucursalSI = new SelectItem(s.getId(), s.getNombre());
			result.add(sucursalSI);
		}

		return result;
	}

	private void dataValidation() throws ValidatorException {
		logger.debug("\t################################ >> dataValidation: ");
		//logger.debug("\t\t################################ >> throw new ValidatorException Cliente!");
		//throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar el Cliente !!", "Debe seleccionalr el Cliente !!"));
		//throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe seleccionar la Forma de Pago!!", "Debe seleccionalr la Forma de Pago!!"));        
	}

	public String confirmarSurtirAlmacenPrincipal() {
		logger.debug("========================================================>>");
		logger.debug("-->>confirmarSurtirAlmacenPrincipal():");
		logger.debug("========================================================>>");
		try {

			dataValidation();
			try{
				for(MovimientoEntreAlmacenes ma:movimientoEntreAlmacenesList){
					logger.debug("\t-->>MovimientoEntreAlmacenes: Producto ["+ma.getProducto().getId()+"] surtir:"+ma.getDestinoSurtir()+" PV: $"+ma.getDestinoPV());
				}
				pedidoVentaBusinessLogic.movimientoEntreElmacenes(movimientoEntreAlmacenesList, sucursalId, Constants.ALMACEN_PRINCIPAL,sessionUserMB.getUsuarioAuthenticated());

				return null;
            } catch (Exception ex) {
                logger.debug("<<++++++++++++++++++++++++++++++++++++++++++++++++++");
                ex.printStackTrace(System.err);
                logger.debug("Error in MB confirmarSurtirAlmacen:", ex);

                throw new ValidatorException(
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.toString(), ex.toString()));
            } finally {
                
            }
	
        } catch (ValidatorException ve) {
            logger.debug("<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    ve.getFacesMessage());
            return null;
        }
	}

	public void sucursalChanged(ValueChangeEvent event) {
		Integer sucursalChanged = (Integer) event.getNewValue();
		logger.debug("-->> sucursalChanged: sucursalChanged=" + sucursalChanged);
	}

	public List<ProductoEnTrancito> getAllProductosEnTransitoDeSucursal() {
		//List<ProductoEnAlmacenesDetalle> productoEnAlmacenDetalleList = new ArrayList<ProductoEnAlmacenesDetalle>();
		//List<Producto> productoList = productoJpaController.findAllProductoConAlmacenesDeSucursal(sucursalId);
		long t1,t2;
		
		logger.debug("==>> getAllProductosEnTransitoDeSucursal: sucursalId=" + sucursalId);		
		t1 = System.currentTimeMillis();
		List<ProductoEnTrancito> findAllProductosEnTransitoDeSucursal = productoJpaController.findAllProductosEnTransitoDeSucursal(sucursalId);
		t2 = System.currentTimeMillis();
		logger.debug("==>> T = "+(t2-t1));
		return findAllProductosEnTransitoDeSucursal;
		

	}

	public List<MovimientoEntreAlmacenes> getAllMovimientoEntreAlmacenesPrincipal() {
		//List<MovimientoEntreAlmacenes> movimientoEntreAlmacenesList = new ArrayList<MovimientoEntreAlmacenes>();
		//List<Producto> productoList = productoJpaController.findAllProductoConAlmacenesDeSucursal(sucursalId);
		logger.debug("==>> getAllMovimientoEntreAlmacenes: sucursalId=" + sucursalId);
		
		movimientoEntreAlmacenesList = productoJpaController.findAllProductosMovimientoEntreAlmacenesDeSucursal(sucursalId, Constants.ALMACEN_PRINCIPAL);

		return movimientoEntreAlmacenesList;
	}

	public List<MovimientoEntreAlmacenes> getAllMovimientoEntreAlmacenesOportunidad() {
		//List<MovimientoEntreAlmacenes> movimientoEntreAlmacenesList = new ArrayList<MovimientoEntreAlmacenes>();
		//List<Producto> productoList = productoJpaController.findAllProductoConAlmacenesDeSucursal(sucursalId);
		logger.debug("==>> getAllMovimientoEntreAlmacenes: sucursalId=" + sucursalId);

		return productoJpaController.findAllProductosMovimientoEntreAlmacenesDeSucursal(sucursalId, Constants.ALMACEN_OPORTUNIDAD);
	}

	public List<ProductoEnAlmacenesDetalle> getProductosEnAlmacenes() {
		List<ProductoEnAlmacenesDetalle> productoEnAlmacenDetalleList = new ArrayList<ProductoEnAlmacenesDetalle>();
		List<Producto> productoList = productoJpaController.findAllProductoConAlmacenesDeSucursal(sucursalId);
		logger.debug("==>> getProductoConMovimientos: sucursalId=" + sucursalId);

		for (Producto producto : productoList) {
			ProductoEnAlmacenesDetalle productoEnAlmacenDetalle = new ProductoEnAlmacenesDetalle(producto);

			Collection<AlmacenProducto> almacenProductoCollection = producto.getAlmacenProductoCollection();

			logger.debug("\t==>> Producto[" + producto.getId() + "]");
			for (AlmacenProducto ap : almacenProductoCollection) {
				logger.debug("\t\t==>> AlmacenProducto: almacen=" + ap.getAlmacen() + "[" + ap.getAlmacen().getTipoAlmacen() + "],sucursal=" + ap.getAlmacen().getSucursal().getId() + ", cantidad=" + ap.getCantidadActual());
				if (ap.getAlmacen().getSucursal().getId().intValue() == sucursalId) {

					if (ap.getAlmacen().getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
						productoEnAlmacenDetalle.setAlmacenPrincipal(ap);
						logger.debug("\t\t\t==>> OK added");
					} else if (ap.getAlmacen().getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
						productoEnAlmacenDetalle.setAlmacenOportunidad(ap);
						logger.debug("\t\t\t==>> OK added");
					}
				}
			}
			productoEnAlmacenDetalleList.add(productoEnAlmacenDetalle);

		}

		return productoEnAlmacenDetalleList;
	}

	public void setSessionUserMB(SessionUserMB sessionUserMB) {
		this.sessionUserMB = sessionUserMB;
	}

	public void setProductoJpaController(ProductoJpaController productoJpaController) {
		this.productoJpaController = productoJpaController;
	}

	/**
	 * @return the sucursal
	 */
	public Integer getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursalId(Integer sucursalId) {
		this.sucursalId = sucursalId;
	}

	/**
	 * @param pedidoVentaBusinessLogic the pedidoVentaBusinessLogic to set
	 */
	public void setPedidoVentaBusinessLogic(PedidoVentaBusinessLogic pedidoVentaBusinessLogic) {
		this.pedidoVentaBusinessLogic = pedidoVentaBusinessLogic;
	}

	/**
	 * @return the scrollerPage
	 */
	public int getScrollerPage() {
		return scrollerPage;
	}

	/**
	 * @param scrollerPage the scrollerPage to set
	 */
	public void setScrollerPage(int scrollerPage) {
		this.scrollerPage = scrollerPage;
	}
}

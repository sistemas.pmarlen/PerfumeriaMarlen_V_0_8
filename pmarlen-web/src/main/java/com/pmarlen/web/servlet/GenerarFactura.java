/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.web.servlet;

import com.pmarlen.businesslogic.reports.GeneradorDeFactura;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author alfredo
 */
public class GenerarFactura extends HttpServlet {

	/**
	 * Processes requests for both HTTP
	 * <code>GET</code> and
	 * <code>POST</code> methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletOutputStream outputStream = null;
		try {
			String pedidoId = request.getParameter("pedidoId");
			
			ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			System.err.println("-->>GenerarFactura servlet: before findBean");
			String realPath=getServletContext().getRealPath("/images");
			System.err.println("-->>GenerarFactura servlet: realPath="+realPath);
			
			GeneradorDeFactura generadorDeFactura = (GeneradorDeFactura)context.getBean("generadorDeFactura");
			System.err.println("-->>GenerarFactura servlet: generadorDeFactura="+generadorDeFactura);
			
			byte[] bytesPdf = generadorDeFactura.generaFatura(Integer.parseInt(pedidoId));
			System.err.println("-->>OK writing PDF bytes");
			
			response.setContentType("application/pdf");
			
			outputStream = response.getOutputStream();
			
			outputStream.write(bytesPdf);
			outputStream.flush();
			outputStream.close();
		} catch(Exception ex){
			ex.printStackTrace(System.err);
			
			response.setContentType("application/pdf");
			System.err.println("-->>writing another PDF !");
			
			outputStream = response.getOutputStream();
			String s = "/home/alfredo/NetBeansProjects/mavenproject1/jasper_out_20121011102520.pdf";
			FileInputStream fis =  new FileInputStream(s);
			byte buffer[] = new byte[1024];
			int r;
			while((r = fis.read(buffer, 0, buffer.length)) != -1){
				outputStream.write(buffer,0,r);
				outputStream.flush();
			}
			fis.close();
			outputStream.close();
			
		} finally {			
			
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP
	 * <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP
	 * <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}

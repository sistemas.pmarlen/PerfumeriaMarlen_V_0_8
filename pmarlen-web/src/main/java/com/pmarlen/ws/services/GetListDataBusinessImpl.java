package com.pmarlen.ws.services;

import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.GetListDataBusinesJpaController;
import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.util.ArrayList;
import java.util.Collection;

import java.util.List;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

@WebService(endpointInterface = "com.pmarlen.wscommons.services.GetListDataBusiness")
@Repository("getListDataBusiness")
public class GetListDataBusinessImpl implements GetListDataBusiness {

	private static Logger logger = LoggerFactory.getLogger(GetListDataBusinessImpl.class);
	@Autowired
	private GetListDataBusinesJpaController getListDataBusinesJpaController;

	public List<Usuario> getUsuarioList() {
		logger.debug("@WebService invocation -->> getUsuarioList");
		List<Usuario> entitiesList = getListDataBusinesJpaController.findUsuarioEntities(true, -1, -1);
		if(entitiesList ==null){
			entitiesList =  new ArrayList<Usuario>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Industria> getIndustriaList() {
		logger.debug("@WebService invocation -->> getIndustriaList");
		List<Industria> entitiesList = getListDataBusinesJpaController.findIndustriaEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Industria>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Linea> getLineaList() {
		logger.debug("@WebService invocation -->> getLineaList");
		List<Linea> entitiesList = getListDataBusinesJpaController.findLineaEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Linea>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Marca> getMarcaList() {
		logger.debug("@WebService invocation -->> getMarcaList");
		List<Marca> entitiesList = getListDataBusinesJpaController.findMarcaEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Marca>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Producto> getProductoList() {
		logger.debug("@WebService invocation -->> getProductoList");
		List<Producto> entitiesList = getListDataBusinesJpaController.findProductoEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Producto>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Multimedio> getProductoMultimedioList() {
		logger.debug("@WebService invocation -->> getProductoMultimedioList");
		List<Multimedio> entitiesList = getListDataBusinesJpaController.findMultimedioEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Multimedio>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}
	
	public List<AlmacenProducto> getAlmacenProductoList(){
		logger.debug("@WebService invocation -->> getAlmacenProductoList");
		List<AlmacenProducto> entitiesList = getListDataBusinesJpaController.findAlmacenProductoEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<AlmacenProducto>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}


	public List<FormaDePago> getFormaDePagoList() {
		logger.debug("@WebService invocation -->> getFormaDePagoList");
		List<FormaDePago> entitiesList = getListDataBusinesJpaController.findFormaDePagoEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<FormaDePago>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public List<Estado> getEstadoList() {
		logger.debug("@WebService invocation -->> getEstadoList");
		List<Estado> entitiesList = getListDataBusinesJpaController.findEstadoEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Estado>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}
	
	public List<Sucursal> getSucursalList(){
		logger.debug("@WebService invocation -->> getSucursalList");
		List<Sucursal> entitiesList = getListDataBusinesJpaController.findSucursalEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList =  new ArrayList<Sucursal>();
		}
		//logger.debug("\t-->> "+entitiesList);
		for(Sucursal s: entitiesList){
			logger.debug("\t-->> "+s.getId()+":"+s.getNombre());
			Collection<Almacen> almacenCollection = s.getAlmacenCollection();
			if(almacenCollection != null) {
				for(Almacen a: almacenCollection){
					logger.debug("\t\t-->> Almacen:"+a.getId());
				}
			}
		}
		return entitiesList;
	}

	public List<Cliente> getClienteList() {
		logger.debug("@WebService invocation -->> getClienteList");
		List<Cliente> entitiesList = getListDataBusinesJpaController.findClienteEntities(true, -1, -1);
		if(entitiesList == null){
			entitiesList = new ArrayList<Cliente>();
		}
		logger.debug("\t-->> "+entitiesList);
		return entitiesList;
	}

	public String getServerVersion() {
		logger.debug("@WebService invocation -->> getServerVersion");
		String serverVersion = Constants.getServerVersion();
		logger.debug("\t-->> "+serverVersion);
		return serverVersion;
	}

	public void setGetListDataBusinesJpaController(GetListDataBusinesJpaController getListDataBusinesJpaController) {
		this.getListDataBusinesJpaController = getListDataBusinesJpaController;
	}
}
	
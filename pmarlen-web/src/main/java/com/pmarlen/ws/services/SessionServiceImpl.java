package  com.pmarlen.ws.services;

import com.pmarlen.model.controller.SessionInfoController;
import com.pmarlen.security.OnlineSessionInfo;
import com.pmarlen.wscommons.services.SessionService;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author alfred
 */

@WebService(endpointInterface = "com.pmarlen.ws.services.SessionService")
@Service("sessionService")
public class SessionServiceImpl implements SessionService {
    
	private SessionInfoController sessionInfoController;
	
	
	public OnlineSessionInfo iAmAlive(OnlineSessionInfo onlineSessionInfo){
		return null;	
	}

	/**
	 * @param sessionInfoController the sessionInfoController to set
	 */
	@Autowired
	public void setSessionInfoController(SessionInfoController sessionInfoController) {
		this.sessionInfoController = sessionInfoController;
	}

}

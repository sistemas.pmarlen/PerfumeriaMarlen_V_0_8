package com.pmarlen.model.controller;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import com.pmarlen.model.beans.Industria;
import com.pmarlen.model.controller.exceptions.IllegalOrphanException;
import com.pmarlen.model.controller.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.pmarlen.model.beans.Multimedio;
import java.util.ArrayList;
import java.util.Collection;
import com.pmarlen.model.beans.Marca;

/**
 * IndustriaJpaController
 */

@Repository("industriaJpaController")

public class IndustriaJpaController {


    private EntityManagerFactory emf = null;

    @Autowired
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Industria Industria) {
//        if (Industria.getMultimedioCollection() == null) {
//            Industria.setMultimedioCollection(new ArrayList<Multimedio>());
//        }
//        if (Industria.getMarcaCollection() == null) {
//            Industria.setMarcaCollection(new ArrayList<Marca>());
//        }
//        EntityManager em = null;
//        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            Collection<Multimedio> attachedMultimedioCollection = new ArrayList<Multimedio>();
//            for (Multimedio multimedioCollectionMultimedioToAttach : Industria.getMultimedioCollection()) {
//                multimedioCollectionMultimedioToAttach = em.getReference(multimedioCollectionMultimedioToAttach.getClass(), multimedioCollectionMultimedioToAttach.getId());
//                attachedMultimedioCollection.add(multimedioCollectionMultimedioToAttach);
//            }
//            Industria.setMultimedioCollection(attachedMultimedioCollection);
//            Collection<Marca> attachedMarcaCollection = new ArrayList<Marca>();
//            for (Marca marcaCollectionMarcaToAttach : Industria.getMarcaCollection()) {
//                marcaCollectionMarcaToAttach = em.getReference(marcaCollectionMarcaToAttach.getClass(), marcaCollectionMarcaToAttach.getId());
//                attachedMarcaCollection.add(marcaCollectionMarcaToAttach);
//            }
//            Industria.setMarcaCollection(attachedMarcaCollection);
//            em.persist(Industria);
//            for (Multimedio multimedioCollectionMultimedio : Industria.getMultimedioCollection()) {
//                multimedioCollectionMultimedio.getIndustriaCollection().add(Industria);
//                multimedioCollectionMultimedio = em.merge(multimedioCollectionMultimedio);
//            }
//            for (Marca marcaCollectionMarca : Industria.getMarcaCollection()) {
//                Industria oldIndustriaOfMarcaCollectionMarca = marcaCollectionMarca.getIndustria();
//                marcaCollectionMarca.setIndustria(Industria);
//                marcaCollectionMarca = em.merge(marcaCollectionMarca);
//                if (oldIndustriaOfMarcaCollectionMarca != null) {
//                    oldIndustriaOfMarcaCollectionMarca.getMarcaCollection().remove(marcaCollectionMarca);
//                    oldIndustriaOfMarcaCollectionMarca = em.merge(oldIndustriaOfMarcaCollectionMarca);
//                }
//            }
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public void edit(Industria Industria) throws IllegalOrphanException, NonexistentEntityException, Exception {
//        EntityManager em = null;
//        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            Industria persistentIndustria = em.find(Industria.class, Industria.getId());
//            Collection<Multimedio> multimedioCollectionOld = persistentIndustria.getMultimedioCollection();
//            Collection<Multimedio> multimedioCollectionNew = Industria.getMultimedioCollection();
//            Collection<Marca> marcaCollectionOld = persistentIndustria.getMarcaCollection();
//            Collection<Marca> marcaCollectionNew = Industria.getMarcaCollection();
//            List<String> illegalOrphanMessages = null;
//            for (Marca marcaCollectionOldMarca : marcaCollectionOld) {
//                if (!marcaCollectionNew.contains(marcaCollectionOldMarca)) {
//                    if (illegalOrphanMessages == null) {
//                        illegalOrphanMessages = new ArrayList<String>();
//                    }
//                    illegalOrphanMessages.add("You must retain Marca " + marcaCollectionOldMarca + " since its Industria field is not nullable.");
//                }
//            }
//            if (illegalOrphanMessages != null) {
//                throw new IllegalOrphanException(illegalOrphanMessages);
//            }
//            Collection<Multimedio> attachedMultimedioCollectionNew = new ArrayList<Multimedio>();
//            for (Multimedio multimedioCollectionNewMultimedioToAttach : multimedioCollectionNew) {
//                multimedioCollectionNewMultimedioToAttach = em.getReference(multimedioCollectionNewMultimedioToAttach.getClass(), multimedioCollectionNewMultimedioToAttach.getId());
//                attachedMultimedioCollectionNew.add(multimedioCollectionNewMultimedioToAttach);
//            }
//            multimedioCollectionNew = attachedMultimedioCollectionNew;
//            Industria.setMultimedioCollection(multimedioCollectionNew);
//            Collection<Marca> attachedMarcaCollectionNew = new ArrayList<Marca>();
//            for (Marca marcaCollectionNewMarcaToAttach : marcaCollectionNew) {
//                marcaCollectionNewMarcaToAttach = em.getReference(marcaCollectionNewMarcaToAttach.getClass(), marcaCollectionNewMarcaToAttach.getId());
//                attachedMarcaCollectionNew.add(marcaCollectionNewMarcaToAttach);
//            }
//            marcaCollectionNew = attachedMarcaCollectionNew;
//            Industria.setMarcaCollection(marcaCollectionNew);
//            Industria = em.merge(Industria);
//            for (Multimedio multimedioCollectionOldMultimedio : multimedioCollectionOld) {
//                if (!multimedioCollectionNew.contains(multimedioCollectionOldMultimedio)) {
//                    multimedioCollectionOldMultimedio.getIndustriaCollection().remove(Industria);
//                    multimedioCollectionOldMultimedio = em.merge(multimedioCollectionOldMultimedio);
//                }
//            }
//            for (Multimedio multimedioCollectionNewMultimedio : multimedioCollectionNew) {
//                if (!multimedioCollectionOld.contains(multimedioCollectionNewMultimedio)) {
//                    multimedioCollectionNewMultimedio.getIndustriaCollection().add(Industria);
//                    multimedioCollectionNewMultimedio = em.merge(multimedioCollectionNewMultimedio);
//                }
//            }
//            for (Marca marcaCollectionNewMarca : marcaCollectionNew) {
//                if (!marcaCollectionOld.contains(marcaCollectionNewMarca)) {
//                    Industria oldIndustriaOfMarcaCollectionNewMarca = marcaCollectionNewMarca.getIndustria();
//                    marcaCollectionNewMarca.setIndustria(Industria);
//                    marcaCollectionNewMarca = em.merge(marcaCollectionNewMarca);
//                    if (oldIndustriaOfMarcaCollectionNewMarca != null && !oldIndustriaOfMarcaCollectionNewMarca.equals(Industria)) {
//                        oldIndustriaOfMarcaCollectionNewMarca.getMarcaCollection().remove(marcaCollectionNewMarca);
//                        oldIndustriaOfMarcaCollectionNewMarca = em.merge(oldIndustriaOfMarcaCollectionNewMarca);
//                    }
//                }
//            }
//            em.getTransaction().commit();
//        } catch (Exception ex) {
//            String msg = ex.getLocalizedMessage();
//            if (msg == null || msg.length() == 0) {
//                Integer id = Industria.getId();
//                if (findIndustria(id) == null) {
//                    throw new NonexistentEntityException("The Industria with id " + id + " no longer exists.");
//                }
//            }
//            throw ex;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
//        EntityManager em = null;
//        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            Industria Industria;
//            try {
//                Industria = em.getReference(Industria.class, id);
//                Industria.getId();
//            } catch (EntityNotFoundException enfe) {
//                throw new NonexistentEntityException("The Industria with id " + id + " no longer exists.", enfe);
//            }
//            List<String> illegalOrphanMessages = null;
//            Collection<Marca> marcaCollectionOrphanCheck = Industria.getMarcaCollection();
//            for (Marca marcaCollectionOrphanCheckMarca : marcaCollectionOrphanCheck) {
//                if (illegalOrphanMessages == null) {
//                    illegalOrphanMessages = new ArrayList<String>();
//                }
//                illegalOrphanMessages.add("This Industria (" + Industria + ") cannot be destroyed since the Marca " + marcaCollectionOrphanCheckMarca + " in its marcaCollection field has a non-nullable Industria field.");
//            }
//            if (illegalOrphanMessages != null) {
//                throw new IllegalOrphanException(illegalOrphanMessages);
//            }
//            Collection<Multimedio> multimedioCollection = Industria.getMultimedioCollection();
//            for (Multimedio multimedioCollectionMultimedio : multimedioCollection) {
//                multimedioCollectionMultimedio.getIndustriaCollection().remove(Industria);
//                multimedioCollectionMultimedio = em.merge(multimedioCollectionMultimedio);
//            }
//            em.remove(Industria);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public List<Industria> findIndustriaEntities() {
        return findIndustriaEntities(true, -1, -1);
    }

    public List<Industria> findIndustriaEntities(int maxResults, int firstResult) {
        return findIndustriaEntities(false, maxResults, firstResult);
    }

    private List<Industria> findIndustriaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Industria as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            List<Industria> resultList = q.getResultList();
            for(Industria x: resultList) {
                Collection<Marca> marcaCollection = x.getMarcaCollection();
                for(Marca marca :marcaCollection) {
                }
            }
            return resultList;
        } finally {
            em.close();
        }
    }

    public Industria findIndustria(Integer id) {
        EntityManager em = getEntityManager();
        try {
            Industria x = em.find(Industria.class, id);
            if(x != null ){
                Collection<Marca> marcaCollection = x.getMarcaCollection();
                for(Marca marca :marcaCollection) {
                }
            }
            return x;
        } finally {
            em.close();
        }
    }

    public int getIndustriaCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Industria as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}

package com.pmarlen.businesslogic.reports;

import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.pmarlen.model.controller.PedidoVentaJpaController;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Hello world!
 *
 */
@Service("generadorDeFactura")
public class GeneradorDeFactura {
	
    @Autowired
    private PedidoVentaJpaController pedidoVentaJpaController;

    public byte[] generaFatura(int pedidoId) {
		byte[] pdfBytes = null;
		PedidoVenta pedidoVenta = null;
        try {
			pedidoVenta = pedidoVentaJpaController.findPedidoVenta(pedidoId);
			
            String reportPath;
            
            reportPath = "/reports/facturaDesign1.jrxml";
            InputStream inputStream = GeneradorDeFactura.class.getResourceAsStream(reportPath);
            
            Collection<Map<String,?>> col = new ArrayList<Map<String,?>>();
            DecimalFormat    df    = new  DecimalFormat("$###,###,###,##0.00");
            DecimalFormat    dfEnt = new  DecimalFormat("###########0.00");
            DecimalFormat    dfBC  = new  DecimalFormat("0000000000000");
            System.out.println("Ok, jrxml loaded");
            double p  = 0.0;            
            double im = 0.0;
            double sp = 0.0;            
            double sim= 0.0;
            int n;
            int numReg  = 75;//Integer.parseInt(args[0]);
            Random rand = new Random(System.currentTimeMillis());
            double dd;
            double d = 0;
			
			Collection<PedidoVentaDetalle> pedidoVentaDetalleCollection = pedidoVenta.getPedidoVentaDetalleCollection();
			numReg = pedidoVentaDetalleCollection.size();
			for(PedidoVentaDetalle pvd:pedidoVentaDetalleCollection){
				Map<String,Object> vals = new HashMap<String,Object> ();
                
                n = pvd.getCantidad();
                
                vals.put("clave",pvd.getProducto().getId());
                vals.put("cantidad",n);
                vals.put("codigoBarras",pvd.getProducto().getCodigoBarras());                
                vals.put("descripcion",pvd.getProducto().getNombre()+"/"+pvd.getProducto().getPresentacion());
                
				p  = pvd.getPrecioVenta();
                im = n * p ; 
                
                sp  += p;
                sim += im;
                
                vals.put("precio",df.format(p));
                //vals.put("desc"  ,df.format(0.0));
                vals.put("importe",df.format(im));
                vals.put("unidadMedida",pvd.getProducto().getUnidadMedida());
                
                col.add(vals);
			}
			
            JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
            System.out.println("Ok, JRDataSource created");
            
            Map parameters = new HashMap();
            
            SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");
            
            
            Date fechaReporte = new Date();
            
            parameters.put("noFactura" ,"AA 1000");
            parameters.put("printImages" ,true);
            parameters.put("folioFiscal" ,"D14BEBE0-AC11-36DB-A34E-9193CD977749");
            parameters.put("noSerCertSAT" ,"D14BEBE0-AC11-36DB-A34E-9193CD977749");
            parameters.put("fechaYHoraCert" ,"2012-09-25 13:16:33");
            parameters.put("lugarExp" ,"Estado de México");
            parameters.put("fechaYHoraExp" ,"2012-09-25 13:16:32");
            parameters.put("noSerCertSAT" ,"D14BEBE0-AC11-36DB-A34E-9193CD977749");
            
            parameters.put("cliente",pedidoVenta.getCliente().getRazonSocial());
            parameters.put("rfc",pedidoVenta.getCliente().getRfc());
            parameters.put("direccion" ,
					pedidoVenta.getCliente().getCalle()+", "+pedidoVenta.getCliente().getNumInterior()+", "+pedidoVenta.getCliente().getNumExterior()+", "+
					pedidoVenta.getCliente().getPoblacion().getTipoAsentamiento()+" "+pedidoVenta.getCliente().getPoblacion().getNombre()+" "+
					pedidoVenta.getCliente().getPoblacion().getMunicipioODelegacion()+", "+pedidoVenta.getCliente().getPoblacion().getEntidadFederativa()+", C.P. "+
					pedidoVenta.getCliente().getPoblacion().getCodigoPostal());
            parameters.put("condiciones" ,pedidoVenta.getComentarios());
            parameters.put("formaDePago" ,pedidoVenta.getFormaDePago());
            
            parameters.put("subtotal" , df.format(sim));
            parameters.put("iva" ,df.format(sim*0.16));
            parameters.put("descuento" ,df.format(d));
            
            double total = sim*1.16 - d;
            parameters.put("total" ,df.format(total));  
            
            String intDecParts[] = dfEnt.format(total).split("\\.");
            
            String letrasParteEntera  = NumeroCastellano.numeroACastellano(Long.parseLong(intDecParts[0])).trim();
            String letrasParteDecimal = NumeroCastellano.numeroACastellano(Long.parseLong(intDecParts[1])).trim();
            
            //parameters.put("importeLetra" ,letrasParteEntera+" Pesos con "+letrasParteDecimal+" centavos M.N.");
            parameters.put("importeLetra" ,(letrasParteEntera+" Pesos "+intDecParts[1]+"/100 M.N.").toUpperCase());
            
            parameters.put("cadenaOriginalSAT"  ,"||1.0|D14BEBE0-AC11-36DB-A34E-9193CD977749|2012-08-07T13:16:33|etu9EqXkMLoLccDqX5oVuDb6KW1pe47/fxcq1z/QDY3HmF4K5Gr40RSuA6PxIdZJSjYXVSAAFPqrFV9IaCCrYBqdlvIlVdsS3DwGrLuLKegPc1E/zZluOI36VWe3TZQuhsirT680qJ9aP6q+yx66+hhN6o21W77Ne5VV20KnXF8=|00001000000103550402||");            
            parameters.put("selloDigitalEmisor" ,"etu9EqXkMLoLccDqX5oVuDb6KW1pe47/fxcq1z/QDY3HmF4K5Gr40RSuA6PxIdZJSjYXVSAAFPqrFV9IaCCrYBqdlvIlVdsS3DwGrLuLKegPc1E/zZluOI36VWe3TZQuhsirT680qJ9aP6q+yx66+hhN6o21W77Ne5VV20KnXF8=");
            parameters.put("selloDigitalSAT"    ,"IPFtJ3piVCQA+ztYwg1dRP4N0fitawkSWxK0+9nNWoC4Juq67mFt+3cv3LeTwiJeHRvSDnfoGz6TTJFl5CUzZE240N0orAHE4c5X3ULgtvF0itL0ev70tYRe8ZWaaj4jK0vHP8qhFYiIyOK/u/K5dEposWwmG/deuIQhycguqv0=");
            
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
            
            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
			/*
            int pageHeight = jasperDesign.getPageHeight(); //jasperReport.getPageHeight();
            int titleHeight = jasperDesign.getTitle().getHeight();
            int pageHeaderHeight = jasperDesign.getPageHeader().getHeight();
            int columnHeaderHeight = jasperDesign.getColumnHeader().getHeight();
            if(parameters.get("rfc")== null) {
                pageHeaderHeight = 0;
            }
			*/
            //int detailHeight = jasperDesign.getDetailSection().getBands()[0].getHeight();
            //int sumaryHeight = jasperDesign.getSummary().getHeight();
            //int allBandsHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight + sumaryHeight;
            //int exactPageHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight * numReg + sumaryHeight;
            
            //System.out.println("Ok, JasperDesign created: pageHeight="+pageHeight+", pageHeaderHeight="+pageHeaderHeight+", columnHeaderHeight="+columnHeaderHeight+", detailHeight="+detailHeight+", allBandsHeight="+allBandsHeight+", exactPageHeight="+exactPageHeight);
            //jasperDesign.setPageHeight(exactPageHeight);
            //System.out.println("\t=>JasperDesign pageHeight="+jasperDesign.getPageHeight());
            
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            System.out.println("Ok, JasperReport compiled: pageHeight="+jasperReport.getPageHeight());            
            
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
            System.out.println("Ok, JasperPrint created");
            //JasperExportManager.exportReportToPdfFile(jasperPrint, "jasper_out_"+sdf.format(new Date())+".pdf");
            //System.out.println("Ok, JasperExportManager executed");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
			System.out.println("Ok, JasperExportManager executed");
			pdfBytes = baos.toByteArray();
            //JasperPrintManager.printReport(jasperPrint, false);
            //System.out.println("Ok, printed. executed");                        
            
            System.out.println("Ok, finished");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            System.exit(1);
        }
		return pdfBytes;
    }
    /*
    static String source[] = "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per. Ius id vidit volumus mandamus, vide veritus democritum te nec, ei eos debet libris consulatu. No mei ferri graeco dicunt, ad cum veri accommodare. Sed at malis omnesque delicata, usu et iusto zzril meliore. Dicunt maiorum eloquentiam cum cu, sit summo dolor essent te. Ne quodsi nusquam legendos has, ea dicit voluptua eloquentiam pro, ad sit quas qualisque. Eos vocibus deserunt quaestio ei. Blandit incorrupte quaerendum in quo, nibh impedit id vis, vel no nullam semper audiam".split(" ");
    private static String biuldRandomName(int maxLength){
        String name = null;
        
        StringBuffer sb = new StringBuffer();
        Random r=new Random();
        while ( sb.length() <= maxLength) {
            sb.append(source[r.nextInt(source.length)]);
            sb.append(" ");
        }
        name = sb.toString().replace("."," ").replace(",","").toUpperCase().trim();
        return name;
    }
	*/
	/**
	 * @param pedidoVentaJpaController the pedidoVentaJpaController to set
	 */
	public void setPedidoVentaJpaController(PedidoVentaJpaController pedidoVentaJpaController) {
		this.pedidoVentaJpaController = pedidoVentaJpaController;
	}
}
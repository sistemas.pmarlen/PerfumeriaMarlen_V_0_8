/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.tasks;

import com.pmarlen.model.controller.SessionInfoController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author alfredo
 */
@Service("updateSessionInfoTask")
public class UpdateSessionInfoTask {

	private Logger logger;
	
	private SessionInfoController sessionInfoController;
	
	public UpdateSessionInfoTask() {
		logger = LoggerFactory.getLogger(UpdateSessionInfoTask.class);
		logger.debug("->created");
	}

	@Async
	//@Scheduled(fixedRate=60000)
	@Scheduled(cron = "*/60 * * * * *")
	public void invokeUpdateSessionInfoTask() {
		logger.debug("===>>>invokeUpdateSessionInfoTask: sessionInfoController.killOldest()");
		sessionInfoController.killOldest();
	}

	/**
	 * @param sessionInfoController the sessionInfoController to set
	 */
	@Autowired
	public void setSessionInfoController(SessionInfoController sessionInfoController) {
		this.sessionInfoController = sessionInfoController;
	}
}

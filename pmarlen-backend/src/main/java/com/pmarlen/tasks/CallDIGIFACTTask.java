/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.tasks;

import com.pmarlen.model.beans.PedidoVenta;
import javax.persistence.EntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author alfredo
 */
@Service("callDIGIFACTTask")
public class CallDIGIFACTTask {

	private EntityManagerFactory emf = null;
	private Logger logger;
	
	public CallDIGIFACTTask() {
		logger = LoggerFactory.getLogger(CallDIGIFACTTask.class);
		logger.debug("->CallDIGIFACTTask, created");
	}

	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Async
	public void invokeDIGIFACTWebService(PedidoVenta pedidoVenta) {
		logger.debug("->CallDIGIFACTTask===>>>invokeDIGIFACTWebService: pedidoVenta="+pedidoVenta);
		try{
			for(int i=0;i<10;i++){
				logger.debug("\t->CallDIGIFACTTask.invokeDIGIFACTWebService===>>>["+i+"] ...wait");
				Thread.sleep(2000);
			}	
			logger.debug("\t->CallDIGIFACTTask.invokeDIGIFACTWebService<<------END TASK");
		} catch(InterruptedException ie){
			logger.error("Interrupted for in invokeDIGIFACTWebService:", ie);
		}	
	}
}

ExploreDB:
	
	mvn exec:java -Dexec.mainClass=com.pmarlen.client.ExploreDB

RM CLEAN:
	rm derby.log ; rm logFile.log ; rm SEPOMEX_DATA.txt ; rm syncronization.bin ; rm -rf PMarlen_DB;

RunApp:
	mvn clean package exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -P preprod -Ddevelopment_host=perfumeriamarlen.dyndns.org -Ddevelopment_port=1080
	mvn clean package -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -P devinjob
	
	mvn clean package exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -Dexec.args="" -P preprod -Dproduction_host=192.168.1.82  -Dproduction_port=1080
	mvn clean package exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -Dexec.args="" -P preprod -Dproduction_host=192.168.0.20  -Dproduction_port=1080
	mvn clean package exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -Dexec.args="" -P preprod -Dproduction_host=192.168.0.20  -Dproduction_port=1080
	mvn clean package exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.DBStarter -Dexec.args="" -P preprod -Dproduction_host=192.168.0.20  -Dproduction_port=1080


	mvn exec:java -Dmaven.test.skip=true -Dexec.mainClass=com.pmarlen.client.Main -Dexec.args="-runningAsSlave=true -masterHost=192.168.1.84 -numCaja=10 -P preprod"

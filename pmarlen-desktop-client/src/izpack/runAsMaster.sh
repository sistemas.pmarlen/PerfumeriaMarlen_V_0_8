#! /bin/sh

NOW=$(date +"%Y%m%d_%H%M%S")

java -jar PerfumeriaMarlen_app.jar  -masterHost=`/sbin/ifconfig -a | grep "inet:" |awk 'BEGIN{}{i1=index($0,"inet:");ineta=substr($0,i1+5);i2=index(ineta," ");ias=substr(ineta,1,i2-1);if(ias != "127.0.0.1"){ia=ias}}END{printf("%s",ia);}'` > "stdout_$NOW.txt" 2> "stderr_$NOW.txt"

AFTER_RUN=$(date +"%Y%m%d_%H%M")

tar cvfz "PMarlen_DB_$AFTER_RUN.tgz" PMarlen_DB derby.log 

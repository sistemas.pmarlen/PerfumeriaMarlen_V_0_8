/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.controller.PreferencesController;
import com.pmarlen.client.model.DesgloseImportePedidoVenta;
import com.pmarlen.client.ticketprinter.TicketPrinteService;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.tracktopell.testbluecovemaven.SendBytesToDevice;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alfredo
 */
public class TicketBlueToothPrinter implements TicketPrinteService {

	private static final String SPACES = "                                                    ";
	public static final String DEFAULT_BT_PRINTER = "00:03:7A:6D:14:BC";
	private static final String TICKET_TEST = "/ticket_layout/TICKET_TEST.txt";
	private static final String TICKET_LAYOUT_FILE = "/ticket_layout/TICKET_DEFINITION_SUC_2.txt";
	private static SimpleDateFormat sdf_fecha_full = new SimpleDateFormat("yyyyMMdd_hhmmss");
	private static SimpleDateFormat sdf_fecha = new SimpleDateFormat("EEE dd/MMM/yyyy");
	private static SimpleDateFormat sdf_hora = new SimpleDateFormat("HH:mm");
	private static int maxCharsPerColumns = 48;
	public static final String BT_PRINTER_MODE = "Bluetooth";
	public static final String TERMAL_PRINTER_MODE = "TermalPOS";
	private String btAdress;
	private ApplicationLogic applicationLogic;

	public TicketBlueToothPrinter() {
	}

	public String getBtAdress() {
		if (applicationLogic != null) {
			btAdress = applicationLogic.getPreferences().getProperty(PreferencesController.PRINTERBLUETOOTHADDRESS);
		}
		return btAdress;
	}

	@Override
	public Object generateNuevoTicket(PedidoVenta pedidoVenta, HashMap<String, String> extraInformation) throws IOException {
		String tiketPrinted = null;
		PrintStream psPrintTicket = null;


		InputStream is = TicketBlueToothPrinter.class.getResourceAsStream(TICKET_LAYOUT_FILE);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		try {
			final DesgloseImportePedidoVenta calculaDesgloseImporte = applicationLogic.calculaDesgloseImporte(pedidoVenta);

			String line = null;
			Date fecha = new Date();

			String ticketInComentarios = "";
			String noAprobacionInComentarios = "";
			String recibidoInComentarios = "";
			String numCajaInComentarios = "1";

			if (pedidoVenta.getComentarios() != null) {
				String comentarios = pedidoVenta.getComentarios();

				HashMap<String, String> extraInfo = applicationLogic.getExtraInfoInComments(comentarios);
				ticketInComentarios = extraInfo.get("ticket");
				noAprobacionInComentarios = extraInfo.get("aprobacion");
				recibidoInComentarios = extraInfo.get("recibido");
				numCajaInComentarios = extraInfo.get("caja");
			}
			tiketPrinted = "TICKET_" + ticketInComentarios + "_" + sdf_fecha_full.format(fecha) + ".TXT";
			psPrintTicket = new PrintStream(new File(tiketPrinted), "ISO-8859-1");
			Date fechaReporte = new Date();
			try {
				fechaReporte = pedidoVenta.getPedidoVentaEstadoCollection().iterator().next().getFecha();
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
			HashMap<String, String> staticVars = new HashMap();
			staticVars.put("${TICKET}", ticketInComentarios);
			staticVars.put("${CAJA}", "#" + numCajaInComentarios);
			staticVars.put("${FECHA}", sdf_fecha.format(fechaReporte).toUpperCase());
			staticVars.put("${HORA}", sdf_hora.format(fechaReporte));

			staticVars.put("${AGENTE_0}", "CAJERO EN TURNO");
			staticVars.put("${FORMA_PAGO}", pedidoVenta.getFormaDePago().getDescripcion());

			List<String> clienteNombreList = justifyText(pedidoVenta.getCliente().getRazonSocial() != null ? pedidoVenta.getCliente().getRazonSocial()
					: pedidoVenta.getCliente().getNombreEstablecimiento(), 35);
			staticVars.put("${CLIENTE_0}", clienteNombreList.get(0));
			if (clienteNombreList.size() > 1) {
				staticVars.put("?{CLIENTE_1}", clienteNombreList.get(1));
			}
			if (clienteNombreList.size() > 2) {
				staticVars.put("?{CLIENTE_2}", clienteNombreList.get(2));
			}
			final boolean esClienteRegistrado = Boolean.parseBoolean(extraInformation.get("esClienteRegistrado"));
			if (esClienteRegistrado) {
				staticVars.put("?{RFC}", pedidoVenta.getCliente().getRfc());

				String direccion =
						pedidoVenta.getCliente().getCalle() + " "
						+ pedidoVenta.getCliente().getNumExterior() + " "
						+ pedidoVenta.getCliente().getNumInterior() + ", "
						+ pedidoVenta.getCliente().getPoblacion().getTipoAsentamiento() + " "
						+ pedidoVenta.getCliente().getPoblacion().getNombre() + ", "
						+ pedidoVenta.getCliente().getPoblacion().getMunicipioODelegacion() + ", "
						+ pedidoVenta.getCliente().getPoblacion().getEntidadFederativa();

				List<String> direccionList = justifyText(direccion, 35);

				staticVars.put("?{DIRECCION_0}", direccionList.get(0));
				if (direccionList.size() > 1) {
					staticVars.put("?{DIRECCION_1}", direccionList.get(1));
				}
				if (direccionList.size() > 2) {
					staticVars.put("?{DIRECCION_2}", direccionList.get(2));
				}
				if (direccionList.size() > 3) {
					staticVars.put("?{DIRECCION_3}", direccionList.get(3));
				}
			}
			boolean skipLine = false;
			boolean detailStart = false;
			boolean expandDetail = false;
			int numIter = 1;
			List<String> iterationLines = new ArrayList<String>();
			List<PedidoVentaDetalle> pedidoVentaDetalleCollection = new ArrayList<PedidoVentaDetalle>();
			Collection<PedidoVentaDetalle> pedidoVentaDetalleCollectionOriginal = pedidoVenta.getPedidoVentaDetalleCollection();

			for (PedidoVentaDetalle pvdOrig : pedidoVentaDetalleCollectionOriginal) {
				pedidoVentaDetalleCollection.add(pvdOrig);
			}


			while ((line = br.readLine()) != null) {
				if (line.contains("#{")) {
					if (line.contains("#{DETAIL_START}")) {
						detailStart = true;
						iterationLines.clear();
						continue;
					} else if (line.contains("#{DETAIL_END}")) {
						detailStart = false;
						expandDetail = true;
					}
				} else if (!detailStart) {
					iterationLines.clear();
					iterationLines.add(line);
					//System.err.print("#_>>"+iterationLines.size()+"\t");
				}

				if (detailStart) {
					iterationLines.add(line);
					//System.err.println("#=>>"+line);
					continue;
				}

				if (expandDetail) {
					numIter = pedidoVentaDetalleCollection.size();
				} else {
					numIter = 1;
				}
				DecimalFormat df_6 = new DecimalFormat("#####0");
				DecimalFormat df_6_2 = new DecimalFormat("###,##0.00");
				DecimalFormat df_9_2 = new DecimalFormat("###,###,##0.00");
				DecimalFormat dfs9_2 = new DecimalFormat("########0.00");


				double sum_importe = 0.0;
				double importe = 0.0;
				double sum_desc = 0.0;
				double desc = 0.0;

				for (int i = 0; i < numIter; i++) {

					if (expandDetail) {
						staticVars.put("${CANT}", alignTextRigth(df_6.format(pedidoVentaDetalleCollection.get(i).getCantidad()), 4));
						String productoPresentacion = pedidoVentaDetalleCollection.get(i).getProducto().getNombre()
								+ "/" + pedidoVentaDetalleCollection.get(i).getProducto().getPresentacion();
						staticVars.put("${PRDCOD}", alignTextRigth(pedidoVentaDetalleCollection.get(i).getProducto().getCodigoBarras(), 14));
						staticVars.put("${PRODUCTO_DESCRIPCION}", alignTextLeft(productoPresentacion, 27));

						importe = pedidoVentaDetalleCollection.get(i).getCantidad() * pedidoVentaDetalleCollection.get(i).getPrecioVenta();
						sum_importe += importe;
						sum_desc += desc;

						staticVars.put("${PRECIO}", alignTextRigth(df_6_2.format(pedidoVentaDetalleCollection.get(i).getPrecioVenta()), 10));
						//staticVars.put("${DESCDET}", alignTextRigth(df_6_2.format(desc),10));                        
						staticVars.put("${IMPORTE}", alignTextRigth(df_6_2.format(importe), 10));
					}

					for (String innerLine : iterationLines) {

						skipLine = false;
						Iterator<String> ik = staticVars.keySet().iterator();
						while (ik.hasNext()) {
							String k = ik.next();
							if (innerLine.contains(k)) {
								innerLine = innerLine.replace(k, staticVars.get(k));
							}
							//System.err.println("\t\t===>>> replace "+k+" ->"+staticVars.get(k));
						}
						if (innerLine.indexOf("?{") >= 0) {
							String optionalField = innerLine.substring(innerLine.indexOf("?{"), innerLine.indexOf("}"));
							if (!staticVars.containsKey(optionalField)) {
								skipLine = true;
							}
						}
						if (!skipLine) {
							//System.err.println("=>>" + innerLine);
							psPrintTicket.print(innerLine + "\r");
						} else {
							//System.err.println("X=>>" + innerLine);
						}

					}
				}

				if (expandDetail) {
					//System.err.println("#=>>______________");
					expandDetail = false;
					if (calculaDesgloseImporte.getDescuentoAplicado() > 0.0) {
						staticVars.put("?{DESCUENTO}", alignTextRigth(df_9_2.format(calculaDesgloseImporte.getDescuentoAplicado()), 12));
					}
					staticVars.put("${SUBTOTAL}", alignTextRigth(df_9_2.format(calculaDesgloseImporte.getSubTotal()), 12));
					//staticVars.put("${DESCUENTO}", alignTextRigth(df_6_2.format(sum_desc),12));
					double total = sum_importe - sum_desc;
					String strTotal = dfs9_2.format(total);
					staticVars.put("${IVA}", alignTextRigth(df_6_2.format(calculaDesgloseImporte.getImporteIVA()), 12));
					staticVars.put("${TOTAL}", alignTextRigth(df_6_2.format(calculaDesgloseImporte.getTotal()), 12));
					String recibimosOriginal = recibidoInComentarios;
					String recibimos = recibimosOriginal;
					if (recibimos != null && recibimos.trim().length() > 0) {
						recibimos = df_6_2.format(Double.parseDouble(recibimos));
						staticVars.put("?{RECIB}", alignTextRigth(recibimos, 12));
						String suCambio = df_6_2.format(Double.parseDouble(recibimosOriginal) - calculaDesgloseImporte.getTotal());
						staticVars.put("?{CAMBI}", alignTextRigth(suCambio, 12));
					}
					if (noAprobacionInComentarios.length() > 0) {
						staticVars.put("?{NOAPROB}", alignTextRigth(noAprobacionInComentarios, 12));
					}

					String enterosLetra = NumeroCastellano.numeroACastellano(Long.parseLong(strTotal.substring(0, strTotal.indexOf(".")))).toUpperCase().trim();
					String centavosLetra = strTotal.substring(strTotal.indexOf(".") + 1);

					List<String> totalLetraList = justifyText("Son [" + enterosLetra + " PESOS " + centavosLetra + "/100 M.N.]", maxCharsPerColumns);

					staticVars.put("${TOTAL_LETRA_0}", totalLetraList.get(0));
					if (totalLetraList.size() > 1) {
						staticVars.put("?{TOTAL_LETRA_1}", totalLetraList.get(1));
					}
				}
			}
		} catch (IOException ex) {
			//ex.printStackTrace(System.err);
			throw new IllegalStateException("No sepuede generar el Ticket:");
		} finally {
			if (psPrintTicket != null) {
				psPrintTicket.close();
			}
		}

		return tiketPrinted;
	}

	@Override
	public Object generateTicket(PedidoVenta pedidoVenta, HashMap<String, String> extraInformation) throws IOException {
		return null;
	}

	private static String alignTextRigth(String text, int maxPerColumn) {
		try {
			if (text.trim().length() < maxPerColumn) {

				return SPACES.substring(0, maxPerColumn - text.trim().length())
						+ text;
			} else {
				return text.trim().substring(0, maxPerColumn);
			}
		} catch (Exception ex) {
			System.err.println("\t==>>> alignTextRigth: ->" + text.trim() + "<-[" + text.trim().length() + "]," + maxPerColumn + ":" + ex.getMessage());
			return text.trim();
		}
	}

	private static String alignTextLeft(String text, int maxPerColumn) {
		try {
			if (text.trim().length() < maxPerColumn) {
				return text + SPACES.substring(0, maxPerColumn - text.trim().length());
			} else {
				return text.trim().substring(0, maxPerColumn);
			}
		} catch (Exception ex) {
			System.err.println("\t==>>> alignTextLeft: ->" + text.trim() + "<-[" + text.trim().length() + "]," + maxPerColumn + ":" + ex.getMessage());
			return text.trim();
		}
	}

	private static List<String> justifyText(String text, int maxPerColumn) {
		List<String> result = new ArrayList<String>();

		String[] words = text.split("\\s");
		String currentLine = "";

		for (String w : words) {
			if (w.trim().length() == 0) {
				continue;
			}
			if (currentLine.length() + 1 + w.length() <= maxPerColumn) {
				if (currentLine.length() == 0) {
					currentLine = w;
				} else {
					currentLine = currentLine + " " + w;
				}

			} else {
				result.add(currentLine);
				currentLine = w;
			}
		}

		if (currentLine.length() > 0) {
			result.add(currentLine);
			currentLine = "";
		}

		return result;
	}

	@Override
	public void testDefaultPrinter() throws IOException {
		InputStream is = TicketBlueToothPrinter.class.getResourceAsStream(TICKET_TEST);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream psTestPrint = new PrintStream(baos, true, "ISO-8859-1");

		try {
			String line = null;
			Date fecha = new Date();

			HashMap<String, String> staticVars = new HashMap();
			//${FECHA},${HORA}
			staticVars.put("${FECHA}", sdf_fecha.format(fecha));
			staticVars.put("${HORA}", sdf_hora.format(fecha));
			Set<String> keySet = staticVars.keySet();
			for (String key : keySet) {
				System.err.println("\t-->>key:" + key + "=" + staticVars.get(key));
			}
			while ((line = br.readLine()) != null) {
				//final String key = "${FECHA}";
				for (String key : keySet) {
					if (line.contains(key)) {
						line = line.replace(key, staticVars.get(key));
					}
				}
				System.err.println("-->>testDefaultPrinter:" + line);
				psTestPrint.print(line + "\r");
			}
			br.close();
			psTestPrint.close();
			baos.close();

			SendBytesToDevice.print(getBtAdress(), new ByteArrayInputStream(baos.toByteArray()));

		} catch (IOException ex) {
			//ex.printStackTrace(System.err);
			throw new IllegalStateException("No sepuede generar el Ticket:");
		} finally {
		}
	}

	@Override
	public void sendToPrinter(Object ticketFileName) throws IOException {
		SendBytesToDevice.print(getBtAdress(), new FileInputStream((String) ticketFileName));
	}

	/**
	 * @param al the al to set
	 */
	public void setApplicationLogic(ApplicationLogic al) {
		this.applicationLogic = al;
	}
}

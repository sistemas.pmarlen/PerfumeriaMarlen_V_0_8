/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.model.DesgloseImportePedidoVenta;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.tracktopell.dbutil.DBInstaller;
import com.tracktopell.dbutil.DerbyDBInstaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRProperties;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class TicketPOSTermalPrinter implements TicketPrinteService {

	private ApplicationLogic applicationLogic;
	public static final double PROPORCION_REAL = 1175.0 / 415.0;
	public static final double A4_PAGE_HEIGTH = 297.0;
	private Logger logger;

	public TicketPOSTermalPrinter() {
		logger = LoggerFactory.getLogger(TicketPOSTermalPrinter.class);
		logger.debug("->ApplicationLogic, created");
	}

	@Override
	public Object generateTicket(PedidoVenta pedidoVenta, HashMap<String, String> extraInformation) throws IOException {
		return null;		
	}

	private void forcedSetValue(Object obj, String property, Object value) {
		try {
			Class cxb = obj.getClass();

			logger.debug("->class=" + cxb);
			logger.debug("->interfaces=" + Arrays.asList(cxb.getInterfaces()));

			Field fz = cxb.getField(property);
			fz.setAccessible(true);
			Object valueZ = fz.get(obj);

			logger.debug("->valueZ=" + valueZ);

			fz.set(obj, value);
		} catch (Exception e) {
			logger.error("forcedSetValue: failed:", e);
		}

	}

	@Override
	public Object generateNuevoTicket(PedidoVenta pedidoVenta, HashMap<String, String> extraInformation) throws IOException {
		logger.debug("-->>>generateNuevoTicket: ENCODING-bug Ver. 9");
		logger.debug("-->>>generateNuevoTicket:pedidoVenta.id=" + pedidoVenta.getId() + ", usuario=" + pedidoVenta.getUsuario());
		logger.debug("-->>>generateNuevoTicket:pedidoVenta.getDescuentoAplicado=" + pedidoVenta.getDescuentoAplicado());
		JRProperties.setProperty("net.sf.jasperreports.default.pdf.encoding", "UTF-8");
		JRExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
			
		final DesgloseImportePedidoVenta calculaDesgloseImporte = applicationLogic.calculaDesgloseImporte(pedidoVenta);
				
		String fileGenarated = null;
		String reportPath;
		String compiledReportPath;
		String compiledReportDir;
		
		JasperPrint jasperPrint = null;
		reportPath = "/ticket_layout/TicketDesign_SUC_#_CAJA_dynamicSize.jrxml";
		compiledReportDir  = "./ticket_layout/";
		compiledReportPath = compiledReportDir+"TicketDesign_SUC_#_CAJA_dynamicSize.jasper";
		
		long t1, t2, t3, t4, t5;
		t1 = System.currentTimeMillis();
		
		InputStream  inputStreamSource  = null;
		JasperDesign jasperDesignSource = null;
		//JasperReport jasperReportSource = null;
		File         reportPathFile     = null;
		reportPathFile = new File(compiledReportPath);
		if(!reportPathFile.exists()) {
			try {
				File jasperDir = new File(compiledReportDir);
				if(!jasperDir.exists()){
					jasperDir.mkdirs();
				}

				logger.debug("\t=>JasperDesign before load jrxml:"+reportPath);
				inputStreamSource = TicketPOSTermalPrinter.class.getResourceAsStream(reportPath);
				logger.debug("\t=>inputStreamSource =" + inputStreamSource);
				jasperDesignSource = JRXmlLoader.load(inputStreamSource);
				logger.debug("\t=>jasperDesignSource=" + jasperDesignSource);
				//jasperReportSource = JasperCompileManager.compileReport(jasperDesignSource);
				//logger.debug("\t=>jasperReportSource=" + jasperReportSource);				
				JasperCompileManager.compileReportToFile(jasperDesignSource, compiledReportPath);
				logger.debug("\t=>Ok, compiled to File:"+reportPathFile);
			} catch (JRException ex) {
				throw new IOException("No se puede compilar el Jasper Report:" + reportPathFile + ":" + ex.getMessage());
			}
		} else {
			logger.debug("\t=>Compiled Jasper Report already exsist !");
		}
			
		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();

		DecimalFormat df = new DecimalFormat("$###,###,###,##0.00");
		DecimalFormat dfEnt = new DecimalFormat("###########0.00");
		
		double p = 0.0;
		int n;
		int numReg = 0;
		Random rand = new Random(System.currentTimeMillis());
		Collection<PedidoVentaDetalle> pedidoVentaDetalleCollection = pedidoVenta.getPedidoVentaDetalleCollection();
		numReg = pedidoVentaDetalleCollection.size();
		logger.debug("->generateNuevoTicket: numReg=" + numReg);

		for (PedidoVentaDetalle pvd : pedidoVentaDetalleCollection) {


			Map<String, Object> vals = new HashMap<String, Object>();

			n = pvd.getCantidad();

			vals.put("cantidad", n);
			vals.put("codigoBarras", pvd.getProducto().getCodigoBarras());
			vals.put("descripcion", pvd.getProducto().getNombre() + "/" + pvd.getProducto().getPresentacion());
			p = pvd.getPrecioVenta();

			vals.put("precio", df.format(p));
			vals.put("importe", df.format(n * p));

			col.add(vals);
		}
		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		logger.debug("Ok, JRDataSource created");

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("EEE dd/MMM/yyyy");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");


		Date fechaReporte = new Date();
		try {
			fechaReporte = pedidoVenta.getPedidoVentaEstadoCollection().iterator().next().getFecha();
		}catch(Exception ex){
			logger.error("->generateNuevoTicket, fechaReporte="+fechaReporte+", no tiene estadoVenta",ex);
		}

		parameters.put("codigoPedido", new Integer(987654112));
		
		//UPDATE FORMA_DE_PAGO SET DESCRIPCION='TARJETA VISA/MASTERCARD' WHERE ID=2;

		String ticketInComentarios = "";
		String noAprobacionInComentarios = "";
		String recibidoInComentarios = "";
		String numCajaInComentarios = "1";

		if (pedidoVenta.getComentarios() != null) {
			String comentarios = pedidoVenta.getComentarios();
			logger.debug("->generateNuevoTicket: comentarios=" + comentarios);
			HashMap<String, String> extraInfo = applicationLogic.getExtraInfoInComments(comentarios);
			ticketInComentarios = extraInfo.get("ticket");
			noAprobacionInComentarios = extraInfo.get("aprobacion");
			recibidoInComentarios = extraInfo.get("recibido");
			numCajaInComentarios = extraInfo.get("caja");
		}

		parameters.put("ticket", ticketInComentarios);
		logger.debug("->generateNuevoTicket: parameter : ticket=" + ticketInComentarios);

		parameters.put("aprobacion", noAprobacionInComentarios);
		logger.debug("->generateNuevoTicket: parameter : aprobacion=" + noAprobacionInComentarios);

		parameters.put("caja", "#" + numCajaInComentarios);
		parameters.put("fecha", sdf_f1.format(fechaReporte).toUpperCase());
		parameters.put("hora", sdf_h1.format(fechaReporte));
		parameters.put("usuario", "CAJERO EN TURNO["+applicationLogic.getSession().getUsuario().getUsuarioId()+"]");
		
		logger.debug("->generateNuevoTicket: parameter : Sucursal:Nombre="+applicationLogic.getSession().getSucursal().getNombre()+", Direccion="+applicationLogic.getSession().getSucursal().getCalle());
		parameters.put("sucNum", "#2");
		parameters.put("sucNombre", "Tecamac CENTRO");
		parameters.put("sucDireccion", "Felipe Villanueva No. 30 Local A, Col. Centro, Tecamac, C.P. 55740, Edo. M\u00E9xico. Tel:55-5934-3788");
				
		parameters.put("cliente", pedidoVenta.getCliente().getRazonSocial());
		final boolean esClienteRegistrado = Boolean.parseBoolean(extraInformation.get("esClienteRegistrado"));
		parameters.put("esClienteRegistrado", esClienteRegistrado);

		parameters.put("rfc", pedidoVenta.getCliente().getRfc());

		parameters.put("direccion",
				pedidoVenta.getCliente().getCalle() + ", "
				+ pedidoVenta.getCliente().getNumExterior() + ", "
				+ pedidoVenta.getCliente().getNumInterior() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getTipoAsentamiento() + " " + pedidoVenta.getCliente().getPoblacion().getNombre() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getMunicipioODelegacion() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getEntidadFederativa() + ", C.P."
				+ pedidoVenta.getCliente().getPoblacion().getCodigoPostal());
		parameters.put("condiciones", pedidoVenta.getFormaDePago().getDescripcion());
		
		parameters.put("subtotal", df.format(calculaDesgloseImporte.getSubTotal()));
		parameters.put("iva", df.format(calculaDesgloseImporte.getImporteIVA()));
		if (calculaDesgloseImporte.getDescuentoAplicado() > 0.0) {
			parameters.put("descuento", df.format(calculaDesgloseImporte.getDescuentoAplicado()));
		} else {
			parameters.put("descuento", "");
		}

		parameters.put("total", df.format(calculaDesgloseImporte.getTotal()));
		if (recibidoInComentarios.length() > 0) {
			double recibimosValue = Double.parseDouble(recibidoInComentarios);
			double cambioValue = recibimosValue - calculaDesgloseImporte.getTotal();
			parameters.put("recibimos", df.format(recibimosValue));
			parameters.put("cambio", df.format(cambioValue));
			logger.debug("->generateNuevoTicket: parameter : recibimos" + df.format(recibimosValue));
			logger.debug("->generateNuevoTicket: parameter : cambio" + df.format(cambioValue));
		} else {
			parameters.put("recibimos", "");
			parameters.put("cambio", "");
		}


		String intDecParts[] = dfEnt.format(calculaDesgloseImporte.getTotal()).split("\\.");

		String letrasParteEntera = NumeroCastellano.numeroACastellano(Long.parseLong(intDecParts[0])).trim();
		
		parameters.put("importeLetra", "Son [" + letrasParteEntera + " Pesos " + intDecParts[1] + "/100 M.N.]");
		String leyendaFooter = "Este no es un comprobante fiscal";
		
		parameters.put("LeyendaFotter", leyendaFooter);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		InputStream inputStream = null;
			
		try {
			t2 = System.currentTimeMillis();
		
			//------------------------------------------------------------------
			JasperReport compiledJasperReport = null;

			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			inputStream = new FileInputStream(compiledReportPath);
			logger.debug("\t=>stream loaded ("+compiledReportPath+") =" + inputStream);			
			compiledJasperReport = (JasperReport) JRLoader.loadObject(inputStream);
			logger.debug("\t=>compiledJasperReport=" + compiledJasperReport);
			t3 = System.currentTimeMillis();
		
			//------------------------------------------------------------------
			int pageHeight = compiledJasperReport.getPageHeight(); //jasperReport.getPageHeight();
			int pageWidth = compiledJasperReport.getPageWidth(); //jasperReport.getPageHeight();

			int titleHeight = compiledJasperReport.getTitle().getHeight();
			int pageHeaderHeight = compiledJasperReport.getPageHeader().getHeight();
			int columnHeaderHeight = compiledJasperReport.getColumnHeader().getHeight();
			int pageFooterHeight = compiledJasperReport.getPageFooter()!=null?compiledJasperReport.getPageFooter().getHeight():0;

			if (!esClienteRegistrado) {
				pageHeaderHeight = 0;
			}
			int detailHeight = compiledJasperReport.getDetailSection().getBands()[0].getHeight();
			int sumaryHeight = compiledJasperReport.getSummary().getHeight();

			int allBandsHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight + sumaryHeight + pageFooterHeight;
			int exactPageHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight * numReg + sumaryHeight + pageFooterHeight;
			int maxPagedPageHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight * 20 + sumaryHeight + pageFooterHeight;

			double exactPageHeightMM = (exactPageHeight / PROPORCION_REAL);

			logger.debug("Ok, JasperDesign created:numReg=" + numReg + ", allBandsHeight=" + allBandsHeight + ",pageHeight=" + pageHeight + ", pageWidth=" + pageWidth + ", maxPagedPageHeight=" + maxPagedPageHeight + ", proporcionReal=" + PROPORCION_REAL);

			logger.debug("Ok, JasperDesign created: "
					+ "  \n+ titleHeight       =\t" + titleHeight
					+ ", \n+ pageHeaderHeight  =\t" + pageHeaderHeight
					+ ", \n+ columnHeaderHeight=\t" + columnHeaderHeight
					+ ", \n+ detailHeight * n  =\t" + detailHeight + " * " + numReg + " = " + (detailHeight * numReg)
					+ ", \n+ sumaryHeight      =\t" + sumaryHeight
					+ ", \n+ pageFooterHeight  =\t" + pageFooterHeight
					+ "  \n = exactPageHeight  =\t" + exactPageHeight
					+ "  \n = Real PageHeight  =\t" + exactPageHeightMM + " mm");
			
			logger.debug("\t==>Before fillReport, with params="+parameters);
			jasperPrint = JasperFillManager.fillReport(compiledJasperReport, parameters, beanColDataSource);
			
			logger.debug("\t=>JasperPrint set "+exactPageHeight+" to PageHeight=" + jasperPrint.getPageHeight());
			jasperPrint.setPageHeight(exactPageHeight);
			logger.debug("\t=>JasperPrint and after set getPageHeight=" + jasperPrint.getPageHeight());
			
			t4 = System.currentTimeMillis();
			logger.debug("\t=>Time enlapsed:" + (t4 - t1));

			fileGenarated = "TicketPedidoVenta_" + sdf.format(new Date()) + ".pdf";
			final String fileGenaratedPDF = fileGenarated;
			final JasperPrint jasperPrintPDF = jasperPrint;
			new Thread() {
				@Override
				public void run() {
					try {
						boolean internalExporToPDF = false;
						if (internalExporToPDF) {
							logger.debug("\t=>Internal, exportReportToPdfFile -->>" + fileGenaratedPDF);
							JasperExportManager.exportReportToPdfFile(jasperPrintPDF, fileGenaratedPDF);
							logger.debug("\t=>End inernal export");
						}
					} catch (Exception ex) {
						ex.printStackTrace(System.err);
					}
				}
			}.start();
			//---------------------------------------------------------
			t5=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after export: DT="+(t5-t4));			
			/*
			logger.debug("Ok, JasperExportManager.printReport call:");
			JasperPrintManager.printReport(jasperPrint, false);
			logger.debug("Ok, printed. executed");
			*/
		} catch (JRException ex) {
			logger.error("Al generar el Ticket:", ex);
			throw new IOException("Al generar el Ticket:" + ex);
		}
		//logger.debug("OK, then to print real PageHeigth : "+(jasperPrint.getPageHeight()/PROPORCION_REAL));
		return jasperPrint;
	}
	public boolean needsSpecialPrintJob(JasperPrint jasperPrint) {
		return ((jasperPrint.getPageHeight() / PROPORCION_REAL) > A4_PAGE_HEIGTH * 1.02);
	}

	public int getRealPrintPageHeight(JasperPrint jasperPrint) {
		return (int) (jasperPrint.getPageHeight() / PROPORCION_REAL);
	}

	@Override
	public void sendToPrinter(Object objectToPrint) throws IOException {
		try {
			JasperPrint jasperPrint = (JasperPrint) objectToPrint;
			boolean needsSpecialPrintJob = needsSpecialPrintJob(jasperPrint);
			logger.debug("sendToPrinter: needsSpecialPrintJob?" + needsSpecialPrintJob + ", getRealPrintPageHeight=" + getRealPrintPageHeight(jasperPrint));
			if (needsSpecialPrintJob) {
				sendSpeciaPrintJob(jasperPrint);
			} else {
				Process printProcess = Runtime.getRuntime().exec(new String[]{"/usr/bin/lpr","linea.txt"});
				JasperPrintManager.printReport(jasperPrint, false);
			}
			logger.debug("sendToPrinter: printed job sent");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw new IOException("Al enviar el Ticket a la Impresora:" + ex.getMessage());
		}
	}

	private void sendSpeciaPrintJob(JasperPrint jasperPrint) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String fileGenarated = "TicketPedidoVenta_" + sdf.format(new Date()) + ".pdf";
		final String fileGenaratedPDF = fileGenarated;
		logger.debug("\t=>sendSpeciaPrintJob");
		JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenaratedPDF);
		logger.debug("\t=>sendSpeciaPrintJob, ok PDF");
		int realPageHeight = getRealPrintPageHeight(jasperPrint);
		Process processPrint = Runtime.getRuntime().exec("/usr/bin/lp -o media=Custom.80x" + realPageHeight + "mm " + fileGenarated);
		processPrint.waitFor();
	}

	@Override
	public void testDefaultPrinter() throws IOException {
		logger.debug("-->>>generateTicket: testDefaultPrinter");

		JasperPrint jasperPrint = null;
		String reportPath = "/ticket_layout/TestPrint_SUC_2.jrxml";

		JRDataSource beanColDataSource = new JREmptyDataSource();

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");

		Date fechaReporte = new Date();

		parameters.put("fecha", sdf_f1.format(fechaReporte));
		parameters.put("hora", sdf_h1.format(fechaReporte));

		JasperDesign jasperDesign = null;
		long t1, t2, t3, t4, t5;
		t1 = System.currentTimeMillis();
		InputStream inputStream = null;
		try {
			logger.debug("\t=>JasperDesign before loar jrxml");
			inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(reportPath);
			logger.debug("\t=>stream loaded =" + inputStream);
			jasperDesign = JRXmlLoader.load(inputStream);
		} catch (JRException ex) {
			throw new IOException("No s epuede cargar el Jasper Report:" + reportPath + ":" + ex.getMessage());
		}
		JasperReport jasperReport = null;
		try {
			t2 = System.currentTimeMillis();
			logger.debug("\t=>Before Load Compiled: DT=" + (t2 - t1));
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			//logger.debug("\t=>stream loaded ="+inputStream);	
			//jasperReport = (JasperReport) JRLoader.loadObject(inputStream);

			t3 = System.currentTimeMillis();
			logger.debug("\t=>JasperDesign after load compiled: DT=" + (t3 - t2));

			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4 = System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT=" + (t4 - t3));
			String fileGenarated = "TestPrint_SUC_2_" + sdf.format(new Date()) + ".pdf";
			JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenarated);
			//---------------------------------------------------------
			JasperPrintManager.printReport(jasperPrint, false);
			logger.debug("Ok, printed. executed");
		} catch (JRException ex) {
			throw new IOException("Al generar el Ticket:" + ex);
		}

	}
	
	private static String toUnicodeScapedStirng(String input){
		String result = null;
		StringBuilder sb =  new StringBuilder();		
		char[] ia = input.toCharArray();
		int i=0;		
		for(char c: ia){
			if(c > 160 && c< 733){
				sb.append("\\u00").append(Integer.toHexString(c).toUpperCase());
			} else {
				sb.append(c);
			}
			i++;
		}
		
		return sb.toString();
	}
	

	@Override
	public void setApplicationLogic(ApplicationLogic al) {
		applicationLogic = al;
	}
	
	public static void main(String[] args) {
		DBInstaller dbInstaller;
        try {
			
			String masterHost = null;
			String query = "SELECT ID FROM PEDIDO_VENTA WHERE COMENTARIOS LIKE '%90011579140681%'";
			
            dbInstaller = new DerbyDBInstaller("classpath:/jdbc.properties",masterHost);
			System.out.println("----->> OK, executing the query: ->"+query+"<-");
			List<HashMap<String, Object>> result = dbInstaller.shellDB(query);
			System.out.println("----->> done:");
			for(HashMap<String, Object> rowMap: result){
				System.out.println(rowMap.toString());
			}
			System.exit(0);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
	}
}

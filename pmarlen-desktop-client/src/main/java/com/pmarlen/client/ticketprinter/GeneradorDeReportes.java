/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class GeneradorDeReportes {
	private static Logger logger = LoggerFactory.getLogger(GeneradorDeReportes.class);
	
	public static String generateReporteVentasXTicket(Object[][] datos, String fechaInicial,String fechaFinal,String usuario,String total) throws IOException {
		logger.debug("-->>>generateReporteVentasXTicket: usuario="+usuario+", datos.length="+datos.length);

		String fileGenarated = null;
		String reportPath,compiledReportPath,reportsDir;
		JasperPrint jasperPrint = null;
		reportPath			= "/reports/reporteVentasXTicket.jrxml";
		reportsDir          = "./reports/";
		compiledReportPath	= reportsDir+"reporteVentasXTicket.jasper";
		
		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		DecimalFormat dfEnt = new DecimalFormat("###########0.00");
		Map<String, Object> vals = null;
		for(Object[] row:datos){
				

			vals = new HashMap<String, Object>();

			vals.put("almacen",			row[0]);
			vals.put("usuario",			row[1]);
			vals.put("cliente",			row[2]);
			vals.put("formaDePago",		row[3]);
			vals.put("numTicket",	    row[5]);
			vals.put("fecha_hora",		row[8]);
			try{
				vals.put("importe",			df.parse((String)row[9] ).doubleValue());
				vals.put("descuento",		df.parse((String)row[10]).doubleValue());
				vals.put("total",			df.parse((String)row[11]).doubleValue());
			}catch(ParseException e){
				logger.error("generateReporteVentasXTicket",e);
				vals.put("importe",			0.0);
				vals.put("descuento",		0.0);
				vals.put("total",			0.0);			
			}
			String fechaExp = row[8].toString().substring(0,10);
			vals.put("fecha",			fechaExp);

			col.add(vals);
		}

		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		logger.debug("Ok, JRDataSource created");

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");

		Date fechaReporte = new Date();
		
		parameters.put("fecha",			sdf_f1.format(fechaReporte));
		parameters.put("hora",			sdf_h1.format(fechaReporte));
		parameters.put("usuario",		usuario);		
		parameters.put("fechaInicial",	fechaInicial);
		parameters.put("fechaFinal",	fechaFinal);
		parameters.put("total",			total);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		InputStream inputStream = null;
		
		JasperDesign jasperDesign = null;
		JasperReport jasperReport = null;
				
		long t1,t2,t3,t4,t5;
		t1=System.currentTimeMillis();
		try {
			File reportCompiledFile = new File(compiledReportPath);
			if(!reportCompiledFile.exists()){
				File reportsDirFile = new File (reportsDir);
				if(!reportsDirFile.exists()){
					reportsDirFile.mkdirs();
				}
				logger.debug("\t=>JasperDesign before load jrxml:"+reportPath);			
				inputStream = GeneradorDeReportes.class.getResourceAsStream(reportPath);
				logger.debug("\t=>stream loaded ="+inputStream);	
				jasperDesign = JRXmlLoader.load(inputStream);
				logger.debug("Ok, JRDataSource preprared, to Load Jasper : from"+compiledReportPath);
				t2=System.currentTimeMillis();
				logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));			
				jasperReport = JasperCompileManager.compileReport(jasperDesign);
				logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));							
				JasperCompileManager.compileReportToFile(jasperDesign, compiledReportPath);
				logger.debug("\t=>OK, report compiled from jasperDesign ("+reportPath+") and compiled into: "+compiledReportPath);
			}else {
				inputStream = new FileInputStream(compiledReportPath);
				
				jasperReport = (JasperReport) JRLoader.loadObject(inputStream);					
				logger.debug("\t=>OK, report loaded from File loaded ="+compiledReportPath);
				t2=System.currentTimeMillis();
			}
			
			t3=System.currentTimeMillis();
			logger.debug("\t=>after load report: DT="+(t3-t1));
			
			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT="+(t4-t3));			
			fileGenarated = "reporteVentasXTicket_" + sdf.format(new Date()) + ".pdf";
			JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenarated);
			
		} catch (JRException ex) {
			logger.error("generateReporteVentasXTicket:",ex);
			throw new IOException("Al generar el Reporte" + ex.getMessage());			
		}
		return fileGenarated;
	}

	public static String generateReporteVentasXProducto(Object[][] datos, String fechaInicial,String fechaFinal,String usuario,String total) throws IOException {
		logger.debug("-->>>generateReporteVentasXProducto: usuario="+usuario+", datos.length="+datos.length);

		String fileGenarated = null;
		String reportPath,compiledReportPath;
		JasperPrint jasperPrint = null;
		reportPath			= "/reports/reporteVentasXProducto.jrxml";
		compiledReportPath	= "/reports/reporteVentasXProducto.jasper";

		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		DecimalFormat df    = new DecimalFormat("$###,###,###,##0.00");
		DecimalFormat dfEnt = new DecimalFormat("###########0.00");
		Map<String, Object> vals = null;
		for(Object[] row:datos){
				

			vals = new HashMap<String, Object>();

			vals.put("cant",			row[0]);
			vals.put("codigo",			row[1]);
			vals.put("nombre",			row[2]);
			vals.put("presentacion",	row[3]);
			vals.put("tipo",			row[4]);
			vals.put("importe",			row[5]);

			col.add(vals);
		}

		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		logger.debug("Ok, JRDataSource created");

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");

		Date fechaReporte = new Date();
		
		parameters.put("fecha",			sdf_f1.format(fechaReporte));
		parameters.put("hora",			sdf_h1.format(fechaReporte));
		parameters.put("usuario",		usuario);		
		parameters.put("fechaInicial",	fechaInicial);
		parameters.put("fechaFinal",	fechaFinal);
		parameters.put("total",			total);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		InputStream inputStream = null;
		
		JasperDesign jasperDesign = null;
		long t1,t2,t3,t4,t5;
		t1=System.currentTimeMillis();
		try {
			logger.debug("\t=>JasperDesign before load jrxml:"+reportPath);			
			inputStream = GeneradorDeReportes.class.getResourceAsStream(reportPath);
			logger.debug("\t=>stream loaded ="+inputStream);	
			jasperDesign = JRXmlLoader.load(inputStream);
			logger.debug("Ok, JRDataSource preprared, to Load Jasper : from"+compiledReportPath);
			JasperReport jasperReport = null;
			t2=System.currentTimeMillis();
			logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));			
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			//logger.debug("\t=>stream loaded ="+inputStream);	
			//jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
			
			t3=System.currentTimeMillis();
			logger.debug("\t=>JasperDesign after load compiled: DT="+(t3-t2));
			
			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT="+(t4-t3));			
			fileGenarated = "reporteVentasXProducto_" + sdf.format(new Date()) + ".pdf";
			JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenarated);
			
		} catch (JRException ex) {
			logger.error("generateReporteVentasXProducto:",ex);
			throw new IOException("Al generar el Reporte" + ex.getMessage());			
		}
		return fileGenarated;
	}

	public static String generateReporteVentasXUsuario(Object[][] datos, String fechaInicial,String fechaFinal,String usuario,String total) throws IOException {
		logger.debug("-->>>generateReporteVentasXUsuario: usuario="+usuario+", datos.length="+datos.length);

		String fileGenarated = null;
		String reportPath,compiledReportPath;
		JasperPrint jasperPrint = null;
		reportPath			= "/reports/reporteVentasXUsuario.jrxml";
		compiledReportPath	= "/reports/reporteVentasXUsuario.jasper";

		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		DecimalFormat df    = new DecimalFormat("$###,###,###,##0.00");
		DecimalFormat dfEnt = new DecimalFormat("###########0.00");
		Map<String, Object> vals = null;
		for(Object[] row:datos){
				

			vals = new HashMap<String, Object>();

			vals.put("nombre",			row[0]);
			vals.put("usuario",			row[1]);
			vals.put("tipo",			row[2]);
			vals.put("fecha",			row[3]);			
			vals.put("importe",			row[4]);

			col.add(vals);
		}

		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		logger.debug("Ok, JRDataSource created");

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");

		Date fechaReporte = new Date();
		
		parameters.put("fecha",			sdf_f1.format(fechaReporte));
		parameters.put("hora",			sdf_h1.format(fechaReporte));
		parameters.put("usuario",		usuario);		
		parameters.put("fechaInicial",	fechaInicial);
		parameters.put("fechaFinal",	fechaFinal);
		parameters.put("total",			total);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		InputStream inputStream = null;
		
		JasperDesign jasperDesign = null;
		long t1,t2,t3,t4,t5;
		t1=System.currentTimeMillis();
		try {
			logger.debug("\t=>JasperDesign before load jrxml:"+reportPath);			
			inputStream = GeneradorDeReportes.class.getResourceAsStream(reportPath);
			logger.debug("\t=>stream loaded ="+inputStream);	
			jasperDesign = JRXmlLoader.load(inputStream);
			logger.debug("Ok, JRDataSource preprared, to Load Jasper : from"+compiledReportPath);
			JasperReport jasperReport = null;
			t2=System.currentTimeMillis();
			logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));			
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			//logger.debug("\t=>stream loaded ="+inputStream);	
			//jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
			
			t3=System.currentTimeMillis();
			logger.debug("\t=>JasperDesign after load compiled: DT="+(t3-t2));
			
			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT="+(t4-t3));			
			fileGenarated = "reporteVentasXProducto_" + sdf.format(new Date()) + ".pdf";
			JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenarated);
			
		} catch (JRException ex) {
			logger.error("generateReporteVentasXUsuario:",ex);
			throw new IOException("Al generar el Reporte" + ex.getMessage());			
		}
		return fileGenarated;
	}	
}

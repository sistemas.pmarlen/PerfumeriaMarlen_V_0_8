/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import java.io.Serializable;
import net.sf.jasperreports.engine.JRExpressionCollector;
import net.sf.jasperreports.engine.JRReport;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.base.JRBaseObjectFactory;
import org.codehaus.groovy.classgen.genArrayAccess;
/**
 *
 * @author alfredo
 */
public class JasperReportDynamicPageHeight extends JasperReport{

	private int modifiedPageHeight;
	
	public JasperReportDynamicPageHeight(JasperReport _super,JRBaseObjectFactory factory, String compileNameSuffix){
		super(_super, _super.getCompilerClass(), _super.getCompileData(), factory,  compileNameSuffix);
		modifiedPageHeight =  _super.getPageHeight();
	}
	
	public void setPageHeight(int param_0){
		modifiedPageHeight = param_0;
	}
	
	//public class JasperReportDynamicPageHeight extends class net.sf.jasperreports.engine.JasperReport {
	
	net.sf.jasperreports.engine.JasperReport _super;
	// Methods from class net.sf.jasperreports.engine.JasperReport
	public java.lang.String getCompilerClass(){
		return _super.getCompilerClass();
	}
	public java.io.Serializable getCompileData(){
		return _super.getCompileData();
	}
	public java.lang.String getCompileNameSuffix(){
		return _super.getCompileNameSuffix();
	}

	// Methods from class net.sf.jasperreports.engine.base.JRBaseReport

	public void setProperty(java.lang.String param_0,java.lang.String param_1){
		_super.setProperty(param_0,param_1);
	}
	public java.lang.String getProperty(java.lang.String param_0){
		return _super.getProperty(param_0);
	}
	public java.lang.String getName(){
		return _super.getName();
	}
	public java.lang.String getLanguage(){
		return _super.getLanguage();
	}
	public net.sf.jasperreports.engine.JRField[] getFields(){
		return _super.getFields();
	}
	public net.sf.jasperreports.engine.JRQuery getQuery(){
		return _super.getQuery();
	}
	public boolean isTitleNewPage(){
		return _super.isTitleNewPage();
	}
	public boolean isSummaryNewPage(){
		return _super.isSummaryNewPage();
	}
	public boolean isSummaryWithPageHeaderAndFooter(){
		return _super.isSummaryWithPageHeaderAndFooter();
	}
	public boolean isFloatColumnFooter(){
		return _super.isFloatColumnFooter();
	}
	//protected void copyTemplates(net.sf.jasperreports.engine.JRReport param_0,net.sf.jasperreports.engine.base.JRBaseObjectFactory param_1){
	//	_super.copyTemplates(param_0,param_1);
	//}
	public int getColumnCount(){
		return _super.getColumnCount();
	}
	public net.sf.jasperreports.engine.type.PrintOrderEnum getPrintOrderValue(){
		return _super.getPrintOrderValue();
	}
	public net.sf.jasperreports.engine.type.RunDirectionEnum getColumnDirection(){
		return _super.getColumnDirection();
	}
	public int getPageWidth(){
		return _super.getPageWidth();
	}
	public int getPageHeight(){
		//return _super.getPageHeight();
		return modifiedPageHeight;
	}
	public net.sf.jasperreports.engine.type.OrientationEnum getOrientationValue(){
		return _super.getOrientationValue();
	}
	public net.sf.jasperreports.engine.type.WhenNoDataTypeEnum getWhenNoDataTypeValue(){
		return _super.getWhenNoDataTypeValue();
	}
	public void setWhenNoDataType(net.sf.jasperreports.engine.type.WhenNoDataTypeEnum param_0){
		_super.setWhenNoDataType(param_0);
	}
	public int getColumnWidth(){
		return _super.getColumnWidth();
	}
	public int getColumnSpacing(){
		return _super.getColumnSpacing();
	}
	public int getLeftMargin(){
		return _super.getLeftMargin();
	}
	public int getRightMargin(){
		return _super.getRightMargin();
	}
	public int getTopMargin(){
		return _super.getTopMargin();
	}
	public int getBottomMargin(){
		return _super.getBottomMargin();
	}
	public java.lang.String getScriptletClass(){
		return _super.getScriptletClass();
	}
	public java.lang.String getFormatFactoryClass(){
		return _super.getFormatFactoryClass();
	}
	public java.lang.String getResourceBundle(){
		return _super.getResourceBundle();
	}
	public java.lang.String[] getPropertyNames(){
		return _super.getPropertyNames();
	}
	public void removeProperty(java.lang.String param_0){
		_super.removeProperty(param_0);
	}
	public java.lang.String[] getImports(){
		return _super.getImports();
	}
	public net.sf.jasperreports.engine.JRStyle getDefaultStyle(){
		return _super.getDefaultStyle();
	}
	public net.sf.jasperreports.engine.JRStyle[] getStyles(){
		return _super.getStyles();
	}
	public net.sf.jasperreports.engine.JRScriptlet[] getScriptlets(){
		return _super.getScriptlets();
	}
	public net.sf.jasperreports.engine.JRParameter[] getParameters(){
		return _super.getParameters();
	}
	public net.sf.jasperreports.engine.JRSortField[] getSortFields(){
		return _super.getSortFields();
	}
	public net.sf.jasperreports.engine.JRVariable[] getVariables(){
		return _super.getVariables();
	}
	public net.sf.jasperreports.engine.JRGroup[] getGroups(){
		return _super.getGroups();
	}
	public net.sf.jasperreports.engine.JRBand getBackground(){
		return _super.getBackground();
	}
	public net.sf.jasperreports.engine.JRBand getTitle(){
		return _super.getTitle();
	}
	public net.sf.jasperreports.engine.JRBand getPageHeader(){
		return _super.getPageHeader();
	}
	public net.sf.jasperreports.engine.JRBand getColumnHeader(){
		return _super.getColumnHeader();
	}
	public net.sf.jasperreports.engine.JRSection getDetailSection(){
		return _super.getDetailSection();
	}
	public net.sf.jasperreports.engine.JRBand getColumnFooter(){
		return _super.getColumnFooter();
	}
	public net.sf.jasperreports.engine.JRBand getPageFooter(){
		return _super.getPageFooter();
	}
	public net.sf.jasperreports.engine.JRBand getLastPageFooter(){
		return _super.getLastPageFooter();
	}
	public net.sf.jasperreports.engine.JRBand getSummary(){
		return _super.getSummary();
	}
	public net.sf.jasperreports.engine.type.WhenResourceMissingTypeEnum getWhenResourceMissingTypeValue(){
		return _super.getWhenResourceMissingTypeValue();
	}
	public void setWhenResourceMissingType(net.sf.jasperreports.engine.type.WhenResourceMissingTypeEnum param_0){
		_super.setWhenResourceMissingType(param_0);
	}
	public net.sf.jasperreports.engine.JRDataset getMainDataset(){
		return _super.getMainDataset();
	}
	public net.sf.jasperreports.engine.JRDataset[] getDatasets(){
		return _super.getDatasets();
	}
	public boolean isIgnorePagination(){
		return _super.isIgnorePagination();
	}
	public boolean hasProperties(){
		return _super.hasProperties();
	}
	public net.sf.jasperreports.engine.JRPropertiesMap getPropertiesMap(){
		return _super.getPropertiesMap();
	}
	public net.sf.jasperreports.engine.JRPropertiesHolder getParentProperties(){
		return _super.getParentProperties();
	}
	public net.sf.jasperreports.engine.JRReportTemplate[] getTemplates(){
		return _super.getTemplates();
	}
	public net.sf.jasperreports.engine.JRBand getNoData(){
		return _super.getNoData();
	}
	public net.sf.jasperreports.engine.JRBand[] getAllBands(){
		return _super.getAllBands();
	}
	public java.util.UUID getUUID(){
		return _super.getUUID();
	}
	public net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport getEventSupport(){
		return _super.getEventSupport();
	}
	
	
	
}
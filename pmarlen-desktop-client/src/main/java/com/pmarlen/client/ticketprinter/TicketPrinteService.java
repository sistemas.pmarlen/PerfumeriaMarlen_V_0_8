/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.model.beans.PedidoVenta;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author alfredo
 */
public interface TicketPrinteService {
    Object generateTicket(PedidoVenta pedidoVenta,HashMap<String,String> extraInformation) throws IOException ;
    Object generateNuevoTicket(PedidoVenta pedidoVenta,HashMap<String,String> extraInformation) throws IOException ;
    void sendToPrinter(Object objectToPrint) throws IOException ;
    void testDefaultPrinter() throws IOException;
	void setApplicationLogic(ApplicationLogic al);
}

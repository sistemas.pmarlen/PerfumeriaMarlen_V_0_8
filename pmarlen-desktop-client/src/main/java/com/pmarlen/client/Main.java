/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.client.controller.LoginControl;
import com.pmarlen.client.model.ApplicationSession;
import com.pmarlen.client.view.SplashWindow;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.Perfil;
import com.pmarlen.model.beans.Usuario;
import com.pmarlen.model.controller.UsuarioJpaController;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractXmlApplicationContext;

/**
 *
 * @author praxis
 */
public class Main {
	
    private static Logger logger = null;//LoggerFactory.getLogger(Main.class);   
    private static SplashWindow splashWindow;
    
    public static void main(String[] args) {
		logger = LoggerFactory.getLogger(Main.class);
		
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        Date currDate      = new Date();
		String masterHost  = null;
		String printerType = null;
		String btAdress    = null;
		String numCaja     = "1";
		int numCajaParsed  = 1;
		boolean hideSplash = false;
		
		int countArgs=0;

		boolean runningAsSlave   = false;
		boolean justPrintVersion =false;
		logger.info("==> 0:main:"); 
		for (String arg : args) {
			logger.info("\t==> 0.1:main args[" + (countArgs++) + "]=" + arg);
			if (arg.equalsIgnoreCase("-noSplash")) {
				hideSplash = true;
			} else if (arg.startsWith("-masterHost=")) {
				masterHost = arg.split("=")[1];
			} else if (arg.startsWith("-runningAsSlave=true")) {
				runningAsSlave = true;
//				} else if( arg.startsWith("-printerType=")){
//					printerType = arg.split("=")[1];
//				} else if( arg.startsWith("-btAdress=")){
//					btAdress = arg.split("=")[1];
			} else if (arg.startsWith("-numCaja=")) {
				numCaja = arg.split("=")[1];
				numCajaParsed = Integer.parseInt(numCaja);
			} else if (arg.startsWith("-version")) {
				justPrintVersion = true;
			}
		}

		if (args.length == 1 && justPrintVersion) {
			System.out.println(Constants.getServerVersion());
			System.exit(0);
		}
		logger.debug("==> 0.9: DEBUG is activated!" );
		
		logger.info("==> 1:start:" );
		logger.debug("==> 1.1: JVM currDate=" + sdf.format(currDate));

        ApplicationStarter applicationStarter = null;

        try {
            splashWindow = new SplashWindow(ApplicationInfo.getInstance().getVersion());
			
            if(!hideSplash) {       
		        splashWindow.centerInScreenAndSetVisible();
            } else {
				logger.debug("\t=>>we don't display Splash");
			}
						
			applicationStarter = ApplicationStarter.getInstance();

			
            applicationStarter.beginProcess(splashWindow,masterHost,runningAsSlave);
        } catch (BusinessException e) {
			e.printStackTrace(System.err);
            logger.error("Error: =" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, e.getMessage(), e.getTitle(), JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (UpdateBugFixingException e) {
			//e.printStackTrace(System.err);
			//logger.error("UpdateBugFixing:" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, "Acción a relizar:"+e.getMessage(),"Checar Versión",  JOptionPane.WARNING_MESSAGE);
			applicationStarter.updateBugFixing();
			JOptionPane.showMessageDialog(splashWindow, "Actualización descargada e instalada, \nPor favor, reinicie la aplicación.", "Actualización", JOptionPane.INFORMATION_MESSAGE);
			System.exit(2);
		} catch (UpdateInminentException e) {
            //e.printStackTrace(System.err);
			//logger.error("UpdateInminent:" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, "Actualizacion inminente:"+e.getMessage(), "Checar Versión", JOptionPane.WARNING_MESSAGE);            
			System.exit(3);
        } catch (Exception e) {			
			e.printStackTrace(System.err);
            logger.error("Error: lm2=" + e, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, e.getMessage(), "Incio", JOptionPane.ERROR_MESSAGE);
            System.exit(3);	
        }
        logger.debug("==>> 2: Ready to login.");

        AbstractXmlApplicationContext context = applicationStarter.getContext();

        logger.debug("==>> 3: context is null ?"+(context == null));
		int numLoopsToWaitContext = 0;
		int maxLoopsToWaitContext = 30;
		try{
			for(numLoopsToWaitContext = 0;numLoopsToWaitContext<maxLoopsToWaitContext;numLoopsToWaitContext++){
				logger.debug("\t==>> 3."+numLoopsToWaitContext+": ...waiting context to be ready"); 
				Thread.sleep(1000);
				if(context.isActive() && context.isRunning()){
					logger.debug("\t==>> 3."+numLoopsToWaitContext+": ok ready !");
					break;
				}
			}
		} catch(InterruptedException ie){
			logger.error("someone interrupted", ie);
		}
		
		
        UsuarioJpaController usuarioJpaController = context.getBean("usuarioJpaController", UsuarioJpaController.class);        
        logger.debug("==>> 4: usuarioJpaController=" + usuarioJpaController);
        ApplicationLogic applicationLogic = context.getBean("applicationLogic", ApplicationLogic.class);        
        logger.debug("==>> 5: applicationLogic=" + applicationLogic+", numCajaParsed="+numCajaParsed);
		logger.debug("\t==>> 5.5: applicationLogic.getSession()=" + applicationLogic.getSession());
		ApplicationSession applicationSession = context.getBean("applicationSession", ApplicationSession.class);
		logger.debug("\t==>> 5.6: applicationSession=" + applicationSession);
		if(applicationLogic.getSession() != null){
			applicationLogic.getSession().setNumCaja(numCajaParsed);
			logger.debug("==>> 6: Ok, normally numCaja assigned to="+applicationLogic.getSession().getNumCaja());
		} else {
			applicationLogic.setApplicationSession(applicationSession);
			logger.debug("==>> 5.7: applicationLogic.getSession()=" + applicationLogic.getSession());
			applicationLogic.getSession().setNumCaja(numCajaParsed);	
			logger.debug("==>> 5.8: Ok, forced numCaja assigned to="+applicationLogic.getSession().getNumCaja());
		}
				
//		if(printerType!=null && printerType.equalsIgnoreCase("T")){
//			logger.debug("==>> 5.1: cambiarTipoImpresionTermicaPOS");
//			applicationLogic.cambiarTipoImpresionTermicaPOS();
//		} else if(printerType!=null && printerType.equalsIgnoreCase("B")){
//			logger.debug("==>> 5.2: cambiarTipoImpresionBlueTooth");
//			applicationLogic.cambiarTipoImpresionBlueTooth();
//			if(btAdress != null){
//				logger.debug("==>> 5.2.1: setBTAdress: btAdress <="+btAdress);
//				applicationLogic.getPreferences().
//						setProperty(PreferencesController.PRINTERBLUETOOTHADDRESS, btAdress);
//				applicationLogic.updatePreferences();
//			}
//		} 
        
        List<Usuario> findUsuarioEntities = usuarioJpaController.findUsuarioEntities();

        for (Usuario usuario : findUsuarioEntities) {
            try {
                //logger.debug("\t==>> 5: findUsuarioEntities[]:" + BeanUtils.describe(usuario));
                //logger.debug("\t==>> 5: findUsuarioEntities[]:" + usuario+", perfiles="+usuario.getPerfilCollection()+", pedidoVentaCollection="+usuario.getPedidoVentaCollection());
                logger.debug("\t=>> 7. findUsuarioEntities: For Usuario:"+usuario);
                Collection<Perfil> perfilCollection = usuario.getPerfilCollection();
                for(Perfil perfil: perfilCollection){
                    logger.debug("\t\t=>> 7.1. fecthing Perfil:"+perfil);
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        splashWindow.setVisible(false);
        splashWindow.dispose();
        
        LoginControl loginControl = context.getBean("loginControl", LoginControl.class);        

        loginControl.estadoInicial();
    }
}

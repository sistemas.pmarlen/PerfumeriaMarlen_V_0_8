/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.client.model.ApplicationSession;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.UsuarioJpaController;
import com.tracktopell.dbutil.DBInstaller;
import com.tracktopell.dbutil.DerbyDBInstaller;
import java.io.*;
import java.util.Collection;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import javax.persistence.Query;
import javax.xml.ws.WebServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractXmlApplicationContext;

/**
 *
 * @author alfred
 */
public class ApplicationStarter {

	private AbstractXmlApplicationContext context;

	public static ApplicationStarter getInstance() {
		if (instance == null) {
			instance = new ApplicationStarter();
		}
		return instance;
	}
	private Logger logger;
	private static ApplicationStarter instance;
	private int loginAttempt;

	private ApplicationStarter() {
		logger = LoggerFactory.getLogger(ApplicationStarter.class);
		logger.debug("->ApplicationStarter, created");
		loginAttempt = 0;
	}

	public void beginProcess(ProgressProcessListener pl, String masterHost, boolean runningAsSlave) throws BusinessException, UpdateInminentException, UpdateBugFixingException {
		logger.debug("->beginProcess()");

		logger.debug("->beginProcess():Application Version=" + ApplicationInfo.getInstance().getVersion());

		pl.updateProgress(10, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_LOAD"));

		if(logger.isDebugEnabled()){
			DBInstaller.logDebug=true;
		}
		DBInstaller dbInstaller = null;
		String classpathjdbcproperties = null;
		if (masterHost != null) {
			classpathjdbcproperties = "classpath:/jdbc_derbynet.properties";
		} else {
			classpathjdbcproperties = "classpath:/jdbc.properties";
		}


		try {

			dbInstaller = new DerbyDBInstaller(classpathjdbcproperties, masterHost);
		} catch (IOException e) {
			throw new BusinessException("beginProcess", "Can't crerate DB:" + e.getMessage());
		}

		if (!dbInstaller.existDB()) {
			pl.updateProgress(15, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_BUILD_DB"));
			dbInstaller.installDBfromScratch();
			logger.debug("->beginProcess():=================>After install: load contex to update Hibernate extra tables");

			InputStreamXmlApplicationContext context_update = new InputStreamXmlApplicationContext(changeContextByMasterHost("/application-context_update.xml", masterHost));
			logger.debug("->beginProcess():=================>After install: shutdown Context");
			context_update.registerShutdownHook();
			context_update.close();
			logger.debug("->beginProcess():=================>After install: OK close context ?");
			context_update = null;
			logger.debug("->beginProcess():=================>After install: OK load the normal context");

		}

		pl.updateProgress(20, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_START_PERSISTENCE"));

		logger.debug("->beginProcess():prepared to load the Spring Context");

		context = new InputStreamXmlApplicationContext(changeContextByMasterHost("/application-context.xml", masterHost));

		logger.debug("->beginProcess():Ok, Context Loaded.");

		PedidoVentaDetalle pvd = null;

		FirstDataSynchronizer firstDataSynchronizer = (FirstDataSynchronizer) context.getBean("firstDataSynchronizer", FirstDataSynchronizer.class);
		/*
		try {
			firstDataSynchronizer.checkServerVersion();
		} catch (UpdateInminentException ex) {
			//logger.warn("->beginProcess():", ex);
			throw ex;
		} catch (UpdateBugFixingException ex) {
			//logger.warn("->beginProcess():", ex);			
			throw ex;
		} catch (IllegalStateException ex) {
			logger.error("->beginProcess():", ex);
			//throw new BusinessException("Coneccion con servidor", "FAIL_CHECK_SERVER_VERSION");
		} catch (WebServiceException ex) {
			logger.error("->beginProcess():", ex);
			//throw new BusinessException("Coneccion con servidor", "FAIL_WEBSERVICE_CONNECTION");
		}
		*/

		pl.updateProgress(23, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TEST_SYNC"));
		//boolean needSyncronization = firstDataSynchronizer.needSyncronization();
		boolean needSyncronization = false;
		if (!runningAsSlave && needSyncronization) {
			logger.debug("->beginProcess():Ok, need Syncronization DB on Master Host");
			try {
				firstDataSynchronizer.firstSyncronization(pl);
			} catch (Exception ex) {
				//logger.error("main:beginProcess", ex);
				ex.printStackTrace(System.err);
				throw new BusinessException(getClass().getSimpleName(), "FAIL_FIRST_SINCHRONIZATION");
			}
		} else {
			pl.updateProgress(30, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_STARTING"));
		}
	}

	private InputStream changeContextByMasterHost(String pathContext, String masterHost) {
		InputStream is = null;

		InputStream isOriginalContext = getClass().getResourceAsStream(pathContext);
		logger.debug("->beginProcess():isOriginalContext=" + isOriginalContext);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pr = new PrintWriter(baos);
		BufferedReader br = new BufferedReader(new InputStreamReader(isOriginalContext));
		String line = null;
		try {
			logger.debug("->beginProcess(): begin to change context with: masterHost=" + masterHost);
			while ((line = br.readLine()) != null) {
				if (line.contains("jdbc:derby:PMarlen_DB") && masterHost != null) {
					line = line.replace("jdbc:derby:PMarlen_DB", "jdbc:derby://" + masterHost + ":1527/PMarlen_DB");
					//logger.debug("\t*>beginProcess(): must fucking canged to->"+line+"<-");					
				}
				pr.println(line);
				logger.debug("\t->beginProcess(): context: line->" + line + "<-");
			}
			pr.close();
			baos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace(System.err);
		}
		byte[] baContext = baos.toByteArray();
		logger.debug("->beginProcess(): baContext=>" + baContext.length);
		is = new ByteArrayInputStream(baContext);

		return is;
	}

	/**
	 * @return the context
	 */
	public AbstractXmlApplicationContext getContext() {
		return context;
	}

	public ApplicationSession login(String id, String password) throws Exception {

		logger.debug("->login: ");
		Query q = null;
		int r = -1;
		ApplicationSession as = null;
		try {
			if (++loginAttempt > 3) {
				throw new IllegalStateException("Mouch Attempts to login");
			}

			UsuarioJpaController usuarioJpaController = context.getBean("usuarioJpaController", UsuarioJpaController.class);

			logger.debug("->asigned user&password: " + id + ", " + password);
			Usuario u = usuarioJpaController.authenticate(id, password);
			logger.debug("--->can be authenticated ? u=" + u);
			if (u == null) {
				throw new IllegalStateException("Usuario o password incorrecto");
			}

			as = (ApplicationSession) context.getBean("applicationSession", ApplicationSession.class);
			logger.debug("--->>> ApplicationSession = " + as);

			as.setUsuario(u);
			as.setRealPassword(password);

			ApplicationLogic applicationLogic = context.getBean("applicationLogic", ApplicationLogic.class);

			Sucursal sucursal = u.getSucursal();
			logger.debug("--->>> previous Sucursal asigned = " + sucursal);
			if (sucursal == null) {
				sucursal = applicationLogic.getSucursalMatriz();
				logger.debug("--->>> Sucursal Matriz asigned");
			} else {
				sucursal = applicationLogic.getSucursal(sucursal.getId());
				if(sucursal.getId() == 4) {
					as.setSoloVentaDeLinea(true);
				}
			}
			logger.debug("--->>>Session setting to Sucursal=" + sucursal.getId());
			as.setSucursal(sucursal);

			Collection<Almacen> almacenCollection = sucursal.getAlmacenCollection();
			logger.debug("\t--->>> Session setting to Almacen:" + almacenCollection);
			for (Almacen almacen : almacenCollection) {
				if (almacen.getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
					logger.debug("\t--->>> Session setting to Almacen PRINCIPAL:" + almacen.getId());
					as.setAlmacen(almacen);
					Collection<AlmacenProducto> almacenProductoCollection = almacen.getAlmacenProductoCollection();

					break;
				}
			}

			applicationLogic.setApplicationSession(as);
			logger.debug("---------------------------------------->>>login, ok");
		} catch (Exception ex) {
			logger.error("login:", ex);
			throw ex;
		}
		return as;
	}

	public void updateBugFixing() {
		InputStream updateBuildDataZipIS = null;
		final String updatE_BUILDzip = "./UPDATE_BUILD.zip";
		try {
			updateBuildDataZipIS = WebServiceConnectionConfig.getInstance().getUpdateBuildDataZip();
			
			FileOutputStream fos = new FileOutputStream(updatE_BUILDzip);
			byte[] buffer = new byte[1024 * 32];
			int r = -1;
			while ((r = updateBuildDataZipIS.read(buffer, 0, buffer.length)) != -1) {
				fos.write(buffer, 0, r);
				fos.flush();
			}
			updateBuildDataZipIS.close();
			fos.close();
		} catch (IOException ex) {
			throw new IllegalStateException("Can't download UPDATE data package:"+ex.getMessage());
		}
		try {
			extractFolder(updatE_BUILDzip);
		} catch (IOException ex) {
			throw new IllegalStateException("Can't extract & deflate UPDATE data paclkage:"+ex.getMessage());
		}

	}

	static public void extractFolder(String zipFile) throws ZipException, IOException {
		System.out.println(zipFile);
		int BUFFER = 2048;
		File file = new File(zipFile);

		ZipFile zip = new ZipFile(file);
		String destPathToInflate = ".";

		Enumeration zipFileEntries = zip.entries();

		// Process each entry
		while (zipFileEntries.hasMoreElements()) {
			// grab a zip file entry
			ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
			String currentEntry = entry.getName();
			File destFile = new File(destPathToInflate, currentEntry);
			//destFile = new File(newPath, destFile.getName());
			File destinationParent = destFile.getParentFile();

			// create the parent directory structure if needed
			destinationParent.mkdirs();

			if (!entry.isDirectory()) {
				BufferedInputStream is = new BufferedInputStream(zip
						.getInputStream(entry));
				int currentByte;
				// establish buffer for writing file
				byte data[] = new byte[BUFFER];

				// write the current file to disk
				FileOutputStream fos = new FileOutputStream(destFile);
				BufferedOutputStream dest = new BufferedOutputStream(fos,
						BUFFER);

				// read and write until last byte is encountered
				while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
					dest.write(data, 0, currentByte);
				}
				dest.flush();
				dest.close();
				is.close();
			}
		}
	}
}

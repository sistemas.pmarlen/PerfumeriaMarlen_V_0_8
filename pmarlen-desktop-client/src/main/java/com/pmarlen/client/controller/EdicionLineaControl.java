/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.model.EntidadFederativa;
import com.pmarlen.client.model.ValidatorHelper;
import com.pmarlen.client.view.EdicionLineaDialog;
import com.pmarlen.model.beans.Linea;
import com.pmarlen.model.beans.Poblacion;
import com.pmarlen.model.controller.BasicInfoDAO;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("edicionLineaControl")
public class EdicionLineaControl {
    private Logger logger;

    @Autowired
    private ApplicationLogic applicationLogic;
    
    @Autowired
    private PrincipalControl principalControl;

    @Autowired
    private BasicInfoDAO basicInfoDAO;
	
    Linea linea;
	
    EdicionLineaDialog edicionLineaDialog;
    private int exitStatus;
    boolean selectingByPoblacion = false;
    JComponentValidator validator;

    public EdicionLineaControl() {
        logger = LoggerFactory.getLogger(EdicionLineaControl.class);
    }
    public void setup() {
        edicionLineaDialog = new EdicionLineaDialog(principalControl.getPrincipalForm());

        validator = new JComponentValidator(edicionLineaDialog, ApplicationInfo.getLocalizedMessage("DLG_EDIT_LINEA_MSG_TITLE")) ;

        //validator.add(new ValidatorHelper(edicionLineaDialog.getNombre(), "[a-zA-Z]{2}[a-zA-Z0-9\\. ]{5,126}", true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_RAZON_SOCIAL"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
        
        edicionLineaDialog.getGuardar().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        edicionLineaDialog.getCancelar().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });
        
        edicionLineaDialog.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                cancelar();
            }
        });
        
        edicionLineaDialog.getRootPane().setDefaultButton(edicionLineaDialog.getGuardar());

    }

    void setLinea(Linea c) {
        this.linea = c;
    }

    public void editarLinea(Linea lineaEditar) {
        linea = lineaEditar;
        estadoInicial();
    }

    public void crearLinea() {
        linea = new Linea();

        linea.setNombre("");

		estadoInicial();
    }

    void estadoInicial() {        
        if(edicionLineaDialog==null){
            setup();
        }        
        edicionLineaDialog.getNombre().setText(linea.getNombre() != null && linea.getNombre().trim().length() == 0 ? linea.getNombre() : "");
        
		edicionLineaDialog.getGuardar().setEnabled(true);
        edicionLineaDialog.getCancelar().setEnabled(true);

        if (!edicionLineaDialog.isVisible()) {
            centerInScreenAndSetVisible(edicionLineaDialog);
        }
    }

    void fillLinea() throws Exception {
		edicionLineaDialog.getNombre().setText(edicionLineaDialog.getNombre().getText().trim().toUpperCase());
        linea.setNombre(edicionLineaDialog.getNombre().getText());
    }

    public void aceptar() {
        logger.debug("[ACEPTAR]");
        try {			
            if ( validator.validate() ) {
				
                fillLinea();
                logger.debug("==>>aceptar(): Linea="+linea.getId());
                applicationLogic.persistLinea(linea);  
                
                edicionLineaDialog.setVisible(false);
                exitStatus = JOptionPane.OK_OPTION;
            }
        } catch (BusinessException ex) {
			logger.error("-->> despues de aceptar guardar BusinessException:", ex);
            JOptionPane.showMessageDialog(edicionLineaDialog,
                    ex.getMessage(),
                    ex.getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
			logger.error("-->> despues de aceptar guardar Exception:", ex);
            JOptionPane.showMessageDialog(edicionLineaDialog,
					ex.getMessage(),
                    BusinessException.getLocalizedMessage("APP_LOGIC_CLIENTE_NOT_SAVED"),                    
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void cancelar() {
        edicionLineaDialog.setVisible(false);
        exitStatus = JOptionPane.CANCEL_OPTION;
    }
    
    void centerInScreenAndSetVisible(JDialog w) {
        int fw = w.getWidth();
        int fh = w.getHeight();
        Rectangle recScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        w.setBounds(((int) recScreen.getWidth() - fw) / 2, ((int) recScreen.getHeight() - fh) / 2,
                fw, fh);
        w.setVisible(true);
    }

    /**
     * @return the exitStatus
     */
    public int getExitStatus() {
        return exitStatus;
    }

    /**
     * @return the applicationLogic
     */
    public ApplicationLogic getApplicationLogic() {
        return applicationLogic;
    }

    /**
     * @param applicationLogic the applicationLogic to set
     */
    public void setApplicationLogic(ApplicationLogic applicationLogic) {
        this.applicationLogic = applicationLogic;
    }

    /**
     * @return the principalControl
     */
    public PrincipalControl getPrincipalControl() {
        return principalControl;
    }

    /**
     * @param principalControl the principalControl to set
     */
    public void setPrincipalControl(PrincipalControl principalControl) {
        this.principalControl = principalControl;
    }

    /**
     * @return the basicInfoDAO
     */
    public BasicInfoDAO getBasicInfoDAO() {
        return basicInfoDAO;
    }

    /**
     * @param basicInfoDAO the basicInfoDAO to set
     */
    public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
        this.basicInfoDAO = basicInfoDAO;
    }

	Linea getLinea() {
		return linea;
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.ticketprinter.TicketBlueToothPrinter;
import com.pmarlen.client.view.PreferencesModalDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class PreferencesController implements ActionListener{
	public static final String PRINTERBLUETOOTHADDRESS  = "printer.bluetooth.address";
	public static final String PRINTER_MODE				= "printer.mode";
	private PreferencesModalDialog pd =  null;
	private ApplicationLogic al;
	private Properties preferences = null;
	private static Logger logger = LoggerFactory.getLogger(PreferencesController.class);

	private PreferencesController(JFrame parent, ApplicationLogic al){
		this.pd =  new PreferencesModalDialog(parent);
		this.pd.getGuardarBtn().addActionListener(this);
		this.al = al;		
		this.preferences = al.getPreferences();
		String propBTAddress = preferences.getProperty(PRINTERBLUETOOTHADDRESS,TicketBlueToothPrinter.DEFAULT_BT_PRINTER);
		logger.debug("read properties -> propPrinterMode= "+propBTAddress);		
		this.pd.getDirBluetoothPrinter().setText(propBTAddress);
		String propPrinterMode = preferences.getProperty(PRINTER_MODE,TicketBlueToothPrinter.TERMAL_PRINTER_MODE);
		
		logger.debug("read properties -> propPrinterMode= "+propPrinterMode);
		if(propPrinterMode.equals(TicketBlueToothPrinter.BT_PRINTER_MODE)){
			this.pd.getImprBlueToothRadioBtn().setSelected(true);
		} else if(propPrinterMode.equals(TicketBlueToothPrinter.TERMAL_PRINTER_MODE)){			
			this.pd.getImprTermicaRadioBtn().setSelected(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			validateFields();
			this.preferences.setProperty(PRINTERBLUETOOTHADDRESS, this.pd.getDirBluetoothPrinter().getText().trim());			
			logger.debug("Guardar Preferences: "+PRINTERBLUETOOTHADDRESS+" <="+this.pd.getDirBluetoothPrinter().getText().trim());
			
			if(this.pd.getImprTermicaRadioBtn().isSelected()){
				this.preferences.setProperty(PRINTER_MODE, TicketBlueToothPrinter.TERMAL_PRINTER_MODE);
				logger.debug("Guardar Preferences: printer.bluetooth.address <="+TicketBlueToothPrinter.TERMAL_PRINTER_MODE);

			}else if(this.pd.getImprBlueToothRadioBtn().isSelected()){
				this.preferences.setProperty(PRINTER_MODE, TicketBlueToothPrinter.BT_PRINTER_MODE);
				logger.debug("Guardar Preferences: printer.bluetooth.address <="+TicketBlueToothPrinter.BT_PRINTER_MODE);
			}
			
			al.updatePreferences();			
			//al.setBTAdress(this.preferences.getProperty("printer.bluetooth.address"));			
			pd.dispose();
		} catch(IllegalArgumentException ex){
			JOptionPane.showMessageDialog(this.pd, ex.getMessage(), "Guadrar", JOptionPane.ERROR_MESSAGE);
		} catch(IllegalStateException ex){
			JOptionPane.showMessageDialog(this.pd, ex.getMessage(), "Guadrar", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void validateFields() throws IllegalArgumentException{
		if(this.pd.getDirBluetoothPrinter().getText().trim().length()!=17){
		//if(this.pd.getDirBluetoothPrinter().getText().matches("^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$")){
			throw new IllegalArgumentException("No es una Direccion Bluetooth valida");
		}
	}
	
	public static void showPreferencesDialog(JFrame parent, ApplicationLogic al){		
		PreferencesController pf = new PreferencesController(parent, al);
		pf.pd.setVisible(true);
		pf = null;
	}
	
}

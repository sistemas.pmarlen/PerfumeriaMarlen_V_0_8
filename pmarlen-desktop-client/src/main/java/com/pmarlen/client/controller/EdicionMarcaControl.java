/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.model.EntidadFederativa;
import com.pmarlen.client.model.ValidatorHelper;
import com.pmarlen.client.view.EdicionMarcaDialog;
import com.pmarlen.model.beans.Industria;
import com.pmarlen.model.beans.Linea;
import com.pmarlen.model.beans.Marca;
import com.pmarlen.model.beans.Poblacion;
import com.pmarlen.model.controller.BasicInfoDAO;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("edicionMarcaControl")
public class EdicionMarcaControl {
    private Logger logger;

    @Autowired
    private ApplicationLogic applicationLogic;
    
    @Autowired
    private PrincipalControl principalControl;

    @Autowired
    private BasicInfoDAO basicInfoDAO;
	
    Marca marca;
	
	private Linea linea;
	
	private Industria industria;

    EdicionMarcaDialog edicionMarcaDialog;
    private int exitStatus;
    boolean selectingByPoblacion = false;
    JComponentValidator validator;

    public EdicionMarcaControl() {
        logger = LoggerFactory.getLogger(EdicionMarcaControl.class);
    }
    public void setup() {
        edicionMarcaDialog = new EdicionMarcaDialog(principalControl.getPrincipalForm());

        validator = new JComponentValidator(edicionMarcaDialog, ApplicationInfo.getLocalizedMessage("DLG_EDIT_LINEA_MSG_TITLE")) ;

        //validator.add(new ValidatorHelper(edicionMarcaDialog.getNombre(), "[a-zA-Z]{2}[a-zA-Z0-9\\. ]{5,126}", true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_RAZON_SOCIAL"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
        
        edicionMarcaDialog.getGuardar().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        edicionMarcaDialog.getCancelar().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });
        
        edicionMarcaDialog.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                cancelar();
            }
        });
        
        edicionMarcaDialog.getRootPane().setDefaultButton(edicionMarcaDialog.getGuardar());

    }

    void setMarca(Marca c) {
        this.marca = c;
    }

    public void editarMarca(Marca marcaEditar,Linea lineaEditar,Industria industriaEditar) {
        linea = lineaEditar;
		industria = industriaEditar;
		
		marca = marcaEditar;
		
        estadoInicial();
    }

    public void crearMarca(Linea lineaEditar,Industria industriaEditar) {
        linea = lineaEditar;
		industria = industriaEditar;
		
		marca = new Marca();

        marca.setNombre("");

		estadoInicial();
    }

    void estadoInicial() {        
        if(edicionMarcaDialog==null){
            setup();
        }        
        edicionMarcaDialog.getLineaNombre().setText(linea.getNombre() != null && linea.getNombre().trim().length() == 0 ? linea.getNombre() : "");
        edicionMarcaDialog.getIndustriaNombre().setText(industria.getNombre() != null && industria.getNombre().trim().length() == 0 ? industria.getNombre() : "");
        
		edicionMarcaDialog.getNombre().setText(marca.getNombre() != null && marca.getNombre().trim().length() == 0 ? marca.getNombre() : "");
        
		edicionMarcaDialog.getGuardar().setEnabled(true);
        edicionMarcaDialog.getCancelar().setEnabled(true);

        if (!edicionMarcaDialog.isVisible()) {
            centerInScreenAndSetVisible(edicionMarcaDialog);
        }
    }

    void fillMarca() throws Exception {
		edicionMarcaDialog.getNombre().setText(edicionMarcaDialog.getNombre().getText().trim().toUpperCase());
        marca.setNombre(edicionMarcaDialog.getNombre().getText());
    }

    public void aceptar() {
        logger.debug("[ACEPTAR]");
        try {			
            if ( validator.validate() ) {
				
                fillMarca();
                logger.debug("==>>aceptar(): Marca="+marca.getId());
                applicationLogic.persistMarca(marca);  
                
                edicionMarcaDialog.setVisible(false);
                exitStatus = JOptionPane.OK_OPTION;
            }
        } catch (BusinessException ex) {
			logger.error("-->> despues de aceptar guardar BusinessException:", ex);
            JOptionPane.showMessageDialog(edicionMarcaDialog,
                    ex.getMessage(),
                    ex.getTitle(),
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
			logger.error("-->> despues de aceptar guardar Exception:", ex);
            JOptionPane.showMessageDialog(edicionMarcaDialog,
					ex.getMessage(),
                    BusinessException.getLocalizedMessage("APP_LOGIC_CLIENTE_NOT_SAVED"),                    
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void cancelar() {
        edicionMarcaDialog.setVisible(false);
        exitStatus = JOptionPane.CANCEL_OPTION;
    }
    
    void centerInScreenAndSetVisible(JDialog w) {
        int fw = w.getWidth();
        int fh = w.getHeight();
        Rectangle recScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        w.setBounds(((int) recScreen.getWidth() - fw) / 2, ((int) recScreen.getHeight() - fh) / 2,
                fw, fh);
        w.setVisible(true);
    }

    /**
     * @return the exitStatus
     */
    public int getExitStatus() {
        return exitStatus;
    }

    /**
     * @return the applicationLogic
     */
    public ApplicationLogic getApplicationLogic() {
        return applicationLogic;
    }

    /**
     * @param applicationLogic the applicationLogic to set
     */
    public void setApplicationLogic(ApplicationLogic applicationLogic) {
        this.applicationLogic = applicationLogic;
    }

    /**
     * @return the principalControl
     */
    public PrincipalControl getPrincipalControl() {
        return principalControl;
    }

    /**
     * @param principalControl the principalControl to set
     */
    public void setPrincipalControl(PrincipalControl principalControl) {
        this.principalControl = principalControl;
    }

    /**
     * @return the basicInfoDAO
     */
    public BasicInfoDAO getBasicInfoDAO() {
        return basicInfoDAO;
    }

    /**
     * @param basicInfoDAO the basicInfoDAO to set
     */
    public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
        this.basicInfoDAO = basicInfoDAO;
    }

	Marca getMarca() {
		return marca;
	}

	/**
	 * @param linea the linea to set
	 */
	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	/**
	 * @param industria the industria to set
	 */
	public void setIndustria(Industria industria) {
		this.industria = industria;
	}
}

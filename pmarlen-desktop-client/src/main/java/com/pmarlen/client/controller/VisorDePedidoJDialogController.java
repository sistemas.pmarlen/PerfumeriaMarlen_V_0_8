/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.model.PedidoVentaDetalleTableModel;
import com.pmarlen.client.view.VisorDePedidoJDialog;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfredo
 */
@Controller("visorDePedidoJDialogController")
public class VisorDePedidoJDialogController implements ActionListener {

	private Logger logger;
	@Autowired
	private ApplicationLogic applicationLogic;
	@Autowired
	private PrincipalControl principalControl;
	private PedidoVenta pedidoVenta;
	private VisorDePedidoJDialog visorDePedidoJDialog;

	VisorDePedidoJDialogController() {
		logger = LoggerFactory.getLogger(VisorDePedidoJDialogController.class);
	}

	void setup() {
		if (visorDePedidoJDialog == null) {
			visorDePedidoJDialog = new VisorDePedidoJDialog(principalControl.getPrincipalForm());
			visorDePedidoJDialog.getCerrarBtn().addActionListener(this);
			visorDePedidoJDialog.getReimprimirTicketBtn().addActionListener(this);
		}
	}

	void estadoInicial() {
		setup();
		ArrayList<PedidoVentaDetalle> detallePedidoList= new ArrayList<PedidoVentaDetalle>();
		detallePedidoList.addAll(pedidoVenta.getPedidoVentaDetalleCollection());
		visorDePedidoJDialog.getDetallePedidoTable().setModel(new PedidoVentaDetalleTableModel(detallePedidoList));
		updateDetallePedidoTable();
		visorDePedidoJDialog.setVisible(true);
	}

	/**
	 * @param applicationLogic the applicationLogic to set
	 */
	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}

	/**
	 * @param principalControl the principalControl to set
	 */
	public void setPrincipalControl(PrincipalControl principalControl) {
		this.principalControl = principalControl;
	}

	/**
	 * @param pedidoVenta the pedidoVenta to set
	 */
	public void setPedidoVenta(PedidoVenta pedidoVenta) {
		this.pedidoVenta = pedidoVenta;
	}

	void updateDetallePedidoTable() {
		double st = 0.0;
		double si = 0.0;
		double sd = 0.0;

		double precioVenta = 0.0;
		int nPzs = 0;
		for (PedidoVentaDetalle dvp : this.pedidoVenta.getPedidoVentaDetalleCollection()) {
			precioVenta = dvp.getPrecioVenta();
			nPzs += dvp.getCantidad();
			si += dvp.getCantidad() * precioVenta;
		}

		logger.debug("->updateDetallePedidoTable: applicationLogic.getDescuentoMayoreoActivado()="+applicationLogic.getDescuentoMayoreoActivado());
		
		if (this.pedidoVenta.getDescuentoAplicado() != null && this.pedidoVenta.getDescuentoAplicado().doubleValue() > 0.0){
			if (nPzs >= 12) {
				sd = si * 0.1;
			} else if (si > 100 && si <= 200) {
				sd = si * 0.05;
			} else if (si > 200) {
				sd = si * 0.1;
			}
		}

		double osi = 0.0;
		double osd = 0.0;
		double otax = 0.0;
		double ostt = 0.0;

		osi = si; //* (1.0 + applicationLogic.getFactorIVA());
		osd = sd; //* (1.0 + applicationLogic.getFactorIVA());
		otax = 0.0;
		ostt = (osi - osd);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		
		pedidoVenta.getComentarios();
		String ticketInComentarios       = "";
		String noAprobacionInComentarios = "";
		String recibidoInComentarios     = "";
		String numCajaInComentarios      = "1";
		
		if(pedidoVenta.getComentarios() != null) {
			String comentarios = pedidoVenta.getComentarios();
			logger.debug("->updateDetallePedidoTable: comentarios="+comentarios);
			HashMap<String,String> extraInfo= applicationLogic.getExtraInfoInComments(comentarios);
			logger.debug("->updateDetallePedidoTable: extraInfo="+extraInfo);
			ticketInComentarios			= extraInfo.get("ticket");
			noAprobacionInComentarios	= extraInfo.get("aprobacion");
			recibidoInComentarios		= extraInfo.get("recibido");
			numCajaInComentarios		= extraInfo.get("caja");
		}

		visorDePedidoJDialog.getFechaYHora().setText(sdf.format(pedidoVenta.getPedidoVentaEstadoCollection().iterator().next().getFecha()));		
		visorDePedidoJDialog.getUsuario().setText(pedidoVenta.getUsuario().getNombreCompleto());
		visorDePedidoJDialog.getCliente().setText(pedidoVenta.getCliente().getRazonSocial());
		
		visorDePedidoJDialog.getCaja().setText(numCajaInComentarios);
		visorDePedidoJDialog.setTitle("PedidoVenta Ticket #"+ticketInComentarios);
		
		if(recibidoInComentarios.length()>0){
			visorDePedidoJDialog.getRecibimosOAprobacion().setText("Recibimos :");	
			visorDePedidoJDialog.getCambioLabel().setText("Cambio :");
			visorDePedidoJDialog.getRecibimos().setText(recibidoInComentarios);
			double cambio = Double.parseDouble(recibidoInComentarios) - ostt;
			visorDePedidoJDialog.getCambio().setText(ApplicationInfo.formatToCurrency(cambio));
		} else if(noAprobacionInComentarios.length()>0){
			visorDePedidoJDialog.getRecibimosOAprobacion().setText("No Aprobación :");			
			visorDePedidoJDialog.getCambioLabel().setText("");
			visorDePedidoJDialog.getCambio().setText("");
			visorDePedidoJDialog.getRecibimos().setText(noAprobacionInComentarios);
		} else {
			visorDePedidoJDialog.getRecibimosOAprobacion().setText("");
			visorDePedidoJDialog.getRecibimos().setText("");
			visorDePedidoJDialog.getCambioLabel().setText("");
			visorDePedidoJDialog.getCambio().setText("");
		}
		
		visorDePedidoJDialog.getFormaDePago().setText(pedidoVenta.getFormaDePago().getDescripcion());
		visorDePedidoJDialog.getTotalArticulos().setText(String.valueOf(nPzs));
		visorDePedidoJDialog.getSubtotal().setText(ApplicationInfo.formatToCurrency(osi));
		visorDePedidoJDialog.getDiscount().setText(ApplicationInfo.formatToCurrency(osd));
		visorDePedidoJDialog.getTotal().setText(ApplicationInfo.formatToCurrency(ostt));
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == visorDePedidoJDialog.getCerrarBtn()) {
			visorDePedidoJDialog.dispose();
		} else if (ae.getSource() == visorDePedidoJDialog.getReimprimirTicketBtn()) {
			reimprimirTicketBtn_actionPerformed();
		}
	}

	private void reimprimirTicketBtn_actionPerformed() {
		new Thread() {
			@Override
			public void run() {
				try {
					visorDePedidoJDialog.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					applicationLogic.printTicketPedido(pedidoVenta, true);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(visorDePedidoJDialog, "Hubo un error al reimprimir:" + ex.getMessage(),
							"Reimprimir", JOptionPane.ERROR_MESSAGE);
				} finally {
					visorDePedidoJDialog.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		}.start();
	}
}

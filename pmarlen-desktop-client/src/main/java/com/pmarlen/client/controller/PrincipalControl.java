/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.model.ClienteComboBoxModel;
import com.pmarlen.client.model.ClienteItemList;
import com.pmarlen.client.model.ClientesTableModel;
import com.pmarlen.client.model.FechaComboBoxModel;
import com.pmarlen.client.model.FormaDePagoComboBoxModel;
import com.pmarlen.client.model.FormaDePagoItemList;
import com.pmarlen.client.model.MarcaTreeModelBuilder;
import com.pmarlen.client.model.PedidoVentaDetalleTableModel;
import com.pmarlen.client.model.PrincipalModel;
import com.pmarlen.client.model.ProductoFastDisplayModel;
import com.pmarlen.client.model.ProductoRowModel;
import com.pmarlen.client.model.ProductoTableModel;
import com.pmarlen.client.model.VisorDeProductosDefaultModel;
import com.pmarlen.client.ticketprinter.GeneradorDeReportes;
import com.pmarlen.client.ticketprinter.GeneradorTarjetasPrecios;
import com.pmarlen.client.view.PrincipalForm;
import com.pmarlen.client.view.VisorDeProductosDefaultPanel;
import com.pmarlen.model.Constants;
import com.pmarlen.model.GeneradorNumTicket;
import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.AlmacenProducto;
import com.pmarlen.model.beans.Cliente;
import com.pmarlen.model.beans.FormaDePago;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.pmarlen.model.beans.Perfil;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.controller.BasicInfoDAO;
import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("principalControl")
public class PrincipalControl {

	private PrincipalForm principalForm;
	private PrincipalModel principalModel;
	String currentTextToSearch;
	private Logger logger;
	@Autowired
	private ApplicationLogic applicationLogic;
	@Autowired
	private BasicInfoDAO basicInfoDAO;
	@Autowired
	private MarcaTreeModelBuilder marcaTreeModelBuilder;
	@Autowired
	private EdicionClientesControl edicionClientesControl;
	@Autowired
	private EdicionProductosControl edicionProductosControl;
	@Autowired
	private VisorDePedidoJDialogController visorDePedidoJDialogController;
	@Autowired
	private JDialogActivacionDescuentoMayoreoController jDialogActivacionDescuentoMayoreoController;
	private static final int CAPTURA_MODO_CODIGO_BARRAS = 1;
	private static final int CAPTURA_MODO_NOMBRE = 2;
	private int capturaModo;
	private int tipoReporte;
	private boolean isAdmin   = false;
	private boolean isFinances = false;
	private boolean formaDePagoTCSelected = false;
	private DecimalFormat dfChecadorPrecios;

	public PrincipalControl() {
		logger = LoggerFactory.getLogger(PrincipalControl.class);
	}

//    public static PrincipalControl getInstance() {
//        if (instance == null) {
//            instance = new PrincipalControl();
//        }
//
//        return instance;
//    }
	public void setup() {
		logger.debug("setup():");
		List<Producto> listProductosVacios = null;
		currentTextToSearch = "";
		principalForm = new PrincipalForm();
		principalModel = new PrincipalModel();

		final Collection<Perfil> perfilCollection = applicationLogic.getSession().getUsuario().getPerfilCollection();
		for (Perfil perfil : perfilCollection) {
			if (perfil.getId().equals("admin") || perfil.getId().equals("root")) {
				isAdmin = true;
			} else if(perfil.getId().equals("finances") || perfil.getId().equals("root")) {
				isFinances = true;
			}
		}
		tipoReporte = 0;
		//=====================MENUS=======================
		principalForm.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitByClick();
			}
		});
		principalForm.getExitMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});
		principalForm.getAcercaDeMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:AcercaDeMenu");
				String aboutText = null;
				try{
					aboutText = "Perfumeria Marlen - Caja \n Version: "+ApplicationInfo.getInstance().getVersion()+"\n Desarrollador: Alfredo Estrada";
				} catch(BusinessException be){
					aboutText = "Perfumeria Marlen - Caja \n Desarrollador: Alfredo Estrada";
				}
				new JOptionPane().showMessageDialog(principalForm, aboutText, "Acerca de",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		principalForm.getViewDetailMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:ViewDetailMenu");
				verDetalle();
			}
		});
		principalForm.getBuscarXCBMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:BuscarXCBMenu");
				changeInputMethodCB();
			}
		});
		principalForm.getBuscarXNombreMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:BuscarXNombreMenu");
				changeInputMethodNombre();
			}
		});
		principalForm.getTestPrintMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:BuscarXNombreMenu");
				testPrinter();
			}
		});

		principalForm.getPreferncesMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:PreferncesMenu");
				PreferencesController.showPreferencesDialog(principalForm, applicationLogic);
			}
		});

		principalForm.getPorFechaHoyRadioBtn().setSelected(true);

		principalForm.getPorFechaHoyRadioBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->RADIO_BUTTON:PorFechaHoyRadioBtn");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
				Date today = new Date();
				principalForm.getFechaReporteInicio().setSelectedItem(sdf.format(today));
				principalForm.getFechaReporteFin().setSelectedItem(sdf.format(today));

				principalForm.getFechaReporteInicio().setEnabled(false);
				principalForm.getFechaReporteFin().setEnabled(false);

				principalForm.getBuscarPorNoTicket().setEnabled(false);
				principalForm.getBuscarPorNoTicket().setText("");
			}
		});
		principalForm.getPorIntervaloRadioBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->RADIO_BUTTON:PorIntervaloRadioBtn");
				principalForm.getFechaReporteInicio().setEnabled(true);
				principalForm.getFechaReporteFin().setEnabled(true);

				principalForm.getBuscarPorNoTicket().setEnabled(false);
				principalForm.getBuscarPorNoTicket().setText("");
			}
		});
		principalForm.getPorNoDeTicketRadioBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->RADIO_BUTTON:PorNoDeTicketRadioBtn");
				principalForm.getFechaReporteInicio().setEnabled(false);
				principalForm.getFechaReporteFin().setEnabled(false);

				principalForm.getBuscarPorNoTicket().setEnabled(true);
				principalForm.getBuscarPorNoTicket().setText("");
			}
		});

		principalForm.getExaminarPedidoBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->BUTTON:ExaminarPedidoBtn");
				examinarPedido();
			}
		});

		principalForm.getTablaReporte().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		principalForm.getActualizarReporteBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tipoReporte == 1) {
					principalForm.getExaminarPedidoBtn().setEnabled(true);
					refreshVentasXTicket();
				} else if (tipoReporte == 2) {
					principalForm.getExaminarPedidoBtn().setEnabled(false);
					refreshVentasXUsuario();
				} else if (tipoReporte == 3) {
					principalForm.getExaminarPedidoBtn().setEnabled(false);
					refreshVentasXProducto();
				}
			}
		});
		principalForm.getBuscarPorNoTicket().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				refreshVentasXTicket();
				principalForm.getTablaReporte().getSelectionModel().setSelectionInterval(0, 0);
				examinarPedido();
			}
		});

		dfChecadorPrecios = new DecimalFormat("###,###,##0.00");
		
		principalForm.getDescuentoMayoreo().setEnabled(false);
		principalForm.getVerVentasXTicket().setEnabled(false);
		principalForm.getVerVentasXUsuario().setEnabled(false);
		principalForm.getVerVentasXProducto().setEnabled(false);
		principalForm.getGenerarPDFBtn().setEnabled(false);
		principalForm.getGeneradorTarjetasPrecios().setEnabled(false);
		principalForm.getPrecioEncontrado().setEditable(false);
		principalForm.getCantidadRecargar().setEditable(false);
		principalForm.getCantidadRecargar().setVisible(false);		
		principalForm.getActualizarPrecioBtn().setVisible(false);
		principalForm.getActualizarPrecioBtn().remove(principalForm.getActualizarPrecioBtn());
		if(applicationLogic.getApplicationSession().isSoloVentaDeLinea()){
			logger.info("[GUI]->Venta Solo Linea, oprtunidad disables, not visible");
			principalForm.getChangePedidoOportunidad().setEnabled(false);
			principalForm.getChangePedidoOportunidad().setVisible(false);
		}
		if (isAdmin) {
			principalForm.getDescuentoMayoreo().setEnabled(true);
			principalForm.getVerVentasXTicket().setEnabled(true);
			principalForm.getVerVentasXUsuario().setEnabled(true);
			principalForm.getVerVentasXProducto().setEnabled(true);
			principalForm.getGenerarPDFBtn().setEnabled(true);
			principalForm.getGeneradorTarjetasPrecios().setEnabled(true);
			principalForm.getPrecioEncontrado().setEditable(true);
			principalForm.getCantidadRecargar().setEditable(true);
			principalForm.getCantidadRecargar().setVisible(true);
			principalForm.getActualizarPrecioBtn().setVisible(true);
			principalForm.getActualizarPrecioBtn().remove(principalForm.getActualizarPrecioBtn());

			/*
			 principalForm.getGeneradorTarjetasPrecios().addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent e) {
			 if (isFrontPanel("visorChecadorDePrecios")) {
			 generaTarjetasDePreciosProductoActual();
			 } else {
			 generaTarjetasDePrecios();
			 }
			 }
			 });
			 */

			principalForm.getDescuentoMayoreo().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jDialogActivacionDescuentoMayoreoController.estadoInicial(principalForm);
					updateDetallePedidoTable();
				}
			});

			principalForm.getGenerarPDFBtn().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					generarReportePDFYMostrarlo();
				}
			});
			principalForm.getActualizarPrecioBtn().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actualizarPrecio();
				}
			});
			dfChecadorPrecios = new DecimalFormat("###,###,##0.00");
		} 
		if(isFinances){
			principalForm.getVerVentasXTicket().setEnabled(true);
			principalForm.getVerVentasXUsuario().setEnabled(true);
			principalForm.getVerVentasXProducto().setEnabled(true);			
			principalForm.getGenerarPDFBtn().setEnabled(true);			
			principalForm.getGenerarPDFBtn().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					generarReportePDFYMostrarlo();
				}
			});
		}		
		principalForm.getVerVentasXTicket().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:VerVentasXTicket");
				tipoReporte = 1;
				((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Ticket");
				verReporteVentasXTicket();
			}
		});
		principalForm.getVerVentasXUsuario().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoReporte = 2;
				((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Usuario");
				verVentasXUsuario();
			}
		});
		principalForm.getVerVentasXProducto().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoReporte = 3;
				((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Producto");
				verVentasXProducto();
			}
		});
		principalForm.getViewClientesMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verClientes();
			}
		});
		principalForm.getViewProductosMenu().setEnabled(false);
//		principalForm.getViewProductosMenu().addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				verProductos();
//			}
//		});
		principalForm.getCodigoBarrasBuscarEnProductos().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarProductoEnProductos();
			}
		});

		principalForm.getDeleteItemPedidoMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminarProductoDePedido();
			}
		});
		principalForm.getProcederPedidoMenu().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				logger.info("[GUI]->MENU:ProcederPedidoMenu");
				procederPedidoSucursal();
			}
		});

		Almacen almacen = applicationLogic.getSession().getAlmacen();
		if (almacen.getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
			principalForm.getChangePedidoNormal().setSelected(true);
			principalForm.getLabel2().setText("VENTA NORMAL[" + almacen.getId() + "]");
		} else if (almacen.getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
			principalForm.getChangePedidoOportunidad().setSelected(true);
			principalForm.getLabel2().setText("VENTA DE OPORTUNIDAD[" + almacen.getId() + "]");
		}

		principalForm.getChangePedidoNormal().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Collection<Almacen> almacenCollection = applicationLogic.getSession().getSucursal().getAlmacenCollection();
				for (Almacen a : almacenCollection) {
					if (a.getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
						applicationLogic.getSession().setAlmacen(a);
						principalForm.getLabel2().setText("VENTA NORMAL[" + a.getId() + "]");
						break;
					}
				}
			}
		});

		principalForm.getChangePedidoOportunidad().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Collection<Almacen> almacenCollection = applicationLogic.getSession().getSucursal().getAlmacenCollection();
				for (Almacen a : almacenCollection) {
					if (a.getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
						applicationLogic.getSession().setAlmacen(a);
						principalForm.getLabel2().setText("VENTA DE OPORTUNIDAD[" + a.getId() + "]");
						break;
					}
				}
			}
		});


		principalForm.getNuevoClienteBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->BUTTON:NuevoClienteBtn");

				createCliente();
			}
		});
		principalForm.getNuevoClienteAComboBox().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->COMBOBOX:NuevoClienteAComboBox");
				createCliente();
			}
		});
		principalForm.getChecadorDePreciosMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->MENU:ChecadorDePreciosMenu");
				checadorDePrecios();
			}
		});

		principalForm.getCodigoBarrasBuscar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String codigoDeBarras = principalForm.getCodigoBarrasBuscar().getText().trim();
				logger.info("[GUI]->TEXTFIELD_ENTER:CodigoBarrasBuscar:codigoDeBarras=" + codigoDeBarras);
				if (codigoDeBarras.length() > 1) {
					checarPrecio(codigoDeBarras);
				}
			}
		});

		principalForm.getCodigoBarrasBuscar().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!isAdmin) {
					limpiarResultadoBusqueda();
				}
			}

			public void focusGained(FocusEvent e) {
				if (isAdmin) {
					limpiarResultadoBusqueda();
				}
			}
		});


		principalForm.getCodigoBuscar().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				//super.focusLost(e);
				Producto producto = null;
				String codigoDeBarras = principalForm.getCodigoBuscar().getText().trim();
				try {
					producto = applicationLogic.findProductoByCodigoDeBarras(codigoDeBarras);
					updateProductoSelected(
							new ProductoFastDisplayModel(producto.getId(), producto.getNombre(), producto),
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCantidadCBPedida().requestFocus();
				} catch (Exception ex) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCodigoBuscar().setText("");
				}
			}
		});

		principalForm.getCodigoBuscar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Producto producto = null;
				String codigoDeBarras = principalForm.getCodigoBuscar().getText().trim();
				logger.info("[GUI]->TEXTFIELD_ENTER:CodigoBuscar:codigoDeBarras=" + codigoDeBarras);
				try {
					producto = applicationLogic.findProductoByCodigoDeBarras(codigoDeBarras);
					updateProductoSelected(
							new ProductoFastDisplayModel(producto.getId(), producto.getNombre(), producto),
							getPrincipalForm().getCantidadCBPedida());
					//principalForm.getCantidadCBPedida().requestFocus();
					agregarNProductoCB();
				} catch (Exception ex) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCodigoBuscar().setText("");
				}
			}
		});

		principalForm.getNombreBuscar().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() >= 1) {
						getPrincipalForm().getProductoCBEncontrados().setSelectedIndex(getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 1 ? 1 : 0);
						getPrincipalForm().getProductoCBEncontrados().requestFocus();
					} else {
						return;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});

		principalForm.getNombreBuscar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->TEXTFIELD_ENTER:NombreBuscar:currentTextToSearch=" + currentTextToSearch);
				if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
					principalForm.getProductoCBEncontrados().setPopupVisible(false);
					getPrincipalForm().getCantidadPedida().requestFocus();
				}
			}
		});

		principalForm.getNombreBuscar().getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!principalForm.getNombreBuscar().getText().toLowerCase().trim().equals(currentTextToSearch)) {
					currentTextToSearch = principalForm.getNombreBuscar().getText().toLowerCase().trim();
					logger.info("[GUI]->TEXTFIELD_INSERTUPDATE:NombreBuscar:currentTextToSearch=" + currentTextToSearch);
					updateProductosFound();
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				if (!principalForm.getNombreBuscar().getText().toLowerCase().trim().equals(currentTextToSearch)) {
					currentTextToSearch = principalForm.getNombreBuscar().getText().toLowerCase().trim();
					logger.info("[GUI]->TEXTFIELD_REMOVEUPDATE:NombreBuscar:currentTextToSearch=" + currentTextToSearch);
					updateProductosFound();
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				logger.info("[GUI]->TEXTFIELD_REMOVEUPDATE:NombreBuscar:principalForm.getNombreBuscar().getText()=" + principalForm.getNombreBuscar().getText() + " != currentTextToSearch=" + currentTextToSearch + " ???");
			}
		});

		principalForm.getRecibimos().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (principalForm.getRecibimos().getText().trim().length() > 0) {
					if (formaDePagoTCSelected) {
					} else {
						try {
							double recibimos = Double.parseDouble(principalForm.getRecibimos().getText().trim());
							double total = applicationLogic.getSession().getTotalFinalPedido();
							if (recibimos < total) {
								throw new IllegalArgumentException("El importe recibido debe ser mayor o igual al TOTAL.");
							}
							actualizarCambioContraRecibido(recibimos);

						} catch (NumberFormatException nfe) {
							principalForm.getRecibimos().setText("");
						} catch (IllegalArgumentException iae) {
							principalForm.getRecibimos().setText("");
							JOptionPane.showMessageDialog(principalForm, iae.getMessage(), "Confirmar Venta", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});

		principalForm.getProductoCBEncontrados().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					logger.info("[GUI]->COMBOBOX_ENTER:ProductoCBEncontrados:getPrincipalForm().getProductoCBEncontrados().getModel().getSize()=" + getPrincipalForm().getProductoCBEncontrados().getModel().getSize());
					if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
						principalForm.getProductoCBEncontrados().setPopupVisible(false);
						getPrincipalForm().getCantidadPedida().requestFocus();
					}
				} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});


		getPrincipalForm().getProductoCBEncontrados().setModel(new DefaultComboBoxModel(
				applicationLogic.getProducto4Display(currentTextToSearch)));
		getPrincipalForm().getProductoCBEncontrados().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object itemSelect = e.getItem();
					updateProductoSelected((ProductoFastDisplayModel) itemSelect, getPrincipalForm().getCantidadPedida());
				}
			}
		});

		getPrincipalForm().getCantidadPedida().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});

		getPrincipalForm().getCantidadPedida().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				logger.info("[GUI]->TEXTFIELD_ENTER:CantidadPedida:text=" + getPrincipalForm().getCantidadPedida().getText());
				agregarNProducto();
			}
		});

		getPrincipalForm().getCantidadCBPedida().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				agregarNProductoCB();
			}
		});

		principalForm.getDetallePedidoTable().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (principalForm.getDetallePedidoTable().getRowCount() > 0) {
					if (principalForm.getDetallePedidoTable().getSelectedRowCount() == 0) {
						principalForm.getDetallePedidoTable().setRowSelectionInterval(0, 0);
					}
				}
			}
		});
		//======================================================================
		List<Cliente> listCliente = null;
		List<FormaDePago> listFormaDePago = null;
		try {
			listCliente = basicInfoDAO.getClientesList();
			listFormaDePago =
					basicInfoDAO.getFormaDePagosList();
		} catch (Exception ex) {
			listCliente = new ArrayList<Cliente>();
			listFormaDePago =
					new ArrayList<FormaDePago>();
			logger.error("", ex);
		}

		principalForm.getCliente().setModel(new ClienteComboBoxModel(listCliente));
		principalForm.getCliente().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				logger.info("[GUI]->COMBOBOX_CHANGED:cliente:" + e.getItem() + ": " + e.getStateChange() + " == " + ItemEvent.SELECTED + "? " + (e.getStateChange() == ItemEvent.SELECTED));
				if (e.getStateChange() == ItemEvent.SELECTED) {
					verifyAllSelections();
				}

			}
		});
		principalForm.getFormaDePago().setModel(new FormaDePagoComboBoxModel(listFormaDePago));
		principalForm.getFormaDePago().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				logger.info("[GUI]->COMBOBOX_CHANGED:cliente: " + e.getItem() + ": " + e.getStateChange() + " == " + ItemEvent.SELECTED + "? " + (e.getStateChange() == ItemEvent.SELECTED));
				if (e.getStateChange() == ItemEvent.SELECTED) {
					verifyAllSelections();
				}

			}
		});

		principalForm.getProcederPedidoBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->BUTTON:ProcederPedidoBtn");
				procederPedidoSucursal();
			}
		});

		principalForm.getReiniciarPedidoBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->BUTTON:ReiniciarPedidoBtn");
				reiniciarPedido();
			}
		});

		principalForm.getAvoidPedidoMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("[GUI]->BUTTON:AvoidPedidoMenu");
				reiniciarPedido();
			}
		});

		principalForm.getClientesTable().setModel(new ClientesTableModel(listCliente));

		principalForm.getClientesTable().getColumnModel().getColumn(0).setPreferredWidth(40);
		principalForm.getClientesTable().getColumnModel().getColumn(1).setPreferredWidth(400);
		principalForm.getClientesTable().getColumnModel().getColumn(2).setPreferredWidth(120);
		principalForm.getClientesTable().getColumnModel().getColumn(3).setPreferredWidth(280);
		principalForm.getClientesTable().getColumnModel().getColumn(4).setPreferredWidth(75);
		principalForm.getClientesTable().getColumnModel().getColumn(5).setPreferredWidth(75);
		principalForm.getClientesTable().getColumnModel().getColumn(6).setPreferredWidth(450);
		principalForm.getClientesTable().getColumnModel().getColumn(7).setPreferredWidth(220);
		principalForm.getClientesTable().getColumnModel().getColumn(8).setPreferredWidth(180);
		principalForm.getClientesTable().getColumnModel().getColumn(9).setPreferredWidth(200);
		principalForm.getClientesTable().getColumnModel().getColumn(10).setPreferredWidth(100);

		principalForm.getClientesTable().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					editCliente();
				} else if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
					deleteCliente();
				}

			}
		});

		principalForm.getClientesTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					editCliente();
				}
			}
		});

		try {
			iconTree1 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/arrow_right_green_16x16.png")));
			iconTree2 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/free_16x16.png")));
			iconTree3 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/enterprise_16x16.png")));
			iconTree4 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/grid_16x16.png")));
			iconTree5 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/box_16x16.png")));
		} catch (Exception ex) {
			logger.error("", ex);
		}
		changeInputMethodCB();
	}
	static Icon iconTree1;
	static Icon iconTree2;
	static Icon iconTree3;
	static Icon iconTree4;
	static Icon iconTree5;

	/**
	 * @return the applicationLogic
	 */
	public ApplicationLogic getApplicationLogic() {
		return applicationLogic;
	}

	/**
	 * @param applicationLogic the applicationLogic to set
	 */
	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}

	/**
	 * @return the basicInfoDAO
	 */
	public BasicInfoDAO getBasicInfoDAO() {
		return basicInfoDAO;
	}

	/**
	 * @param basicInfoDAO the basicInfoDAO to set
	 */
	public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
		this.basicInfoDAO = basicInfoDAO;
	}

	public void setMarcaTreeModelBuilder(MarcaTreeModelBuilder marcaTreeModelBuilder) {
		this.marcaTreeModelBuilder = marcaTreeModelBuilder;
	}

	/**
	 * @param edicionClientesControl the edicionClientesControl to set
	 */
	public void setEdicionClientesControl(EdicionClientesControl edicionClientesControl) {
		this.edicionClientesControl = edicionClientesControl;
	}

	void maximizeWindow() {
		principalForm.setExtendedState(principalForm.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}

	private void limpiarResultadoBusqueda() {
		principalForm.getNombreDescripcionEncontrado().setText("");
		principalForm.getCodigoBarrasBuscar().setText("");
		principalForm.getPrecioEncontrado().setText("");
		principalForm.getCodigoBarrasEncontrado().setText("");
		principalForm.getCantidadActual().setText("");
		principalForm.getCantidadRecargar().setText("");
	}
	/*
	 class MyMarcaTreeNodeRenderer extends DefaultTreeCellRenderer {

	 public MyMarcaTreeNodeRenderer() {
	 }

	 public Component getTreeCellRendererComponent(
	 JTree tree,
	 Object value,
	 boolean sel,
	 boolean expanded,
	 boolean leaf,
	 int row,
	 boolean hasFocus) {

	 super.getTreeCellRendererComponent(
	 tree, value, sel,
	 expanded, leaf, row,
	 hasFocus);
	 if (!leaf && isRootNode(value)) {
	 setIcon(iconTree1);
	 setToolTipText("-");
	 } else if (isLineaNode(value)) {
	 setIcon(iconTree2);
	 setToolTipText("Linea");
	 } else if (isIndustriaNode(value)) {
	 setIcon(iconTree3);
	 setToolTipText("Industria");
	 } else if (isMarcaNode(value)) {
	 setIcon(iconTree4);
	 setToolTipText("Marca");
	 } else {
	 setIcon(iconTree5);
	 setToolTipText("?");
	 }

	 return this;
	 }

	 protected boolean isRootNode(Object value) {
	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
	 return (node.getUserObject() instanceof String);
	 }

	 protected boolean isMarcaNode(Object value) {
	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
	 return (node.getUserObject() instanceof MarcaTreeNode);
	 }

	 protected boolean isLineaNode(Object value) {
	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
	 return (node.getUserObject() instanceof LineaTreeNode);
	 }

	 protected boolean isIndustriaNode(Object value) {
	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
	 return (node.getUserObject() instanceof IndustriaTreeNode);
	 }
	 }
	 */
	/*
	 class DiscountDVPCellEditor extends AbstractCellEditor
	 implements TableCellEditor {

	 final JSpinner spinner = new JSpinner();

	 // Initializes the spinner.
	 public DiscountDVPCellEditor() {
	 spinner.setModel(new SpinnerNumberModel(0, 0, 5, 1));
	 }

	 // Prepares the spinner component and returns it.
	 public Component getTableCellEditorComponent(JTable table, Object value,
	 boolean isSelected, int row, int column) {

	 double maxDesc = 0.0;
	 ((SpinnerNumberModel) spinner.getModel()).setMaximum(new Integer((int) Math.round(maxDesc * 100.0)));
	 logger.info("->:getTableCellEditorComponent(" + row + "," + column + "): spinner.setValue(value=" + value + " class " + value.getClass() + "):  maxDesc=" + maxDesc);
	 spinner.setValue(new Integer(value.toString()));
	 new Thread() {
	 public void run() {
	 logger.info("->:getTableCellEditorComponent() shuld requesting fucking  foucs ?" + (!spinner.isFocusOwner()));
	 for (int ifo = 0; ifo < 10 && !spinner.isFocusOwner(); ifo++) {
	 logger.info("->:getTableCellEditorComponent() requesting foucs [" + (ifo + 1) + "/10]");
	 spinner.requestFocus();
	 try {
	 Thread.sleep(250);
	 } catch (InterruptedException ex) {
	 }
	 }
	 }
	 }.start();

	 return spinner;
	 }

	 public boolean isCellEditable(EventObject evt) {
	 logger.info("-> DiscountDVPCellEditor: event " + evt.getClass() + " = " + evt);
	 if (evt instanceof MouseEvent) {
	 return ((MouseEvent) evt).getClickCount() >= 2;
	 } else if (evt instanceof KeyEvent) {
	 KeyEvent keyEvent = (KeyEvent) evt;
	 boolean canEdit = keyEvent.getKeyCode() == KeyEvent.VK_F2;
	 logger.info("-> DiscountDVPCellEditor: EDIT Cell Render ?" + canEdit);
	 return canEdit;
	 } else if (evt instanceof ActionEvent) {
	 ActionEvent actionEvent = (ActionEvent) evt;

	 }
	 return false;
	 }

	 // Returns the spinners current value.
	 public Object getCellEditorValue() {
	 return spinner.getValue();
	 }
	 }
	 */

	void refreshClientesList() {
		List<Cliente> listCliente = null;

		try {
			listCliente = basicInfoDAO.getClientesList();
		} catch (Exception ex) {
			listCliente = new ArrayList<Cliente>();
			logger.error("", ex);
		}

		principalForm.getCliente().setModel(new ClienteComboBoxModel(listCliente));
		principalForm.getClientesTable().setModel(new ClientesTableModel(listCliente));
		principalForm.getClientesTable().updateUI();
		principalForm.getCliente().updateUI();

	}

	void refreshProductosList() {
		List<ProductoRowModel> listProducto = null;

		try {
			logger.info("->refreshProductosList: Sucursal="+getApplicationLogic().getSession().getSucursal()); 
			listProducto = basicInfoDAO.getProductosList(getApplicationLogic().getSession().getSucursal());

		} catch (Exception ex) {
			listProducto = new ArrayList<ProductoRowModel>();
			logger.error("", ex);
		}

		try {
			logger.info("->refreshProductosList: ProductosTable.parent.size=" + principalForm.getProductosTable().getParent().getSize());

			DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
			dtcr.setHorizontalAlignment(DefaultTableCellRenderer.RIGHT);

			if (!(principalForm.getProductosTable().getModel() instanceof ProductoTableModel)
					|| principalForm.getProductosTable().getModel() == null) {

				principalForm.getProductosTable().setModel(new ProductoTableModel(listProducto));

				principalForm.getProductosTable().getColumnModel().getColumn(0).setPreferredWidth(50);
				principalForm.getProductosTable().getColumnModel().getColumn(1).setPreferredWidth(150);
				principalForm.getProductosTable().getColumnModel().getColumn(2).setPreferredWidth(180);
				principalForm.getProductosTable().getColumnModel().getColumn(3).setPreferredWidth(180);
				principalForm.getProductosTable().getColumnModel().getColumn(4).setPreferredWidth(180);
				principalForm.getProductosTable().getColumnModel().getColumn(5).setPreferredWidth(230);
				principalForm.getProductosTable().getColumnModel().getColumn(6).setPreferredWidth(230);
				principalForm.getProductosTable().getColumnModel().getColumn(7).setPreferredWidth(50);
				principalForm.getProductosTable().getColumnModel().getColumn(8).setPreferredWidth(50);
				principalForm.getProductosTable().getColumnModel().getColumn(9).setPreferredWidth(50);
				principalForm.getProductosTable().getColumnModel().getColumn(10).setPreferredWidth(75);
				principalForm.getProductosTable().getColumnModel().getColumn(11).setPreferredWidth(75);
				principalForm.getProductosTable().getColumnModel().getColumn(12).setPreferredWidth(75);
				principalForm.getProductosTable().getColumnModel().getColumn(13).setPreferredWidth(75);
				principalForm.getProductosTable().getColumnModel().getColumn(14).setPreferredWidth(75);

				principalForm.getProductosTable().getColumnModel().getColumn(0).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(1).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(7).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(8).setCellRenderer(dtcr);
				//principalForm.getProductosTable().getColumnModel().getColumn(9 ).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(10).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(11).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(12).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(13).setCellRenderer(dtcr);
				principalForm.getProductosTable().getColumnModel().getColumn(14).setCellRenderer(dtcr);

				principalForm.getProductosTable().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				principalForm.getProductosTable().addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent evt) {
						if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
							editProducto();
						} else if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
							//deleteProducto();
						}

					}
				});

				principalForm.getProductosTable().addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if (e.getClickCount() == 2) {
							editProducto();
						}
					}
				});

			} else {
				((ProductoTableModel) principalForm.getProductosTable().getModel()).setListProducto(listProducto);
			}
			principalForm.getProductosTable().updateUI();

		} catch (Exception e) {
			logger.error("In refreshProductosList:", e);
		}
	}

	private void buscarProductoEnProductos() {

		String codigoBarras = principalForm.getCodigoBarrasBuscarEnProductos().getText().trim();
		if (codigoBarras.length() > 5) {
			ProductoTableModel ptm = ((ProductoTableModel) principalForm.getProductosTable().getModel());
			int index = ptm.getProductoPorCodigoBarras(codigoBarras);

			if (index >= 0) {
				principalForm.getProductosTable().getSelectionModel().setSelectionInterval(index, index);
				scrollToVisible(principalForm.getProductosTable(), index, 1);
				principalForm.getProductosTable().requestFocus();
			}
			principalForm.getCodigoBarrasBuscarEnProductos().setText("");
		}

	}

	public static void scrollToVisible(JTable table, int rowIndex, int vColIndex) {
		if (!(table.getParent() instanceof JViewport)) {
			return;
		}
		JViewport viewport = (JViewport) table.getParent();

		// This rectangle is relative to the table where the
		// northwest corner of cell (0,0) is always (0,0).
		Rectangle rect = table.getCellRect(rowIndex, vColIndex, true);

		// The location of the viewport relative to the table
		Point pt = viewport.getViewPosition();

		// Translate the cell location so that it is relative
		// to the view, assuming the northwest corner of the
		// view is (0,0)
		rect.setLocation(rect.x - pt.x, rect.y - pt.y);

		table.scrollRectToVisible(rect);

		// Scroll the area into view
		//viewport.scrollRectToVisible(rect);
	}

	public void updateDetallePedidoTable() {
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
		principalForm.getReiniciarPedidoBtn().setEnabled(sizeDetail > 0);
		principalForm.getAvoidPedidoMenu().setEnabled(sizeDetail > 0);
		principalForm.getDetallePedidoTable().updateUI();

		if (sizeDetail > 0) {
			//boolean esFiscal = true;
			double st = 0.0;
			double si = 0.0;
			double sd = 0.0;

			double precioVenta = 0.0;
			int nPzs = 0;
			for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
				precioVenta = dvp.getPrecioVenta();
				nPzs += dvp.getCantidad();
				si += dvp.getCantidad() * precioVenta;
			}
			logger.debug("->updateDetallePedidoTable: applicationLogic.getDescuentoMayoreoActivado()=" + applicationLogic.getDescuentoMayoreoActivado());

			if (applicationLogic.getDescuentoMayoreoActivado() && applicationLogic.getSession().getAlmacen().getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
				if (nPzs >= 12) {
					sd = si * 0.1;
				} else if (si > 100 && si <= 200) {
					sd = si * 0.05;
				} else if (si > 200) {
					sd = si * 0.1;
				}
			}

			double osi = 0.0;
			double osd = 0.0;
			double otax = 0.0;
			double ostt = 0.0;

			osi = si; //* (1.0 + applicationLogic.getFactorIVA());
			osd = sd; //* (1.0 + applicationLogic.getFactorIVA());
			otax = 0.0;
			ostt = (osi - osd);
			applicationLogic.getSession().setTotalFinalPedido(ostt);
			applicationLogic.getSession().getPedidoVenta().setDescuentoAplicado(sd);
			principalForm.getTotalArticulos().setText(String.valueOf(nPzs));
			principalForm.getSubtotal().setText(ApplicationInfo.formatToCurrency(osi));
			principalForm.getDiscount().setText(ApplicationInfo.formatToCurrency(osd));
			principalForm.getTotal().setText(ApplicationInfo.formatToCurrency(ostt));

			verifyAllSelections();

			principalForm.getChangePedidoNormal().setEnabled(false);
			principalForm.getChangePedidoOportunidad().setEnabled(false);
		} else {
			principalForm.getTotalArticulos().setText("");
			principalForm.getSubtotal().setText("");
			principalForm.getDiscount().setText("");
			principalForm.getTotal().setText("");

			principalForm.getChangePedidoNormal().setEnabled(true);
			principalForm.getChangePedidoOportunidad().setEnabled(true);
		}
	}

	void actualizarCambioContraRecibido(double recibido) {
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
		if (sizeDetail > 0) {

			double cambio = 0.0;

			cambio = recibido - applicationLogic.getSession().getTotalFinalPedido();
			principalForm.getCambio().setText(ApplicationInfo.formatToCurrency(cambio));
		} else {
			principalForm.getCambio().setText("");
		}
	}

	void toFrontPanel(String nombre) {
		CardLayout cl = (CardLayout) (principalForm.getCardPanel().getLayout());

		if (nombre.equals(principalModel.getFrontPanel())) {
			return;
		}

		principalModel.setFrontPanel(nombre);
		cl.show(principalForm.getCardPanel(), nombre);
	}

	boolean isFrontPanel(String nombre) {
		CardLayout cl = (CardLayout) (principalForm.getCardPanel().getLayout());
		return principalModel.getFrontPanel().equals(nombre);
	}

	void updateImageForDisplayInPanel(int index, VisorDeProductosDefaultModel dpdm, VisorDeProductosDefaultPanel vpdp) {
		BufferedImage bi = null;
		Producto prod = null;
		if (index != -1) {
			prod = dpdm.getSelected(index);
			bi = dpdm.getImageForDisplay(index);
			vpdp.setImageForPanelDisplayer(index);
		}

		vpdp.getVisorDeAtributos().updateLabels(prod);
	}
	List<Producto> listProductosSinSurtido;

	void verDetalle() {
		toFrontPanel("visorDePedidoActual");
		focusForCapture();
	}

	void verReporteVentasXTicket() {
		refreshRangoDeFechas();
		refreshVentasXTicket();
		principalForm.getPanleNoTicket().setVisible(true);
		toFrontPanel("visorDeReportes");
		principalForm.getDetallePedidoTable().requestFocus();
	}

	void verClientes() {
		refreshClientesList();
		toFrontPanel("visorDeClientes");
		principalForm.getClientesTable().requestFocus();
	}

	void verProductos() {
		refreshProductosList();
		toFrontPanel("visorDeProductos");
		principalForm.getClientesTable().requestFocus();
	}

	private int whereWasAdded(Producto producto) {
		int index = 0;
		logger.debug("->whereWasAdded:" + producto);
		for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
			if (dvp.getProducto().getId().intValue() == producto.getId().intValue()) {
				logger.debug("->whereWasAdded:\t was @ idex=" + index);
				return index;
			}
			index++;
		}
		logger.debug("->whereWasAdded,  Not found :(");

		return -1;
	}

	void agregarNProducto() {
		logger.debug("->agregarNProducto:");
		int cantPedida = 0;
		Producto productoAgregar = null;
		try {
			productoAgregar = applicationLogic.getSession().getProductoBuscadoActual();
			cantPedida = Integer.parseInt(getPrincipalForm().getCantidadPedida().getText().trim());
			if (cantPedida > 0 && productoAgregar != null) {
				logger.debug("->agregarNProducto: add N=" + cantPedida);

				applicationLogic.addProductoNToCurrentPedidoVenta(productoAgregar, cantPedida);
				int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
				logger.debug("->agregarNProducto:: OK , sizeDetail=" + cantPedida);
				updateDetallePedidoTable();
				getPrincipalForm().getNombreBuscar().setText("");
				getPrincipalForm().getNombreBuscar().requestFocus();
				if(! applicationLogic.isWithDuplicates()){
					final int whereWasAdded = whereWasAdded(productoAgregar);
					logger.debug("->agregarNProducto:whereWasAdded=" + whereWasAdded);
					scrollToVisible(principalForm.getDetallePedidoTable(), whereWasAdded, 1);
					principalForm.getDetallePedidoTable().getSelectionModel().setSelectionInterval(whereWasAdded, whereWasAdded);
				} else {					
					final int whereWasAdded = sizeDetail-1;
					logger.debug("->agregarNProducto: with Duplicates, whereWasAdded=" + whereWasAdded);
					scrollToVisible(principalForm.getDetallePedidoTable(), whereWasAdded, 1);
					principalForm.getDetallePedidoTable().getSelectionModel().setSelectionInterval(whereWasAdded, whereWasAdded);
				}

			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			//logger.error("->agregarNProducto", e);
			JOptionPane.showMessageDialog(principalForm, e.getMessage(), "Agregar producto", JOptionPane.ERROR_MESSAGE);
			prepareForCaptureClear();
		}
		getPrincipalForm().getCantidadPedida().setText("");

	}

	void agregarNProductoCB() {
		logger.info("->agregarNProductoCB");
		int cantPedida = 0;
		Producto productoAgregar = null;
		try {
			productoAgregar = applicationLogic.getSession().getProductoBuscadoActual();
			cantPedida = Integer.parseInt(getPrincipalForm().getCantidadCBPedida().getText().trim());
			if (cantPedida > 0 && productoAgregar != null) {
				logger.info("->add N=" + cantPedida);

				applicationLogic.addProductoNToCurrentPedidoVenta(productoAgregar, cantPedida);

				updateDetallePedidoTable();
				getPrincipalForm().getCodigoBuscar().setText("");
				getPrincipalForm().getCodigoBuscar().requestFocus();
				final int whereWasAdded = whereWasAdded(productoAgregar);
				scrollToVisible(principalForm.getDetallePedidoTable(), whereWasAdded, 1);
				principalForm.getDetallePedidoTable().getSelectionModel().setSelectionInterval(whereWasAdded, whereWasAdded);
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(principalForm, e.getMessage(), "Agregar producto", JOptionPane.ERROR_MESSAGE);
			prepareForCaptureClear();
		}
		getPrincipalForm().getCantidadCBPedida().setText("");
	}

	void changeInputMethodNombre() {
		JPanel pnl = principalForm.getBuscarCardsPanel();
		CardLayout cl = (CardLayout) pnl.getLayout();
		cl.show(pnl, "buscarNombreCard");
		capturaModo = CAPTURA_MODO_NOMBRE;
		focusForCapture();
	}

	void changeInputMethodCB() {
		JPanel pnl = principalForm.getBuscarCardsPanel();
		CardLayout cl = (CardLayout) pnl.getLayout();
		cl.show(pnl, "buscarCBCard");
		capturaModo = CAPTURA_MODO_CODIGO_BARRAS;
		focusForCapture();
	}

	private void prepareForCaptureClear() {
		getPrincipalForm().getCodigoBuscar().setText("");
		getPrincipalForm().getNombreBuscar().setText("");
		getPrincipalForm().getCantidadPedida().setText("");
		getPrincipalForm().getCantidadCBPedida().setText("");

		focusForCapture();
	}

	private void focusForCapture() {
		if (capturaModo == CAPTURA_MODO_CODIGO_BARRAS) {
			getPrincipalForm().getCodigoBuscar().requestFocus();
		} else if (capturaModo == CAPTURA_MODO_NOMBRE) {
			getPrincipalForm().getNombreBuscar().requestFocus();
		}
	}

	void verifyAllSelections() {
		int sizeDetail = applicationLogic.getSession().
				getPedidoVenta().getPedidoVentaDetalleCollection().size();
		logger.debug("->verifyAllSelections(): sizeDetail=" + sizeDetail);
		Cliente clienteSelected = null;
		FormaDePago formaDePagoSelected = null;

		if ((ClienteItemList) principalForm.getCliente().getSelectedItem() != null) {
			clienteSelected = ((ClienteItemList) principalForm.getCliente().getSelectedItem()).getCliente();
			if (clienteSelected.getId() == null) {
				clienteSelected = null;
			}
		} else {
			clienteSelected = null;
		}

		if (((FormaDePagoItemList) principalForm.getFormaDePago().getSelectedItem()) != null) {
			formaDePagoSelected = ((FormaDePagoItemList) principalForm.getFormaDePago().getSelectedItem()).getFormaDePago();
			if (formaDePagoSelected == null) {
				formaDePagoSelected = null;
			} else {

				if (formaDePagoSelected.getId() != null && formaDePagoSelected.getId().intValue() == Constants.FORMA_DE_PAGO_TC) {
					principalForm.getRecibimosOAprobacion().setText("No. Aprobación :");
					principalForm.getCambio().setText("");
					principalForm.getCambio().setVisible(false);
					principalForm.getCambioLabel().setVisible(false);

					formaDePagoTCSelected = true;
				} else {
					principalForm.getRecibimosOAprobacion().setText("Recibimos :");
					principalForm.getCambio().setVisible(true);
					principalForm.getCambioLabel().setVisible(true);
					formaDePagoTCSelected = false;
				}
			}
		} else {
			formaDePagoSelected = null;
		}


		applicationLogic.setClienteToCurrentPedidoVenta(clienteSelected);
		applicationLogic.setFormaDePagoToCurrentPedidoVenta(formaDePagoSelected);
	}

	private void validatePedidoSelections() throws ValidationException {
		int sizeDetail = applicationLogic.getSession().
				getPedidoVenta().getPedidoVentaDetalleCollection().size();

		if (sizeDetail == 0) {
			if (capturaModo == CAPTURA_MODO_CODIGO_BARRAS) {
				throw new ValidationException("Tiene que capturar un Producto por Codigo de Barras", getPrincipalForm().getCodigoBuscar());
			} else if (capturaModo == CAPTURA_MODO_NOMBRE) {
				throw new ValidationException("Tiene que capturar un Producto por Nombre", getPrincipalForm().getNombreBuscar());
			}
		}

		if (principalForm.getCliente().getSelectedIndex() == 0) {
			throw new ValidationException("Tiene que elegir un Cliente", principalForm.getCliente());
		}

		if (principalForm.getFormaDePago().getSelectedIndex() == 0) {
			throw new ValidationException("Tiene que elegir una Forma de Pago", principalForm.getFormaDePago());
		}

		if (principalForm.getRecibimos().getText().trim().length() == 0) {
			if (!formaDePagoTCSelected) {
				throw new ValidationException("Tiene que capturar el Importe Recibido", principalForm.getRecibimos());
			} else {
				throw new ValidationException("Tiene que capturar el No. de Aprobación", principalForm.getRecibimos());
			}
		} else {
			if (formaDePagoTCSelected) {
				String strNumAprob = principalForm.getRecibimos().getText().trim();
				if (strNumAprob.length() < 6 || principalForm.getRecibimos().getText().trim().length() > 12) {
					throw new ValidationException("No. de Aprobación deben ser 6 a 12 digitos", principalForm.getRecibimos());
				}
				try {
					Long.parseLong(strNumAprob);
				} catch (NumberFormatException nfe) {
					throw new ValidationException("No. de Aprobación deben ser solo digitos", principalForm.getRecibimos());
				}
			} else {
				double recibimos = -1.0;
				try {
					recibimos = Double.parseDouble(principalForm.getRecibimos().getText().trim());
				} catch (NumberFormatException nfe) {
					throw new ValidationException("La cantidad no es importe valido", principalForm.getRecibimos());
				}
				double total = applicationLogic.getSession().getTotalFinalPedido();

				if (recibimos < total) {
					throw new ValidationException("El Importe Recibido debe ser mayor o igual al TOTAL.", principalForm.getRecibimos());
				}
			}
		}

	}
//	void procederPedido() {
//		logger.info("->procederPedido(): confirm ?");
//
//		JComponent missingComponent = validatePedidoSelections();
//		if (missingComponent != null) {
//			missingComponent.requestFocus();
//			return;
//		}
//		int confirm = JOptionPane.showConfirmDialog(principalForm, ApplicationInfo.getLocalizedMessage("LABEL_PROCED_CONFIRMATION"),
//				ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//				JOptionPane.YES_NO_OPTION,
//				JOptionPane.QUESTION_MESSAGE);
//		if (confirm == JOptionPane.NO_OPTION) {
//			return;
//		}
//
//		try {
//			applicationLogic.persistCurrentPedidoVenta();
//			PedidoVenta pedidoVenta = applicationLogic.getSession().getPedidoVenta();
//			String pseudoNumeroPedido = ApplicationInfo.getNumPseudoPedido(
//					applicationLogic.getSession().getPedidoVenta());
//
//			JOptionPane.showMessageDialog(principalForm,
//					ApplicationInfo.getLocalizedMessage("LABEL_PEDIDO_SAVED") + pseudoNumeroPedido,
//					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//					JOptionPane.INFORMATION_MESSAGE);
//
//
//			Icon printIcon = null;
//			String imgImpresora = null;
//			//imgImpresora = "/imgs/Zebra_MZ_320.jpg";
//			imgImpresora = "/imgs/Epson_tm_t20.jpg";
//			try {
//
//				printIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream(imgImpresora)));
//			} catch (IOException ex) {
//				logger.error("", ex);
//			}
//
//			int confirmPrint = JOptionPane.showConfirmDialog(principalForm,
//					ApplicationInfo.getLocalizedMessage("LABEL_PRINT_CONFIRMATION"),
//					ApplicationInfo.getLocalizedMessage("LABEL_PRINT_TICKET"),
//					JOptionPane.YES_NO_OPTION,
//					JOptionPane.QUESTION_MESSAGE,
//					printIcon);
//
//			try {
//				applicationLogic.printTicketPedido(pedidoVenta, confirmPrint == JOptionPane.YES_OPTION,
//						principalForm.getRecibimos().getText(), principalForm.getCambio().getText());
//			} catch (BusinessException ex) {
//				JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
//						ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//						JOptionPane.ERROR_MESSAGE);
//			}
//			logger.info("->procederPedido(): ok estado inicial");
//			estadoInicial(true);
//		} catch (BusinessException ex) {
//			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
//					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//					JOptionPane.ERROR_MESSAGE);
//		}
//
//	}
//	
	long st1, st2, st3;

	void procederPedidoSucursal() {
		logger.debug("->procederPedidoSucursal(): confirm ?");

		try {
			validatePedidoSelections();
		} catch (ValidationException ve) {
			logger.debug("->procederPedidoSucursal(): ValidationException:" + ve);

			JComponent missingComponent = ve.getWrongComponent();
			missingComponent.requestFocus();
			JOptionPane.showMessageDialog(principalForm, ve.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		st1 = System.currentTimeMillis();
		logger.debug("->procederPedidoSucursal(): >>> SAVE > PRINT");
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			logger.debug("\t->procederPedidoSucursal(): persistCurrentPedidoVenta");
			PedidoVenta pedidoVenta = applicationLogic.getSession().getPedidoVenta();
			int numCaja = applicationLogic.getSession().getNumCaja();
			String recibimosTxt = principalForm.getRecibimos().getText().trim();

			String numTicketNuevo = GeneradorNumTicket.getNumTicket(
					applicationLogic.getSession().getUsuario().getUsuarioId(),
					applicationLogic.getSession().getAlmacen().getId(),
					numCaja);
			logger.debug("\t->procederPedidoSucursal(): numTicketNuevo=" + numTicketNuevo);
			String extraIfoInComments = "<ticket>" + numTicketNuevo + "</ticket>";
			extraIfoInComments += "<caja>" + numCaja + "</caja>";

			if (formaDePagoTCSelected) {
				extraIfoInComments += "<noAprobacion>" + recibimosTxt + "</noAprobacion>";
			} else {
				extraIfoInComments += "<recibido>" + recibimosTxt + "</recibido>";
			}
			pedidoVenta.setComentarios(extraIfoInComments);
			pedidoVenta.setUsuario(applicationLogic.getSession().getUsuario());

			//pedidoVenta.setCliente(applicationLogic.getSession().getCliente());

			pedidoVenta.setAlmacen(applicationLogic.getSession().getAlmacen());
			new Thread() {
				@Override
				public void run() {
					try {
						principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						applicationLogic.printTicketPedido(applicationLogic.getSession().getPedidoVenta(), true);
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(principalForm, "Se guardara , pero hubo un error al imprimir:" + ex.getMessage(), "Imprimir", JOptionPane.ERROR_MESSAGE);
					} finally {
						principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					}
				}
			}.start();
			applicationLogic.persistCurrentPedidoVenta();

			PedidoVenta pedidoVentaSaved = applicationLogic.getSession().getPedidoVenta();

			estadoInicial(true);
			st3 = System.currentTimeMillis();

			logger.debug("\t->procederPedido(): ok, estadoInicial, T=" + (st3 - st1));

			//logger.info("\t->procederPedidoSucursal(): printTicketPedido");
			//applicationLogic.printTicketPedido(pedidoVenta,true,principalForm.getRecibimos().getText(),principalForm.getCambio().getText());            
			JOptionPane.showMessageDialog(principalForm, "VENTA REGISTRADA",
					"VENTA",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}
	private Object[][] infoConcentradoReporte = null;
	private String fechaInicialReporte = null;
	private String fechaFinalReporte = null;
	private String usuarioReporte = null;
	private String totalReporte = null;

	void refreshVentasXTicket() {
		Object[][] infoConcentrado = null;
		Object[][] infoConcentradoNuevo = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}
		if (principalForm.getPorNoDeTicketRadioBtn().isSelected()) {
			String buscarNoTicket = principalForm.getBuscarPorNoTicket().getText().trim();
			principalForm.getBuscarPorNoTicket().setText(buscarNoTicket);
			if (!buscarNoTicket.matches("^([0-9])+$")) {
				logger.debug("----->refreshVentasXTicket : X not match !");
				return;
			}
			logger.debug("----->refreshVentasXTicket : before search, buscarNoTicket=" + buscarNoTicket);
			infoConcentrado = applicationLogic.concentradoVentasTicket(buscarNoTicket);
		} else {
			infoConcentrado = applicationLogic.concentradoVentasTicket(fechaInicial, fechaFinal);
		}
		infoConcentradoNuevo = new Object[infoConcentrado.length][];
		logger.debug("----->refreshVentasXTicket :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		Double descuento = null;
		String extraInfoValue = "";
		String ticketInComentarios = "";
		String noAprobacionInComentarios = "";
		String recibidoInComentarios = "";
		String numCajaInComentarios = "";
		HashMap<String, String> extraInfo = null;

		int i = 0;
		for (Object[] row : infoConcentrado) {
			logger.debug("----->refreshVentasXTicket:row[" + i + "].length=" + row.length + ",content=" + Arrays.asList(row));

			infoConcentradoNuevo[i] = new Object[12];

			descuento = (Double) row[7];
			if (descuento == null) {
				descuento = 0.0;
			}
			importe = (Double) row[8];
			total += importe - descuento;

			if (row[4] != null) {
				extraInfoValue = (String) row[4];
				logger.debug("----->refreshVentasXTicket: \textraInfoValue=" + extraInfoValue);
				extraInfo = applicationLogic.getExtraInfoInComments(extraInfoValue);
				logger.debug("----->refreshVentasXTicket: \tafter get from embbeded XML, extraInfo=" + extraInfo);
				ticketInComentarios = extraInfo.get("ticket");
				noAprobacionInComentarios = extraInfo.get("aprobacion");
				recibidoInComentarios = extraInfo.get("recibido");
				if (recibidoInComentarios.trim().length() > 0) {
					recibidoInComentarios = df.format(Double.parseDouble(recibidoInComentarios));
				}
				numCajaInComentarios = extraInfo.get("caja");

			}

			infoConcentradoNuevo[i][0] = row[0];
			infoConcentradoNuevo[i][1] = row[1];
			infoConcentradoNuevo[i][2] = row[2];
			infoConcentradoNuevo[i][3] = row[3];

			infoConcentradoNuevo[i][4] = numCajaInComentarios;
			infoConcentradoNuevo[i][5] = ticketInComentarios;
			infoConcentradoNuevo[i][6] = recibidoInComentarios;
			infoConcentradoNuevo[i][7] = noAprobacionInComentarios;

			infoConcentradoNuevo[i][8] = row[6];
			infoConcentradoNuevo[i][9]  = df.format(importe);
			infoConcentradoNuevo[i][10] = df.format(descuento);
			infoConcentradoNuevo[i][11] = df.format(importe - descuento);

			i++;
		}

		String[] columnNames = {
			"Tipo",
			"Usuario",
			"Cliente",
			"F. Pago",
			"#Caja",
			"#Ticket",
			"Recibido",
			"# Aprob.",
			"Fecha-hora",
			"Importe",
			"Desc.",
			"Total"
		};

		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentradoNuevo, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);

		TableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		((DefaultTableCellRenderer) rightRenderer).setHorizontalAlignment(JLabel.RIGHT);

		principalForm.getTablaReporte().getColumnModel().getColumn(4).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(6).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(7).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(9).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(10).setCellRenderer(rightRenderer);
		principalForm.getTablaReporte().getColumnModel().getColumn(11).setCellRenderer(rightRenderer);

		logger.debug("----->refreshVentasXTicket : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentradoNuevo;
	}

	private void examinarPedido() {
		if (tipoReporte == 1) {
			int selectedRow = principalForm.getTablaReporte().getSelectedRow();
			if (selectedRow < 0) {
				JOptionPane.showMessageDialog(principalForm, "Tiene que seleciconar que Venta a imprimir", "Reimprimir", JOptionPane.WARNING_MESSAGE);
				principalForm.getTablaReporte().requestFocus();
				return;
			}
			String pedidoVentaTicket = principalForm.getTablaReporte().getModel().getValueAt(selectedRow, 5).toString();
			logger.debug("-->>examinarPedido: Ok, examinarPedido:pedidoVentaTicket=" + pedidoVentaTicket);
			try {
				PedidoVenta pv = null;
				pv = applicationLogic.findPedidoVentaByTicket(pedidoVentaTicket);
				logger.debug("-->> examinarPedido: PedidoVenta=" + pv + ", PedidoVentaDetalleCollection=" + pv.getPedidoVentaDetalleCollection());
				visorDePedidoJDialogController.setPedidoVenta(pv);
				visorDePedidoJDialogController.estadoInicial();
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
				JOptionPane.showMessageDialog(principalForm, "Error al examinar pedido:" + ex.getMessage(), "Examinar Pedido", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	void generarReportePDFYMostrarlo() {

		if (infoConcentradoReporte == null) {
			JOptionPane.showMessageDialog(principalForm, "Tiene que actualizar el reporte", "Generar PDF", JOptionPane.WARNING_MESSAGE);
			return;
		}

		String pdfPath = null;
		try {
			if (tipoReporte == 1) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXTicket(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			} else if (tipoReporte == 2) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXUsuario(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			} else if (tipoReporte == 3) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXProducto(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			}

		} catch (IOException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(), "Generar PDF", JOptionPane.ERROR_MESSAGE);
		}

		if (pdfPath == null) {
			return;
		}

		try {
			Process exec = Runtime.getRuntime().exec(new String[]{"/usr/bin/xdg-open", pdfPath});
		} catch (Exception ex) {
			logger.error("Show PDF", ex);
			JOptionPane.showMessageDialog(principalForm, "Se genero el PDF en :" + pdfPath + ", \nPero sucedio un error al mostrarlo", "Mostrar PDF", JOptionPane.ERROR_MESSAGE);
		}

	}

	void actualizarPrecio() {

		final String precioCapturado = getPrincipalForm().getPrecioEncontrado().getText().trim();
		final String cantidadRecargarCapturado = getPrincipalForm().getCantidadRecargar().getText().trim();
		logger.debug("-->>actualizarPrecio:precioCapturado=" + precioCapturado+", cantidadRecargarCapturado="+cantidadRecargarCapturado);

		if (precioCapturado.length() == 0) {
			return;
		}
		double precioNuevo;
		int    cantidadParaRecargar;
		try {
			precioNuevo = Double.parseDouble(precioCapturado);
			cantidadParaRecargar = Integer.parseInt(cantidadRecargarCapturado);
			if (precioNuevo >= 0.1) {
				logger.info("->" + precioNuevo + " => ID=" + productoEscaneado.getId());
				applicationLogic.actualizaPrecio(productoEscaneado, applicationLogic.getSession().getAlmacen(), precioNuevo, cantidadParaRecargar);
				JOptionPane.showMessageDialog(principalForm, "OK, actualizado Precio y Cantidad", "Actualizar", JOptionPane.INFORMATION_MESSAGE);
				principalForm.getCodigoBarrasBuscar().requestFocus();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(), "Actualizar", JOptionPane.ERROR_MESSAGE);
		}


	}

	void testPrinter() {
		try {
			applicationLogic.testPrinter();
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("APP_MENU_TEST_PRINT"),
					JOptionPane.ERROR_MESSAGE);
		}

	}

	void reiniciarPedido() {
		logger.debug("->reiniciarPedido()");
		int confirm = JOptionPane.showConfirmDialog(principalForm, ApplicationInfo.getLocalizedMessage("LABEL_RESET"),
				ApplicationInfo.getLocalizedMessage("RESET_PEDIDO_CONFIRMATION"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		if (confirm == JOptionPane.YES_OPTION) {
			estadoInicial(true);
		}

	}

	void eliminarProductoDePedido() {
		logger.debug("->eliminarProductoDePedido()");
		int rowsBefore = principalForm.getDetallePedidoTable().getRowCount();
		int indexProdToDelete = principalForm.getDetallePedidoTable().getSelectedRow();
		logger.debug("\t==>>principalForm.getDetallePedidoTable().getSelectedRow=" + indexProdToDelete);
		if (indexProdToDelete >= 0) {
			applicationLogic.deleteProductoFromCurrentPedidoVenta(indexProdToDelete);
			updateDetallePedidoTable();
			if (rowsBefore == 1) {
				focusForCapture();
			}
		}
	}

	void createCliente() {
		logger.debug("->createCliente()");
		edicionClientesControl.crearCliente();
		if (edicionClientesControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshClientesList();
		}

	}

	Cliente getSelectedCliente() {
		int cteSel = principalForm.getClientesTable().getSelectedRow();
		Cliente cs = null;
		if (cteSel != -1) {
			cs = ((ClientesTableModel) principalForm.getClientesTable().getModel()).getAt(cteSel);
		}
		return cs;
	}

	void editCliente() {
		Cliente c = getSelectedCliente();
		logger.debug("->editCliente():" + c);
		logger.debug("\t->editCliente(): problemas con CuentaBancariaCollection ??");
		edicionClientesControl.editarCliente(c);
		if (edicionClientesControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshClientesList();
		}

	}

	void deleteCliente() {
		Cliente c = getSelectedCliente();
		logger.debug("->deleteCliente():" + c);

		try {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_MSG_TITLE"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				applicationLogic.removeCliente(c);
			}

			refreshClientesList();
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm,
					ex.getMessage(),
					ex.getTitle(),
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			logger.error("", ex);
			JOptionPane.showMessageDialog(principalForm,
					BusinessException.getLocalizedMessage("APP_LOGIC_CLIENTE_NOT_DELETED"),
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_MSG_TITLE"),
					JOptionPane.ERROR_MESSAGE);
		}

	}

	void createProducto() {
		logger.debug("->createProducto()");
		edicionProductosControl.crearProducto();
		if (edicionProductosControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshProductosList();
		}

	}

	ProductoRowModel getProductoSelected() {
		int prodSel = principalForm.getProductosTable().getSelectedRow();
		ProductoRowModel cs = null;
		if (prodSel != -1) {
			cs = ((ProductoTableModel) principalForm.getProductosTable().getModel()).getAt(prodSel);
		}
		return cs;
	}

	void editProducto() {
		ProductoRowModel p = getProductoSelected();
		logger.debug("->editProducto():" + p);
		edicionProductosControl.editarProducto(p);
		if (edicionProductosControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshProductosList();
		}

	}

	void exitByClick() {
		logger.info("[GUI]->exitByClick()");
		try {
			applicationLogic.exit();
			principalForm.setVisible(false);
			principalForm.dispose();
			logger.debug("->exitByClick(): OK normal exit!");
			System.exit(0);
		} catch (BusinessException ex) {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ex.getMessage() + ApplicationInfo.getLocalizedMessage("EXIT_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("APP_MENU_EXIT"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				principalForm.setVisible(false);
				principalForm.dispose();
				System.exit(0);
			}

		}
	}

	void exit() {
		logger.info("->exit()");
		try {
			applicationLogic.exit();
			principalForm.setVisible(false);
			principalForm.dispose();
			logger.debug("->exit(): OK normal exit!");
			System.exit(0);

		} catch (BusinessException ex) {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ex.getMessage() + ApplicationInfo.getLocalizedMessage("EXIT_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("APP_MENU_EXIT"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				principalForm.setVisible(false);
				principalForm.dispose();
				System.exit(0);
			}

		}
	}
	/*
	 void enviarPedidos() {
	 logger.info("->enviarPedidos()");

	 if (applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size() > 0) {
	 JOptionPane.showMessageDialog(principalForm,
	 ApplicationInfo.getLocalizedMessage("SAVE_BEFORE_SEND"),
	 ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
	 JOptionPane.QUESTION_MESSAGE);
	 }

	 try {
	 applicationLogic.sendPedidosAndDelete(new ProgressProcessListener() {
	 public void updateProgress(int prog, String msg) {
	 }

	 public int getProgress() {
	 return 0;
	 }
	 });
	 refreshVentasXTicket();

	 JOptionPane.showMessageDialog(principalForm, ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS_OK"),
	 ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
	 JOptionPane.WARNING_MESSAGE);
	 } catch (Exception ex) {
	 JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
	 ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
	 JOptionPane.ERROR_MESSAGE);
	 } finally {
	 //principalForm.getLabel3().setText("");
	 }
	 }
	 */

	private void verVentasXUsuario() {
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		refreshRangoDeFechas();
		refreshVentasXUsuario();
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		principalForm.getPanleNoTicket().setVisible(false);
		toFrontPanel("visorDeReportes");
		principalForm.getTablaReporte().requestFocus();
	}

	private void refreshRangoDeFechas() {
		List<Date> rangoDeFechas = applicationLogic.getRangoFechasDeVentas();
		principalForm.getFechaReporteInicio().setModel(new FechaComboBoxModel(rangoDeFechas));
		principalForm.getFechaReporteFin().setModel(new FechaComboBoxModel(rangoDeFechas));
	}

	private void refreshVentasXUsuario() {
		Object[][] infoConcentrado = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}

		infoConcentrado = applicationLogic.concentradoVentasUsuario(fechaInicial, fechaFinal);
		logger.debug("----->verConcentradoVentsUsuario :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		for (Object[] row : infoConcentrado) {
			importe = (Double) row[4];
			total += importe;
			row[4] = df.format(importe);
		}

		String[] columnNames = {
			"Nombre Completo",
			"Usuario",
			"Tipo Almacen",
			"Fecha",
			"Importe"};
		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentrado, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);
		logger.debug("----->refreshVentasXUsuario : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentrado;
	}

	private void verVentasXProducto() {
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		refreshRangoDeFechas();
		refreshVentasXProducto();
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		principalForm.getPanleNoTicket().setVisible(false);
		toFrontPanel("visorDeReportes");
		principalForm.getTablaReporte().requestFocus();
	}

	private void refreshVentasXProducto() {
		Object[][] infoConcentrado = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}

		infoConcentrado = applicationLogic.concentradoVentasProducto(fechaInicial, fechaFinal);
		logger.debug("----->refreshVentasXProducto :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		for (Object[] row : infoConcentrado) {
			importe = (Double) row[5];
			total += importe;
			row[5] = df.format(importe);
		}
		String[] columnNames = {
			"Cantidad",
			"CODIGO",
			"Producto",
			"Presentacion",
			"Tipo Venta",
			"Importe"};
		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentrado, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);
		logger.debug("----->refreshVentasXProducto : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentrado;
	}

	public void estadoInicial(boolean paraSucursal) {
		logger.debug("\t->Estado Inicial");
		applicationLogic.startNewPedidoVentaSession();

		principalForm.getLabel1().setText(ApplicationInfo.getLocalizedMessage("USUARIOACTUAL")
				+ applicationLogic.getSession().getUsuario().getNombreCompleto());
		principalForm.getLabel3().setText("SUCURSAL: " + applicationLogic.getSession().getSucursal().getId());
		principalForm.getRecibimos().setText("");
		principalForm.getCambio().setText("");
		principalForm.getTotalArticulos().setText("");

		//principalForm.setExtendedState(principalForm.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		////principalForm.getDeleteItemPedidoMenu().setEnabled(false);
		if (paraSucursal) {
			ClienteComboBoxModel clienteCBM = (ClienteComboBoxModel) principalForm.getCliente().getModel();
			int nc = clienteCBM.getSize();
			int scf = 0;
			for (int cci = 1; cci < nc; cci++) {
				if (((ClienteItemList) clienteCBM.getElementAt(cci)).getCliente().getId().intValue() == 1) {
					scf = cci;
				}
			}
			principalForm.getCliente().setSelectedIndex(scf);
			principalForm.getFormaDePago().setSelectedIndex(1);
		} else {
			principalForm.getCliente().setSelectedIndex(0);
			principalForm.getFormaDePago().setSelectedIndex(0);
		}
		formaDePagoTCSelected = false;
		verifyAllSelections();
		applicationLogic.getSession().setTotalFinalPedido(0.0);

		PedidoVentaDetalleTableModel pedidoVentaDetalleTableModel = new PedidoVentaDetalleTableModel(
				applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection());

		pedidoVentaDetalleTableModel.setPrincipalControl(this);
		pedidoVentaDetalleTableModel.setApplicationLogic(applicationLogic);
		//======================================================================
		principalForm.getDetallePedidoTable().setModel(pedidoVentaDetalleTableModel);
		final PedidoVentaDetalle pedidoVentaDetalleZero = new PedidoVentaDetalle();
		pedidoVentaDetalleZero.setCantidad(0);
		pedidoVentaDetalleZero.setPrecioVenta(0.0);
		pedidoVentaDetalleZero.setProducto(new Producto(0));

		applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().add(pedidoVentaDetalleZero);

		TableColumn column = null;
		TableColumnModel columnModel = principalForm.getDetallePedidoTable().getColumnModel();
		int tableWidth = principalForm.getDetallePedidoTable().getWidth();
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
		dtcr.setHorizontalAlignment(DefaultTableCellRenderer.RIGHT);

		for (int i = 0; i < 5; i++) {
			column = columnModel.getColumn(i);
			if (i == 0) {
				column.setPreferredWidth((tableWidth * 5) / 100);
			} else if (i == 1) {
				column.setPreferredWidth((tableWidth * 15) / 100);
			} else if (i == 2) {
				column.setPreferredWidth((tableWidth * 60) / 100); //third column is bigger
			} else {
				column.setPreferredWidth((tableWidth * 10) / 100);
				if (i > 2) {
					column.setCellRenderer(dtcr);
				}
			}
		}

		principalForm.getTotal().setText("$ 0.00");
		principalForm.getSubtotal().setText("$ 0.00");
		principalForm.getDiscount().setText("$ 0.00");

		principalForm.getDetallePedidoTable().updateUI();

		applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().clear();

		principalForm.getCliente().updateUI();
		principalForm.getFormaDePago().updateUI();

		principalForm.getDetallePedidoTable().updateUI();

		principalForm.getChangePedidoNormal().setEnabled(true);
		principalForm.getChangePedidoOportunidad().setEnabled(true);
		//======================================================================
		toFrontPanel("visorDePedidoActual");
		if (!principalForm.isVisible()) {
			principalForm.setVisible(true);
		}
		focusForCapture();

	}

	void generaTarjetasDePrecios() {
		try {
//			if(!applicationLogic.getSession().getUsuario().getNombreCompleto().equals("root")) {
//				throw new Exception("Solo un administrador puede generar las listas");
//			}
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			GeneradorTarjetasPrecios.generateTicket(applicationLogic.getProductosForPrinting(), applicationLogic.getSession().getAlmacen());

		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					"Generar Tarjetas de precios",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	void generaTarjetasDePreciosProductoActual() {
		try {
//			if(!applicationLogic.getSession().getUsuario().getNombreCompleto().equals("root")) {
//				throw new Exception("Solo un administrador puede generar las listas");
//			}
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			GeneradorTarjetasPrecios.generateTicket(productoEscaneado, applicationLogic.getSession().getAlmacen());

		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					"Generar Tarjetas de precios",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	void updateProductoSelected(ProductoFastDisplayModel selDisplayProducto, JTextField cantidadPedidaTF) {
		logger.debug("\t->itemStateChanged:" + selDisplayProducto + ", n Prod. surtidos=" + (listProductosSinSurtido != null ? listProductosSinSurtido.size() : null));

		if (selDisplayProducto != null) {
			applicationLogic.getSession().setProductoBuscadoActual(selDisplayProducto.getProducto());

			getPrincipalForm().getProductoCBEncontrados().setEnabled(true);

			cantidadPedidaTF.setText("1");
			cantidadPedidaTF.setSelectionStart(0);
			cantidadPedidaTF.setSelectionEnd(1);
			cantidadPedidaTF.setEnabled(true);
		} else {
			applicationLogic.getSession().setProductoBuscadoActual(null);
			getPrincipalForm().getProductoCBEncontrados().setEnabled(false);
			cantidadPedidaTF.setText("");
			cantidadPedidaTF.setEnabled(false);
		}

	}

	void updateProductosFound() {
		logger.debug("->updateProductosFound(): currentTextToSearch=" + currentTextToSearch);
		getPrincipalForm().getProductoCBEncontrados().setModel(new DefaultComboBoxModel(
				applicationLogic.getProducto4Display(currentTextToSearch)));
		getPrincipalForm().getProductoCBEncontrados().updateUI();
		if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
			updateProductoSelected(
					(ProductoFastDisplayModel) getPrincipalForm().getProductoCBEncontrados().getModel().getElementAt(0),
					getPrincipalForm().getCantidadPedida());
			getPrincipalForm().getProductoCBEncontrados().setPopupVisible(true);
		} else {
			updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
			getPrincipalForm().getProductoCBEncontrados().setPopupVisible(false);
		}

	}

	private void checadorDePrecios() {
		limpiarResultadoBusqueda();
		toFrontPanel("visorChecadorDePrecios");
		principalForm.getCodigoBarrasBuscar().requestFocus();
	}
	private Producto productoEscaneado = null;

	private void checarPrecio(String codigoEscaneado) {

		try {
			productoEscaneado = applicationLogic.findProductoByCodigoDeBarras(codigoEscaneado);
			principalForm.getNombreDescripcionEncontrado().setText(productoEscaneado.getNombre() + " (" + productoEscaneado.getPresentacion() + ")");
			final Collection<AlmacenProducto> almacenProductoCollection = productoEscaneado.getAlmacenProductoCollection();
			String precioAlmacen = null;
			String existenciaActual = null;
			int almacenActual = applicationLogic.getSession().getAlmacen().getId().intValue();
			for (AlmacenProducto ap : almacenProductoCollection) {
				
				if (ap.getAlmacen().getId().intValue() == almacenActual) {
					precioAlmacen = dfChecadorPrecios.format(ap.getPrecioVenta());
					existenciaActual = String.valueOf(ap.getCantidadActual());					
				}
			}
			boolean enOferta= basicInfoDAO.productoEnOferta2x1(productoEscaneado.getId(), almacenActual);
			
			if (precioAlmacen == null || existenciaActual == null) {
				precioAlmacen = "";
				existenciaActual = "NO HAY EXISTENCIA";
			}
			principalForm.getCodigoBarrasBuscar().setText("");
			principalForm.getPrecioEncontrado().setText(precioAlmacen);
			principalForm.getCantidadActual().setText("# "+existenciaActual+" +");
			principalForm.getCantidadRecargar().setText("0");
			principalForm.getCodigoBarrasEncontrado().setText(productoEscaneado.getContenido() + " " + productoEscaneado.getUnidadMedida()+(enOferta?" (2x1)":""));
		} catch (Exception ex) {
			limpiarResultadoBusqueda();
		}
	}

	/**
	 * @return the principalForm
	 */
	public PrincipalForm getPrincipalForm() {
		return principalForm;
	}

	/**
	 * @param edicionProductosControl the edicionProductosControl to set
	 */
	public void setEdicionProductosControl(EdicionProductosControl edicionProductosControl) {
		this.edicionProductosControl = edicionProductosControl;
	}

	/**
	 * @param visorDePedidoJDialogController the visorDePedidoJDialogController
	 * to set
	 */
	public void setVisorDePedidoJDialogController(VisorDePedidoJDialogController visorDePedidoJDialogController) {
		this.visorDePedidoJDialogController = visorDePedidoJDialogController;
	}
}
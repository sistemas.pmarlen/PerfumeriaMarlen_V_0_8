/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.model.IndustriaComboBoxModel;
import com.pmarlen.client.model.IndustriaItemList;
import com.pmarlen.client.model.LineaComboBoxModel;
import com.pmarlen.client.model.LineaItemList;
import com.pmarlen.client.model.MarcaComboBoxModel;
import com.pmarlen.client.model.MarcaItemList;
import com.pmarlen.client.model.ProductoRowModel;
import com.pmarlen.client.view.EdicionProductoDialog;
import com.pmarlen.model.beans.Industria;
import com.pmarlen.model.beans.Linea;
import com.pmarlen.model.beans.Marca;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.controller.BasicInfoDAO;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("edicionProductosControl")
public class EdicionProductosControl {

	private Logger logger;
	@Autowired
	private ApplicationLogic applicationLogic;
	@Autowired
	private PrincipalControl principalControl;
	@Autowired
	private EdicionLineaControl edicionLineaControl;
	@Autowired
	private EdicionIndustriaControl edicionIndustriaControl;
	@Autowired
	private EdicionMarcaControl edicionMarcaControl;

	@Autowired
	private BasicInfoDAO basicInfoDAO;
	Producto producto;
	ProductoRowModel productoRowModel;
	private List<Linea> lineas;
	private List<Industria> industrias;
	private List<Marca> marcas;
	private LineaItemList lineaItemSelected;
	private IndustriaItemList industriaItemSelected;
	private MarcaItemList marcaItemSelected;
	EdicionProductoDialog edicionProductoDialog;
	//private static EdicionProductosControl instance;
	private int exitStatus;
	JComponentValidator validator;

	public EdicionProductosControl() {
		logger = LoggerFactory.getLogger(EdicionProductosControl.class);
	}

	public void setup() {
		edicionProductoDialog = new EdicionProductoDialog(principalControl.getPrincipalForm());
		edicionProductoDialog.getLinea().addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent pme) {
				logger.info("->Linea.popupMenuWillBecomeVisible");
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent pme) {
				logger.info("->Linea.popupMenuWillBecomeInvisible");
				if (hasMarcaValidSelectionsState()) {
					logger.info("\t->Linea.popupMenuWillBecomeInvisible: Update Marca valid selection!");
					updateMarcasWithValidSelection();
				} else {
					logger.info("\t->Linea.popupMenuWillBecomeInvisible: Update Marca invalid selection!");
					updateMarcasWithInvalidSelection();
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent pme) {
				logger.info("->Linea.popupMenuCanceled");
				if (hasMarcaValidSelectionsState()) {
					logger.info("\t->Linea.popupMenuCanceled: Update Marca valid selection!");
					updateMarcasWithValidSelection();
				} else {
					logger.info("\t->Linea.popupMenuCanceled: Update Marca invalid selection!");
					updateMarcasWithInvalidSelection();
				}
			}
		});
		edicionProductoDialog.getIndustria().addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent pme) {
				logger.info("->Industria.popupMenuWillBecomeVisible");
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent pme) {
				logger.info("->Industria.popupMenuWillBecomeInvisible");
				if (hasMarcaValidSelectionsState()) {
					logger.info("\t->Industria.popupMenuWillBecomeInvisible: Update Marca valid selection!");
					updateMarcasWithValidSelection();
				} else {
					logger.info("\t->Industria.popupMenuWillBecomeInvisible: Update Marca invalid selection!");
					updateMarcasWithInvalidSelection();
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent pme) {
				logger.info("->Linea.popupMenuCanceled");
				if (hasMarcaValidSelectionsState()) {
					logger.info("\t->Industria.popupMenuCanceled: Update Marca valid selection!");
					updateMarcasWithValidSelection();
				} else {
					logger.info("\t->Industria.popupMenuCanceled: Update Marca invalid selection!");
					updateMarcasWithInvalidSelection();
				}
			}
		});

		edicionProductoDialog.getLinea().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					lineaItemSelected = (LineaItemList) edicionProductoDialog.getLinea().getSelectedItem();

					logger.info("->Linea.itemStateChanged: Linea.id=" + lineaItemSelected.getId() + ", isPopupVisible?" + edicionProductoDialog.getLinea().isPopupVisible());
					if (hasMarcaValidSelectionsState()) {
						logger.info("\t->Linea.itemStateChanged: Update Marca valid selection, popup will call");
					} else {
						logger.info("\t->Linea.itemStateChanged: Update Marca invalid selection, popup will call");
					}
				}
			}
		});

		edicionProductoDialog.getIndustria().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					industriaItemSelected = (IndustriaItemList) edicionProductoDialog.getIndustria().getSelectedItem();

					logger.info("->Industria.itemStateChanged: Industria.id=" + industriaItemSelected.getId() + ", isPopupVisible?" + edicionProductoDialog.getIndustria().isPopupVisible());
					if (hasMarcaValidSelectionsState()) {
						logger.info("\t->Industria.itemStateChanged: Update Marca valid selection, popup will call");
					} else {
						logger.info("\t->Industria.itemStateChanged: Update Marca invalid selection, popup will call");
					}
				}
			}
		});
		refreshLineasList();
		refreshIndustriasList();

		marcas = new ArrayList<Marca>();

		edicionProductoDialog.getMarca().setModel(new MarcaComboBoxModel(marcas));

		validator = new JComponentValidator(edicionProductoDialog, ApplicationInfo.getLocalizedMessage("DLG_EDIT_PRODUCT_MSG_TITLE"));
		/*
		 validator.add(new ValidatorHelper(edicionProductoDialog.getNombre(), "[a-zA-Z]{2}[a-zA-Z0-9\\. ]{5,126}", true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_RAZON_SOCIAL"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getRfc(),         "[a-zA-Z]{3,4}[0-9]{6}[a-zA-Z0-9]{3}", true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_RFC"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getCodigoBarras(),       "[a-zA-Z0-9\\. #-,]{1,128}", true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_CALLE"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getPresentacion(),  "[a-zA-Z0-9\\. #-,]{1,16}", false, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_NOINT"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getId(),  "[a-zA-Z0-9\\. #-,]{1,16}", false, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_NOEXT"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 //        validator.add(new ValidatorHelper(edicionProductoDialog.getEntidadFederativa(),  ""                  , true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_ENTFED"),ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED_LIST")));
		 //        validator.add(new ValidatorHelper(edicionProductoDialog.getMunicipioODelegacion(),  ""               , true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_MPIO"),ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED_LIST")));
		 //        validator.add(new ValidatorHelper(edicionProductoDialog.getCodigoPostal(),  ""                       , true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_CP"),ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED_LIST")));
		 //        validator.add(new ValidatorHelper(edicionProductoDialog.getPoblacion(),  ""                          , true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_POB"),ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED_LIST")));
		 //		validator.add(new ValidatorHelper(edicionProductoDialog.getDireccion(),  "[a-zA-Z0-9\\. #-,]{5,255}"  , true, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_DIR"),ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED_LIST")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getNombreIndustria(),  "[0-9]{4,8}(,[0-9]{4,8})*", false, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_TELSNO"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getNombreLinea(),  "([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)", false, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_MAIL"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 validator.add(new ValidatorHelper(edicionProductoDialog.getNombreMarca(),  "[a-zA-Z0-9\\. #-,]{1,255}", false, ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_COMMENT"), ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLIENTE_MSG_REQUIRED")));
		 */

		edicionProductoDialog.getGuardar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aceptar();
			}
		});

		edicionProductoDialog.getCancelar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelar();
			}
		});
		
		edicionProductoDialog.getNuevaLineaBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nuevaLinea();
			}
		});
		edicionProductoDialog.getNuevaIndustriaBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nuevaIndustria();
			}
		});
		edicionProductoDialog.getNuevaMarcaBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nuevaMarca();
			}
		});

		edicionProductoDialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				cancelar();
			}
		});

		edicionProductoDialog.getRootPane().setDefaultButton(edicionProductoDialog.getGuardar());

	}

	private boolean hasMarcaValidSelectionsState() {
		return lineaItemSelected != null && lineaItemSelected.getId() > 0
				&& industriaItemSelected != null && industriaItemSelected.getId() > 0;
	}

	private void updateMarcasWithValidSelection() {
		logger.info("->updateMarcasWithValidSelection: lineaItemSelected=" + lineaItemSelected.getId() + ", industriaItemSelected=" + industriaItemSelected.getId());
		try {
			marcas = basicInfoDAO.getMarcaListByLineaIndustria(lineaItemSelected.getLinea(), industriaItemSelected.getIndustria());
			logger.info("\t->updateMarcasWithValidSelection: marcas.size=" + marcas.size());
		} catch (Exception ex) {
			logger.error("updateMarcasWithValidSelection:", ex);
			marcas = new ArrayList<Marca>();
		}
		edicionProductoDialog.getMarca().setModel(new MarcaComboBoxModel(marcas));
		edicionProductoDialog.getMarca().updateUI();
		edicionProductoDialog.getMarca().setEnabled(true);
	}

	private void updateMarcasWithInvalidSelection() {
		logger.info("->updateMarcasWithInvalidSelection");
		marcas = new ArrayList<Marca>();
		edicionProductoDialog.getMarca().setModel(new MarcaComboBoxModel(marcas));
		edicionProductoDialog.getMarca().updateUI();
		edicionProductoDialog.getMarca().setEnabled(false);
	}

	void setProducto(Producto c) {
		this.producto = c;
	}

	public void editarProducto(ProductoRowModel productoEditar) {
		//producto = productoEditar;
		productoRowModel = productoEditar;
		estadoInicial();
	}

	public void crearProducto() {
		//producto = new Producto();
		productoRowModel = new ProductoRowModel(producto);

		estadoInicial();
	}

	void estadoInicial() {
		if (edicionProductoDialog == null) {
			setup();
		}

		if (productoRowModel.getId() != null) {
			logger.info("->estadoInicial: productoRowModel.getId()=" + productoRowModel.getId() + ", marcaNombre=" + productoRowModel.getMarcaNombre());
			makeSelectionsFromNames();
		}

		//edicionProductoDialog.getMarca().setEnabled(false);

		edicionProductoDialog.getId().setText(productoRowModel.getId() != null ? String.valueOf(productoRowModel.getId()) : "");
		edicionProductoDialog.getCodigoBarras().setText(productoRowModel.getCodigoBarras());
		edicionProductoDialog.getNombre().setText(productoRowModel.getNombre());
		edicionProductoDialog.getPresentacion().setText(productoRowModel.getPresentacion());

		//edicionProductoDialog.getLineaNombre().setText(productoRowModel.getLineaNombre());
		//edicionProductoDialog.getIndustriaNombre().setText(productoRowModel.getIndustriaNombre());
		//edicionProductoDialog.getMarcaNombre().setText(productoRowModel.getMarcaNombre());

		edicionProductoDialog.getUnidadesPorCaja().setText(String.valueOf(productoRowModel.getUnidadesPorCaja()));
		edicionProductoDialog.getContenido().setText(productoRowModel.getContenido());
		edicionProductoDialog.getUnidadMedida().setText(productoRowModel.getUnidadMedida());

		edicionProductoDialog.getCosto().setText(String.valueOf(productoRowModel.getCosto()));

		edicionProductoDialog.getPrecioVentaNormal().setText(String.valueOf(productoRowModel.getPrecioVentaNormal()));
		edicionProductoDialog.getPrecioVentaOportunidad().setText(String.valueOf(productoRowModel.getPrecioVentaOportunidad()));
		edicionProductoDialog.getCantActualNormal().setText(String.valueOf(productoRowModel.getCantActualNormal()));
		edicionProductoDialog.getCantActualOportunidad().setText(String.valueOf(productoRowModel.getCantActualOportunidad()));

		edicionProductoDialog.getGuardar().setEnabled(true);
		edicionProductoDialog.getCancelar().setEnabled(true);

		if (!edicionProductoDialog.isVisible()) {
			centerInScreenAndSetVisible(edicionProductoDialog);
		}
	}

	void fillProducto() throws Exception {
		/*
		 producto.setId(edicionProductoDialog.getId().getText().trim());
		 producto.setPresentacion(edicionProductoDialog.getPresentacion().getText().trim());
        
		 producto.setNombreIndustria(edicionProductoDialog.getIndustriaNombre().getText().trim());
        
		 producto.setCodigoBarras(edicionProductoDialog.getCodigoBarras().getText());
		 producto.setNombre(edicionProductoDialog.getNombre().getText().trim());
		 producto.setRfc(edicionProductoDialog.getRfc().getText().trim());
		 //producto.setPoblacion((Poblacion) edicionProductoDialog.getPoblacion().getSelectedItem());
		 producto.setNombreMarca(edicionProductoDialog.getMarcaNombre().getText().trim()+"~"+edicionProductoDialog.getDireccion().getText());
		 producto.setNombreLinea(edicionProductoDialog.getLineaNombre().getText().trim());
		 */
	}

	public void aceptar() {
		logger.debug("[ACEPTAR]");
		try {

			if (validator.validate()) {

				fillProducto();

				applicationLogic.persistProducto(producto);

				edicionProductoDialog.setVisible(false);
				exitStatus = JOptionPane.OK_OPTION;
			}
		} catch (BusinessException ex) {
			logger.error("-->> despues de aceptar guardar BusinessException:", ex);
			JOptionPane.showMessageDialog(edicionProductoDialog,
					ex.getMessage(),
					ex.getTitle(),
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			logger.error("-->> despues de aceptar guardar Exception:", ex);
			JOptionPane.showMessageDialog(edicionProductoDialog,
					ex.getMessage(),
					BusinessException.getLocalizedMessage("APP_LOGIC_CLIENTE_NOT_SAVED"),
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void cancelar() {
		edicionProductoDialog.setVisible(false);
		exitStatus = JOptionPane.CANCEL_OPTION;
	}
	
	public void nuevaLinea() {
		logger.info("->nuevaLinea()");
		edicionLineaControl.crearLinea();
		if (edicionLineaControl.getExitStatus() == JOptionPane.YES_OPTION) {
			Linea lineaAgregada = edicionLineaControl.getLinea();
			refreshLineasList();
			edicionProductoDialog.getLinea().updateUI();
			selectLinea(lineaAgregada);
			edicionProductoDialog.getLinea().requestFocus();			
		}
	}
	
	public void nuevaIndustria() {
		logger.info("->nuevaIndustria()");
		edicionIndustriaControl.crearIndustria();
		if (edicionLineaControl.getExitStatus() == JOptionPane.YES_OPTION) {
			Industria industriaAgregada = edicionIndustriaControl.getIndustria();
			refreshIndustriasList();
			edicionProductoDialog.getIndustria().updateUI();
			selectIndustria(industriaAgregada);
			edicionProductoDialog.getIndustria().requestFocus();			
		}
	}
	
	public void nuevaMarca() {
		logger.info("->nuevaIndustria()");
		edicionMarcaControl.crearMarca(this.lineaItemSelected.getLinea(), this.industriaItemSelected.getIndustria());
//		if (edicionLineaControl.getExitStatus() == JOptionPane.YES_OPTION) {
//			Industria industriaAgregada = edicionIndustriaControl.getIndustria();
//			refreshIndustriasList();
//			edicionProductoDialog.getIndustria().updateUI();
//			selectIndustria(industriaAgregada);
//			edicionProductoDialog.getIndustria().requestFocus();			
//		}
	}

	void centerInScreenAndSetVisible(JDialog w) {
		int fw = w.getWidth();
		int fh = w.getHeight();
		Rectangle recScreen = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		w.setBounds(((int) recScreen.getWidth() - fw) / 2, ((int) recScreen.getHeight() - fh) / 2,
				fw, fh);
		w.setVisible(true);
	}

	/**
	 * @return the exitStatus
	 */
	public int getExitStatus() {
		return exitStatus;
	}

	/**
	 * @return the applicationLogic
	 */
	public ApplicationLogic getApplicationLogic() {
		return applicationLogic;
	}

	/**
	 * @param applicationLogic the applicationLogic to set
	 */
	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}

	/**
	 * @return the principalControl
	 */
	public PrincipalControl getPrincipalControl() {
		return principalControl;
	}

	/**
	 * @param principalControl the principalControl to set
	 */
	public void setPrincipalControl(PrincipalControl principalControl) {
		this.principalControl = principalControl;
	}

	/**
	 * @return the basicInfoDAO
	 */
	public BasicInfoDAO getBasicInfoDAO() {
		return basicInfoDAO;
	}

	/**
	 * @param basicInfoDAO the basicInfoDAO to set
	 */
	public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
		this.basicInfoDAO = basicInfoDAO;
	}

	private void makeSelectionsFromNames() {
		logger.info("->makeSelectionsFromNames:");
		for (int i = 0; i < lineas.size(); i++) {
			Linea lineaIter = lineas.get(i);
			if (lineaIter.getNombre().equals(productoRowModel.getLineaNombre())) {
				lineaItemSelected = (LineaItemList) edicionProductoDialog.getLinea().getItemAt(i + 1);
				edicionProductoDialog.getLinea().setSelectedIndex(i + 1);
				logger.info("\t->makeSelectionsFromNames: OK, Linea selected to:lineaIter.getNombre()=" + lineaIter.getNombre() + " == productoRowModel.getLineaNombre()=" + productoRowModel.getLineaNombre() + ", index=" + (i + 1));
				break;
			}
		}
		for (int j = 0; j < industrias.size(); j++) {
			Industria industriaIter = industrias.get(j);
			if (industriaIter.getNombre().equals(productoRowModel.getIndustriaNombre())) {
				industriaItemSelected = (IndustriaItemList) edicionProductoDialog.getIndustria().getItemAt(j + 1);
				edicionProductoDialog.getIndustria().setSelectedIndex(j + 1);
				logger.info("\t->makeSelectionsFromNames: OK, Industria selected to: industriaIter.getNombre()=" + industriaIter.getNombre() + "== productoRowModel.getLineaNombre()=" + productoRowModel.getIndustriaNombre() + ", index=" + (j + 1));
				break;
			}
		}

		if (this.hasMarcaValidSelectionsState()) {
			updateMarcasWithValidSelection();
			for (int k = 0; k < marcas.size(); k++) {
				Marca marcaIter = marcas.get(k);
				if (marcaIter.getNombre().equals(productoRowModel.getMarcaNombre())) {
					marcaItemSelected = (MarcaItemList) edicionProductoDialog.getMarca().getItemAt(k + 1);
					edicionProductoDialog.getMarca().setSelectedIndex(k + 1);
					logger.info("\t->makeSelectionsFromNames: OK, Marca selected to: marcaIter.getNombre()=" + marcaIter.getNombre() + "== productoRowModel.geMarcaNombre()=" + productoRowModel.getMarcaNombre() + ", index=" + (k + 1));
					break;
				}
			}
		} else {
			updateMarcasWithInvalidSelection();
		}
	}
	private void selectLinea(Linea linea) {
		logger.info("->selectLinea:");
		for (int i = 0; i < lineas.size(); i++) {
			Linea lineaIter = lineas.get(i);
			if (lineaIter.getNombre().equals(linea.getNombre())) {
				lineaItemSelected = (LineaItemList) edicionProductoDialog.getLinea().getItemAt(i + 1);
				edicionProductoDialog.getLinea().setSelectedIndex(i + 1);				
				break;
			}
		}
	}
	
	private void selectIndustria(Industria industria) {
		logger.info("->selectIndustria:");
		for (int i = 0; i < industrias.size(); i++) {
			Industria industriaIter = industrias.get(i);
			if (industriaIter.getNombre().equals(industria.getNombre())) {
				industriaItemSelected = (IndustriaItemList) edicionProductoDialog.getIndustria().getItemAt(i + 1);
				edicionProductoDialog.getIndustria().setSelectedIndex(i + 1);				
				break;
			}
		}
	}
	/**
	 * @param edicionLineaControl the edicionLineaControl to set
	 */
	public void setEdicionLineaControl(EdicionLineaControl edicionLineaControl) {
		this.edicionLineaControl = edicionLineaControl;
	}
	/**
	 * @param edicionIndustriaControl the edicionIndustriaControl to set
	 */
	public void setEdicionIndustriaControl(EdicionIndustriaControl edicionIndustriaControl) {
		this.edicionIndustriaControl = edicionIndustriaControl;
	}
	/**
	 * @param edicionMarcaControl the edicionMarcaControl to set
	 */
	public void setEdicionMarcaControl(EdicionMarcaControl edicionMarcaControl) {
		this.edicionMarcaControl = edicionMarcaControl;
	}

	public void refreshLineasList() {
		try {
			lineas = basicInfoDAO.getLineaList();
		} catch (Exception ex) {
			lineas = new ArrayList<Linea>();
		}
		edicionProductoDialog.getLinea().setModel(new LineaComboBoxModel(lineas));
	}

	public void refreshIndustriasList() {
		try {
			industrias = basicInfoDAO.getIndustriaList();
		} catch (Exception ex) {
			industrias = new ArrayList<Industria>();
		}
		edicionProductoDialog.getIndustria().setModel(new IndustriaComboBoxModel(industrias));
	}
}

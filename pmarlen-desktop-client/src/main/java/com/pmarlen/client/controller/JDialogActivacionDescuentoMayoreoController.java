/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.model.SecurityToken;
import com.pmarlen.client.view.JDialogActivacionDescuentoMayoreo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfredo
 */
@Controller("jDialogActivacionDescuentoMayoreoController")
public class JDialogActivacionDescuentoMayoreoController implements ActionListener {

	@Autowired
	private ApplicationLogic applicationLogic;
	private String phraseGenerated;
	private JDialogActivacionDescuentoMayoreo jDialogActivacionDescuentoMayoreo;
	private int intentos;
	
	public JDialogActivacionDescuentoMayoreoController() {
	}

	public void estadoInicial(JFrame parent) {
		if (jDialogActivacionDescuentoMayoreo == null) {
			jDialogActivacionDescuentoMayoreo = new JDialogActivacionDescuentoMayoreo(parent);
			jDialogActivacionDescuentoMayoreo.getAceptar().addActionListener(this);
			jDialogActivacionDescuentoMayoreo.getCancelar().addActionListener(this);
		}
		
		intentos = 0;
		jDialogActivacionDescuentoMayoreo.getToken().setText("");
		
		boolean descuentoActivado = applicationLogic.isDescuentoMayoreoEnabledForCurrentSucursal();
		
		if (descuentoActivado) {
			jDialogActivacionDescuentoMayoreo.getActivado().setSelected(descuentoActivado);
		} else {
			jDialogActivacionDescuentoMayoreo.getDesactivado().setSelected(!descuentoActivado);
		}

		phraseGenerated = SecurityToken.getNextPhrase();

		jDialogActivacionDescuentoMayoreo.getPhrase().setText(phraseGenerated);		
		jDialogActivacionDescuentoMayoreo.setVisible(true);
		
		jDialogActivacionDescuentoMayoreo.getToken().requestFocus();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == jDialogActivacionDescuentoMayoreo.getAceptar()) {
			aceptar();
		} else if (ae.getSource() == jDialogActivacionDescuentoMayoreo.getCancelar()) {
			cancelar();
		}
	}

	private void aceptar() {
		String tokenByUser = jDialogActivacionDescuentoMayoreo.getToken().getText().trim();
		intentos++;
		final String tokenPattern = "([0-9]){6}";
		
		if (! tokenByUser.matches(tokenPattern)) {
			jDialogActivacionDescuentoMayoreo.getToken().setText("");
			JOptionPane.showMessageDialog(jDialogActivacionDescuentoMayoreo, "Este no corresponde a un Token", "Activación > Token", JOptionPane.ERROR_MESSAGE);
			jDialogActivacionDescuentoMayoreo.getToken().requestFocus();
			if(intentos>=3){
				cancelar();
			}
			return;
		}
		
		if (! SecurityToken.matchPhraseWithHisToken(phraseGenerated, tokenByUser)) {
			jDialogActivacionDescuentoMayoreo.getToken().setText("");
			JOptionPane.showMessageDialog(jDialogActivacionDescuentoMayoreo, "Token capturado erroneo", "Activación > Validación Token", JOptionPane.ERROR_MESSAGE);
			jDialogActivacionDescuentoMayoreo.getToken().requestFocus();
			if(intentos>=3){
				cancelar();
			}
		} else {
			if (jDialogActivacionDescuentoMayoreo.getActivado().isSelected()) {
				applicationLogic.enableDescuentoMayoreoForCurrentSucursal();
				JOptionPane.showMessageDialog(jDialogActivacionDescuentoMayoreo, "Descuento Activado ( 5% y 10%  en Mayoreo)", "Activación", JOptionPane.WARNING_MESSAGE);
				jDialogActivacionDescuentoMayoreo.dispose();				
			} else if (jDialogActivacionDescuentoMayoreo.getDesactivado().isSelected()) {
				applicationLogic.disableDescuentoMayoreoForCurrentSucursal();
				JOptionPane.showMessageDialog(jDialogActivacionDescuentoMayoreo, "Descuento Desactivado, solo precios propios", "Activación", JOptionPane.WARNING_MESSAGE);
				jDialogActivacionDescuentoMayoreo.dispose();
			}
		}
	}

	private void cancelar() {
		jDialogActivacionDescuentoMayoreo.dispose();
	}

	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.PersistEntityWithTransactionDAO;
import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alfred
 */
@Repository("firstDataSynchronizer")
public class FirstDataSynchronizer {

    private Logger logger;

    private PersistEntityWithTransactionDAO persistEntityWithTransactionDAO;

    private SynchronizationWithServerRegistryController synchronizationWithServerRegistryController;

        
    public FirstDataSynchronizer() {
        logger = LoggerFactory.getLogger(FirstDataSynchronizer.class);
    }

    /**
     * @param persistEntityWithTransactionDAO the persistEntityWithTransactionDAO to set
     */
    @Autowired
    public void setPersistEntityWithTransactionDAO(PersistEntityWithTransactionDAO persistEntityWithTransactionDAO) {
        this.persistEntityWithTransactionDAO = persistEntityWithTransactionDAO;
    }

        /**
     * @param synchronizationWithServerRegistryController the synchronizationWithServerRegistryController to set
     */
    @Autowired
    public void setSynchronizationWithServerRegistryController(SynchronizationWithServerRegistryController synchronizationWithServerRegistryController) {
        this.synchronizationWithServerRegistryController = synchronizationWithServerRegistryController;
    }

    public void checkServerVersion() throws IllegalStateException,UpdateInminentException,UpdateBugFixingException, BusinessException {

        String serverVersion = null;
		logger.debug("-> checkServerVersion:Constants.getServerVersion()="+Constants.getServerVersion());
        GetListDataBusiness getListDataBusinessServiceClient = WebServiceConnectionConfig.getInstance().getGetListDataBusiness();
        logger.debug("-> checkServerVersion: WS Stub getListDataBusinessServiceClient=" + getListDataBusinessServiceClient);
        serverVersion = getListDataBusinessServiceClient.getServerVersion();

        logger.debug("\t--->>checkServerVersion():serverVersion=" + serverVersion);

        String[] serverVersionPart = serverVersion.split("\\.");
        String[] mainVersionPart = ApplicationInfo.getInstance().getVersion().split("\\.");
        if (!serverVersionPart[0].equalsIgnoreCase(mainVersionPart[0])){
			throw new IllegalStateException("No se tiene una version compatible con servidor.\n"
                    + " Instale una version compatible con la Version:" + serverVersionPart[0] + "." + serverVersionPart[1] + "@xxx");
		} else if(Long.parseLong(serverVersionPart[1]) > Long.parseLong(mainVersionPart[1]) ) {
            throw new UpdateInminentException("Inminent Update "+ApplicationInfo.getInstance().getVersion()+" -> "+serverVersion);
        } else if(Long.parseLong(serverVersionPart[2]) > Long.parseLong(mainVersionPart[2])) {
            throw new UpdateBugFixingException("Actualización de versión "+ApplicationInfo.getInstance().getVersion()+" a versión "+serverVersion);
        } else if(Long.parseLong(serverVersionPart[2]) > Long.parseLong(mainVersionPart[2])) {
            logger.debug("Dynamic patch :P !!");
        }
		logger.debug("<----checkServerVersion():OK");

    }

    public void firstSyncronization(ProgressProcessListener progressListener) throws Exception {
        logger.debug("====================>> firstSyncronization(): just on Master Host !!");

        progressListener.updateProgress(30, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_CREATING_WS"));
        GetListDataBusiness getListDataBusinessServiceClient = WebServiceConnectionConfig.getInstance().getGetListDataBusiness();

        try {
            logger.debug("firstSyncronization():delete all objs");
            progressListener.updateProgress(31, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_DELETE_ALL_OBJECTESS"));
            persistEntityWithTransactionDAO.deleteAllObjects();
			
            // =====================================================================
			progressListener.updateProgress(33, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_SUCURSALLIST"));
            List<Sucursal> resultSucursalList = getListDataBusinessServiceClient.getSucursalList();
            if(resultSucursalList == null){
				resultSucursalList = new ArrayList<Sucursal>();
			}
			progressListener.updateProgress(35, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_SUCURSALLIST"));
            persistEntityWithTransactionDAO.inicializarSucursal(resultSucursalList, progressListener);
            
            // =====================================================================
			
            progressListener.updateProgress(36, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_USUARIOLIST"));
            List<Usuario> usuarioReceivedList = getListDataBusinessServiceClient.getUsuarioList();
			if(usuarioReceivedList == null){
				usuarioReceivedList = new ArrayList<Usuario>();
			}
            progressListener.updateProgress(38, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_USUARIOLIST"));
            logger.debug("firstSyncronization():WebService:getUsuarioList():Result = " + usuarioReceivedList);
            persistEntityWithTransactionDAO.inicializarUsuarios(usuarioReceivedList, progressListener);
            usuarioReceivedList = null;
			
            // =====================================================================
            progressListener.updateProgress(40, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_LINEALIST"));
            List<Linea> lineaReceivedList = getListDataBusinessServiceClient.getLineaList();
			if(lineaReceivedList == null){
				lineaReceivedList = new ArrayList<Linea>();
			}
            logger.debug("firstSyncronization():WebService:getLineaList():Result = " + lineaReceivedList);
            progressListener.updateProgress(43, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_LINEALIST"));
            persistEntityWithTransactionDAO.inicializarLinea(lineaReceivedList, progressListener);
            lineaReceivedList = null;
            // =====================================================================
            progressListener.updateProgress(45, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_INDUSTRIALIST"));
            List<Industria> industriaRecivedList = getListDataBusinessServiceClient.getIndustriaList();
            if(industriaRecivedList == null){
				industriaRecivedList = new ArrayList<Industria>();
			}
			logger.debug("firstSyncronization():WebService:getIndustriaList():Result = " + industriaRecivedList);
            progressListener.updateProgress(48, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_INDUSTRIALIST"));
            persistEntityWithTransactionDAO.inicializarIndustrias(industriaRecivedList, progressListener);
            industriaRecivedList = null;
            // =====================================================================
            progressListener.updateProgress(50, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_MARCALIST"));
            List<Marca> marcaReceivedList = getListDataBusinessServiceClient.getMarcaList();
			if(marcaReceivedList == null){
				marcaReceivedList = new ArrayList<Marca>();
			}
            logger.debug("firstSyncronization():WebService:getMarcaList():Result = " + marcaReceivedList);
            progressListener.updateProgress(53, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_MARCALIST"));
            persistEntityWithTransactionDAO.inicializarMarcas(marcaReceivedList, progressListener);
            marcaReceivedList = null;
            // =====================================================================            
            progressListener.updateProgress(55, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_PRODUCTOLIST"));
            List<Producto> productoReceivedList = getListDataBusinessServiceClient.getProductoList();
            if(productoReceivedList == null){
				productoReceivedList = new ArrayList<Producto>();
			}
			logger.debug("firstSyncronization():WebService:getProductoList():Result size = " + productoReceivedList.size());
            progressListener.updateProgress(56, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_PRODUCTOLIST"));
            persistEntityWithTransactionDAO.inicializarProductos(productoReceivedList, progressListener);
            productoReceivedList = null;
			// =====================================================================            
            progressListener.updateProgress(58, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_ALMACENPRODUCTOLIST"));
            List<AlmacenProducto> almacenProductoReceivedList = getListDataBusinessServiceClient.getAlmacenProductoList();
            if(almacenProductoReceivedList == null){
				almacenProductoReceivedList = new ArrayList<AlmacenProducto>();
			}
			logger.debug("firstSyncronization():WebService:getAlmacenProductoList():Result size = " + almacenProductoReceivedList.size());
            progressListener.updateProgress(56, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_ALMACENPRODUCTOLIST"));
            persistEntityWithTransactionDAO.inicializarAlmacenProductos(almacenProductoReceivedList, progressListener);
            almacenProductoReceivedList = null;
            // =====================================================================

            progressListener.updateProgress(60, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_FORMADEPAGOLIST"));
            List<FormaDePago> formaDePagoReceivedList = getListDataBusinessServiceClient.getFormaDePagoList();
            if(formaDePagoReceivedList == null){
				formaDePagoReceivedList = new ArrayList<FormaDePago>();
			}
			logger.debug("firstSyncronization():WebService:getFormaDePagoList():Result size = " + formaDePagoReceivedList.size());
            progressListener.updateProgress(63, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_FORMADEPAGOLIST"));
            persistEntityWithTransactionDAO.inicializarFormaDePago(formaDePagoReceivedList, progressListener);
            formaDePagoReceivedList = null;
            // =====================================================================
            progressListener.updateProgress(65, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_ESTADOLIST"));
            List<Estado> estadoReceivedList = getListDataBusinessServiceClient.getEstadoList();
            if(estadoReceivedList == null){
				estadoReceivedList = new ArrayList<Estado>();
			}
			logger.debug("firstSyncronization():WebService:getEstadoList():Result size = " + estadoReceivedList.size());
            progressListener.updateProgress(68, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_ESTADOLIST"));
            persistEntityWithTransactionDAO.inicializarEstado(estadoReceivedList, progressListener);
            estadoReceivedList = null;
            // =====================================================================

            progressListener.updateProgress(70, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_CLIENTELIST"));
            List<Cliente> resultClienteList = getListDataBusinessServiceClient.getClienteList();
            if(resultClienteList == null){
				resultClienteList = new ArrayList<Cliente>();
			}
//			logger.debug("firstSyncronization():WebService:getClienteList():Result size = " + resultClienteList.size());
            progressListener.updateProgress(73, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_CLIENTELIST"));
            persistEntityWithTransactionDAO.inicializarCliente(resultClienteList, progressListener);
            resultClienteList = null;
            // =====================================================================
            progressListener.updateProgress(74, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_MULTIMEDIOLIST"));
            List<Multimedio> resultMultimedioList = getListDataBusinessServiceClient.getProductoMultimedioList();
            if(resultMultimedioList == null){
				resultMultimedioList = new ArrayList<Multimedio>();
			}
			progressListener.updateProgress(75, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_MULTIMEDIOLIST"));
            persistEntityWithTransactionDAO.inicializarMultimedio(resultMultimedioList, progressListener);
            resultMultimedioList = null;
            // =====================================================================
            progressListener.updateProgress(80, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_UPDATE_LASTSYNC"));
			
            synchronizationWithServerRegistryController.updateLastSyncronization();

            //persistEntityWithTransactionDAO.reloadEMF();

            logger.debug("firstSyncronization():end OK");
            progressListener.updateProgress(92, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_OK"));

        } catch (Exception ex) {
            throw ex;
        } finally {
            getListDataBusinessServiceClient = null;
            //logger.debug("firstSyncronization():gc");
            //System.gc();
        }
    }

    boolean needSyncronization() {
        return synchronizationWithServerRegistryController.needSyncronization();
    }

    public boolean isSyncronizatedInThisSession() {
        return synchronizationWithServerRegistryController.isSyncronizatedInThisSession();
    }

}

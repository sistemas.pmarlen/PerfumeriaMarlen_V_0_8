/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EdicionClienteDialog.java
 *
 * Created on 9/04/2009, 07:14:39 PM
 */

package com.pmarlen.client.view;

import com.pmarlen.client.ApplicationInfo;
import java.awt.Dialog;
import java.awt.Window;

/**
 *
 * @author alfred
 */
public class EdicionProductoDialog extends javax.swing.JDialog {

    /** Creates new form EdicionClienteDialog */
    public EdicionProductoDialog(Window f) {
        super(f,Dialog.ModalityType.DOCUMENT_MODAL);
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel15 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        id = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        codigoBarras = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        presentacion = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        linea = new javax.swing.JComboBox();
        nuevaLineaBtn = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        industria = new javax.swing.JComboBox();
        nuevaIndustriaBtn = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        marca = new javax.swing.JComboBox();
        nuevaMarcaBtn = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        unidadesPorCaja = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        contenido = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        unidadMedida = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        costo = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        precioVentaNormal = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        cantActualNormal = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        precioVentaOportunidad = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        cantActualOportunidad = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        guardar = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Editar Producto");
        getContentPane().setLayout(new java.awt.BorderLayout(0, 5));
        getContentPane().add(jPanel15, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.GridLayout(11, 1, 0, 2));

        jPanel3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_CLIENTE")); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel3.add(jLabel1);

        id.setEditable(false);
        id.setColumns(5);
        id.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel3.add(id);

        jPanel1.add(jPanel3);

        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Codigo de Barras :");
        jLabel2.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel4.add(jLabel2);

        codigoBarras.setColumns(12);
        codigoBarras.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel4.add(codigoBarras);

        jPanel1.add(jPanel4);

        jPanel5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Nombre :");
        jLabel3.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel5.add(jLabel3);

        nombre.setColumns(30);
        jPanel5.add(nombre);

        jPanel1.add(jPanel5);

        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_NOEXT")); // NOI18N
        jLabel4.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel6.add(jLabel4);

        presentacion.setColumns(30);
        jPanel6.add(presentacion);

        jPanel1.add(jPanel6);

        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_NOINT")); // NOI18N
        jLabel5.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel7.add(jLabel5);
        jPanel7.add(linea);

        nuevaLineaBtn.setText("+");
        jPanel7.add(nuevaLineaBtn);

        jPanel1.add(jPanel7);

        jPanel11.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_POB")); // NOI18N
        jLabel9.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel11.add(jLabel9);
        jPanel11.add(industria);

        nuevaIndustriaBtn.setText("+");
        jPanel11.add(nuevaIndustriaBtn);

        jPanel1.add(jPanel11);

        jPanel8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_MPIO")); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel8.add(jLabel7);
        jPanel8.add(marca);

        nuevaMarcaBtn.setText("+");
        jPanel8.add(nuevaMarcaBtn);

        jPanel1.add(jPanel8);

        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Unidades X Caja :");
        jLabel8.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel9.add(jLabel8);

        unidadesPorCaja.setColumns(5);
        unidadesPorCaja.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel9.add(unidadesPorCaja);

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TEL")); // NOI18N
        jLabel10.setPreferredSize(new java.awt.Dimension(150, 18));
        jPanel9.add(jLabel10);

        contenido.setColumns(5);
        contenido.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel9.add(contenido);

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TEL")); // NOI18N
        jLabel14.setPreferredSize(new java.awt.Dimension(150, 18));
        jPanel9.add(jLabel14);

        unidadMedida.setColumns(5);
        jPanel9.add(unidadMedida);

        jPanel1.add(jPanel9);

        jPanel12.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0));

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Costo :");
        jLabel13.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel12.add(jLabel13);

        costo.setColumns(8);
        costo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel12.add(costo);

        jPanel1.add(jPanel12);

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel13.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Almacén Normal :");
        jLabel11.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel13.add(jLabel11);

        jLabel15.setText("Precio Venta :");
        jPanel13.add(jLabel15);

        precioVentaNormal.setColumns(8);
        precioVentaNormal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel13.add(precioVentaNormal);

        jLabel17.setText("Inventario Actual :");
        jPanel13.add(jLabel17);

        cantActualNormal.setColumns(8);
        cantActualNormal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel13.add(cantActualNormal);

        jPanel1.add(jPanel13);

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel14.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText(ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_COMMENT")); // NOI18N
        jLabel12.setPreferredSize(new java.awt.Dimension(180, 18));
        jPanel14.add(jLabel12);

        jLabel16.setText("Precio Venta :");
        jPanel14.add(jLabel16);

        precioVentaOportunidad.setColumns(8);
        precioVentaOportunidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel14.add(precioVentaOportunidad);

        jLabel18.setText("Inventario Actual :");
        jPanel14.add(jLabel18);

        cantActualOportunidad.setColumns(8);
        cantActualOportunidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel14.add(cantActualOportunidad);

        jPanel1.add(jPanel14);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        guardar.setText(ApplicationInfo.getLocalizedMessage("SAVE")); // NOI18N
        jPanel2.add(guardar);

        cancelar.setText(ApplicationInfo.getLocalizedMessage("CANCELL")); // NOI18N
        jPanel2.add(cancelar);

        getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelar;
    private javax.swing.JTextField cantActualNormal;
    private javax.swing.JTextField cantActualOportunidad;
    private javax.swing.JTextField codigoBarras;
    private javax.swing.JTextField contenido;
    private javax.swing.JTextField costo;
    private javax.swing.JButton guardar;
    private javax.swing.JTextField id;
    private javax.swing.JComboBox industria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JComboBox linea;
    private javax.swing.JComboBox marca;
    private javax.swing.JTextField nombre;
    private javax.swing.JButton nuevaIndustriaBtn;
    private javax.swing.JButton nuevaLineaBtn;
    private javax.swing.JButton nuevaMarcaBtn;
    private javax.swing.JTextField precioVentaNormal;
    private javax.swing.JTextField precioVentaOportunidad;
    private javax.swing.JTextField presentacion;
    private javax.swing.JTextField unidadMedida;
    private javax.swing.JTextField unidadesPorCaja;
    // End of variables declaration//GEN-END:variables

	/**
	 * @return the cancelar
	 */
	public javax.swing.JButton getCancelar() {
		return cancelar;
	}

	/**
	 * @return the cantActualNormal
	 */
	public javax.swing.JTextField getCantActualNormal() {
		return cantActualNormal;
	}

	/**
	 * @return the cantActualOportunidad
	 */
	public javax.swing.JTextField getCantActualOportunidad() {
		return cantActualOportunidad;
	}

	/**
	 * @return the codigoBarras
	 */
	public javax.swing.JTextField getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * @return the contenido
	 */
	public javax.swing.JTextField getContenido() {
		return contenido;
	}

	/**
	 * @return the costo
	 */
	public javax.swing.JTextField getCosto() {
		return costo;
	}

	/**
	 * @return the guardar
	 */
	public javax.swing.JButton getGuardar() {
		return guardar;
	}

	/**
	 * @return the id
	 */
	public javax.swing.JTextField getId() {
		return id;
	}

	/**
	 * @return the nombre
	 */
	public javax.swing.JTextField getNombre() {
		return nombre;
	}

	/**
	 * @return the precioVentaNormal
	 */
	public javax.swing.JTextField getPrecioVentaNormal() {
		return precioVentaNormal;
	}

	/**
	 * @return the precioVentaOportunidad
	 */
	public javax.swing.JTextField getPrecioVentaOportunidad() {
		return precioVentaOportunidad;
	}

	/**
	 * @return the presentacion
	 */
	public javax.swing.JTextField getPresentacion() {
		return presentacion;
	}

	/**
	 * @return the unidadMedida
	 */
	public javax.swing.JTextField getUnidadMedida() {
		return unidadMedida;
	}

	/**
	 * @return the unidadesPorCaja
	 */
	public javax.swing.JTextField getUnidadesPorCaja() {
		return unidadesPorCaja;
	}

	/**
	 * @return the industria
	 */
	public javax.swing.JComboBox getIndustria() {
		return industria;
	}

	/**
	 * @return the marca
	 */
	public javax.swing.JComboBox getMarca() {
		return marca;
	}

	/**
	 * @return the linea
	 */
	public javax.swing.JComboBox getLinea() {
		return linea;
	}

	/**
	 * @return the nuevaIndustriaBtn
	 */
	public javax.swing.JButton getNuevaIndustriaBtn() {
		return nuevaIndustriaBtn;
	}

	/**
	 * @return the nuevaLineaBtn
	 */
	public javax.swing.JButton getNuevaLineaBtn() {
		return nuevaLineaBtn;
	}

	/**
	 * @return the nuevaMarcaBtn
	 */
	public javax.swing.JButton getNuevaMarcaBtn() {
		return nuevaMarcaBtn;
	}

}

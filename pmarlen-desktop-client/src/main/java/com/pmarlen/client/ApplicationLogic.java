/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

//import com.pmarlen.client.business.dao.BasicInfoDAO;
import com.pmarlen.businesslogic.exception.AuthenticationException;
import com.pmarlen.businesslogic.exception.PedidoVentaException;
import com.pmarlen.client.controller.PreferencesController;
import com.pmarlen.client.model.ApplicationSession;
import com.pmarlen.client.model.DesgloseImportePedidoVenta;
import com.pmarlen.client.model.ProductoFastDisplayModel;
import com.pmarlen.client.ticketprinter.TicketBlueToothPrinter;
import com.pmarlen.client.ticketprinter.TicketPOSTermalPrinter;
import com.pmarlen.client.ticketprinter.TicketPOSTextPrinter;
import com.pmarlen.client.ticketprinter.TicketPrinteService;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.BasicInfoDAO;
import com.pmarlen.model.controller.PersistEntityWithTransactionDAO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("applicationLogic")
public class ApplicationLogic {

	public static final String CONFIG_PREFERENCES_DIR = "./config/";
	public static final String CONFIG_PREFERENCESPROPERTIES = "./config/Preferences.properties";
	public static final String CONFIG_PREFERENCESPROPERTIES_CLASSPATH = "/config/Preferences.properties";
	private ApplicationSession applicationSession;
	private SendDataSynchronizer sendDataSynchronizer;
	private Logger logger;
	private EntityManagerFactory emf;
	private TicketPrinteService ticketPrinterService;
	public static int IMPRESION_TIPO_TERMICAPOS = 1;
	public static int IMPRESION_TIPO_BLUETOOTH = 2;
	//private int impresionTipo;
	private Properties preferences;
	private Boolean descuentoMayoreoActivado = null;

	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}
	private BasicInfoDAO basicInfoDAO;

	@Autowired
	public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
		this.basicInfoDAO = basicInfoDAO;
	}
	PersistEntityWithTransactionDAO persistEntityWithTransactionDAO;

	@Autowired
	public void setPersistEntityWithTransactionDAO(PersistEntityWithTransactionDAO persistEntityWithTransactionDAO) {
		this.persistEntityWithTransactionDAO = persistEntityWithTransactionDAO;
	}
	private double factorIVA;

	public ApplicationLogic() {
		factorIVA = 0.16;
		logger = LoggerFactory.getLogger(ApplicationLogic.class);
		logger.debug("->ApplicationLogic, created");
		
		cambiarTipoImpresionTermicaPOS();
	}

	public void cambiarTipoImpresionTermicaPOS() {
		//impresionTipo = IMPRESION_TIPO_TERMICAPOS;
		ticketPrinterService = new TicketPOSTextPrinter();
		ticketPrinterService.setApplicationLogic(this);
	}

	public void cambiarTipoImpresionBlueTooth() {
		//impresionTipo = IMPRESION_TIPO_BLUETOOTH;
		ticketPrinterService = new TicketBlueToothPrinter();
		ticketPrinterService.setApplicationLogic(this);
	}

	public void deleteProductoFromCurrentPedidoVenta(int indexProdToDelete) {
		try {
			Collection<PedidoVentaDetalle> afeterDelete = new ArrayList<PedidoVentaDetalle>();

			final Iterator<PedidoVentaDetalle> iterator = applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().iterator();
			PedidoVentaDetalle pvdToDelete = null;
			for (int nd = 0; iterator.hasNext(); nd++) {
				final PedidoVentaDetalle next = iterator.next();
				if (nd != indexProdToDelete) {
					afeterDelete.add(next);
				}
			}

			applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().clear();
			applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().addAll(afeterDelete);
			updateTotalPedido();
		} catch (IndexOutOfBoundsException ioe) {
			throw new IllegalArgumentException("Producto [index] Not exist in PedidoVentaDetalle:" + ioe.getMessage());
		}
	}

	public void setMarcaPorLinea(int idMarca) {
		logger.debug("->setMarcaDeProductos: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Marca marca = em.find(Marca.class, idMarca);
			applicationSession.setMarcaPorLinea(marca);
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void setMarcaPorIndustria(int idMarca) {
		logger.debug("->setMarcaDeProductos: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Marca marca = em.find(Marca.class, idMarca);
			applicationSession.setMarcaPorIndustria(marca);
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public List<Producto> getProductosForPrinting() {
		logger.debug("->setMarcaDeProductos: ");
		List<Producto> result = null;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			final Query q = em.createQuery("select x from Producto x");
			result = q.getResultList();
			for (Producto p : result) {
				final Collection<AlmacenProducto> almacenProductoCollection = p.getAlmacenProductoCollection();
				for (AlmacenProducto ap : almacenProductoCollection) {
					ap.getAlmacen().getId();
				}
			}
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
		return result;
	}

	public void startNewPedidoVentaSession() {
		applicationSession.setProductoBuscadoActual(null);
		applicationSession.setPedidoVenta(new PedidoVenta());
		applicationSession.getPedidoVenta().
				setPedidoVentaDetalleCollection(new ArrayList<PedidoVentaDetalle>());
	}

	public PedidoVentaDetalle searchProducto(Producto prod) {
		PedidoVentaDetalle dvpFound = null;
		logger.debug("===>>>searchProducto(" + prod.getId() + ")");
		for (PedidoVentaDetalle dvp : applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection()) {
			logger.debug("\t===>>>comparing " + prod.getId() + " != " + dvp.getProducto().getId() + " ? ");
			if (dvp.getProducto().getId().equals(prod.getId())) {
				dvpFound = dvp;
				logger.debug("\t\t===>>>FOUND");
				break;
			}
		}
		return dvpFound;
	}
	boolean withDuplicates = true;
	public boolean isWithDuplicates(){
		return withDuplicates;
	}
			
	public void addProductoNToCurrentPedidoVenta(Producto prod, int n) {
		PedidoVentaDetalle dvpFound = null;
		PedidoVentaDetalle dvp = null;
		if(! withDuplicates) {
			dvpFound = searchProducto(prod);					
			if (dvpFound == null) {
				dvp = new PedidoVentaDetalle();
				dvp.setProducto(prod);
				dvp.setCantidad(n);
				dvp.setPrecioVenta(-1.0);
			} else {
				dvp = dvpFound;
				dvp.setCantidad(dvp.getCantidad() + n);
			}
			boolean ea = checarExistanciaEnAlmacenActual(prod.getId(), dvp.getCantidad());
			logger.debug("->addProductoNToCurrentPedidoVenta: ea ?" + ea);
			if (!ea) {
				throw new IllegalStateException("¡ NO HAY EXISTENCIA EN ESTE ALMACÉN !");
			}
			double precio=checarPrecioEnAlmacenActual(prod.getId(), dvp.getCantidad());
			dvp.setPrecioVenta(precio);

			if (dvpFound == null) {
				applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().add(dvp);
				
			}
		} else {
			dvp = new PedidoVentaDetalle();
			dvp.setProducto(prod);
			dvp.setCantidad(n);
			dvp.setPrecioVenta(-1.0);
			
			boolean es2x1 = checarEsPromocion2x1(prod.getId());
			boolean ea = false;
			if(es2x1){
				ea = checarExistanciaEnAlmacenActual(prod.getId(), dvp.getCantidad() * 2);
				logger.debug("->addProductoNToCurrentPedidoVenta: 2x1 ea ?" + ea);
				if (!ea) {
					throw new IllegalStateException("¡ NO HAY EXISTENCIA EN ESTE ALMACÉN + 2x1 !");
				}
				double precio=checarPrecioEnAlmacenActual(prod.getId(), dvp.getCantidad());
				dvp.setPrecioVenta(precio);

				applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().add(dvp);			
				
				logger.debug("->addProductoNToCurrentPedidoVenta: Agregando "+n+" Art. de promocion 2x1, precio = 0.0");
				dvp = new PedidoVentaDetalle();
				dvp.setProducto(prod);
				dvp.setCantidad(n);
				dvp.setPrecioVenta(0.0);
				
				applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().add(dvp);
				
			} else {
				ea = checarExistanciaEnAlmacenActual(prod.getId(), dvp.getCantidad());
				logger.debug("->addProductoNToCurrentPedidoVenta: ea ?" + ea);
				if (!ea) {
					throw new IllegalStateException("¡ NO HAY EXISTENCIA EN ESTE ALMACÉN !");
				}
				double precio=checarPrecioEnAlmacenActual(prod.getId(), dvp.getCantidad());
				dvp.setPrecioVenta(precio);

				applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().add(dvp);			

			}
		}		
		updateTotalPedido();
	}

	public void updateTotalPedido() {
		double st = 0.0;
		double si = 0.0;
		double sd = 0.0;

		double precioVenta = 0.0;
		int nPzs = 0;
		for (PedidoVentaDetalle dvp : getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
			precioVenta = dvp.getPrecioVenta();
			nPzs += dvp.getCantidad();
			si += dvp.getCantidad() * precioVenta;
		}

		logger.debug("->updateTotalPedido: applicationLogic.getDescuentoMayoreoActivado()=" + getDescuentoMayoreoActivado());

		if (getDescuentoMayoreoActivado() && getSession().getAlmacen().getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
			if (nPzs >= 12) {
				sd = si * 0.1;
			} else if (si > 100 && si <= 200) {
				sd = si * 0.05;
			} else if (si > 200) {
				sd = si * 0.1;
			}
		}

		double osi = 0.0;
		double osd = 0.0;
		double otax = 0.0;
		double ostt = 0.0;

		osi = si;
		osd = sd;
		otax = 0.0;
		ostt = (osi - osd);
		getSession().setTotalFinalPedido(ostt);
		getSession().getPedidoVenta().setDescuentoAplicado(sd);
	}

	public Producto consultarProductoAlmacenActual(String codigoDeBarras) {
		logger.debug("->consultarProductoAlmacenActual: codigoDeBarras=" + codigoDeBarras);
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			final Query q = em.createQuery("select x from Producto x where x.codigoBarras = :codigoDeBarras");
			Producto p = (Producto) q.setParameter("codigoDeBarras", codigoDeBarras).getSingleResult();
			final Collection<AlmacenProducto> almacenProductoCollection = p.getAlmacenProductoCollection();
			if (almacenProductoCollection.size() > 0) {
				for (AlmacenProducto ap : almacenProductoCollection) {
				}
			}
			return p;
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
			return null;
		}

	}
	public double checarPrecioEnAlmacenActual(int productoId, int cantidad) {
		logger.debug("->checarPrecioEnAlmacenActual:productoId=" + productoId + ", cantidad=" + cantidad);
		EntityManager em = null;
		double precioActual=0.0;
		try {
			final int almacenId = applicationSession.getAlmacen().getId().intValue();

			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Producto p = em.find(Producto.class, productoId);
			final Collection<AlmacenProducto> almacenProductoCollection = p.getAlmacenProductoCollection();
			int cantidaActual = 0;
			AlmacenProducto apObjetivo = null;
			if (almacenProductoCollection.size() > 0) {
				for (AlmacenProducto ap : almacenProductoCollection) {
					if (ap.getAlmacen().getId().intValue() == almacenId) {
						cantidaActual = ap.getCantidadActual();
						apObjetivo = ap;
						break;
					}
				}
			}

			if (apObjetivo == null ) {
				return 0.0;
			}
			logger.debug("\t->cantidaActual=" + cantidaActual + ", precioVenta=" + apObjetivo.getPrecioVenta());
			precioActual = apObjetivo.getPrecioVenta();
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
		return precioActual;

	}
	
	public boolean checarEsPromocion2x1(Integer productoId) {
        logger.debug("->checarEsPromocion2x1: productoId="+productoId);
        EntityManager em = null;
        String result = null;
		boolean enFoerta2x1=false;
        try {
            em = emf.createEntityManager();
            logger.debug("->checarEsPromocion2x1: em, created");
			Query q = em.createNativeQuery("SELECT OFERTA_2X1 FROM PRODUCTO WHERE ID=:productoId ");
			q.setParameter("productoId", productoId);			
            result = (String)q.getSingleResult();
			if(result != null && result.equalsIgnoreCase("SI")){
				enFoerta2x1= true;
			}
        } catch(NoResultException nre){
			logger.debug( "->checarEsPromocion2x1:Producto not found:");
		} catch (Exception e) {
            logger.error( "->checarEsPromocion2x1:Exception caught:", e);
        } finally {
            if (em != null) {
                em.close();
                logger.debug("->checarEsPromocion2x1: Ok, Entity Manager Closed");
            }
        }
        return enFoerta2x1;
    }
	
	public boolean checarExistanciaEnAlmacenActual(int productoId, int cantidad) {
		logger.debug("->checarExistanciaEnAlmacenActual:productoId=" + productoId + ", cantidad=" + cantidad);
		EntityManager em = null;
		try {
			final int almacenId = applicationSession.getAlmacen().getId().intValue();

			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Producto p = em.find(Producto.class, productoId);
			final Collection<AlmacenProducto> almacenProductoCollection = p.getAlmacenProductoCollection();
			int cantidaActual = 0;
			AlmacenProducto apObjetivo = null;
			if (almacenProductoCollection.size() > 0) {
				for (AlmacenProducto ap : almacenProductoCollection) {
					if (ap.getAlmacen().getId().intValue() == almacenId) {
						cantidaActual = ap.getCantidadActual();
						apObjetivo = ap;
						break;
					}
				}
			}

			if (apObjetivo == null || cantidaActual < cantidad
					) {
				return false;
			}
			//pvd.setPrecioVenta(apObjetivo.getPrecioVenta());
			logger.debug("\t->cantidaActual=" + cantidaActual + ", precioVenta=" + apObjetivo.getPrecioVenta());

		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
		return true;

	}

//    public void editProductoCajaToCurrentPedidoVenta(Producto prod, int cantidadPedida) {
//        PedidoVentaDetalle dvpFound = searchProducto(prod);
//        PedidoVentaDetalle dvp = null;
//        if (dvpFound == null) {
//            throw new IllegalArgumentException("Producto not exist in PedidoVentaDetalle");
//        }
//
//        if (cantidadPedida == 0) {
//            applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().remove(dvp);
//        } else {
//            dvp.setCantidad(cantidadPedida);
//        }
//    }
	public void setClienteToCurrentPedidoVenta(Cliente cliente) {
		applicationSession.getPedidoVenta().setCliente(cliente);
	}

	public void setFormaDePagoToCurrentPedidoVenta(FormaDePago formaDePago) {
		applicationSession.getPedidoVenta().setFormaDePago(formaDePago);
	}

	public void persistCurrentPedidoVenta() throws BusinessException {
		logger.debug("->persistCurrentPedidoVenta: ");

		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			em.getTransaction().begin();
			logger.debug("->begin transaction");
			PedidoVenta nuevoPedidoVenta = new PedidoVenta();
			logger.debug("->prepare PedidoVenta");
			Usuario usuario = applicationSession.getUsuario();
			logger.debug("->\tUsuario:" + usuario);
			logger.debug("->\tSucursal:" + usuario.getSucursal());
			logger.debug("->\tSucursal in Session:" + applicationSession.getSucursal());
			logger.debug("->\tAlmacen in Session:" + applicationSession.getAlmacen());

			Collection<PedidoVentaDetalle> pedidoVentaDetalleCollection = applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection();
//			int numProductos = 0;
//			double total = 0.0;
//			double descuentoAplicado = 0.0;
//			for (PedidoVentaDetalle pvd : pedidoVentaDetalleCollection) {
//				total += pvd.getCantidad() * pvd.getPrecioVenta();
//				numProductos += pvd.getCantidad();
//			}
			logger.debug("->\tapplicationSession.getPedidoVenta().getDescuentoAplicado()" + applicationSession.getPedidoVenta().getDescuentoAplicado());


			nuevoPedidoVenta.setUsuario(em.getReference(Usuario.class, usuario.getUsuarioId()));
			nuevoPedidoVenta.setCliente(em.getReference(Cliente.class, applicationSession.getPedidoVenta().getCliente().getId()));
			nuevoPedidoVenta.setFormaDePago(em.getReference(FormaDePago.class, applicationSession.getPedidoVenta().getFormaDePago().getId()));
			nuevoPedidoVenta.setDescuentoAplicado(applicationSession.getPedidoVenta().getDescuentoAplicado());
			nuevoPedidoVenta.setComentarios(applicationSession.getPedidoVenta().getComentarios());
			logger.debug("->\tComentarios ?" + nuevoPedidoVenta.getComentarios());
//			nuevoPedidoVenta.setFactoriva(getFactorIVA());
//			if (numProductos >= 12) {
//				descuentoAplicado = total * 0.1;
//			}
//			nuevoPedidoVenta.setDescuentoAplicado(descuentoAplicado);
			nuevoPedidoVenta.setAlmacen(applicationSession.getAlmacen());

			em.persist(nuevoPedidoVenta);
			logger.debug("->ok, Pedido inserted");

			PedidoVentaEstado pedidoVentaEstado = null;
			pedidoVentaEstado = new PedidoVentaEstado();

			pedidoVentaEstado.setPedidoVenta(nuevoPedidoVenta);
			pedidoVentaEstado.setEstado(new Estado(Constants.ESTADO_CAPTURADO));
			pedidoVentaEstado.setFecha(new Date());
			pedidoVentaEstado.setUsuario(usuario);
			//pedidoVentaEstado.setComentarios("TEST CASE INSERT");

			List<PedidoVentaEstado> listPedidoVentaEstados = new ArrayList<PedidoVentaEstado>();
			listPedidoVentaEstados.add(pedidoVentaEstado);

			List<PedidoVentaDetalle> pedidoVentaDetalleCollectionInsert = new ArrayList<PedidoVentaDetalle>();

			em.persist(pedidoVentaEstado);
			logger.debug("->ok, PedidoVentaEstado inserted");
			
			for (PedidoVentaDetalle pvd : pedidoVentaDetalleCollection) {
				PedidoVentaDetalle pvdInsert = new PedidoVentaDetalle();
				
				pvdInsert.setCantidad(pvd.getCantidad());
				pvdInsert.setPedidoVenta(nuevoPedidoVenta);
				pvdInsert.setPrecioVenta(pvd.getPrecioVenta());
				pvdInsert.setProducto(em.getReference(Producto.class, pvd.getProducto().getId()));

				pedidoVentaDetalleCollectionInsert.add(pvdInsert);
				pvd.setPedidoVenta(nuevoPedidoVenta);
				pvd.setProducto(pvd.getProducto());
				em.persist(pvd);

				logger.debug("->\tok, PedidoVentaDetalle inserted");
				
				Query queryAP = em.createQuery("Select ap from AlmacenProducto ap where ap.almacen.id = :almacenId and ap.producto.id =:productoId");
				queryAP.setParameter("almacenId", applicationSession.getAlmacen().getId());
				queryAP.setParameter("productoId", pvd.getProducto().getId());
				
				AlmacenProducto ap = (AlmacenProducto)queryAP.getSingleResult();
				
				ap.setCantidadActual(ap.getCantidadActual()-pvd.getCantidad());
				
				em.flush();
				
				logger.debug("->\tok, AlmacenProducto updated to -"+pvd.getCantidad());

			}

			em.getTransaction().commit();
			logger.debug("->committed.");
			//em.refresh(nuevoPedidoVenta);
			//applicationSession.setPedidoVenta(nuevoPedidoVenta);
//			logger.debug("->refreshed: nuevoPedidoVenta.id=" + nuevoPedidoVenta.getId());
//			Collection<PedidoVentaDetalle> pedidoVentaDetalleCollectionForIteration = nuevoPedidoVenta.getPedidoVentaDetalleCollection();
//			for (PedidoVentaDetalle dvpI : pedidoVentaDetalleCollectionForIteration) {
//				logger.debug("->\trefreshed: nuevoPedidoVenta.getPedidoVentaDetalleCollection.producto=" + dvpI.getProducto());
//			}
//			//

		} catch (Exception e) {
			logger.error("Exception caught:", e);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_PEDIDO_NOT_PERSISTED");
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}

	}

	public Sucursal getSucursalMatriz() throws BusinessException {
		logger.debug("->getSucursalMatriz: ");
		Sucursal sucursalMatriz = null;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Query q = em.createQuery("select s from Sucursal s where s.sucursal is null");
			sucursalMatriz = (Sucursal) q.getSingleResult();
			Collection<Almacen> almacenCollection = sucursalMatriz.getAlmacenCollection();
			for (Almacen a : almacenCollection) {
				a.getTipoAlmacen();
				Collection<AlmacenProducto> almacenProductoCollection = a.getAlmacenProductoCollection();
				for (AlmacenProducto ap : almacenProductoCollection) {
					ap.getProducto();
				}
			}
			return sucursalMatriz;

		} catch (NoResultException nre) {
			throw new BusinessException("Login", "No hay sucursl Default");
		} catch (Exception e) {
			throw new BusinessException("Login", "No hay sucursl Default:" + e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}

	}

	public Sucursal getSucursal(int sucursalId) throws BusinessException {
		logger.debug("->getSucursalMatriz: ");
		Sucursal sucursalMatriz = null;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Query q = em.createQuery("select s from Sucursal s where s.id =:sucursalId");
			q.setParameter("sucursalId", sucursalId);

			sucursalMatriz = (Sucursal) q.getSingleResult();
			Collection<Almacen> almacenCollection = sucursalMatriz.getAlmacenCollection();
			for (Almacen a : almacenCollection) {
				a.getTipoAlmacen();
				Collection<AlmacenProducto> almacenProductoCollection = a.getAlmacenProductoCollection();
				for (AlmacenProducto ap : almacenProductoCollection) {
					ap.getProducto();
				}
			}
		} catch (Exception e) {

			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
			return sucursalMatriz;
		}

	}

	public void sendPedidosAndDelete(ProgressProcessListener pl) throws AuthenticationException, PedidoVentaException {
		sendDataSynchronizer.sendAndDeletePedidos();
	}

	public void persistCliente(Cliente c) throws BusinessException {
		Cliente parecidoRfc = null;
		if (c.getId() == null) {
			try {
				parecidoRfc = basicInfoDAO.getClienteByRFC(c);
			} catch (NoResultException ex1) {
				logger.debug("->Ok, not Found by RFC");
			} catch (NonUniqueResultException ex) {
				throw new BusinessException(
						ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
						"APP_LOGIC_CLIENTE_EXIST_RFC");
			}
		}

		try {

			if (c.getId() == null) {
				//persistEntityWithTransactionDAO.persistCliente(c);
				persistEntityWithTransactionDAO.create(c);
			} else {
				persistEntityWithTransactionDAO.updateCliente(c);

			}
		} catch (NoResultException ex) {
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_FOUND");
		} catch (Exception ex) {
			logger.error("Exception caught:", ex);
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_SAVED");
		}
	}

	public void removeCliente(Cliente c) throws BusinessException {
		try {
			persistEntityWithTransactionDAO.deleteCliente(c);
		} catch (NoResultException ex) {
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_FOUND");
		} catch (Exception ex) {
			logger.error("Exception caught:", ex);
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_DELETED");
		}
	}

	public void persistProducto(Producto p) throws BusinessException {
		Producto parecidoCB = null;
		if (p.getId() == null) {
			try {
				parecidoCB = basicInfoDAO.getProductoByCodigoBarras(p);
			} catch (NoResultException ex1) {
				logger.debug("->Ok, not Found by RFC");
			} catch (NonUniqueResultException ex) {
				throw new BusinessException(
						ApplicationInfo.getLocalizedMessage("DLG_EDIT_PRODUCTO_TITLE"),
						"APP_LOGIC_PRODUCTO_EXIST_CB");
			}
		}

		try {

			if (p.getId() == null) {
				//persistEntityWithTransactionDAO.persistCliente(c);
				persistEntityWithTransactionDAO.create(p);
			} else {
				persistEntityWithTransactionDAO.updateProducto(p);

			}
		} catch (NoResultException ex) {
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_FOUND");
		} catch (Exception ex) {
			logger.error("Exception caught:", ex);
			throw new BusinessException(
					ApplicationInfo.getLocalizedMessage("DLG_EDIT_CLEINTE_TITLE"),
					"APP_LOGIC_CLIENTE_NOT_SAVED");
		}
	}

	public void printTicketPedido(PedidoVenta pedidoVenta, boolean sendToTprinter) throws BusinessException {
		Object printedObject = null;

		try {
			String propPrinterMode = preferences.getProperty(PreferencesController.PRINTER_MODE, TicketBlueToothPrinter.TERMAL_PRINTER_MODE);

			logger.debug("printTicketPedido: read properties -> propPrinterMode= " + propPrinterMode);
			if (propPrinterMode.equals(TicketBlueToothPrinter.BT_PRINTER_MODE)) {
				cambiarTipoImpresionBlueTooth();
			} else if (propPrinterMode.equals(TicketBlueToothPrinter.TERMAL_PRINTER_MODE)) {
				cambiarTipoImpresionTermicaPOS();
			}


			HashMap<String, String> extraInformation = new HashMap<String, String>();
			logger.debug("-> printTicketPedido: pedidoVenta.getDescuentoAplicado=" + pedidoVenta.getDescuentoAplicado());
			printedObject = ticketPrinterService.generateNuevoTicket(pedidoVenta, extraInformation);
			logger.debug("-> printTicketPedido: printedObject=" + printedObject);
		} catch (Exception ex) {
			logger.error("-> printTicketPedido:TicketPrinter.generateTicket ", ex);
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_TICKET_NOT_GENERATED");
		}

		if (sendToTprinter) {
			try {
				logger.debug("-> printTicketPedido:now send to default Printer");
				ticketPrinterService.sendToPrinter(printedObject);

			} catch (Exception ex) {
				logger.error("-> printTicketPedido:TicketPrinter.generateTicket ", ex);
				throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_TICKET_NOT_PRINTED");
			}
		}
	}

	public void testPrinter() throws BusinessException {
		try {
			String propPrinterMode = preferences.getProperty(PreferencesController.PRINTER_MODE, TicketBlueToothPrinter.TERMAL_PRINTER_MODE);

			logger.debug("testPrinter: read properties -> propPrinterMode= " + propPrinterMode);
			if (propPrinterMode.equals(TicketBlueToothPrinter.BT_PRINTER_MODE)) {
				cambiarTipoImpresionBlueTooth();
			} else if (propPrinterMode.equals(TicketBlueToothPrinter.TERMAL_PRINTER_MODE)) {
				cambiarTipoImpresionTermicaPOS();
			}



			logger.debug("-> testPrinter:TicketPrinter.testDefaultPrinter ?");
			ticketPrinterService.testDefaultPrinter();
			logger.debug("-> testPrinter: Ok !");
		} catch (Exception ex) {
			logger.error("-> testPrinter:TicketPrinter.testDefaultPrinter", ex);
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_TICKET_NOT_PRINTED");
		}
	}

	public void exit() throws BusinessException {
		if (applicationSession.getPedidoVenta().getPedidoVentaDetalleCollection().size() > 0) {
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_PEDIDO_NOT_SAVED");
		}
	}

	@Autowired
	public void setApplicationSession(ApplicationSession as) {
		applicationSession = as;
		readPreferences();

	}

	public boolean getDescuentoMayoreoActivado() {
		return isDescuentoMayoreoEnabledForCurrentSucursal();
	}

	public ApplicationSession getSession() {
		return applicationSession;
	}
	ProductoFastDisplayModel[] arrProds;
	//LinkedHashMap<Integer,Producto> productosFastTable;

	/**
	 * @return the producto4Display
	 */
	public ProductoFastDisplayModel[] getProducto4Display() {
		logger.debug("->getProducto4Display()");
		if (arrProds == null) {
			try {
				//productosFastTable = new LinkedHashMap<Integer,Producto> ();
				List<Producto> producto4Display = null;
				producto4Display = basicInfoDAO.getProductos4DisplayList();
				arrProds = new ProductoFastDisplayModel[producto4Display.size()];
				int ap = 0;
				for (Producto p : producto4Display) {
					arrProds[ap++] = new ProductoFastDisplayModel(
							p.getId(),
							p.getId() != null ? (p.getNombre() + "(" + p.getPresentacion() + "):" + p.getContenido() + "" + p.getUnidadMedida()) : "",
							p);
					//productosFastTable.put(p.getId(), p);
				}
			} catch (Exception ex) {
				logger.error("", ex);
			}
		}

		return arrProds;
	}

	public Producto findProductoByCodigoDeBarras(String codigoDeBarras) throws Exception {
		logger.debug("->findProductoByCodigoDeBarras(): search this fucking codigoDeBarras=" + codigoDeBarras);
		Producto productoFound = basicInfoDAO.findProductoByCodigoDeBarras(codigoDeBarras);
		logger.debug("->findProductoByCodigoDeBarras(): productoFound=" + productoFound+", PrecioBase="+productoFound.getPrecioBase());
		if(productoFound.getPrecioBase() < 0.0 ) {
			productoFound = null;
			logger.debug("->findProductoByCodigoDeBarras(): elegible for disabled !");
		}
		return productoFound;
	}

	public ProductoFastDisplayModel[] getProducto4Display(String searchString) {
		ArrayList<ProductoFastDisplayModel> found = new ArrayList<ProductoFastDisplayModel>();
		ProductoFastDisplayModel[] foundArray = null;
		ProductoFastDisplayModel[] search = getProducto4Display();
		if (search != null) {
			if (searchString.trim().length() > 0) {
				for (ProductoFastDisplayModel pfd : search) {
					if (pfd.getForDisplay().toLowerCase().indexOf(searchString.toLowerCase()) >= 0) {
						found.add(pfd);
					}
				}
			}
		}

		foundArray = new ProductoFastDisplayModel[found.size()];
		found.toArray(foundArray);
		return foundArray;
	}

	/**
	 * @return the factorIVA
	 */
	public double getFactorIVA() {
		return factorIVA;
	}

	/**
	 * @param factorIVA the factorIVA to set
	 */
	public void setFactorIVA(double factorIVA) {
		this.factorIVA = factorIVA;
	}
//    public Producto getProducto(Integer id){
//        return productosFastTable.get(id);
//    }

	void setComentariosToCurrentPedidoVenta(String comentarios) {
		this.getSession().getPedidoVenta().setComentarios(comentarios);
	}

	/**
	 * @return the applicationSession
	 */
	@Autowired
	public ApplicationSession getApplicationSession() {
		return applicationSession;
	}

	/**
	 * @param sendDataSynchronizer the sendDataSynchronizer to set
	 */
	@Autowired
	public void setSendDataSynchronizer(SendDataSynchronizer sendDataSynchronizer) {
		this.sendDataSynchronizer = sendDataSynchronizer;
	}
	private BufferedImage defaultImageForProducto;

	public BufferedImage getDefaultImageForProducto() {
		if (this.defaultImageForProducto == null) {
			try {
				this.defaultImageForProducto = ImageIO.read(ApplicationLogic.class.getResource("/imgs/proximamente_2.jpg"));
			} catch (IOException ex) {
				logger.error("DefaultImageForProducto not found:", ex);
			}
		}
		return this.defaultImageForProducto;
	}

	public static String getMD5Encrypted(String e) {

		MessageDigest mdEnc = null; // Encryption algorithm
		try {
			mdEnc = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException ex) {
			return null;
		}
		mdEnc.update(e.getBytes(), 0, e.length());
		return (new BigInteger(1, mdEnc.digest())).toString(16);
	}

	public Object[][] concentradoVentasUsuario(Date fechaCorteInicial, Date fechaCorteFinal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfParam = new SimpleDateFormat("yyyy-MM-dd");
		logger.debug("---------->concentradoVentasUsuario: fechaCorteInicial=" + sdfParam.format(fechaCorteInicial) + ", =fechaCorteFinal=" + sdfParam.format(fechaCorteFinal));
		EntityManager em = null;
		Object[][] result = null;

		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			final String query = "SELECT   U.NOMBRE_COMPLETO,PV.USUARIO_ID,A.TIPO_ALMACEN,CAST(PVE.FECHA AS DATE) AS FECHA_DIARIA, PV.DESCUENTO_APLICADO,(SUM(CANTIDAD*PRECIO_VENTA)) AS IMPORTE_PEDIDO\n"
					+ "FROM     PEDIDO_VENTA_DETALLE PVD, PEDIDO_VENTA PV, PEDIDO_VENTA_ESTADO PVE,USUARIO U,ALMACEN A\n"
					+ "WHERE    1=1\n"
					+ "AND      PVD.PEDIDO_VENTA_ID = PV.ID\n"
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID\n"
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID\n"
					+ "AND      PV.ALMACEN_ID       = A.ID\n"
					+ "AND      U.USUARIO_ID        = PV.USUARIO_ID\n"
					+ "AND      PVE.ESTADO_ID       = 1\n"
					+ "AND      CAST(PVE.FECHA AS DATE)  >= :fechaCorteInicial \n"
					+ "AND      CAST(PVE.FECHA AS DATE)  <= :fechaCorteFinal \n"
					+ "GROUP BY U.NOMBRE_COMPLETO,PV.USUARIO_ID,A.TIPO_ALMACEN,PVE.FECHA,PV.DESCUENTO_APLICADO\n"
					+ "ORDER BY U.NOMBRE_COMPLETO,PV.USUARIO_ID,A.TIPO_ALMACEN,PVE.FECHA,PV.DESCUENTO_APLICADO";

			logger.debug("============>concentradoVentasUsuario: query=" + query);

			Query q = em.createNativeQuery(query);

			q.setParameter("fechaCorteInicial", sdfParam.format(fechaCorteInicial));
			q.setParameter("fechaCorteFinal", sdfParam.format(fechaCorteFinal));

			ArrayList resultList = (ArrayList) q.getResultList();
			int index = 0;
			LinkedHashMap<String, Object[]> concentradoUsuario = new LinkedHashMap<String, Object[]>();
			logger.debug("============>concentradoVentasUsuario: resultList.size=" + resultList.size());
			final Iterator iterator = resultList.iterator();

			while (iterator.hasNext()) {
				final Object next = iterator.next();
				Object[] row = (Object[]) next;

				String usr = (String) row[0];
				String date = sdf.format((Date) row[3]);
				Double descuento = (Double) row[4];
				if (descuento == null) {
					descuento = 0.0;
				}
				Double importe = ((Double) row[5]) - descuento;
				row[4] = importe;

				Object[] rowEncontrado = (Object[]) concentradoUsuario.get(usr + date);
				Double importeEncontrado = null;

				if (rowEncontrado != null) {
					importeEncontrado = (Double) rowEncontrado[4];

					rowEncontrado[4] = importeEncontrado + importe;

					concentradoUsuario.put(usr + date, rowEncontrado);
				} else {
					concentradoUsuario.put(usr + date, row);
				}
				index++;
			}

			final Set<String> keySetUsrs = concentradoUsuario.keySet();
			result = new Object[keySetUsrs.size()][];
			int i = 0;

			for (String usrDateKey : keySetUsrs) {
				final Object[] row = concentradoUsuario.get(usrDateKey);

				if (((Integer) row[2]).intValue() == Constants.ALMACEN_PRINCIPAL) {
					row[2] = "NORMAL";
				} else {
					row[2] = "OPORTUNIDAD";
				}
				row[3] = sdf.format((Date) row[3]);
				//row[4] =  //df.format((Double)row[4]);

				result[i] = row;
				i++;
			}
		} catch (Exception e) {
			logger.error("-->>>concentradoVentasUsuario:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}
		return result;
	}

	public Object[][] concentradoVentasProducto(Date fechaCorteInicial, Date fechaCorteFinal) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdfParam = new SimpleDateFormat("yyyy-MM-dd");
		logger.debug("---------->concentradoVentasProducto: fechaCorteInicial=" + sdf.format(fechaCorteInicial) + ", =fechaCorteFinal=" + sdf.format(fechaCorteFinal));
		EntityManager em = null;
		Object[][] result = null;

		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Query q = em.createNativeQuery(
					"SELECT     SUM(PVD.CANTIDAD),P.CODIGO_BARRAS,P.NOMBRE,P.PRESENTACION,A.TIPO_ALMACEN, (SUM(PVD.CANTIDAD*PVD.PRECIO_VENTA)) AS IMPORTE_PEDIDO "
					+ "FROM     PEDIDO_VENTA_DETALLE PVD, PEDIDO_VENTA PV, PEDIDO_VENTA_ESTADO PVE,PRODUCTO P,ALMACEN A "
					+ "WHERE    1=1 "
					+ "AND      PVD.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVD.PRODUCTO_ID     = P.ID "
					+ "AND      PV.ALMACEN_ID       = A.ID "
					+ "AND      PVE.ESTADO_ID = 1 "
					+ "AND      CAST(PVE.FECHA AS DATE)  >= :fechaCorteInicial "
					+ "AND      CAST(PVE.FECHA AS DATE)  <= :fechaCorteFinal "
					+ "GROUP BY P.CODIGO_BARRAS,P.NOMBRE,P.PRESENTACION,A.TIPO_ALMACEN,PVD.CANTIDAD "
					+ "ORDER BY P.NOMBRE,P.PRESENTACION,A.TIPO_ALMACEN ");

			q.setParameter("fechaCorteInicial", sdfParam.format(fechaCorteInicial));
			q.setParameter("fechaCorteFinal", sdfParam.format(fechaCorteFinal));

			ArrayList resultList = (ArrayList) q.getResultList();

			LinkedHashMap<String, Object[]> concentradoUsuario = new LinkedHashMap<String, Object[]>();
			//logger.debug("============>concentradoVentasProducto: resultList.size="+resultList.size());


			result = new Object[resultList.size()][];
			int i = 0;
			//DecimalFormat df = new DecimalFormat("$###,###,##0.00");

			final Iterator iterator = resultList.iterator();
			while (iterator.hasNext()) {
				final Object next = iterator.next();
				Object[] row = (Object[]) next;

				//logger.debug("============>concentradoVentasProducto["+i+"]: prod="+row[1]);
				if (((Integer) row[4]).intValue() == Constants.ALMACEN_PRINCIPAL) {
					row[4] = "NORMAL";
				} else {
					row[4] = "OPORTUNIDAD";
				}
				//row[5] =  df.format((Double)row[5]);

				result[i] = row;
				i++;
			}
		} catch (Exception e) {
			logger.error("-->>>concentradoVentasProducto:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}
		return result;
	}

	public Object[][] concentradoVentasTicket(Date fechaCorteInicial, Date fechaCorteFinal) {
		return concentradoVentasTicket(fechaCorteInicial, fechaCorteFinal, null);
	}

	public Object[][] concentradoVentasTicket(String buscarNoTicket) {
		return concentradoVentasTicket(null, null, buscarNoTicket);
	}

	private Object[][] concentradoVentasTicket(Date fechaCorteInicial, Date fechaCorteFinal, String buscarNoTicket) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		SimpleDateFormat sdfParam = new SimpleDateFormat("yyyy-MM-dd");
		//logger.debug("---------->concentradoVentasTicket: fechaCorteInicial=" + sdf.format(fechaCorteInicial) + ", =fechaCorteFinal=" + sdf.format(fechaCorteFinal));
		EntityManager em = null;
		Object[][] result = null;

		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			//                                             0             1              2              3             4            5        6                7                               8                                 
			final String queryPorFechas = "SELECT   A.TIPO_ALMACEN,U.NOMBRE_COMPLETO,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,PVE.FECHA,PV.DESCUENTO_APLICADO,(SUM(PVD.CANTIDAD*PVD.PRECIO_VENTA)) AS IMPORTE_PEDIDO "
					+ "FROM     PEDIDO_VENTA_DETALLE PVD, PEDIDO_VENTA PV, PEDIDO_VENTA_ESTADO PVE,USUARIO U, CLIENTE C,ALMACEN A, FORMA_DE_PAGO FP "
					+ "WHERE    1=1 "
					+ "AND      PVD.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PV.CLIENTE_ID       = C.ID "
					+ "AND      PV.USUARIO_ID       = U.USUARIO_ID "
					+ "AND      PV.ALMACEN_ID       = A.ID "
					+ "AND      PV.FORMA_DE_PAGO_ID = FP.ID "
					+ "AND      PVE.ESTADO_ID       = 1"
					+ "AND      CAST(PVE.FECHA AS DATE)  >= :fechaCorteInicial "
					+ "AND      CAST(PVE.FECHA AS DATE)  <= :fechaCorteFinal "
					+ "GROUP BY A.TIPO_ALMACEN,PVE.FECHA,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,U.NOMBRE_COMPLETO,PV.DESCUENTO_APLICADO "
					+ "ORDER BY A.TIPO_ALMACEN,PVE.FECHA,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,U.NOMBRE_COMPLETO,PV.DESCUENTO_APLICADO ";

			final String queryPorTicket = "SELECT   A.TIPO_ALMACEN,U.NOMBRE_COMPLETO,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,PVE.FECHA,PV.DESCUENTO_APLICADO,(SUM(PVD.CANTIDAD*PVD.PRECIO_VENTA)) AS IMPORTE_PEDIDO "
					+ "FROM     PEDIDO_VENTA_DETALLE PVD, PEDIDO_VENTA PV, PEDIDO_VENTA_ESTADO PVE,USUARIO U, CLIENTE C,ALMACEN A, FORMA_DE_PAGO FP "
					+ "WHERE    1=1 "
					+ "AND      PVD.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PVE.PEDIDO_VENTA_ID = PV.ID "
					+ "AND      PV.CLIENTE_ID       = C.ID "
					+ "AND      PV.USUARIO_ID       = U.USUARIO_ID "
					+ "AND      PV.ALMACEN_ID       = A.ID "
					+ "AND      PV.FORMA_DE_PAGO_ID = FP.ID "
					+ "AND      PVE.ESTADO_ID       = 1 "
					+ "AND      PV.COMENTARIOS LIKE :buscarNoTicket "
					+ "GROUP BY A.TIPO_ALMACEN,PVE.FECHA,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,U.NOMBRE_COMPLETO,PV.DESCUENTO_APLICADO "
					+ "ORDER BY A.TIPO_ALMACEN,PVE.FECHA,C.RAZON_SOCIAL,FP.DESCRIPCION,PV.COMENTARIOS,PV.ID,U.NOMBRE_COMPLETO,PV.DESCUENTO_APLICADO ";
			Query q = null;
			if (fechaCorteFinal != null && fechaCorteInicial != null && buscarNoTicket == null) {
				logger.debug("============>concentradoVentasTicket: [1] fechaCorteInicial=" + fechaCorteInicial + ", fechaCorteFinal=" + fechaCorteFinal);
				q = em.createNativeQuery(queryPorFechas);
				q.setParameter("fechaCorteInicial", sdfParam.format(fechaCorteInicial));
				q.setParameter("fechaCorteFinal", sdfParam.format(fechaCorteFinal));
			} else if (fechaCorteFinal == null && fechaCorteInicial == null && buscarNoTicket != null) {
				logger.debug("============>concentradoVentasTicket: [2] buscarNoTicket=" + buscarNoTicket);
				q = em.createNativeQuery(queryPorTicket);
				q.setParameter("buscarNoTicket", "%" + buscarNoTicket + "%");
			}
			final ArrayList resultList = (ArrayList) q.getResultList();
			
			LinkedHashMap<String, Object[]> concentradoUsuario = new LinkedHashMap<String, Object[]>();
			result = new Object[resultList.size()][];
			int i = 0;
			//DecimalFormat df = new DecimalFormat("$###,###,##0.00");

			final Iterator iterator = resultList.iterator();
			while (iterator.hasNext()) {
				final Object next = iterator.next();
				Object[] row = (Object[]) next;

				if (((Integer) row[0]).intValue() == Constants.ALMACEN_PRINCIPAL) {
					row[0] = "NORM";
				} else if (((Integer) row[0]).intValue() == Constants.ALMACEN_OPORTUNIDAD) {				
					row[0] = "OPOR";
				}
				row[6] = sdf.format((Timestamp) row[6]);
				result[i] = row;
				i++;
			}
		} catch (Exception e) {
			logger.error("-->>>concentradoVentasTicket:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}
		return result;
	}

	public PedidoVenta findPedidoVenta(Integer id) {
		EntityManager em = emf.createEntityManager();
		try {
			PedidoVenta pedidoVenta = em.find(PedidoVenta.class, id);

			pedidoVenta.getUsuario();
			pedidoVenta.getCliente();
			pedidoVenta.getCliente().getPoblacion();
			pedidoVenta.getAlmacen();
			pedidoVenta.getFormaDePago();

			Collection<PedidoVentaDetalle> detalleVentaPedidoCollection = pedidoVenta.getPedidoVentaDetalleCollection();
			for (PedidoVentaDetalle detalleVentaPedido : detalleVentaPedidoCollection) {
			}

			Collection<PedidoVentaEstado> pedidoVentaEstadoCollection = pedidoVenta.getPedidoVentaEstadoCollection();
			for (PedidoVentaEstado pedidoVentaEstado : pedidoVentaEstadoCollection) {
			}

			return pedidoVenta;
		} finally {
			em.close();
		}
	}

	public List<Date> getRangoFechasDeVentas() {
		List<Date> result = new ArrayList<Date>();
		EntityManager em = null;

		try {
			em = emf.createEntityManager();
			logger.debug("->EntityManager em, created");
			Query q = em.createNativeQuery(
					"SELECT DISTINCT(CAST(PVE.FECHA AS DATE)) AS FECHA_DIARIA FROM     PEDIDO_VENTA_ESTADO PVE");

			ArrayList resultList = (ArrayList) q.getResultList();

			final Iterator iterator = resultList.iterator();
			while (iterator.hasNext()) {
				final Object next = iterator.next();

				result.add((Date) next);
			}

			if (result.size() == 0) {
				result.add(new Date());
			}
		} catch (Exception e) {
			logger.error("-->>>getRangoFechasDeVentas:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}

		return result;
	}

	private void readPreferences() {
		logger.debug("->readPreferences()");
		InputStream is = null;
		try {
			is = new FileInputStream(CONFIG_PREFERENCESPROPERTIES);
			logger.debug("->readPreferences(): read from File");
		} catch (FileNotFoundException ex) {
			logger.debug("->readPreferences(): now, try to read from classpath");
			is = ApplicationLogic.class.getResourceAsStream(CONFIG_PREFERENCESPROPERTIES_CLASSPATH);
		}

		Properties preferencesNew = new Properties();

		if (is != null) {
			try {
				preferencesNew.load(is);
				is.close();
			} catch (IOException ex) {
				logger.debug("->readPreferences(): there isn't Preferences :O !");
			}
		}
		logger.debug("->readPreferences(): after Read:" + preferencesNew);
		setPreferences(preferencesNew);
	}

	public void updatePreferences() {
		FileOutputStream fos = null;
		try {
			File dirPrefs = new File(CONFIG_PREFERENCES_DIR);
			if (!dirPrefs.exists()) {
				dirPrefs.mkdirs();
			}
			fos = new FileOutputStream(CONFIG_PREFERENCESPROPERTIES);
			getPreferences().store(fos, "Updated :" + new Date());
		} catch (IOException ex) {
			logger.error("readPreferences:", ex);
			throw new IllegalStateException("No s epuede guardad:" + ex.getMessage());
		}
	}

	/**
	 * @return the preferences
	 */
	public Properties getPreferences() {
		if (preferences == null) {
			readPreferences();
		}
		return preferences;
	}

	/**
	 * @param preferences the preferences to set
	 */
	private void setPreferences(Properties preferences) {
		this.preferences = preferences;
	}

	public HashMap<String, String> getExtraInfoInComments(String comentarios) {
		HashMap<String, String> extraInfo = new HashMap<String, String>();

		extraInfo.put("ticket", "");
		extraInfo.put("aprobacion", "");
		extraInfo.put("recibido", "");
		extraInfo.put("caja", "1");

		if (comentarios.contains("<ticket>") && comentarios.contains("</ticket>")) {
			extraInfo.put("ticket", comentarios.substring(comentarios.indexOf("<ticket>") + 8, comentarios.indexOf("</ticket>")));
		}
		if (comentarios.contains("<noAprobacion>") && comentarios.contains("</noAprobacion>")) {
			extraInfo.put("aprobacion", comentarios.substring(comentarios.indexOf("<noAprobacion>") + 14, comentarios.indexOf("</noAprobacion>")));
		}
		if (comentarios.contains("<recibido>") && comentarios.contains("</recibido>")) {
			extraInfo.put("recibido", comentarios.substring(comentarios.indexOf("<recibido>") + 10, comentarios.indexOf("</recibido>")));
		}
		if (comentarios.contains("<caja>") && comentarios.contains("</caja>")) {
			extraInfo.put("caja", comentarios.substring(comentarios.indexOf("<caja>") + 6, comentarios.indexOf("</caja>")));
		}
		return extraInfo;
	}

	public void actualizaPrecio(Producto productoEscaneado, Almacen almacen, Double precioNuevo,int cantidadParaRecargar) {
		EntityManager em = null;
		int updated = 0;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			logger.debug("->EntityManager em, created");
			Query q = em.createNativeQuery(
					"UPDATE ALMACEN_PRODUCTO SET PRECIO_VENTA=:precioVenta, CANTIDAD_ACTUAL = CANTIDAD_ACTUAL + :cantidadParaRecargar WHERE PRODUCTO_ID=:productoId AND ALMACEN_ID=:almacenId");

			q.setParameter("precioVenta", precioNuevo);
			q.setParameter("cantidadParaRecargar", cantidadParaRecargar);
			
			q.setParameter("productoId", productoEscaneado.getId());
			q.setParameter("almacenId", almacen.getId());

			updated = q.executeUpdate();

			if (updated < 1) {
				throw new IllegalArgumentException("No se pudo actualizar, pues no se encotro producto");
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("-->>>getRangoFechasDeVentas:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}

		}

	}

	public void persistLinea(Linea linea) throws BusinessException {
		logger.debug("->persistLinea: ");

		EntityManager em = null;
		boolean existWithSameName = false;
		try {
			em = emf.createEntityManager();
			Query q = em.createQuery("select x from Linea x where upper(x.nombre) = upper(:nombre)");
			q.setParameter("nombre", linea.getNombre());
			List resultList = q.getResultList();
			if (resultList.size() > 0) {
				existWithSameName = true;
				throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_LINEA_EXIST_ERROR");
			}
		} catch (BusinessException be) {
			throw be;
		} catch (Exception e) {
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_LINEA_SEARCH_ERROR");
		} finally {
			if (em != null && existWithSameName) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}

		try {
			em.getTransaction().begin();
			logger.debug("->begin transaction");
			Linea lineaNueva = new Linea();
			lineaNueva.setNombre(linea.getNombre().toUpperCase());
			em.persist(lineaNueva);
			logger.debug("->ok, Linea inserted");
			em.getTransaction().commit();
			em.refresh(lineaNueva);
			linea.setId(lineaNueva.getId());
			logger.debug("->committed.");
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_LINEA_NOT_PERSISTED");
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void persistIndustria(Industria industria) throws BusinessException {
		logger.debug("->persistIndustria: ");

		EntityManager em = null;
		boolean existWithSameName = false;
		try {
			em = emf.createEntityManager();
			Query q = em.createQuery("select x from Industria x where upper(x.nombre) = upper(:nombre)");
			q.setParameter("nombre", industria.getNombre());
			List resultList = q.getResultList();
			if (resultList.size() > 0) {
				existWithSameName = true;
				throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_INDUSTRIA_EXIST_ERROR");
			}
		} catch (BusinessException be) {
			throw be;
		} catch (Exception e) {
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_INDUSTRIA_SEARCH_ERROR");
		} finally {
			if (em != null && existWithSameName) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}

		try {
			em.getTransaction().begin();
			logger.debug("->begin transaction");
			Industria industriaNueva = new Industria();
			industriaNueva.setNombre(industria.getNombre().toUpperCase());
			em.persist(industriaNueva);
			logger.debug("->ok, Industria inserted");
			em.getTransaction().commit();
			em.refresh(industriaNueva);
			industria.setId(industriaNueva.getId());
			logger.debug("->committed.");
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw new BusinessException(getClass().getSimpleName(), "APP_LOGIC_INDUSTRIA_NOT_PERSISTED");
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void persistMarca(Marca marca) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public PedidoVenta findPedidoVentaByTicket(String pedidoVentaTicket) {
		EntityManager em = emf.createEntityManager();
		try {
			PedidoVenta pedidoVenta = null;

			Query q = em.createQuery("select x from PedidoVenta x where lower(x.comentarios) like :pedidoVentaTicket");
			//+ " order by x.usuario.nombreCompleto, x.cliente.rfc,x.cliente.poblacion.nombre,x.almacen.id,x.formaDePago.descripcion");
			q.setParameter("pedidoVentaTicket", "%<ticket>" + pedidoVentaTicket + "</ticket>%");

			pedidoVenta = (PedidoVenta) q.getSingleResult();

			pedidoVenta.getUsuario();
			pedidoVenta.getCliente();
			pedidoVenta.getCliente().getPoblacion();
			pedidoVenta.getAlmacen();
			pedidoVenta.getFormaDePago();

			Collection<PedidoVentaDetalle> detalleVentaPedidoCollection = pedidoVenta.getPedidoVentaDetalleCollection();
			for (PedidoVentaDetalle detalleVentaPedido : detalleVentaPedidoCollection) {
			}

			Collection<PedidoVentaEstado> pedidoVentaEstadoCollection = pedidoVenta.getPedidoVentaEstadoCollection();
			for (PedidoVentaEstado pedidoVentaEstado : pedidoVentaEstadoCollection) {
			}

			return pedidoVenta;
		} finally {
			em.close();
		}
	}

	public boolean isDescuentoMayoreoEnabledForCurrentSucursal() {
		logger.debug("->isDescuentoMayoreoEnabledForCurrentSucursal: ");
		boolean result = false;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			Sucursal freshSucursal = em.find(applicationSession.getSucursal().getClass(), applicationSession.getSucursal().getId());
			String coments = freshSucursal.getComentarios();
			logger.debug("->isDescuentoMayoreoEnabledForCurrentSucursal: freshSucursal=" + freshSucursal + ", Comentarios=" + freshSucursal.getComentarios());
			if (coments != null) {
				result = coments.contains("<descuentoMayoreo>true</descuentoMayoreo>");
			}
		} catch (Exception e) {
			logger.error("Exception caught:", e);
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
		return result;
	}

	public void enableDescuentoMayoreoForCurrentSucursal() {
		logger.debug("->enableDescuentoMayoreoForCurrentSucursal: ");
		boolean result = false;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Sucursal freshSucursal = em.find(applicationSession.getSucursal().getClass(), applicationSession.getSucursal().getId());

			freshSucursal.setComentarios("<descuentoMayoreo>true</descuentoMayoreo>");
			applicationSession.getSucursal().setComentarios(freshSucursal.getComentarios());

			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void disableDescuentoMayoreoForCurrentSucursal() {
		logger.debug("->disableDescuentoMayoreoForCurrentSucursal: ");
		boolean result = false;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Sucursal freshSucursal = em.find(applicationSession.getSucursal().getClass(), applicationSession.getSucursal().getId());

			freshSucursal.setComentarios("<descuentoMayoreo>false</descuentoMayoreo>");
			applicationSession.getSucursal().setComentarios(freshSucursal.getComentarios());

			em.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public DesgloseImportePedidoVenta calculaDesgloseImporte(PedidoVenta pedidoVenta) {
		DesgloseImportePedidoVenta desgloseImportePedidoVenta = null;
		Double descuentoAplicado = pedidoVenta.getDescuentoAplicado()!=null?pedidoVenta.getDescuentoAplicado():0.0;
		Double factorIva         = pedidoVenta.getFactoriva()<=0.0? factorIVA : pedidoVenta.getFactoriva();
		DecimalFormat df = new DecimalFormat("########0.00");
		
		logger.debug("->calculaDesgloseImporte: descuentoAplicado = "+df.format(descuentoAplicado));
		logger.debug("->calculaDesgloseImporte: factorIva         = "+df.format(factorIva));
		double importe = 0.0;
		double sumaImporte = 0.0;
		double subTotal = 0.0;
		double importeIVA = 0.0;
		double total = 0.0;
		logger.debug("->calculaDesgloseImporte: detalle pedido venta___________________________");	
		for (PedidoVentaDetalle pvd : pedidoVenta.getPedidoVentaDetalleCollection()) {			
			importe = pvd.getCantidad() * pvd.getPrecioVenta();
			sumaImporte += importe; 
			logger.debug("->calculaDesgloseImporte:\t"+pvd.getCantidad()+" * ["+pvd.getProducto().getId()+"] "+
					df.format(pvd.getPrecioVenta())+" = "+df.format(importe));
		}
		
		total      = sumaImporte - descuentoAplicado;
		subTotal   = total/ (1.0 + factorIva); 
		importeIVA = total - subTotal;
		logger.debug("->calculaDesgloseImporte: -----------------------------------------------");	
		logger.debug("->calculaDesgloseImporte: Suma Importe       = "+df.format(sumaImporte));
		logger.debug("->calculaDesgloseImporte: Descuento Aplicado = "+df.format(descuentoAplicado));
		logger.debug("->calculaDesgloseImporte: Importe IVA        = "+df.format(importeIVA));
		logger.debug("->calculaDesgloseImporte: Subtotal           = "+df.format(subTotal));
		logger.debug("->calculaDesgloseImporte: ===============================================");		
		logger.debug("->calculaDesgloseImporte: Total              = "+df.format(total));		
		
		desgloseImportePedidoVenta =  new DesgloseImportePedidoVenta(sumaImporte, subTotal, importeIVA, total, descuentoAplicado);
		
		return desgloseImportePedidoVenta;
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.client.view.MasterDBManagerFrame;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.derby.drda.NetworkServerControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class DBManager {

	private static Logger logger = LoggerFactory.getLogger(DBManager.class);
	private static MasterDBManagerFrame masterDBManagerFrame;
	private static boolean estadoIniciado = false;
	private static String masterHostLocaly = null;
	private static Properties prop = new Properties();

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		Date currDate = new Date();

		prop.setProperty("user", "PMARLEN_PROD");
		prop.setProperty("password", "PMARLEN_PROD_PASSWORD");
//		try {		
//			ensureSingletonDBMangerInSystem();
//		} catch( IllegalStateException iee){
//			JOptionPane.showMessageDialog(null, "Ya ha iniciado esta aplicaicon", "Master DB Manager", JOptionPane.ERROR_MESSAGE);
//            System.exit(1);
//		}

		logger.debug("==> DBStarter:" + sdf.format(currDate));

		try {
			int countArgs = 0;
			for (String arg : args) {
				logger.debug("=>> 1.1: args[" + (countArgs++) + "]=" + arg);
				if (arg.startsWith("-masterHostLocaly=")) {
					masterHostLocaly = arg.split("=")[1];
				}
			}

			if (masterHostLocaly == null) {
				throw new IllegalStateException("falta argumento apra especificar: masterHost");
			}

		} catch (Exception ex) {
			logger.error("", ex);
		}

		DBManager dbm = new DBManager();

		dbm.estadoInicial();

	}

	private static void ensureSingletonDBMangerInSystem() {
		try {
			ServerSocket serverSocket = new ServerSocket(ApplicationInfo.MASTER_DB_MANAGER_PORT);
			logger.debug("the socket to ensure unique instance, it's started");
		} catch (IOException ex) {
			logger.error("Error:", "Can't start the socket to ensure unique instance");
			throw new IllegalStateException();
		}
	}

	public DBManager() {
		masterDBManagerFrame = new MasterDBManagerFrame();

		masterDBManagerFrame.getAccionDB().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cambiarEstado();
			}
		});

		if (canConnectAsClient()) {
			logger.debug("==> 1: iniciada");
			estadoIniciado = true;
			masterDBManagerFrame.getRestadoDB().setText("BASE DE DATOS INICIADA");
			masterDBManagerFrame.getRestadoDB().setForeground(Color.GREEN);
			masterDBManagerFrame.getAccionDB().setText("DETENER");
		} else {
			logger.debug("==> 2: detenida");
			estadoIniciado = false;
			masterDBManagerFrame.getRestadoDB().setText("BASE DE DATOS DETENIDA");
			masterDBManagerFrame.getRestadoDB().setForeground(Color.RED);
			masterDBManagerFrame.getAccionDB().setText("INICIAR");
		}

		masterDBManagerFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				connectForShutdown();
			}
		});

	}

	public void cambiarEstado() {
		logger.debug("==> 6: cambiarEstado: estadoIniciado ? " + estadoIniciado);
		if (estadoIniciado) {
			logger.debug("==> 7: shutdow");
			
			connectForShutdown();
			estadoIniciado = false;
			masterDBManagerFrame.getRestadoDB().setText("BASE DE DATOS DETENIDA");
			masterDBManagerFrame.getRestadoDB().setForeground(Color.RED);
			masterDBManagerFrame.getAccionDB().setText("INICIAR");
			logger.debug("==> 8: OK shutdow");

		} else {

			estadoIniciado = true;
			masterDBManagerFrame.getRestadoDB().setText("BASE DE DATOS INICIADA");
			masterDBManagerFrame.getRestadoDB().setForeground(Color.GREEN);
			masterDBManagerFrame.getAccionDB().setText("DETENER");

			logger.debug("==> 10: Thread started ");
			new Thread() {
				public void run() {
					logger.debug("==> 15: startDBMaster()");
					startDBMaster();
				}
			}.start();
		}
	}

	private static void startDBMaster() {
		logger.debug("==>18 ... NetworkServerControl.main -h " + masterHostLocaly);
		NetworkServerControl.main(new String[]{"start", "-h", masterHostLocaly});
	}

	public boolean canConnectAsClient() {
		Connection conn = null;
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
			conn = DriverManager.getConnection("jdbc:derby://" + masterHostLocaly + ":1527/PMarlen_DB", prop);
			if (conn != null) {
				logger.debug("==> 11: canConnectAsClient(): connected !!");
				return true;
			}
		} catch (Exception ex) {
		} finally {
			if (conn != null) {
				try {
					conn.close();
					logger.debug("==> 12: canConnectAsClient(): Ok, closed client !!");
				} catch (SQLException ex) {
					logger.error("==> 13: canConnectAsClient(): ", ex);
				}
			}
		}
		return false;
	}

	public void connectForShutdown() {
		System.exit(1);
	}

	private void estadoInicial() {

		masterDBManagerFrame.setVisible(true);
	}
}

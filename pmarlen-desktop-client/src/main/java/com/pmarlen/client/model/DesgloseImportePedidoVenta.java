/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.model;

/**
 *
 * @author alfredo
 */
public class DesgloseImportePedidoVenta {
	private double sumaImporte;
	private double subTotal;
	private double importeIVA;
	private double total;
	private double descuentoAplicado;

	public DesgloseImportePedidoVenta(double sumaImporte, double subTotal, double importeIVA, double total,double descuentoAplicado) {
		this.sumaImporte = sumaImporte;
		this.subTotal = subTotal;
		this.importeIVA = importeIVA;
		this.total = total;
		this.descuentoAplicado = descuentoAplicado;
	}

	/**
	 * @return the sumaImporte
	 */
	public double getSumaImporte() {
		return sumaImporte;
	}

	/**
	 * @return the subTotal
	 */
	public double getSubTotal() {
		return subTotal;
	}

	/**
	 * @return the importeIVA
	 */
	public double getImporteIVA() {
		return importeIVA;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @return the descuentoAplicado
	 */
	public double getDescuentoAplicado() {
		return descuentoAplicado;
	}
	
	
	
}

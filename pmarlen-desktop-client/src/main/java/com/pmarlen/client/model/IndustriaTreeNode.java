/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Industria;

/**
 *
 * @author alfred
 */
public class IndustriaTreeNode extends EntityDisplayableItem{
    private Industria industria;

    public IndustriaTreeNode(Industria industria){
        super(industria.getId(),industria.getNombre());
        this.industria = industria;
    }

    /**
     * @return the marca
     */
    public Industria getIndustria() {
        return industria;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.controller.PrincipalControl;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author alfred
 */


public class PedidoVentaDetalleTableModel implements TableModel{
    private ArrayList<PedidoVentaDetalle> detallePedidoList;
    String[] colNames;
    private ApplicationLogic applicationLogic;
	private Logger logger;

    private PrincipalControl principalControl;
    
    public PedidoVentaDetalleTableModel(Collection<PedidoVentaDetalle> detallePedidoList) {
		logger = LoggerFactory.getLogger(PedidoVentaDetalleTableModel.class);

		this.detallePedidoList = (ArrayList<PedidoVentaDetalle>)detallePedidoList;
        
        colNames = new String[]{
                "Cantidad","CODIGO","Nombre / Presentacion", "Precio","Importe"};
    }
    public int getRowCount() {
        return this.detallePedidoList.size();
    }

    public int getColumnCount() {
        return colNames.length;
    }

    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex) {
        if(columnIndex==0){
            return Integer.class;
        } else {
			return String.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if(columnIndex==0){
            return true;
        } else {
			return false;
		}
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        
        if(columnIndex==0){
            return detallePedidoList.get(rowIndex).getCantidad();
        } else if(columnIndex==1){
            return detallePedidoList.get(rowIndex).getProducto().getCodigoBarras();
        } else if(columnIndex==2){
            return  detallePedidoList.get(rowIndex).getProducto().getNombre()+
					" / "+
                    detallePedidoList.get(rowIndex).getProducto().getPresentacion();
        } else if(columnIndex==3){
			double precioVenta = detallePedidoList.get(rowIndex).getPrecioVenta();
			return ApplicationInfo.formatToCurrency( precioVenta );
        } else if(columnIndex==4){
			return ApplicationInfo.formatToCurrency( 
					detallePedidoList.get(rowIndex).getPrecioVenta() * 
					detallePedidoList.get(rowIndex).getCantidad() );
        } else {
            throw new IllegalArgumentException("getValueAt("+rowIndex+", "+columnIndex+")");
        }

    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        if(columnIndex==0){
            try {
                int intVal = Integer.parseInt(aValue.toString().trim());
                if( intVal > 0) {
					
					if(applicationLogic.checarExistanciaEnAlmacenActual(detallePedidoList.get(rowIndex).getProducto().getId(), intVal)){					
						detallePedidoList.get(rowIndex).setCantidad(new Integer(aValue.toString()));
					}else {
						//throw new IllegalArgumentException("¡ No se puede modificar a esta cantidad, no hay existencia en ALmaceén!");
						JOptionPane.showMessageDialog(null, "¡ No se puede modificar a esta cantidad, no hay tanta existencia en Almaceén!", "Actualizar Cantidad", JOptionPane.ERROR_MESSAGE);
						return;
					}
                } else if( intVal ==  0) {
                    this.applicationLogic.deleteProductoFromCurrentPedidoVenta(rowIndex);                    
                }
                principalControl.updateDetallePedidoTable();
            }catch(NumberFormatException nfe){
				
            }
        } 

    }

    public void addTableModelListener(TableModelListener l) {        
    }

    public void removeTableModelListener(TableModelListener l) {
        
    }

    /**
     * @param applicationLogic the applicationLogic to set
     */
    public void setApplicationLogic(ApplicationLogic applicationLogic) {
        this.applicationLogic = applicationLogic;
    }

    /**
     * @param PrincipalControl the PrincipalControl to set
     */
    public void setPrincipalControl(PrincipalControl principalControl) {
        this.principalControl = principalControl;
    }
}

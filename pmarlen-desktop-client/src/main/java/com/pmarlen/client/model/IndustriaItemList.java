/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Industria;

/**
 *
 * @author alfred
 */
public class IndustriaItemList extends EntityDisplayableItem{
    private Industria linea;

    public IndustriaItemList(Industria industria){
        super(industria.getId(),industria.getNombre());
        this.linea = industria;
    }

    /**
     * @return the industria
     */
    public Industria getIndustria() {
        return linea;
    }

}

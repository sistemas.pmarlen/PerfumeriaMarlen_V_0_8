/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.pmarlen.model.beans.PedidoVentaEstado;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


/**
 *
 * @author alfred
 */
public class PedidosGuardadosTableModel implements TableModel{
    private List<PedidoVenta> pedidosList;
    String[] colNames;
	static DecimalFormat df;
	
	private static String formatoMoneda(double f){
		if(df == null){
			df = new DecimalFormat ("$ ###,##0.00");
		}
		return df.format(f);	
	}
	
    public PedidosGuardadosTableModel(List<PedidoVenta> pedidosList) {
        this.pedidosList = (List<PedidoVenta>)pedidosList;
        colNames = new String[]{"Tipo", "Usuario", "Nombre o Raz\u00F3n Social", "Forma de pago","Fecha","Importe"};
    }
    public int getRowCount() {
        return this.pedidosList.size();
    }

    public int getColumnCount() {
        return colNames.length;
    }

    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex==0){
			Almacen a = pedidosList.get(rowIndex).getAlmacen();
            if (a.getTipoAlmacen() == Constants.ALMACEN_PRINCIPAL) {
				return "NORMAL";
			} else if (a.getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
				return "OPORTUNIDAD";
			} else{	
				return "?";
			}
        } else if(columnIndex==1){
            return pedidosList.get(rowIndex).getUsuario().getNombreCompleto();
        } else if(columnIndex==2){
            return pedidosList.get(rowIndex).getCliente().getRazonSocial();
        } else if(columnIndex==3){
            return pedidosList.get(rowIndex).getFormaDePago().getDescripcion();
        } else if(columnIndex==4){
            PedidoVentaEstado pve = pedidosList.get(rowIndex).getPedidoVentaEstadoCollection().iterator().next();
            return ApplicationInfo.formatToLongDate(pve.getFecha());
        } else if(columnIndex==5){
			PedidoVenta pedido = pedidosList.get(rowIndex);
			double t=0.0;
			Collection<PedidoVentaDetalle> pedidoVentaDetalleCollection = pedido.getPedidoVentaDetalleCollection();
			for(PedidoVentaDetalle pvd: pedidoVentaDetalleCollection){
				t += pvd.getCantidad() * pvd.getPrecioVenta();
			}
			t = pedido.getDescuentoAplicado()!=null?t*pedido.getDescuentoAplicado():t;
            return formatoMoneda(t);
        } else {
            return null;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }

    public void addTableModelListener(TableModelListener l) {        
    }

    public void removeTableModelListener(TableModelListener l) {
        
    }
}

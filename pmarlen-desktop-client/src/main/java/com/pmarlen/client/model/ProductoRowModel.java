package com.pmarlen.client.model;

import com.pmarlen.model.beans.Producto;

public class ProductoRowModel {
    private Integer id;
    private String codigoBarras;    
    private String marcaNombre;
	private String lineaNombre;
	private String industriaNombre;
    private String nombre;
    private String presentacion;
    private int unidadesPorCaja;
    private String unidadMedida;
    private double costo;
    private double precioVentaNormal;
    private double precioVentaOportunidad;
    private String contenido;
	private int cantActualNormal;
	private int cantActualOportunidad;
	
	public ProductoRowModel() {
	}

	public ProductoRowModel(Producto p) {
		this.id = p.getId();
		this.codigoBarras = p.getCodigoBarras();
		if(p.getMarca() != null){
			this.marcaNombre = p.getMarca().getNombre();
			this.lineaNombre = p.getMarca().getLinea().getNombre();
			this.industriaNombre = p.getMarca().getIndustria().getNombre();
		} else {
			this.marcaNombre = null;
			this.lineaNombre = null;
			this.industriaNombre = null;
		}
		this.nombre = p.getNombre();
		this.presentacion = p.getPresentacion();
		this.unidadesPorCaja = p.getUnidadesPorCaja();
		this.unidadMedida = p.getUnidadMedida();
		this.costo = p.getCosto();
		this.contenido = p.getContenido();
	}

	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * @param codigoBarras the codigoBarras to set
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * @return the marcaNombre
	 */
	public String getMarcaNombre() {
		return marcaNombre;
	}

	/**
	 * @param marcaNombre the marcaNombre to set
	 */
	public void setMarcaNombre(String marcaNombre) {
		this.marcaNombre = marcaNombre;
	}

	/**
	 * @return the lineaNombre
	 */
	public String getLineaNombre() {
		return lineaNombre;
	}

	/**
	 * @param lineaNombre the lineaNombre to set
	 */
	public void setLineaNombre(String lineaNombre) {
		this.lineaNombre = lineaNombre;
	}

	/**
	 * @return the industriaNombre
	 */
	public String getIndustriaNombre() {
		return industriaNombre;
	}

	/**
	 * @param industriaNombre the industriaNombre to set
	 */
	public void setIndustriaNombre(String industriaNombre) {
		this.industriaNombre = industriaNombre;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the presentacion
	 */
	public String getPresentacion() {
		return presentacion;
	}

	/**
	 * @param presentacion the presentacion to set
	 */
	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	/**
	 * @return the unidadesPorCaja
	 */
	public int getUnidadesPorCaja() {
		return unidadesPorCaja;
	}

	/**
	 * @param unidadesPorCaja the unidadesPorCaja to set
	 */
	public void setUnidadesPorCaja(int unidadesPorCaja) {
		this.unidadesPorCaja = unidadesPorCaja;
	}

	/**
	 * @return the unidadMedida
	 */
	public String getUnidadMedida() {
		return unidadMedida;
	}

	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	/**
	 * @return the costo
	 */
	public double getCosto() {
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(double costo) {
		this.costo = costo;
	}

	/**
	 * @return the precioVentaNormal
	 */
	public double getPrecioVentaNormal() {
		return precioVentaNormal;
	}

	/**
	 * @param precioVentaNormal the precioVentaNormal to set
	 */
	public void setPrecioVentaNormal(double precioVentaNormal) {
		this.precioVentaNormal = precioVentaNormal;
	}

	/**
	 * @return the precioVentaOportunidad
	 */
	public double getPrecioVentaOportunidad() {
		return precioVentaOportunidad;
	}

	/**
	 * @param precioVentaOportunidad the precioVentaOportunidad to set
	 */
	public void setPrecioVentaOportunidad(double precioVentaOportunidad) {
		this.precioVentaOportunidad = precioVentaOportunidad;
	}

	/**
	 * @return the contenido
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * @param contenido the contenido to set
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * @return the cantActualNormal
	 */
	public int getCantActualNormal() {
		return cantActualNormal;
	}

	/**
	 * @param cantActualNormal the cantActualNormal to set
	 */
	public void setCantActualNormal(int cantActualNormal) {
		this.cantActualNormal = cantActualNormal;
	}

	/**
	 * @return the cantActualOportunidad
	 */
	public int getCantActualOportunidad() {
		return cantActualOportunidad;
	}

	/**
	 * @param cantActualOportunidad the cantActualOportunidad to set
	 */
	public void setCantActualOportunidad(int cantActualOportunidad) {
		this.cantActualOportunidad = cantActualOportunidad;
	}
}
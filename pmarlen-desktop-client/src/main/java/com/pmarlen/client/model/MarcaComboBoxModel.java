/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Marca;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author alfred
 */
public class MarcaComboBoxModel implements ComboBoxModel{
    ArrayList<MarcaItemList> elements;
    MarcaItemList selected;
    
    public MarcaComboBoxModel(Collection<Marca> list){
        elements = new ArrayList<MarcaItemList>();
        Marca fi = new Marca();
        fi.setNombre("--");
        elements.add(new MarcaItemList(fi));
        for(Marca fil: list){
            elements.add(new MarcaItemList(fil));
        }
        selected = elements.get(0);
    }
    
    public void setSelectedItem(Object anItem) {
        Marca formaDePago4Select = ((MarcaItemList)anItem).getMarca();
        if( formaDePago4Select.getId() == null) {
            selected = null;
            return;
        }
        for(MarcaItemList fpl: elements){
            if(formaDePago4Select.getId().equals(fpl.getMarca().getId())){
                selected = fpl;
                break;
            }
        }
    }

    public Object getSelectedItem() {
        return selected;
    }

    public int getSize() {
        return elements.size();
    }

    public Object getElementAt(int index) {
        return elements.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}

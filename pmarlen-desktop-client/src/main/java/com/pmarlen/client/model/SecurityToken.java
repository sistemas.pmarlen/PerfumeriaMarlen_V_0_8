/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.model;

import com.lowagie.text.pdf.PRAcroForm;
import java.text.DecimalFormat;

/**
 *
 * @author alfredo
 */
public class SecurityToken {

	private static DecimalFormat decimalFormat = new DecimalFormat("0000000000000000");

	public static String getNextPhrase() {
		String nextPhrase = "";

		long seed = System.currentTimeMillis();
		
		seed = (long)Math.abs(Math.random()*seed);

		nextPhrase = decimalFormat.format(Math.abs(seed));
		nextPhrase = nextPhrase.substring(nextPhrase.length() - 6);

		return nextPhrase;
	}

	public static String getTokenForPhrase(String phrase) {
		String token = "";

		long phraseNumber = Long.parseLong(phrase);
		long alphaNumber = phraseNumber * phraseNumber - phraseNumber;

		token = decimalFormat.format(alphaNumber);
		token = token.substring(token.length() - 6);

		return token;
	}

	public static boolean matchPhraseWithHisToken(String phrase, String tokenOfPhrase) {
		
		//System.err.print("matchPhraseWithHisToken("+phrase+","+tokenOfPhrase+"): ");
		
		long phraseNumber = Long.parseLong(phrase);
		long alphaNumber = phraseNumber * phraseNumber - phraseNumber;

		String token = "";

		token = decimalFormat.format(alphaNumber);
		token = token.substring(token.length() - 6);
		final boolean validation = token.equals(tokenOfPhrase);
		
		System.err.print(validation);
	
		return validation;
	}

	public static void main(String[] args) {
		String phrase = null;
		String token = null;

		phrase = "824657";
		token  = getTokenForPhrase(phrase);

		System.out.print("[" + phrase + "]");
		System.out.print("[" + token + "]");

		System.out.println(" match     ? " + matchPhraseWithHisToken(phrase, token));

	}
}

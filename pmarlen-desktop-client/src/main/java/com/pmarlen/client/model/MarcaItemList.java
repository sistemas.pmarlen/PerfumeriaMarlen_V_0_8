/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Marca;

/**
 *
 * @author alfred
 */
public class MarcaItemList extends EntityDisplayableItem{
    private Marca marca;

    public MarcaItemList(Marca industria){
        super(industria.getId(),industria.getNombre());
        this.marca = industria;
    }

    /**
     * @return the marca
     */
    public Marca getMarca() {
        return marca;
    }

}

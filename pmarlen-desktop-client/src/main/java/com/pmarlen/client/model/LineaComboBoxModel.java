/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Linea;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author alfred
 */
public class LineaComboBoxModel implements ComboBoxModel{
    ArrayList<LineaItemList> elements;
    LineaItemList selected;
    
    public LineaComboBoxModel(Collection<Linea> list){
        elements = new ArrayList<LineaItemList>();
        Linea fp = new Linea();
        fp.setNombre("--");
        elements.add(new LineaItemList(fp));
        for(Linea fpl: list){
            elements.add(new LineaItemList(fpl));
        }
        selected = elements.get(0);
    }
    
    public void setSelectedItem(Object anItem) {
        Linea formaDePago4Select = ((LineaItemList)anItem).getLinea();
        if( formaDePago4Select.getId() == null) {
            selected = null;
            return;
        }
        for(LineaItemList fpl: elements){
            if(formaDePago4Select.getId().equals(fpl.getLinea().getId())){
                selected = fpl;
                break;
            }
        }
    }

    public Object getSelectedItem() {
        return selected;
    }

    public int getSize() {
        return elements.size();
    }

    public Object getElementAt(int index) {
        return elements.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.model.beans.Producto;
import java.text.DecimalFormat;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


/**
 *
 * @author alfred
 */
public class ProductoTableModel implements TableModel{
    private List<ProductoRowModel> productoList;
	private DecimalFormat df = new DecimalFormat("$ ###,##0.00");	
    String[] colNames;
    public ProductoTableModel(List<ProductoRowModel> clienteList) {
        this.productoList = (List<ProductoRowModel>)clienteList;
        colNames = new String[]{
			"ID", "CBarras","Linea","Industria","Marca","Nombre","Presentaci\u00F3n", "UxC","Cont." ,"Med",
            "Costo","#Pz. Normal","$ Normal","#Pz. Opor.","$ Opor."};
    }
    public int getRowCount() {
        return this.productoList.size();
    }

    public int getColumnCount() {
        return colNames.length;
    }

    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex==0){
            return String.valueOf(productoList.get(rowIndex).getId());
        } else if(columnIndex==1){
            return productoList.get(rowIndex).getCodigoBarras();
        } else if(columnIndex==2){
            return productoList.get(rowIndex).getLineaNombre();
        } else if(columnIndex==3){
            return productoList.get(rowIndex).getIndustriaNombre();
        } else if(columnIndex==4){
            return productoList.get(rowIndex).getMarcaNombre();
        } else if(columnIndex==5){
            return productoList.get(rowIndex).getNombre();
        } else if(columnIndex==6){
            return productoList.get(rowIndex).getPresentacion();
        } else if(columnIndex==7){
            return String.valueOf(productoList.get(rowIndex).getUnidadesPorCaja());
        } else if(columnIndex==8){
			return productoList.get(rowIndex).getContenido();
        } else if(columnIndex==9){
            return productoList.get(rowIndex).getUnidadMedida();
        } else if(columnIndex==10){
			return df.format(productoList.get(rowIndex).getCosto());
        } else if(columnIndex==11){
			return productoList.get(rowIndex).getCantActualNormal();
        } else if(columnIndex==12){
            return df.format(productoList.get(rowIndex).getPrecioVentaOportunidad());
        } else if(columnIndex==13){
			return productoList.get(rowIndex).getCantActualOportunidad();
        } else if(columnIndex==14){
            return df.format(productoList.get(rowIndex).getPrecioVentaOportunidad());
        } else {
            return null;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }

    public void addTableModelListener(TableModelListener l) {        
    }

    public void removeTableModelListener(TableModelListener l) {
        
    }
	
	public ProductoRowModel getAt(int selectedIndex){
		return productoList.get(selectedIndex);
	}

	public void setListProducto(List<ProductoRowModel> listProducto) {
		this.productoList = listProducto;
	}
	
	public int getProductoPorCodigoBarras(String codigoBarras){
		int index=-1;
		int i=0;
		for(ProductoRowModel prm: productoList){
			if(prm.getCodigoBarras().startsWith(codigoBarras)){
				index = i;
				break;
			}
			i++;
		}
		
		return index;
	}
}

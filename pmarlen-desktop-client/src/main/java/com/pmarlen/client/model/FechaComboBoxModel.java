/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Cliente;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author alfred
 */
public class FechaComboBoxModel implements ComboBoxModel{
    ArrayList<String> elements;
    String selected;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	
	public static Date parseOwnElement(String s){
		try {
			return sdf.parse(s);
		} catch (ParseException ex) {
			return null;
		}
	}
	
    public FechaComboBoxModel(Collection<Date> list){
        elements = new ArrayList<String>();
        //elements.add("--");
        for(Date cl: list){
            elements.add(sdf.format(cl));
        }
        
        selected = elements.get(list.size()-1);
    }
    
    public void setSelectedItem(Object anItem) {
        
        String elementSelect = ((String)anItem);
        if( elementSelect == null) {
            selected = null;
            return;
        }
        for(String cil: elements){
            if(elementSelect.equals(cil)){
                selected = cil;
                break;
            }
        }
    }

    public Object getSelectedItem() {
        return selected;
    }

    public int getSize() {
        return elements.size();
    }

    public Object getElementAt(int index) {
        return elements.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}

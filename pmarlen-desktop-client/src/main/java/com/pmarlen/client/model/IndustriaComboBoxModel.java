/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Industria;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author alfred
 */
public class IndustriaComboBoxModel implements ComboBoxModel{
    ArrayList<IndustriaItemList> elements;
    IndustriaItemList selected;
    
    public IndustriaComboBoxModel(Collection<Industria> list){
        elements = new ArrayList<IndustriaItemList>();
        Industria fi = new Industria();
        fi.setNombre("--");
        elements.add(new IndustriaItemList(fi));
        for(Industria fil: list){
            elements.add(new IndustriaItemList(fil));
        }
        selected = elements.get(0);
    }
    
    public void setSelectedItem(Object anItem) {
        Industria formaDePago4Select = ((IndustriaItemList)anItem).getIndustria();
        if( formaDePago4Select.getId() == null) {
            selected = null;
            return;
        }
        for(IndustriaItemList fpl: elements){
            if(formaDePago4Select.getId().equals(fpl.getIndustria().getId())){
                selected = fpl;
                break;
            }
        }
    }

    public Object getSelectedItem() {
        return selected;
    }

    public int getSize() {
        return elements.size();
    }

    public Object getElementAt(int index) {
        return elements.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeListDataListener(ListDataListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.client.ticketprinter.TicketBlueToothPrinter;
import com.pmarlen.client.ticketprinter.TicketPOSTermalPrinter;
import com.pmarlen.model.beans.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.junit.*;
import org.junit.Assert.*;

/**
 *
 * @author alfredo
 */
@Ignore
public class TicketPrinterTest {
    
    public TicketPrinterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
	@Ignore
    public void testGenerateBlueToothTicket() {
        
        PedidoVenta pedidoVenta = new PedidoVenta(1234);
        
        Cliente cliente = new Cliente();
        cliente.setCalle("MAXIMILIANO");
        cliente.setNumExterior("EX11");
        cliente.setNumInterior("IN11");
        Poblacion poblacion = new Poblacion();
        poblacion.setNombre("SAN JUAN TEJUXILIAPAN EL GRANDE");
        poblacion.setEntidadFederativa("VERACRUZ");
        poblacion.setMunicipioODelegacion("SANTA VERTA DE LA BURGUER");
        
        cliente.setPoblacion(poblacion);
        
        cliente.setNombreEstablecimiento("NOMBRE DE NOMBRE DEL CLIENTE CON ESTABLECIMIENTO DEL\tCLIENTE");
        
        pedidoVenta.setCliente(cliente);
        pedidoVenta.setComentarios("comentarios");
        
        pedidoVenta.setFactoriva(new Double(0.16));
		FormaDePago formaDePago = new FormaDePago();
		formaDePago.setDescripcion("EFECTIVO");
        pedidoVenta.setFormaDePago(formaDePago);
        
        Usuario agente = new Usuario();
        agente.setNombreCompleto("AGENTE   CON     NOMBRE\t\nMUY MUY LARGO Y PATERNO LARGO Y MATERNO LARGO");
        
        pedidoVenta.setUsuario(agente);
        
        ArrayList<PedidoVentaDetalle> pvdList = new ArrayList<PedidoVentaDetalle>();
        
        for(int i =0; i<15;i++) {
            final PedidoVentaDetalle pvd = new PedidoVentaDetalle();
            
            pvd.setCantidad(25+(int)(Math.random()*25.0));
            pvd.setPrecioVenta(250+(int)(Math.random()*250.0));
            
            Producto producto = new Producto();
            
            producto.setNombre("Producto XXX_"+i);
            producto.setPresentacion(Integer.toHexString(producto.getNombre().hashCode()).toUpperCase()+" "+Integer.toHexString(producto.getNombre().hashCode()).toUpperCase());
            
            pvd.setProducto(producto);
            
            pvdList.add(pvd);
        }
        
        pedidoVenta.setPedidoVentaDetalleCollection(pvdList);
        TicketBlueToothPrinter tpbt = new TicketBlueToothPrinter();
		try {
			HashMap<String,String> extraInformation = new HashMap<String,String>();
			extraInformation.put("recibimos","$ 500.00");
			extraInformation.put("cambio","$ 27.30");
			tpbt.generateTicket(pedidoVenta,extraInformation);
		} catch (IOException ex) {
			Assert.fail(ex.getMessage());
		}
    
    }
	
	@Test	
	public void testGeneratePOSTermalTicket() {
        
        PedidoVenta pedidoVenta = new PedidoVenta(1234);
        
        Cliente cliente = new Cliente();
        cliente.setCalle("MAXIMILIANO");
        cliente.setNumExterior("EX11");
        cliente.setNumInterior("IN11");
        Poblacion poblacion = new Poblacion();
        poblacion.setNombre("SAN JUAN TEJUXILIAPAN EL GRANDE");
        poblacion.setEntidadFederativa("VERACRUZ");
        poblacion.setMunicipioODelegacion("SANTA VERTA DE LA BURGUER");
        
        cliente.setPoblacion(poblacion);
        cliente.setRazonSocial("RAZON SOCIAL CLIENTE MULTILINEA DEMACIADO LARGA Y CON ACENTOS S.A. DE C.V.");
        cliente.setNombreEstablecimiento("NOMBRE DE NOMBRE DEL CLIENTE CON ESTABLECIMIENTO DEL\tCLIENTE");
		cliente.setRfc("XXXX123456ZZZ");
        
        pedidoVenta.setCliente(cliente);
        pedidoVenta.setComentarios("comentarios");
		pedidoVenta.setAlmacen(new Almacen(3));

        pedidoVenta.setFactoriva(new Double(0.16));
		FormaDePago formaDePago = new FormaDePago();
		formaDePago.setDescripcion("EFECTIVO");
        pedidoVenta.setFormaDePago(formaDePago);
        
        Usuario agente = new Usuario();
        agente.setNombreCompleto("VENDEDOR [004]");
        agente.setUsuarioId("vendedor04");
        
        pedidoVenta.setUsuario(agente);
        
        ArrayList<PedidoVentaDetalle> pvdList = new ArrayList<PedidoVentaDetalle>();
		int numRecs = 10;
        
        for(int i =0; i<numRecs;i++) {
            final PedidoVentaDetalle pvd = new PedidoVentaDetalle();
            
            pvd.setCantidad(10+(int)(Math.random()*10.0));
            pvd.setPrecioVenta(100+(int)(Math.random()*90.0));
            
            Producto producto = new Producto();
            
			producto.setCodigoBarras("123456789"+i);
            producto.setNombre("PRODUCTO XXX_"+i);
            producto.setPresentacion(Integer.toHexString(producto.getNombre().hashCode()).toUpperCase()+" "+Integer.toHexString(producto.getNombre().hashCode()).toUpperCase());
            
            pvd.setProducto(producto);
            
            pvdList.add(pvd);
        }
        
        pedidoVenta.setPedidoVentaDetalleCollection(pvdList);
        TicketPOSTermalPrinter tppt = new TicketPOSTermalPrinter();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmm");
		try {
			HashMap<String,String> extraInformation = new HashMap<String,String>();
			extraInformation.put("recibimos","500.00");
			extraInformation.put("cambio","27.30");
			extraInformation.put("esClienteRegistrado","true");
			
			JasperPrint jasperPrintPDF = (JasperPrint)tppt.generateNuevoTicket(pedidoVenta,extraInformation);
			System.out.println("-> print in other job ? "+tppt.needsSpecialPrintJob(jasperPrintPDF));
			//JasperExportManager.exportReportToPdfFile(jasperPrintPDF, "testGeneratePOSTermalTicket_"+sdf.format(new Date())+".pdf");			
			long dt = System.currentTimeMillis();
			System.out.println("-> CURRENT DATE:"+dt+", SECS FROM 1970:"+(dt/1000));
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			//Assert.fail(ex.getMessage());
		}
    
    }
}

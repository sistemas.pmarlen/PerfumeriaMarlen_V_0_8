all: pom.xml
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.0.20 -Dmaven.test.skip
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.0.20 -Dmaven.test.skip

uall: pom.xml
	mvn  tomcat:undeploy -P preprod
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.0.20 -Dmaven.test.skip 
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.0.20 -Dmaven.test.skip

sall: pom.xml
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.1.82 -Dmaven.test.skip
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.1.82 -Dmaven.test.skip

suall: pom.xml
	mvn  tomcat:undeploy -P preprod
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.1.82 -Dmaven.test.skip
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.1.82 -Dmaven.test.skip

pall: pom.xml
	sudo /etc/init.d/tomcat6 stop
	sudo /etc/init.d/tomcat6 start
	mvn  clean install -P preprod -e -Dmaven.test.skip
	mvn  tomcat:deploy -P preprod -Dmaven.test.skip

puall: pom.xml
	mvn  tomcat:undeploy -P preprod
	sudo /etc/init.d/tomcat6 stop
	sudo /etc/init.d/tomcat6 start
	mvn  clean install -P preprod -e -Dmaven.test.skip
	mvn  tomcat:deploy -P preprod -Dmaven.test.skip

mall: pom.xml
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.1.66
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.1.66 -Dmaven.test.skip

muall: pom.xml
	mvn  tomcat:undeploy -P preprod
	~/apache-tomcat-6.0.35/bin/shutdown.sh
	~/apache-tomcat-6.0.35/bin/startup.sh
	mvn  clean install -P preprod -e -Dproduction_host=192.168.1.66
	mvn  tomcat:deploy -P preprod -Dproduction_host=192.168.1.66 -Dmaven.test.skip

